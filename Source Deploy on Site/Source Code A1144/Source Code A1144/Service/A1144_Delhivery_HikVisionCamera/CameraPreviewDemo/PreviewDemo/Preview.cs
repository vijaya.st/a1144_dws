using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.IO;

using System.Runtime.InteropServices;

namespace PreviewDemo
{
	/// <summary>
	/// Form1 的摘要说明。
	/// </summary>
	public class Preview : System.Windows.Forms.Form
	{
        private uint iLastErr = 0;
		private Int32 m_lUserID = -1;
		private bool m_bInitSDK = false;
        private bool m_bRecord = false;
        private bool m_bTalk = false;
		private Int32 m_lRealHandle = -1;
        private int lVoiceComHandle = -1;
        private string str;

        CHCNetSDK.REALDATACALLBACK RealData = null;
        public CHCNetSDK.NET_DVR_PTZPOS m_struPtzCfg;
        private Button btn_Exit;
        /*private Button PtzGet;
        private Button PtzSet;*/
        private Label label19;
        /*private ComboBox comboBox1;
        private TextBox textBoxPanPos;
        private TextBox textBoxTiltPos;
        private TextBox textBoxZoomPos;*/
        private Label label20;
        private Label label21;
        private Label label22;
        private Button button1;

        //private GroupBox groupBox1;

        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.Container components = null;

		public Preview()
		{
			//
			// Windows 窗体设计器支持所必需的
			//
			InitializeComponent();

			m_bInitSDK = CHCNetSDK.NET_DVR_Init();
			if (m_bInitSDK == false)
			{
				MessageBox.Show("NET_DVR_Init error!");
				return;
			}
			else
			{
                //保存SDK日志 To save the SDK log
                CHCNetSDK.NET_DVR_SetLogToFile(3, "C:\\SdkLog\\", true);
			}
			//
			// TODO: 在 InitializeComponent 调用后添加任何构造函数代码
			//
		}

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if (m_lRealHandle >= 0)
			{
				CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle);
			}
			if (m_lUserID >= 0)
			{
				CHCNetSDK.NET_DVR_Logout(m_lUserID);
			}
			if (m_bInitSDK == true)
			{
				CHCNetSDK.NET_DVR_Cleanup();
			}
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows 窗体设计器生成的代码
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
        {
            this.btn_Exit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Exit
            // 
            this.btn_Exit.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Exit.Location = new System.Drawing.Point(178, 28);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(112, 35);
            this.btn_Exit.TabIndex = 11;
            this.btn_Exit.Tag = "";
            this.btn_Exit.Text = "Cancel";
            this.btn_Exit.UseVisualStyleBackColor = false;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Location = new System.Drawing.Point(39, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 33);
            this.button1.TabIndex = 38;
            this.button1.Text = "Open Camera";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Preview
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(344, 94);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_Exit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Preview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Preview";
            this.Load += new System.EventHandler(this.Preview_Load);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		

		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		//private void btnLogin_Click(object sender, System.EventArgs e)
		//{
  //          Login();

  //      }

  //      public void Login()
  //      {
  //          //if (textBoxIP.Text == "" || textBoxPort.Text == "" ||
  //          //    textBoxUserName.Text == "" || textBoxPassword.Text == "")
  //          //{
  //          //    MessageBox.Show("Please input IP, Port, User name and Password!");
  //          //    return;
  //          //}
  //          if (m_lUserID < 0)
  //          {
  //              string DVRIPAddress = "192.168.1.64";   //textBoxIP.Text; //设备IP地址或者域名
  //              Int16 DVRPortNumber = 8000;             //Int16.Parse(textBoxPort.Text);//设备服务端口号
  //              string DVRUserName = "admin";           //textBoxUserName.Text;//设备登录用户名
  //              string DVRPassword = "Ashokj@9";        //textBoxPassword.Text;//设备登录密码

  //              CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();

  //              //登录设备 Login the device
  //              m_lUserID = CHCNetSDK.NET_DVR_Login_V30(DVRIPAddress, DVRPortNumber, DVRUserName, DVRPassword, ref DeviceInfo);
  //              if (m_lUserID < 0)
  //              {
  //                  iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //                  str = "NET_DVR_Login_V30 failed, error code= " + iLastErr; //登录失败，输出错误号
  //                  MessageBox.Show(str);
  //                  return;
  //              }
  //              else
  //              {
  //                  //登录成功
  //                  LivePreview();
  //                  //MessageBox.Show("Login Success!");
  //                  btnLogin.Text = "Logout";
  //              }

  //          }
  //          else
  //          {
  //              //注销登录 Logout the device
  //              if (m_lRealHandle >= 0)
  //              {
  //                  MessageBox.Show("Please stop live view firstly");
  //                  return;
  //              }

  //              if (!CHCNetSDK.NET_DVR_Logout(m_lUserID))
  //              {
  //                  iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //                  str = "NET_DVR_Logout failed, error code= " + iLastErr;
  //                  MessageBox.Show(str);
  //                  return;
  //              }
  //              m_lUserID = -1;
  //              btnLogin.Text = "Login";
  //          }
  //          return;
  //      }

		//private void btnPreview_Click(object sender, System.EventArgs e)
		//{
  //          LivePreview();

  //      }

  //      public void LivePreview()
  //      {
  //          if (m_lUserID < 0)
  //          {
  //              MessageBox.Show("Please login the device firstly");
  //              return;
  //          }

  //          if (m_lRealHandle < 0)
  //          {
  //              CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO();
  //              lpPreviewInfo.hPlayWnd = RealPlayWnd.Handle;//预览窗口
  //              lpPreviewInfo.lChannel = Int16.Parse(textBoxChannel.Text);//预te览的设备通道
  //              lpPreviewInfo.dwStreamType = 0;//码流类型：0-主码流，1-子码流，2-码流3，3-码流4，以此类推
  //              lpPreviewInfo.dwLinkMode = 0;//连接方式：0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP 
  //              lpPreviewInfo.bBlocked = true; //0- 非阻塞取流，1- 阻塞取流
  //              lpPreviewInfo.dwDisplayBufNum = 1; //播放库播放缓冲区最大缓冲帧数
  //              lpPreviewInfo.byProtoType = 0;
  //              lpPreviewInfo.byPreviewMode = 0;


  //              //if (textBoxID.Text != "")
  //              //{
  //              //    lpPreviewInfo.lChannel = -1;
  //              //    byte[] byStreamID = System.Text.Encoding.Default.GetBytes(textBoxID.Text);
  //              //    lpPreviewInfo.byStreamID = new byte[32];
  //              //    byStreamID.CopyTo(lpPreviewInfo.byStreamID, 0);
  //              //}


  //              if (RealData == null)
  //              {
  //                  RealData = new CHCNetSDK.REALDATACALLBACK(RealDataCallBack);//预览实时流回调函数
  //              }

  //              IntPtr pUser = new IntPtr();//用户数据

  //              //打开预览 Start live view 
  //              m_lRealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(m_lUserID, ref lpPreviewInfo, null/*RealData*/, pUser);
  //              if (m_lRealHandle < 0)
  //              {
  //                  iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //                  str = "NET_DVR_RealPlay_V40 failed, error code= " + iLastErr; //预览失败，输出错误号
  //                  MessageBox.Show(str);
  //                  return;
  //              }
  //              else
  //              {
  //                  //预览成功
  //                  btnPreview.Text = "Stop Live View";
  //              }
  //          }
  //          else
  //          {
  //              //停止预览 Stop live view 
  //              if (!CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle))
  //              {
  //                  iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //                  str = "NET_DVR_StopRealPlay failed, error code= " + iLastErr;
  //                  MessageBox.Show(str);
  //                  return;
  //              }
  //              m_lRealHandle = -1;
  //              btnPreview.Text = "Live View";

  //          }
  //          return;
  //      }

  //      public void RealDataCallBack(Int32 lRealHandle, UInt32 dwDataType, IntPtr pBuffer, UInt32 dwBufSize, IntPtr pUser)
		//{
  //          if (dwBufSize > 0)
  //          {
  //              byte[] sData = new byte[dwBufSize];
  //              Marshal.Copy(pBuffer, sData, 0, (Int32)dwBufSize);

  //              string str = "实时流数据.ps";
  //              FileStream fs = new FileStream(str, FileMode.Create);
  //              int iLen = (int)dwBufSize;
  //              fs.Write(sData, 0, iLen);
  //              fs.Close();            
  //          }
		//}

  //      private void btnBMP_Click(object sender, EventArgs e)
  //      {
  //          string sBmpPicFileName;
  //          //图片保存路径和文件名 the path and file name to save
  //          sBmpPicFileName = "BMP_test.bmp"; 

  //          //BMP抓图 Capture a BMP picture
  //          if (!CHCNetSDK.NET_DVR_CapturePicture(m_lRealHandle, sBmpPicFileName))
  //          {
  //              iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //              str = "NET_DVR_CapturePicture failed, error code= " + iLastErr;
  //              MessageBox.Show(str);
  //              return;
  //          }
  //          else
  //          {
  //              str = "Successful to capture the BMP file and the saved file is " + sBmpPicFileName;
  //              MessageBox.Show(str); 
  //          }
  //          return;
  //      }

  //      private void btnJPEG_Click(object sender, EventArgs e)
  //      {
  //          GetImage();
  //      }

  //      public void GetImage()
  //      {
  //          string sJpegPicFileName;
  //          //图片保存路径和文件名 the path and file name to save
  //          //sJpegPicFileName =  "JPEG_test.jpg";

  //          sJpegPicFileName = DateTime.Now.ToString("yyyyMMdd_hhmmss") + "AMBPL.jpg";

  //          int lChannel = Int16.Parse(textBoxChannel.Text); //通道号 Channel number

  //          CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA();
  //          lpJpegPara.wPicQuality = 0; //图像质量 Image quality
  //          lpJpegPara.wPicSize = 0xff; //抓图分辨率 Picture size: 2- 4CIF，0xff- Auto(使用当前码流分辨率)，抓图分辨率需要设备支持，更多取值请参考SDK文档


  //          //JPEG抓图 Capture a JPEG picture
  //          if (!CHCNetSDK.NET_DVR_CaptureJPEGPicture(m_lUserID, lChannel, ref lpJpegPara, sJpegPicFileName))
  //          {
  //              iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //              str = "NET_DVR_CaptureJPEGPicture failed, error code= " + iLastErr;
  //              MessageBox.Show(str);
  //              return;
  //          }
  //          //else
  //          //{

  //          //    str = "Successful to capture the JPEG file and the saved file is " + DateTime.Now.ToString("yyyy�-慚M�-慸d扵扝H�:抦m�:抯s") + sJpegPicFileName;
  //          //    MessageBox.Show(str);
  //          //}
  //          return;
  //      }

        
  //      private void btnRecord_Click(object sender, EventArgs e)
  //      {
  //          //录像保存路径和文件名 the path and file name to save
  //          string sVideoFileName;
  //          sVideoFileName = "Record_test.mp4";

  //          if (m_bRecord == false)
  //          {
  //              //强制I帧 Make a I frame
  //              int lChannel = Int16.Parse(textBoxChannel.Text); //通道号 Channel number
  //              CHCNetSDK.NET_DVR_MakeKeyFrame(m_lUserID, lChannel);

  //              //开始录像 Start recording
  //              if (!CHCNetSDK.NET_DVR_SaveRealData(m_lRealHandle, sVideoFileName))
  //              {
  //                  iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //                  str = "NET_DVR_SaveRealData failed, error code= " + iLastErr;
  //                  MessageBox.Show(str);
  //                  return;
  //              }
  //              else
  //              {                  
  //                  btnRecord.Text = "Stop Record";
  //                  m_bRecord = true;
  //              }
  //          }
  //          else
  //          {
  //              //停止录像 Stop recording
  //              if (!CHCNetSDK.NET_DVR_StopSaveRealData(m_lRealHandle))
  //              {
  //                  iLastErr = CHCNetSDK.NET_DVR_GetLastError();
  //                  str = "NET_DVR_StopSaveRealData failed, error code= " + iLastErr;
  //                  MessageBox.Show(str);
  //                  return;
  //              }
  //              else
  //              {
  //                  str = "Successful to stop recording and the saved file is " + sVideoFileName;
  //                  MessageBox.Show(str);
  //                  btnRecord.Text = "Start Record";
  //                  m_bRecord = false;
  //              }            
  //          }

  //          return;
  //      }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            //停止预览 Stop live view 
            if (m_lRealHandle >= 0)
            {
                CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle);
                m_lRealHandle = -1;
            }

            //注销登录 Logout the device
            if (m_lUserID >= 0)
            {
                CHCNetSDK.NET_DVR_Logout(m_lUserID);
                m_lUserID = -1;
            }

            CHCNetSDK.NET_DVR_Cleanup();

            Application.Exit();
        }

        private void btnPTZ_Click(object sender, EventArgs e)
        {

        }

        public void VoiceDataCallBack(int lVoiceComHandle, IntPtr pRecvDataBuffer, uint dwBufSize, byte byAudioFlag, System.IntPtr pUser)
        {
            byte[] sString = new byte[dwBufSize];
            Marshal.Copy(pRecvDataBuffer, sString, 0, (Int32)dwBufSize);

            if (byAudioFlag ==0)
            {
                //将缓冲区里的音频数据写入文件 save the data into a file
                string str = "PC采集音频文件.pcm";
                FileStream fs = new FileStream(str, FileMode.Create);
                int iLen = (int)dwBufSize;
                fs.Write(sString, 0, iLen);
                fs.Close();
            }
            if (byAudioFlag == 1)
            {
                //将缓冲区里的音频数据写入文件 save the data into a file
                string str = "设备音频文件.pcm";
                FileStream fs = new FileStream(str, FileMode.Create);
                int iLen = (int)dwBufSize;
                fs.Write(sString, 0, iLen);
                fs.Close();
            }

        }

        //private void btnVioceTalk_Click(object sender, EventArgs e)
        //{
        //    if (m_bTalk == false)
        //    {
        //        //开始语音对讲 Start two-way talk
        //        CHCNetSDK.VOICEDATACALLBACKV30 VoiceData = new CHCNetSDK.VOICEDATACALLBACKV30(VoiceDataCallBack);//预览实时流回调函数

        //        lVoiceComHandle = CHCNetSDK.NET_DVR_StartVoiceCom_V30(m_lUserID, 1, true, VoiceData, IntPtr.Zero);
        //        //bNeedCBNoEncData [in]需要回调的语音数据类型：0- 编码后的语音数据，1- 编码前的PCM原始数据

        //        if (lVoiceComHandle < 0)
        //        {
        //            iLastErr = CHCNetSDK.NET_DVR_GetLastError();
        //            str = "NET_DVR_StartVoiceCom_V30 failed, error code= " + iLastErr;
        //            MessageBox.Show(str);
        //            return;
        //        }
        //        else
        //        {
        //            btnVioceTalk.Text = "Stop Talk";
        //            m_bTalk = true;
        //        }
        //    }
        //    else
        //    {
        //        //停止语音对讲 Stop two-way talk
        //        if (!CHCNetSDK.NET_DVR_StopVoiceCom(lVoiceComHandle))
        //        {
        //            iLastErr = CHCNetSDK.NET_DVR_GetLastError();
        //            str = "NET_DVR_StopVoiceCom failed, error code= " + iLastErr;
        //            MessageBox.Show(str);
        //            return;
        //        }
        //        else
        //        {
        //            btnVioceTalk.Text = "Start Talk";
        //            m_bTalk = false;
        //        }
        //    }
        //}

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void Preview_Load(object sender, EventArgs e)
        {

        }

        private void Ptz_Set_Click(object sender, EventArgs e)
        {

        }

        private void PreSet_Click(object sender, EventArgs e)
        {
            PreSet dlg = new PreSet();
            dlg.m_lUserID = m_lUserID;
            dlg.m_lChannel = 1;
            dlg.m_lRealHandle = m_lRealHandle;
            dlg.ShowDialog();
            
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();
            this.Hide();
            form.ShowDialog();
            this.Close();

        }
    }
}
