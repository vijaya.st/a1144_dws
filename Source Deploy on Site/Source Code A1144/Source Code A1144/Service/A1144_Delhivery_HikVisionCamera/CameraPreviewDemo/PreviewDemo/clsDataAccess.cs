﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace PreviewDemo
{
    public class clsDataAccess
    {
        static string databaseOwner = "dbo";
        static ErrorLog oErrorLog = new ErrorLog();

        public static string ProjectCode = System.Configuration.ConfigurationManager.AppSettings["ProjectCode"];
        public static string GetConnectionString()
        {

            string con = System.Configuration.ConfigurationManager.AppSettings["DBConnection"];
            return con;

        }

        public static DataSet GetDataSet(string procedureName, SqlCommand command)
        {
            SqlConnection connection = new SqlConnection(GetConnectionString());
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                command.CommandText = databaseOwner + "." + procedureName;
                command.Connection = connection;

                //Mark As Stored Procedure
                command.CommandType = CommandType.StoredProcedure;

                da.SelectCommand = command;
                da.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
            }
            return ds;
        }
    }

    
}