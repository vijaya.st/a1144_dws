﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using S7.Net;
using System.Collections.Generic;

namespace PreviewDemo
{
    public partial class Form1 : Form
    {

        ErrorLog oErrorLog = new ErrorLog();
        Plc plc;
        private Thread _threadDWS_Data_Log, _threadReadFromPLC, _threadOld_Images_Delete;
        private System.Timers.Timer _timerDWS_Data_Log, _timerReadFromPLC, _timerOld_Images_Delete;

        string PLCIP, Triggerflag, ImageCapturedFlag, ImageCapturedFlagAlarm = "";

        DataSet GetDWS_Data_Log = new DataSet();

        private uint iLastErr = 0;
        private Int32 m_lUserID = -1;
        private bool m_bInitSDK = false;
        private bool m_bRecord = false;
        private bool m_bTalk = false;
        private Int32 m_lRealHandle = -1;
        private int lVoiceComHandle = -1;
        private string str;

        CHCNetSDK.REALDATACALLBACK RealData = null;
        public CHCNetSDK.NET_DVR_PTZPOS m_struPtzCfg;
        public Form1()
        {
            InitializeComponent();
            btnPreview.Hide();
            btnJPEG.Hide();
            //label12.Hide();
            btnLogin.Hide();
            Login();
            StartDWS_Data_Log();
            StartDWS_Data_Log_write();
           // StartOld_Images_Delete();
            PLCIP = System.Configuration.ConfigurationManager.AppSettings["PLCIP"];
            Triggerflag = System.Configuration.ConfigurationManager.AppSettings["Triggerflag"];
            ImageCapturedFlag = System.Configuration.ConfigurationManager.AppSettings["ImageCapturedFlag"];
            ImageCapturedFlagAlarm = System.Configuration.ConfigurationManager.AppSettings["ImageCapturedFlagAlarm"];

            plc = new Plc(CpuType.S71500, PLCIP, 0, 1);
            plc.Open();

            oErrorLog.WriteErrorLog("Initialesed IP Camera.. PLC Connection Susseccfull ");
        }

        #region DWS_Data
        private void StartDWS_Data_Log()
        {
            _threadDWS_Data_Log = new Thread(StartDWSDataTimer);
            _threadDWS_Data_Log.Start();
        }

        private void StartDWSDataTimer()
        {
            int interval = 500;//Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Annunciation_Interval"]);

            _timerDWS_Data_Log = new System.Timers.Timer(interval);
            _timerDWS_Data_Log.Elapsed += _timerDWS_Data_Log_Elapsed;
            _timerDWS_Data_Log.Start();
        }

        private void _timerDWS_Data_Log_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                int isReady = Convert.ToInt32(plc.Read(Triggerflag));//Trigger flag
                //oErrorLog.WriteErrorLog("getdatebase value" + isReady.ToString() + isReady);
                if (isReady == 1)
                {

                    GetDWS_Data_Log = clsMain.GetDWSDataLog();
                    oErrorLog.WriteErrorLog("getdatebase value" + isReady.ToString() + isReady);
                    foreach (DataRow dr in GetDWS_Data_Log.Tables[0].Rows)
                    {
                        //MessageBox.Show(isReady.ToString());
                        //oErrorLog.WriteErrorLog("11111 Before JPEGImage..  " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
                        string BarcodeImage = dr["Barcode"].ToString();
                        string Barcode = dr["Barcode"].ToString().Replace("\n", "").Replace("\r", "");

                        string sJpegPicFileName;
                        sJpegPicFileName = Barcode + ".jpg";
                        int lChannel = Int16.Parse(1.ToString()); //Í¨µÀºÅ Channel number
                        CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA();
                        lpJpegPara.wPicQuality = 5;  //0; //Í¼ÏñÖÊÁ¿ Image quality
                        lpJpegPara.wPicSize = 0xff; //×¥Í¼·Ö±æÂÊ Picture size: 2- 4CIF£¬0xff- Auto(Ê¹ÓÃµ±Ç°ÂëÁ÷·Ö±æÂÊ)£¬×¥Í¼·Ö±æÂÊÐèÒªÉè±¸Ö§³Ö£¬¸ü¶àÈ¡ÖµÇë²Î¿¼SDKÎÄµµ
                        plc.Write(Triggerflag, 0);            //JPEG×¥Í¼ Capture a JPEG picture
                        plc.Write(ImageCapturedFlag, 1); //Image Captured Flag
                        
                        plc.Write(ImageCapturedFlagAlarm, false);
                        

                        // Thread.Sleep(1500);
                        clsMain.UpDateIsImage(BarcodeImage);
                        oErrorLog.WriteErrorLog("After JPEGImage..  " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));

                        if (!CHCNetSDK.NET_DVR_CaptureJPEGPicture(m_lUserID, lChannel, ref lpJpegPara, sJpegPicFileName))
                        {
                            try
                            {
                                plc.Write(ImageCapturedFlagAlarm, true);  //image is not captured
                                plc.Write(ImageCapturedFlag, 2);
                                oErrorLog.WriteErrorLog("Image is not captured.." + isReady.ToString());
                                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                                str = "NET_DVR_CaptureJPEGPicture failed, error code= " + iLastErr;
                                //MessageBox.Show(str);
                                return;
                            }
                            catch (Exception ex)
                            {
                                oErrorLog.WriteErrorLog("Error in Image Capture .." + isReady.ToString() + ex.Message);
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " " + ex.ToString());
            }
            finally
            {

            }
        }

        #endregion

        #region DWS_Data_WeightCapture
        private void StartDWS_Data_Log_write()
        {
            _threadReadFromPLC = new Thread(StartDWSDataTimerWrite);
            _threadReadFromPLC.Start();
        }

        private void StartDWSDataTimerWrite()
        {
            int interval = 1000;//Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Annunciation_Interval"]);

            _timerReadFromPLC = new System.Timers.Timer(interval);
            _timerReadFromPLC.Elapsed += _timerReadFromPLC_Elapsed; ;
            _timerReadFromPLC.Start();
        }
        private void _timerReadFromPLC_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                _timerReadFromPLC.Stop();
                GetDWS_Data_Log = clsMain.GetDWSDataLogWrite();
                foreach (DataRow dr in GetDWS_Data_Log.Tables[0].Rows)
                {
                    // oErrorLog.WriteErrorLog("11111 Before JPEGImage..  " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
                    string BarcodeWeight = dr["Barcode"].ToString();
                    string cleanedBarcode = BarcodeWeight.Replace("\n", "").Replace("\r", "");

                    string Length = dr["Length"].ToString() + " cm";
                    string Width = dr["Width"].ToString() + " cm";
                    string Height = dr["Height"].ToString() + " cm";
                    string Weight = dr["Weight"].ToString() + " g";
                    string IsWeight = dr["IsWeight"].ToString();
                    string Volume = dr["Volume"].ToString() + " cm3";
                    string RealVolume = dr["RealVolume"].ToString() + " cm3";

                    string sJpegPicFileName;
                    sJpegPicFileName = cleanedBarcode + ".jpg";

                    //IsWeight = "1";

                    try
                    {
                        if (IsWeight == "1")
                        {
                            string sourcePath = AppDomain.CurrentDomain.BaseDirectory + sJpegPicFileName;
                            //oErrorLog.WriteErrorLog("Source Pathe >>"+sourcePath);
                            if (cleanedBarcode != "" && cleanedBarcode != "null")
                            {
                                // Here We will upload image with watermark Text
                                string fileName = @sourcePath;// @"D:\Ashok\Projects\CameraPreviewDemo\PreviewDemo\bin\101236548512365.jpg";
                                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                                Image image = Image.FromStream(fs);
                                fs.Close();

                                Bitmap b = new Bitmap(image);
                                Graphics graphics = Graphics.FromImage(b);
                                SolidBrush brush = new SolidBrush(Color.FromArgb(255, Color.White));
                                Font font = new Font("TimesNewRoman", 22);
                                graphics.DrawString("Location : " + "MAA_Poonamallee_HB", font, brush, new PointF(1345, 100));
                                graphics.DrawString("AWB_No : " + cleanedBarcode, font, brush, new PointF(1340, 130));
                                graphics.DrawString("Length : " + Length, font, brush, new PointF(1365, 160));
                                graphics.DrawString("width : " + Width, font, brush, new PointF(1386, 190));
                                graphics.DrawString("Height : " + Height, font, brush, new PointF(1375, 220));
                                graphics.DrawString("Weight : " + Weight, font, brush, new PointF(1362, 250));
                                graphics.DrawString("Volume : " + Volume, font, brush, new PointF(1356, 280));
                                graphics.DrawString("Real Volume : " + RealVolume, font, brush, new PointF(1290, 310));
                                graphics.DrawString("Sorter ID : " + "1143_1", font, brush, new PointF(1338, 340));
                                b.Save(fileName, image.RawFormat);
                                image.Dispose();
                                b.Dispose();
                            }
                            else
                            {
                                oErrorLog.WriteErrorLog("ERROR in Data JPEGImage.. sJpegPicFileName =  " + cleanedBarcode + ".jpg" + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
                            }
                            clsMain.UpDateIsWeight(BarcodeWeight);
                            BarcodeWeight = string.Empty;
                            cleanedBarcode = string.Empty;
                            //oErrorLog.WriteErrorLog("Image Capture Successfully.." + isReady.ToString());
                        }
                    }
                    catch (Exception Ex)
                    {
                        //plc.Write("DB1100.DBW22", 6);
                        oErrorLog.WriteErrorLog("Error to capture image.." + Ex.ToString());
                    }
                    oErrorLog.WriteErrorLog("After JPEGImage.. sJpegPicFileName =  " + BarcodeWeight + ".jpg" + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));

                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " " + ex.ToString());
            }
            finally
            {
                _timerReadFromPLC.Start();
            }
        }

        #endregion

        #region Old_Images_Delete
        private void StartOld_Images_Delete()
        {
            _threadDWS_Data_Log = new Thread(StartOld_Images_DeleteTimer);
            _threadDWS_Data_Log.Start();
        }

        private void StartOld_Images_DeleteTimer()
        {
            int interval = 50000;//Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Annunciation_Interval"]);

            _timerOld_Images_Delete = new System.Timers.Timer(interval);
            _timerOld_Images_Delete.Elapsed += _timerOld_Images_Delete_Elapsed;
            _timerOld_Images_Delete.Start();
        }

        public static readonly List<string> ImageExtensions = new List<string> { ".JPG" };


        private void _timerOld_Images_Delete_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);
            int iCnt = 0;

            foreach (string file in files)
            {
                // divList.InnerHtml = "Found: " + files.Length + " files.";
                FileInfo info = new FileInfo(file);
                info.Refresh();
                var folder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                var files1 = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);
                string sourcePath = AppDomain.CurrentDomain.BaseDirectory + "Q10" + ".jpg";
                // process imagek
                if (file != sourcePath)
                {
                    if (ImageExtensions.Contains(Path.GetExtension(file).ToUpperInvariant()))
                    {
                        if (info.LastWriteTime <= DateTime.Now.AddDays(-30))
                        {
                            oErrorLog.WriteErrorLog(file+ "Images deleted Successfully..");
                            info.Delete();
                            iCnt += 1;
                        }
                    }
                    else
                    {
                    }
                }
                // divList.InnerHtml = divList.InnerHtml + "<br > " + iCnt + " files deleted.";
            }
        }

        #endregion
        private void btnLogin_Click(object sender, System.EventArgs e)
        {
            Login();
        }

        public void Login()
        {

            if (m_lUserID < 0)
            {
                oErrorLog.WriteErrorLog("In Login Details...");
                string DVRIPAddress = "192.168.1.64";   //textBoxIP.Text; //Éè±¸IPµØÖ·»òÕßÓòÃû
                Int16 DVRPortNumber = 8000;             //Int16.Parse(textBoxPort.Text);//Éè±¸·þÎñ¶Ë¿ÚºÅ
                string DVRUserName = "admin";           //textBoxUserName.Text;//Éè±¸µÇÂ¼ÓÃ»§Ãû
                string DVRPassword = "Ashokj@9";        //textBoxPassword.Text;//Éè±¸µÇÂ¼ÃÜÂë

                CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();

                //µÇÂ¼Éè±¸ Login the device
                m_lUserID = CHCNetSDK.NET_DVR_Login_V30(DVRIPAddress, DVRPortNumber, DVRUserName, DVRPassword, ref DeviceInfo);
                if (m_lUserID < 0)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_Login_V30 failed, error code= " + iLastErr; //µÇÂ¼Ê§°Ü£¬Êä³ö´íÎóºÅ
                    oErrorLog.WriteErrorLog("NET_DVR_Login_V30 failed, error code= " + iLastErr);
                    //MessageBox.Show(str);
                    return;
                }
                else
                {
                    //µÇÂ¼³É¹¦
                    LivePreview();
                    //MessageBox.Show("Login Success!");
                    btnLogin.Text = "Logout";
                }

            }
            else
            {
                //×¢ÏúµÇÂ¼ Logout the device
                if (m_lRealHandle >= 0)
                {
                    MessageBox.Show("Please stop live view firstly");
                    return;
                }

                if (!CHCNetSDK.NET_DVR_Logout(m_lUserID))
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_Logout failed, error code= " + iLastErr;
                    MessageBox.Show(str);
                    return;
                }
                m_lUserID = -1;
                btnLogin.Text = "Login";
            }
            return;
        }

        private void btnPreview_Click(object sender, System.EventArgs e)
        {
            LivePreview();
        }

        public void LivePreview()
        {
            if (m_lUserID < 0)
            {
                MessageBox.Show("Please login the device firstly");
                return;
            }

            if (m_lRealHandle < 0)
            {
                CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO();
                lpPreviewInfo.hPlayWnd = RealPlayWnd.Handle;//Ô¤ÀÀ´°¿Ú
                lpPreviewInfo.lChannel = Int16.Parse(1.ToString());//Ô¤teÀÀµÄÉè±¸Í¨µÀ
                lpPreviewInfo.dwStreamType = 0;//ÂëÁ÷ÀàÐÍ£º0-Ö÷ÂëÁ÷£¬1-×ÓÂëÁ÷£¬2-ÂëÁ÷3£¬3-ÂëÁ÷4£¬ÒÔ´ËÀàÍÆ
                lpPreviewInfo.dwLinkMode = 0;//Á¬½Ó·½Ê½£º0- TCP·½Ê½£¬1- UDP·½Ê½£¬2- ¶à²¥·½Ê½£¬3- RTP·½Ê½£¬4-RTP/RTSP£¬5-RSTP/HTTP 
                lpPreviewInfo.bBlocked = true; //0- ·Ç×èÈûÈ¡Á÷£¬1- ×èÈûÈ¡Á÷
                lpPreviewInfo.dwDisplayBufNum = 1; //²¥·Å¿â²¥·Å»º³åÇø×î´ó»º³åÖ¡Êý
                lpPreviewInfo.byProtoType = 0;
                lpPreviewInfo.byPreviewMode = 0;

                if (RealData == null)
                {
                    RealData = new CHCNetSDK.REALDATACALLBACK(RealDataCallBack);//Ô¤ÀÀÊµÊ±Á÷»Øµ÷º¯Êý
                }

                IntPtr pUser = new IntPtr();//ÓÃ»§Êý¾Ý

                //´ò¿ªÔ¤ÀÀ Start live view 
                m_lRealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(m_lUserID, ref lpPreviewInfo, null/*RealData*/, pUser);
                if (m_lRealHandle < 0)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_RealPlay_V40 failed, error code= " + iLastErr; //Ô¤ÀÀÊ§°Ü£¬Êä³ö´íÎóºÅ
                    MessageBox.Show(str);
                    return;
                }
                else
                {
                    //Ô¤ÀÀ³É¹¦
                    btnPreview.Text = "Stop Live View";
                }
            }
            else
            {
                //Í£Ö¹Ô¤ÀÀ Stop live view 
                if (!CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle))
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_StopRealPlay failed, error code= " + iLastErr;
                    MessageBox.Show(str);
                    return;
                }
                m_lRealHandle = -1;
                btnPreview.Text = "Live View";

            }
            return;
        }

        public void RealDataCallBack(Int32 lRealHandle, UInt32 dwDataType, IntPtr pBuffer, UInt32 dwBufSize, IntPtr pUser)
        {
            if (dwBufSize > 0)
            {
                byte[] sData = new byte[dwBufSize];
                Marshal.Copy(pBuffer, sData, 0, (Int32)dwBufSize);

                string str = "ÊµÊ±Á÷Êý¾Ý.ps";
                FileStream fs = new FileStream(str, FileMode.Create);
                int iLen = (int)dwBufSize;
                fs.Write(sData, 0, iLen);
                fs.Close();
            }
        }


        private void btnJPEG_Click(object sender, EventArgs e)
        {
            GetImage();

        }

        private void RealPlayWnd_Click(object sender, EventArgs e)
        {

        }


        public void GetImage()
        {
            oErrorLog.WriteErrorLog("Before JPEGImage..  " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
            string sJpegPicFileName;
            //Í¼Æ¬±£´æÂ·¾¶ºÍÎÄ¼þÃû the path and file name to save
            //sJpegPicFileName =  "JPEG_test.jpg";

            sJpegPicFileName = DateTime.Now.ToString("yyyyMMdd_hhmmss") + "AMBPL.jpg";

            int lChannel = Int16.Parse(1.ToString()); //Í¨µÀºÅ Channel number

            CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA();
            lpJpegPara.wPicQuality = 5;  //0; //Í¼ÏñÖÊÁ¿ Image quality
            lpJpegPara.wPicSize = 0xff; //×¥Í¼·Ö±æÂÊ Picture size: 2- 4CIF£¬0xff- Auto(Ê¹ÓÃµ±Ç°ÂëÁ÷·Ö±æÂÊ)£¬×¥Í¼·Ö±æÂÊÐèÒªÉè±¸Ö§³Ö£¬¸ü¶àÈ¡ÖµÇë²Î¿¼SDKÎÄµµ


            //JPEG×¥Í¼ Capture a JPEG picture
            if (!CHCNetSDK.NET_DVR_CaptureJPEGPicture(m_lUserID, lChannel, ref lpJpegPara, sJpegPicFileName))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_CaptureJPEGPicture failed, error code= " + iLastErr;
                MessageBox.Show(str);
                return;
            }
            //else
            //{
            //   // oErrorLog.WriteErrorLog("Captured JPEGImage..  " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
            //    str = "Successful to capture the JPEG file and the saved file is " + DateTime.Now.ToString("yyyy’-‘MM’-‘dd’T’HH’:’mm’:’ss") + sJpegPicFileName;
            //    MessageBox.Show(str);
            //}
            oErrorLog.WriteErrorLog("After JPEGImage..  " + " " + System.DateTimeOffset.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
            return;

        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            //Í£Ö¹Ô¤ÀÀ Stop live view 
            if (m_lRealHandle >= 0)
            {
                CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle);
                m_lRealHandle = -1;
            }

            //×¢ÏúµÇÂ¼ Logout the device
            if (m_lUserID >= 0)
            {
                CHCNetSDK.NET_DVR_Logout(m_lUserID);
                m_lUserID = -1;
            }

            CHCNetSDK.NET_DVR_Cleanup();

            Application.Exit();
        }


    }
}
