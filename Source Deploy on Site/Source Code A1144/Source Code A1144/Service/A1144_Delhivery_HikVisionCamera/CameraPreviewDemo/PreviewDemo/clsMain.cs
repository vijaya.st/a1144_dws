﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace PreviewDemo
{

    public class clsMain
    {
        static ErrorLog oErrorLog = new ErrorLog();
        public static DataSet GetDWSDataLog()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();

                //cmd.Parameters.AddWithValue("@CellNo", CellNo);
                ds = clsDataAccess.GetDataSet("DWS_Data_Log_Top_Record_ForImage", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in DWS_Data_Log_Top_Record_ForImage :" + ex.ToString());
            }
            return ds;
        }

        public static DataSet GetDWSDataLogWrite()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();

                //cmd.Parameters.AddWithValue("@CellNo", CellNo);
                ds = clsDataAccess.GetDataSet("DWS_Data_Log_Top_Record_ForImage_Weight", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in DWS_Data_Log_Top_Record_ForImage_Weight :" + ex.ToString());
            }
            return ds;
        }

        
        public static DataSet UpDateIsWeight(string Barcode)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();

                cmd.Parameters.AddWithValue("@Barcode", Barcode);
                ds = clsDataAccess.GetDataSet("DWS_Data_Log_Top_Record_IsWeight_Update", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in DWS_Data_Log_Top_Record_IsWeight_Update :" + ex.ToString());
            }
            return ds;
        }

        public static DataSet UpDateIsImage(string Barcode)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();

                cmd.Parameters.AddWithValue("@Barcode", Barcode);
                ds = clsDataAccess.GetDataSet("DWS_Data_Log_Top_Record_IsImage_Update", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in DWS_Data_Log_Top_Record_IsImage_Update :" + ex.ToString());
            }
            return ds;
        }
    }

    
}