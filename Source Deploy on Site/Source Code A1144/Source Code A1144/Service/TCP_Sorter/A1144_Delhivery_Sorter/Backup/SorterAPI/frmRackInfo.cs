﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;


namespace SorterAPI
{
    public partial class frmRackInfo : Form
    {
        public frmRackInfo()
        {
            InitializeComponent();
        }

        private int _SorterNo;
        public int mSorterNo
        {
            get
            {
                return _SorterNo;
            }
            set
            {
                _SorterNo = value;
            }
        }

        private void btnRackSave_Click(object sender, EventArgs e)
        {
            clsAPI obj = new clsAPI();
            obj.SorterNo = mSorterNo;
            obj.RackId = Convert.ToInt32(lbl1.Text);
        }

        private void frmRackInfo_Load(object sender, EventArgs e)
        {
            label1.Text = "Assign Bin to Sorter No. " + mSorterNo;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close() ;
        }

        private void txt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
