﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using System.Data.SqlClient;

namespace SorterAPI
{
    public partial class frmSorterPlan : Form
    {
        public int mSorterNo { get; set; }
        public int PlanDetail_Id { get; set; } 
       //DataSet dsPLCDiam = new DataSet();
        DataSet dstResults = new DataSet();
      //DataTable dtSorterInfo = new DataTable();
        DataView myView;// = new DataView();
        ErrorLog oErrorLog = new ErrorLog();

        public frmSorterPlan()
        {
            InitializeComponent();
        }

        private void frmSorterPlan_Load(object sender, EventArgs e)
        {
            lblHeading.Text = "Sorter no. " + mSorterNo;


            ReadData("select Pincode,City from CityPincode",
                       ref dstResults, "CityPincode");

            //Creates a DataView from our table's default view
            myView = ((DataTable)dstResults.Tables["CityPincode"]).DefaultView;

            dgvHubList.DataSource = "";
            dgvHubList.DataSource = myView;
            dgvHubList.Columns[0].Width = 100;
            dgvHubList.Columns[1].Width = 160;            
            dgvHubList.RowHeadersVisible = false; // hide first column of grid view

            //bind bin dropdown start

            DataTable dtbin = new DataTable();
            int Branch;
            Branch = Convert.ToInt32(mSorterNo);
            dtbin = clsAPI.getBin(Branch);
            if (dtbin.Rows.Count > 0)
            {
                if (dtbin.Rows[0]["ArmDirection"].ToString() == "Up")
                {
                    int bin = Convert.ToInt32(dtbin.Rows[0]["NoofBagsUp"]);
                    drpBinUp.Items.Clear();
                    for (int i = 1; i <= bin; i++)
                    {
                        drpBinUp.Items.Add(i);
                    }
                    lblUp.Visible = true;
                    drpBinUp.Visible = true;
                    lblDown.Visible = false;
                    drpBinDown.Visible = false;
                }
                else if (dtbin.Rows[0]["ArmDirection"].ToString() == "Down")
                {
                    int bin = Convert.ToInt32(dtbin.Rows[0]["NoofBagsDown"]);
                    drpBinDown.Items.Clear();
                    for (int i = 1; i <= bin; i++)
                    {
                        drpBinDown.Items.Add(i);
                    }
                    lblUp.Visible = false;
                    drpBinUp.Visible = false;
                    lblDown.Visible = true;
                    drpBinDown.Visible = true;
                }
                else
                {
                    int binup = Convert.ToInt32(dtbin.Rows[0]["NoofBagsUp"]);
                    int bindown = Convert.ToInt32(dtbin.Rows[0]["NoofBagsDown"]);

                    drpBinDown.Items.Clear();
                    for (int i = 1; i <= bindown; i++)
                    {
                        drpBinDown.Items.Add(i);
                    }
                    drpBinUp.Items.Clear();
                    for (int i = 1; i <= binup; i++)
                    {
                        drpBinUp.Items.Add(i);
                    }
                    lblUp.Visible = true;
                    drpBinUp.Visible = true;
                    lblDown.Visible = true;
                    drpBinDown.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Bin not assign to sorter " + Branch, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblUp.Visible = false;
                drpBinUp.Visible = false;
                lblDown.Visible = false;
                drpBinDown.Visible = false;
                drpBinUp.Items.Clear();
                drpBinDown.Items.Clear();
                this.Close();
            }
            //end
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Length == 3)
            {
                DataTable dtch = new DataTable();
                int pin = Convert.ToInt32(txtSearch.Text);
                dtch = clsAPI.CheckPin(pin);
                
                if (dtch.Rows.Count > 0)
                {
                    DialogResult ch = MessageBox.Show("This pincode already existing, do you want save it?? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ch == DialogResult.Yes)
                    {
                        if (drpBinUp.Text != "" || drpBinDown.Text != "")
                        {
                            clsAPI obj = new clsAPI();
                            obj.Pincode = Convert.ToInt32(txtSearch.Text);
                            obj.SorterNo = mSorterNo;
                            if (drpBinUp.Text != "")
                            {
                                obj.NoOfBagsUp = Convert.ToInt32(drpBinUp.Text);
                            }
                            if (drpBinDown.Text != "")
                            {
                                obj.NoOfBagsDown = Convert.ToInt32(drpBinDown.Text);
                            }
                            obj.PlanDetailId = PlanDetail_Id; // when data insert from Assignbag and LoactionSorter form that time Planid will be 0

                            int j = clsAPI.InsertLocationSorter(obj);
                            if (j > 0)
                            {
                                MessageBox.Show("Record successfully Saved!!");
                                txtSearch.Text = "";
                               //drpBranch.Text = "";
                                drpBinUp.Text = "";
                                drpBinDown.Text = "";
                                drpBinUp.Visible = false;
                                drpBinDown.Visible = false;
                                lblDown.Visible = false;
                                lblUp.Visible = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("All values are mandatory!!");
                        }
                    }
                }
                else
                {
                    if (drpBinUp.Text != "" || drpBinDown.Text != "")
                    {
                        clsAPI obj = new clsAPI();
                        obj.Pincode = Convert.ToInt32(txtSearch.Text);
                        obj.SorterNo = mSorterNo;
                        if (drpBinUp.Text != "")
                        {
                            obj.NoOfBagsUp = Convert.ToInt32(drpBinUp.Text);
                        }
                        if (drpBinDown.Text != "")
                        {
                            obj.NoOfBagsDown = Convert.ToInt32(drpBinDown.Text);
                        }
                        obj.PlanDetailId = PlanDetail_Id;// when data insert from Assignbag and LoactionSorter form that time Planid will be 0

                        int j = clsAPI.InsertLocationSorter(obj);
                        if (j > 0)
                        {
                            MessageBox.Show("Record successfully Saved!!");
                            txtSearch.Text = "";
                            // drpBranch.Text = "";
                            drpBinUp.Text = "";
                            drpBinDown.Text = "";
                            drpBinUp.Visible = false;
                            drpBinDown.Visible = false;
                            lblDown.Visible = false;
                            lblUp.Visible = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("All values are mandatory!!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Only three digit pincode allowed!!");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rdNationalHub_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dtNHub = new DataTable();
                if (rdNationalHub.Checked == true)
                {
                    ////  dgvHubList.DataSource = dt;
                    dstResults.Tables["CityPincode"].Clear();// = null;

                    ReadData("select Pincode,City from CityPincode",
                        ref dstResults, "CityPincode");

                    //Creates a DataView from our table's default view
                    myView = ((DataTable)dstResults.Tables["CityPincode"]).DefaultView;

                    dgvHubList.DataSource = "";
                    dgvHubList.RowHeadersVisible = false;
                    dgvHubList.DataSource = myView;
                    dgvHubList.Columns[0].Width = 100;
                    dgvHubList.Columns[1].Width = 160;
                    //  dgvHubList.DataSource = dtNHub;
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            string outputInfo = "";
            string[] keyWords = txtSearch.Text.Split(' ');

            foreach (string word in keyWords)
            {
                if (outputInfo.Length == 0)
                {
                    //outputInfo = "(Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo = "Convert(Pincode, 'System.String') LIKE '" + word + "%'";

                }
                else
                {
                    //outputInfo += " AND (Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo += " AND (Pincode LIKE '" + word + "%')";
                }
            }

            //Applies the filter to the DataView
            myView.RowFilter = outputInfo;
        }

        public void ReadData(string strSQL, ref DataSet dstMain, string strTableName)
        {
            try
            {
                string connectionString = clsDataAccess.GetConnectionString(); 
                SqlConnection cnn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand(strSQL, cnn);
                cnn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dstMain, strTableName);
                da.Dispose();
                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
