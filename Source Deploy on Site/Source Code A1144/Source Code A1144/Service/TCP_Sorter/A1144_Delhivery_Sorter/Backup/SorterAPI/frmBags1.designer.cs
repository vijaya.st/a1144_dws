﻿namespace GatiProject
{
    partial class frmBags1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.grpMix = new System.Windows.Forms.GroupBox();
            this.lblRemoveOU = new System.Windows.Forms.Label();
            this.btnOuRemove = new System.Windows.Forms.Button();
            this.txtOuRemove = new System.Windows.Forms.TextBox();
            this.txtOUCodeI = new System.Windows.Forms.TextBox();
            this.cmdMstOUCODE = new System.Windows.Forms.ComboBox();
            this.lbloutxt = new System.Windows.Forms.Label();
            this.lblouMix = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdRemove = new System.Windows.Forms.RadioButton();
            this.rdI = new System.Windows.Forms.RadioButton();
            this.rdD = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpSingle = new System.Windows.Forms.GroupBox();
            this.drpBranch = new System.Windows.Forms.ComboBox();
            this.txtOUCodeD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.drpBag = new System.Windows.Forms.ComboBox();
            this.lblUp = new System.Windows.Forms.Label();
            this.lblDown = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grpMix.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpSingle.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(119, 360);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 40);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // grpMix
            // 
            this.grpMix.BackColor = System.Drawing.SystemColors.Control;
            this.grpMix.Controls.Add(this.lblRemoveOU);
            this.grpMix.Controls.Add(this.btnOuRemove);
            this.grpMix.Controls.Add(this.txtOuRemove);
            this.grpMix.Controls.Add(this.txtOUCodeI);
            this.grpMix.Controls.Add(this.cmdMstOUCODE);
            this.grpMix.Controls.Add(this.lbloutxt);
            this.grpMix.Controls.Add(this.lblouMix);
            this.grpMix.Location = new System.Drawing.Point(18, 89);
            this.grpMix.Name = "grpMix";
            this.grpMix.Size = new System.Drawing.Size(471, 173);
            this.grpMix.TabIndex = 10;
            this.grpMix.TabStop = false;
            this.grpMix.Visible = false;
            // 
            // lblRemoveOU
            // 
            this.lblRemoveOU.AutoSize = true;
            this.lblRemoveOU.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblRemoveOU.Location = new System.Drawing.Point(26, 58);
            this.lblRemoveOU.Name = "lblRemoveOU";
            this.lblRemoveOU.Size = new System.Drawing.Size(128, 19);
            this.lblRemoveOU.TabIndex = 37;
            this.lblRemoveOU.Text = "Enter OU Code";
            // 
            // btnOuRemove
            // 
            this.btnOuRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnOuRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOuRemove.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnOuRemove.ForeColor = System.Drawing.Color.White;
            this.btnOuRemove.Location = new System.Drawing.Point(169, 99);
            this.btnOuRemove.Name = "btnOuRemove";
            this.btnOuRemove.Size = new System.Drawing.Size(113, 35);
            this.btnOuRemove.TabIndex = 27;
            this.btnOuRemove.Text = "Remove";
            this.btnOuRemove.UseVisualStyleBackColor = false;
            this.btnOuRemove.Click += new System.EventHandler(this.btnOuRemove_Click);
            // 
            // txtOuRemove
            // 
            this.txtOuRemove.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOuRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOuRemove.Location = new System.Drawing.Point(170, 50);
            this.txtOuRemove.MaxLength = 6;
            this.txtOuRemove.Name = "txtOuRemove";
            this.txtOuRemove.Size = new System.Drawing.Size(131, 31);
            this.txtOuRemove.TabIndex = 26;
            // 
            // txtOUCodeI
            // 
            this.txtOUCodeI.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOUCodeI.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOUCodeI.Location = new System.Drawing.Point(303, 69);
            this.txtOUCodeI.MaxLength = 6;
            this.txtOUCodeI.Name = "txtOUCodeI";
            this.txtOUCodeI.Size = new System.Drawing.Size(131, 31);
            this.txtOUCodeI.TabIndex = 36;
            this.txtOUCodeI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOUCodeI_KeyPress);
            // 
            // cmdMstOUCODE
            // 
            this.cmdMstOUCODE.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cmdMstOUCODE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmdMstOUCODE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMstOUCODE.FormattingEnabled = true;
            this.cmdMstOUCODE.Location = new System.Drawing.Point(303, 20);
            this.cmdMstOUCODE.Name = "cmdMstOUCODE";
            this.cmdMstOUCODE.Size = new System.Drawing.Size(131, 33);
            this.cmdMstOUCODE.TabIndex = 35;
            // 
            // lbloutxt
            // 
            this.lbloutxt.AutoSize = true;
            this.lbloutxt.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lbloutxt.Location = new System.Drawing.Point(9, 73);
            this.lbloutxt.Name = "lbloutxt";
            this.lbloutxt.Size = new System.Drawing.Size(128, 19);
            this.lbloutxt.TabIndex = 5;
            this.lbloutxt.Text = "Enter OU Code";
            // 
            // lblouMix
            // 
            this.lblouMix.AutoSize = true;
            this.lblouMix.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblouMix.Location = new System.Drawing.Point(9, 28);
            this.lblouMix.Name = "lblouMix";
            this.lblouMix.Size = new System.Drawing.Size(193, 19);
            this.lblouMix.TabIndex = 1;
            this.lblouMix.Text = "OU Code for Mixed Bag";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.rdRemove);
            this.groupBox3.Controls.Add(this.rdI);
            this.groupBox3.Controls.Add(this.rdD);
            this.groupBox3.Location = new System.Drawing.Point(18, 26);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(471, 59);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            // 
            // rdRemove
            // 
            this.rdRemove.AutoSize = true;
            this.rdRemove.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.rdRemove.Location = new System.Drawing.Point(320, 21);
            this.rdRemove.Name = "rdRemove";
            this.rdRemove.Size = new System.Drawing.Size(120, 23);
            this.rdRemove.TabIndex = 2;
            this.rdRemove.Text = "Remove OU";
            this.rdRemove.UseVisualStyleBackColor = true;
            this.rdRemove.CheckedChanged += new System.EventHandler(this.rdRemove_CheckedChanged);
            // 
            // rdI
            // 
            this.rdI.AutoSize = true;
            this.rdI.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.rdI.Location = new System.Drawing.Point(177, 21);
            this.rdI.Name = "rdI";
            this.rdI.Size = new System.Drawing.Size(53, 23);
            this.rdI.TabIndex = 1;
            this.rdI.Text = "Mix";
            this.rdI.UseVisualStyleBackColor = true;
            this.rdI.CheckedChanged += new System.EventHandler(this.rdI_CheckedChanged);
            // 
            // rdD
            // 
            this.rdD.AutoSize = true;
            this.rdD.Checked = true;
            this.rdD.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.rdD.Location = new System.Drawing.Point(30, 21);
            this.rdD.Name = "rdD";
            this.rdD.Size = new System.Drawing.Size(75, 23);
            this.rdD.TabIndex = 0;
            this.rdD.TabStop = true;
            this.rdD.Text = "Single";
            this.rdD.UseVisualStyleBackColor = true;
            this.rdD.CheckedChanged += new System.EventHandler(this.rdD_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(279, 360);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(118, 39);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Exit";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.grpMix);
            this.groupBox1.Controls.Add(this.grpSingle);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(4, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(509, 280);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OU Mapping";
            // 
            // grpSingle
            // 
            this.grpSingle.BackColor = System.Drawing.SystemColors.Control;
            this.grpSingle.Controls.Add(this.drpBranch);
            this.grpSingle.Controls.Add(this.txtOUCodeD);
            this.grpSingle.Controls.Add(this.label3);
            this.grpSingle.Controls.Add(this.drpBag);
            this.grpSingle.Controls.Add(this.lblUp);
            this.grpSingle.Controls.Add(this.lblDown);
            this.grpSingle.Location = new System.Drawing.Point(18, 91);
            this.grpSingle.Name = "grpSingle";
            this.grpSingle.Size = new System.Drawing.Size(471, 169);
            this.grpSingle.TabIndex = 11;
            this.grpSingle.TabStop = false;
            // 
            // drpBranch
            // 
            this.drpBranch.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBranch.FormattingEnabled = true;
            this.drpBranch.Location = new System.Drawing.Point(303, 62);
            this.drpBranch.Name = "drpBranch";
            this.drpBranch.Size = new System.Drawing.Size(131, 33);
            this.drpBranch.TabIndex = 31;
            // 
            // txtOUCodeD
            // 
            this.txtOUCodeD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOUCodeD.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOUCodeD.Location = new System.Drawing.Point(303, 15);
            this.txtOUCodeD.MaxLength = 6;
            this.txtOUCodeD.Name = "txtOUCodeD";
            this.txtOUCodeD.Size = new System.Drawing.Size(131, 31);
            this.txtOUCodeD.TabIndex = 25;
            this.txtOUCodeD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOUCodeD_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(7, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Enter OU Code";
            // 
            // drpBag
            // 
            this.drpBag.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBag.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBag.FormattingEnabled = true;
            this.drpBag.Location = new System.Drawing.Point(303, 114);
            this.drpBag.Name = "drpBag";
            this.drpBag.Size = new System.Drawing.Size(131, 33);
            this.drpBag.TabIndex = 34;
            // 
            // lblUp
            // 
            this.lblUp.AutoSize = true;
            this.lblUp.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblUp.Location = new System.Drawing.Point(9, 122);
            this.lblUp.Name = "lblUp";
            this.lblUp.Size = new System.Drawing.Size(120, 19);
            this.lblUp.TabIndex = 32;
            this.lblUp.Text = "Select Bag No";
            // 
            // lblDown
            // 
            this.lblDown.AutoSize = true;
            this.lblDown.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblDown.Location = new System.Drawing.Point(9, 70);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(126, 19);
            this.lblDown.TabIndex = 33;
            this.lblDown.Text = "Select Gate No";
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::SorterAPI.Properties.Resources.OUMappingHeader;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Location = new System.Drawing.Point(4, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(509, 56);
            this.panel5.TabIndex = 24;
            // 
            // frmBags1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 418);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBags1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmBags1";
            this.Load += new System.EventHandler(this.frmBags1_Load);
            this.grpMix.ResumeLayout(false);
            this.grpMix.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.grpSingle.ResumeLayout(false);
            this.grpSingle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox grpMix;
        private System.Windows.Forms.Label lbloutxt;
        private System.Windows.Forms.Label lblouMix;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdI;
        private System.Windows.Forms.RadioButton rdD;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpSingle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox drpBag;
        private System.Windows.Forms.ComboBox drpBranch;
        private System.Windows.Forms.Label lblDown;
        private System.Windows.Forms.Label lblUp;
        private System.Windows.Forms.ComboBox cmdMstOUCODE;
        private System.Windows.Forms.TextBox txtOUCodeI;
        private System.Windows.Forms.TextBox txtOUCodeD;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rdRemove;
        private System.Windows.Forms.TextBox txtOuRemove;
        private System.Windows.Forms.Button btnOuRemove;
        private System.Windows.Forms.Label lblRemoveOU;
    }
}