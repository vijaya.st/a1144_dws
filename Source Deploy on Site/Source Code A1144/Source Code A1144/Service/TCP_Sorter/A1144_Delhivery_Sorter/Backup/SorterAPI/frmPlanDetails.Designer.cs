﻿namespace SorterAPI
{
    partial class frmPlanDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPlanDetails));
            this.tb2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnSavePlan = new System.Windows.Forms.Button();
            this.drpPlanPriority = new System.Windows.Forms.ComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.grpGeneratebtn = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.txtPlanTitle = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.tb1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtUserNameSearch = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.drpPriorityUpdate = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPlanTitleUpdate = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.FromdateUpdate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.TodateUpdate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvPlanDetails = new System.Windows.Forms.DataGridView();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabCntrl = new System.Windows.Forms.TabControl();
            this.tb2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tb1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlanDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabCntrl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb2
            // 
            this.tb2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tb2.BackgroundImage = global::SorterAPI.Properties.Resources.diemensions21211;
            this.tb2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tb2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb2.Controls.Add(this.groupBox5);
            this.tb2.Location = new System.Drawing.Point(4, 41);
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(875, 475);
            this.tb2.TabIndex = 5;
            this.tb2.Text = "  Create Plan  ";
            this.tb2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Teal;
            this.groupBox5.Controls.Add(this.panel5);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(95, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(682, 427);
            this.groupBox5.TabIndex = 62;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Create New Plan";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel5.Controls.Add(this.btnSavePlan);
            this.panel5.Controls.Add(this.drpPlanPriority);
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.grpGeneratebtn);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.dtTo);
            this.panel5.Controls.Add(this.dtFrom);
            this.panel5.Controls.Add(this.txtPlanTitle);
            this.panel5.Controls.Add(this.label61);
            this.panel5.Controls.Add(this.label55);
            this.panel5.Location = new System.Drawing.Point(25, 20);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(633, 384);
            this.panel5.TabIndex = 0;
            // 
            // btnSavePlan
            // 
            this.btnSavePlan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnSavePlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePlan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnSavePlan.Location = new System.Drawing.Point(189, 157);
            this.btnSavePlan.Name = "btnSavePlan";
            this.btnSavePlan.Size = new System.Drawing.Size(86, 30);
            this.btnSavePlan.TabIndex = 60;
            this.btnSavePlan.Text = "Save";
            this.btnSavePlan.UseVisualStyleBackColor = false;
            this.btnSavePlan.Click += new System.EventHandler(this.btnSavePlan_Click);
            // 
            // drpPlanPriority
            // 
            this.drpPlanPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpPlanPriority.FormattingEnabled = true;
            this.drpPlanPriority.Location = new System.Drawing.Point(187, 112);
            this.drpPlanPriority.Name = "drpPlanPriority";
            this.drpPlanPriority.Size = new System.Drawing.Size(121, 24);
            this.drpPlanPriority.TabIndex = 97;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnClose.Location = new System.Drawing.Point(316, 157);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 30);
            this.btnClose.TabIndex = 61;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(26, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 16);
            this.label3.TabIndex = 96;
            this.label3.Text = "Plan Priority";
            // 
            // grpGeneratebtn
            // 
            this.grpGeneratebtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(172)))), ((int)(((byte)(175)))));
            this.grpGeneratebtn.Enabled = false;
            this.grpGeneratebtn.Location = new System.Drawing.Point(27, 210);
            this.grpGeneratebtn.Name = "grpGeneratebtn";
            this.grpGeneratebtn.Size = new System.Drawing.Size(578, 152);
            this.grpGeneratebtn.TabIndex = 95;
            this.grpGeneratebtn.TabStop = false;
            this.grpGeneratebtn.Text = "Configure Sorter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(339, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 16);
            this.label2.TabIndex = 94;
            this.label2.Text = "To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(184, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 93;
            this.label1.Text = "From";
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(342, 73);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(111, 22);
            this.dtTo.TabIndex = 92;
            this.dtTo.Value = new System.DateTime(2016, 1, 5, 17, 58, 46, 0);
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(187, 73);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(111, 22);
            this.dtFrom.TabIndex = 91;
            this.dtFrom.Value = new System.DateTime(2016, 1, 5, 17, 58, 46, 0);
            this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
            // 
            // txtPlanTitle
            // 
            this.txtPlanTitle.BackColor = System.Drawing.Color.LightGray;
            this.txtPlanTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlanTitle.ForeColor = System.Drawing.Color.Black;
            this.txtPlanTitle.Location = new System.Drawing.Point(187, 21);
            this.txtPlanTitle.MaxLength = 50;
            this.txtPlanTitle.Name = "txtPlanTitle";
            this.txtPlanTitle.Size = new System.Drawing.Size(420, 22);
            this.txtPlanTitle.TabIndex = 84;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(26, 76);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(145, 16);
            this.label61.TabIndex = 90;
            this.label61.Text = "Plan Validity Period";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(26, 23);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(74, 16);
            this.label55.TabIndex = 77;
            this.label55.Text = "Plan Title";
            // 
            // tb1
            // 
            this.tb1.AutoScroll = true;
            this.tb1.BackColor = System.Drawing.Color.White;
            this.tb1.Controls.Add(this.groupBox1);
            this.tb1.Controls.Add(this.dgvPlanDetails);
            this.tb1.Controls.Add(this.pictureBox2);
            this.tb1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb1.Location = new System.Drawing.Point(4, 41);
            this.tb1.Name = "tb1";
            this.tb1.Padding = new System.Windows.Forms.Padding(8, 8, 3, 3);
            this.tb1.Size = new System.Drawing.Size(875, 475);
            this.tb1.TabIndex = 0;
            this.tb1.Text = "  Manage Plans   ";
            this.tb1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Teal;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.drpPriorityUpdate);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btnReset);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtPlanTitleUpdate);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.FromdateUpdate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TodateUpdate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(33, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(808, 189);
            this.groupBox1.TabIndex = 109;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Update Plan Detail";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.txtUserNameSearch);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(20, 142);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(718, 41);
            this.groupBox2.TabIndex = 115;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filter";
            // 
            // textBox3
            // 
            this.textBox3.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox3.Location = new System.Drawing.Point(560, 13);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(123, 21);
            this.textBox3.TabIndex = 114;
            this.textBox3.Text = "User name";
            this.textBox3.Click += new System.EventHandler(this.textBox3_Click);
            // 
            // txtUserNameSearch
            // 
            this.txtUserNameSearch.Location = new System.Drawing.Point(560, 14);
            this.txtUserNameSearch.Name = "txtUserNameSearch";
            this.txtUserNameSearch.Size = new System.Drawing.Size(123, 21);
            this.txtUserNameSearch.TabIndex = 111;
            this.txtUserNameSearch.TextChanged += new System.EventHandler(this.txtUserNameSearch_TextChanged);
            this.txtUserNameSearch.Leave += new System.EventHandler(this.txtUserNameSearch_Leave);
            // 
            // textBox1
            // 
            this.textBox1.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.textBox1.Location = new System.Drawing.Point(169, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 21);
            this.textBox1.TabIndex = 113;
            this.textBox1.Text = "Plan Title";
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(398, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 16);
            this.label10.TabIndex = 112;
            this.label10.Text = "Search by UserName";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(15, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(148, 16);
            this.label9.TabIndex = 110;
            this.label9.Text = "Search by Plan Title";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(169, 15);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(121, 21);
            this.textBox2.TabIndex = 109;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBox2.Leave += new System.EventHandler(this.textBox2_Leave);
            // 
            // drpPriorityUpdate
            // 
            this.drpPriorityUpdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpPriorityUpdate.FormattingEnabled = true;
            this.drpPriorityUpdate.Location = new System.Drawing.Point(178, 112);
            this.drpPriorityUpdate.Name = "drpPriorityUpdate";
            this.drpPriorityUpdate.Size = new System.Drawing.Size(121, 24);
            this.drpPriorityUpdate.TabIndex = 108;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnUpdate.Location = new System.Drawing.Point(329, 110);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(86, 27);
            this.btnUpdate.TabIndex = 98;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 15);
            this.label8.TabIndex = 100;
            this.label8.Text = "Plan Title";
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnReset.Enabled = false;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnReset.Location = new System.Drawing.Point(456, 110);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(86, 27);
            this.btnReset.TabIndex = 99;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(17, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 15);
            this.label7.TabIndex = 102;
            this.label7.Text = "Plan Validity Period";
            // 
            // txtPlanTitleUpdate
            // 
            this.txtPlanTitleUpdate.BackColor = System.Drawing.Color.LightGray;
            this.txtPlanTitleUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlanTitleUpdate.ForeColor = System.Drawing.Color.Black;
            this.txtPlanTitleUpdate.Location = new System.Drawing.Point(178, 21);
            this.txtPlanTitleUpdate.MaxLength = 50;
            this.txtPlanTitleUpdate.Name = "txtPlanTitleUpdate";
            this.txtPlanTitleUpdate.Size = new System.Drawing.Size(420, 22);
            this.txtPlanTitleUpdate.TabIndex = 101;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(17, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 15);
            this.label4.TabIndex = 107;
            this.label4.Text = "Plan Priority";
            // 
            // FromdateUpdate
            // 
            this.FromdateUpdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FromdateUpdate.Location = new System.Drawing.Point(178, 73);
            this.FromdateUpdate.Name = "FromdateUpdate";
            this.FromdateUpdate.Size = new System.Drawing.Size(111, 22);
            this.FromdateUpdate.TabIndex = 103;
            this.FromdateUpdate.Value = new System.DateTime(2016, 1, 10, 0, 0, 0, 0);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(330, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 16);
            this.label5.TabIndex = 106;
            this.label5.Text = "To";
            // 
            // TodateUpdate
            // 
            this.TodateUpdate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.TodateUpdate.Location = new System.Drawing.Point(333, 73);
            this.TodateUpdate.Name = "TodateUpdate";
            this.TodateUpdate.Size = new System.Drawing.Size(111, 22);
            this.TodateUpdate.TabIndex = 104;
            this.TodateUpdate.Value = new System.DateTime(2016, 1, 10, 13, 40, 46, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(175, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 16);
            this.label6.TabIndex = 105;
            this.label6.Text = "From";
            // 
            // dgvPlanDetails
            // 
            this.dgvPlanDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPlanDetails.Location = new System.Drawing.Point(7, 204);
            this.dgvPlanDetails.Name = "dgvPlanDetails";
            this.dgvPlanDetails.Size = new System.Drawing.Size(860, 265);
            this.dgvPlanDetails.TabIndex = 46;
            this.dgvPlanDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPlanDetails_CellContentClick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(875, 475);
            this.pictureBox2.TabIndex = 45;
            this.pictureBox2.TabStop = false;
            // 
            // tabCntrl
            // 
            this.tabCntrl.Controls.Add(this.tb1);
            this.tabCntrl.Controls.Add(this.tb2);
            this.tabCntrl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabCntrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCntrl.Location = new System.Drawing.Point(5, 5);
            this.tabCntrl.Name = "tabCntrl";
            this.tabCntrl.Padding = new System.Drawing.Point(7, 10);
            this.tabCntrl.SelectedIndex = 0;
            this.tabCntrl.Size = new System.Drawing.Size(883, 520);
            this.tabCntrl.TabIndex = 1;
            this.tabCntrl.SelectedIndexChanged += new System.EventHandler(this.tabCntrl_SelectedIndexChanged);
            // 
            // frmPlanDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SorterAPI.Properties.Resources.diemension_HihgResolution;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(894, 531);
            this.Controls.Add(this.tabCntrl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmPlanDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Plan Details";
            this.Load += new System.EventHandler(this.frmPlanDetails_Load);
            this.tb2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tb1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlanDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabCntrl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tb2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtPlanTitle;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSavePlan;
        private System.Windows.Forms.TabPage tb1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabControl tabCntrl;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.GroupBox grpGeneratebtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvPlanDetails;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox drpPlanPriority;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox drpPriorityUpdate;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker TodateUpdate;
        private System.Windows.Forms.DateTimePicker FromdateUpdate;
        private System.Windows.Forms.TextBox txtPlanTitleUpdate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtUserNameSearch;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}