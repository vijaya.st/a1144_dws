﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using S7;
using System.Xml;

namespace SorterAPI
{
    public partial class frmSorterAPIConfigurator3 : Form
    {
        S7.PLC plc = null;
        DataTable dtPLCIP = new DataTable();
        DataTable dtPTLsys = new DataTable();
        string PLCIP = "";

        XmlDocument dom = new XmlDocument();
        XmlElement x;

        public frmSorterAPIConfigurator3()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                          Screen.PrimaryScreen.Bounds.Y + 100);
            //string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = System.Convert.ToInt32(screenWidth);
            //int H = System.Convert.ToInt32(screenHeight);

            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);

            //this.TopMost = true;
            //this.StartPosition = FormStartPosition.Manual;
           // this.MaximumSize = new Size(1200, 800);

            //XmlComment dec = dom.CreateComment("This is data of all Employees");
            //dom.AppendChild(dec);
            //x = dom.CreateElement("students");
            //dom.AppendChild(x);


            
        }

        private void frmSorterAPIConfigurator3_Load(object sender, EventArgs e)
        {
         //   string str_directory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
           // panel1.BackgroundImage = Image.FromFile(str_directory + "\\images\\ptlsystem_banner.png");
           // panel2.BackgroundImage = Image.FromFile(str_directory + "\\images\\ptlimage_title.png");
           // panel3.BackgroundImage = Image.FromFile(str_directory + "\\images\\ptl_middle.png"); 
           // pnlClose.BackgroundImage = Image.FromFile(str_directory + "\\images\\close1.png"); 

            getPTLDWSData();
        }

        private void getPTLDWSData()
        {
            dtPTLsys = clsAPI.getPTLSystemData();
            if (dtPTLsys.Rows.Count > 0)
            {
                btnPTLSystem.Text = "Update";
                txtMinNoArticle.Text = dtPTLsys.Rows[0][1].ToString();
                txtMaxNoArticle.Text = dtPTLsys.Rows[0][2].ToString();
                txtMaxWeightArticle.Text = dtPTLsys.Rows[0][3].ToString();
                txtBagClosurePrefix.Text = dtPTLsys.Rows[0][4].ToString();

                if (Convert.ToInt32(dtPTLsys.Rows[0][5]) == 1)
                {
                    PTLPanel.Enabled = true;
                    btnPTLSystem.Enabled = true;
                  //  btnPTLCancel.Enabled = true;
                }
                else
                {
                    PTLPanel.Enabled = false;
                    btnPTLSystem.Enabled = false;
                  //  btnPTLCancel.Enabled = false;
                }
            }
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblCustInfo_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator4 f4 = new frmSorterAPIConfigurator4();
            f4.Show();
            this.Close();
        }

        private void lblPLCData_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator2 f2 = new frmSorterAPIConfigurator2();
            f2.Show();
            this.Close();
        }

        private void lblPTLSys_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator3 f3 = new frmSorterAPIConfigurator3();
            f3.Show();
            this.Close();
        }

        private void lblLinkDB_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator5 f5 = new frmSorterAPIConfigurator5();
            f5.Show();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator1 f1 = new frmSorterAPIConfigurator1();
            f1.Show();
            this.Close();
        }

        private void lblAssignBag_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator6 f6 = new frmSorterAPIConfigurator6();
            f6.Show();
            this.Close();
        }

        private void pnlClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPTLSystem_Click(object sender, EventArgs e)
        {
            if (txtMinNoArticle.Text != "" && txtMaxNoArticle.Text != "" && txtMaxWeightArticle.Text != "" && txtBagClosurePrefix.Text != "")
            {
                clsAPI obj = new clsAPI();
                obj.MinNoArticle = Convert.ToInt32(txtMinNoArticle.Text);
                obj.MaxNoArticle = Convert.ToInt32(txtMaxNoArticle.Text);
                obj.MaxWeightArticle = Convert.ToInt32(txtMaxWeightArticle.Text);
                obj.BagClosurePrefix = txtBagClosurePrefix.Text;

                if (btnPTLSystem.Text == "Save")
                {
                    int i = clsAPI.InsertPTLSystem(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully Insert");
                    }
                }
                else
                {
                    int i = clsAPI.UpdatePTLSystem(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully Update");
                    }
                }

            }
            else
            {
                MessageBox.Show("All value are mandatory");
            }
        }

        private void txtMinNoArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxWeightArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxNoArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void testDWSSetting_Click(object sender, EventArgs e)
        {
            dtPLCIP = clsAPI.getPort();
            PLCIP = dtPLCIP.Rows[0]["PLCIP"].ToString();
            try{
            plc = new PLC(CPU_Type.S7300, PLCIP, 0, 2);
            ErrorCode errCode = plc.Open();

            if (errCode == ErrorCode.NoError)
            {
                plc.Write("db203.dbd0", txtDistTravebyConvencoderpulse.Text);
                plc.Write("db203.dbd4", txtPropixSpeed.Text);
                plc.Write("db203.dbd12", txtRequiredDistance.Text);
                plc.Write("db203.dbw16", txtIdDataLoadingZone.Text);
                plc.Write("db203.dbw18", txtDataTrakingZone.Text);

                plc.Close();                
            }
            }
                catch(Exception ex)
            {

                }
        
        }

        private void btnUpdateDWS_Click(object sender, EventArgs e)
        {
            //if (txtDistTravebyConvencoderpulse.Text != "" && txtPropixSpeed.Text != "" && txtRequiredDistance.Text != "" && txtIdDataLoadingZone.Text != ""
            //    && txtDataTrakingZone.Text != "")
            //{
            //    //clsAPI obj = new clsAPI();
            //    //obj.DistTravebyConvencoderpulse = txtDistTravebyConvencoderpulse.Text;
            //    //obj.PropixSpeed = txtPropixSpeed.Text;
            //    //obj.RequiredDistance = txtRequiredDistance.Text;
            //    //obj.IdDataLoadingZone = txtIdDataLoadingZone.Text;
            //    //obj.DataTrakingZone = txtDataTrakingZone.Text;
            //    //int i = clsAPI.UpdateDWSSettingData(obj);
            //    //if (i > 0)
            //    //{
            //    //    MessageBox.Show("Data successfully Update");
            //    //}

            //    string PATH = "xmldata.xml";
            //    XmlDocument doc = new XmlDocument();

            //    //If there is no current file, then create a new one
            //    if (!System.IO.File.Exists(PATH))
            //    {
            //        //Create neccessary nodes
            //        XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");
            //        XmlComment comment = doc.CreateComment("This is an XML Generated File");

            //        XmlElement root = doc.CreateElement("PLCValues");
            //        XmlElement DistTravebyConvencoderpulse = doc.CreateElement("DistTravebyConvencoderpulse");
            //      //  XmlAttribute Name = doc.CreateAttribute("Name");
            //        XmlElement PropixSpeed = doc.CreateElement("PropixSpeed");
            //        XmlElement RequiredDistance = doc.CreateElement("RequiredDistance");
            //         XmlElement IdDataLoadingZone = doc.CreateElement("IdDataLoadingZone");
            //         XmlElement DataTrakingZone = doc.CreateElement("DataTrakingZone");
                    
                        

            //        //Add the values for each nodes
            //  //      Name.Value = txtDistTravebyConvencoderpulse.Text;
            //        DistTravebyConvencoderpulse.InnerText = txtDistTravebyConvencoderpulse.Text;
            //        PropixSpeed.InnerText = txtPropixSpeed.Text;
            //        RequiredDistance.InnerText = txtRequiredDistance.Text;
            //         PropixSpeed.InnerText = txtIdDataLoadingZone.Text;
            //         RequiredDistance.InnerText = txtDataTrakingZone.Text;

                    
                    

            //        //Construct the document
            //        doc.AppendChild(declaration);
            //        doc.AppendChild(comment);
            //        doc.AppendChild(root);
            //        root.AppendChild(person);
            //       // person.Attributes.Append(Name);
            //        person.AppendChild(Designation);
            //        person.AppendChild(Location);

            //        doc.Save(PATH);
            //    }
            //    else //If there is already a file
            //    {
            //        //Load the XML File
            //        doc.Load(PATH);

            //        //Get the root element
            //        XmlElement root = doc.DocumentElement;

            //        XmlElement person = doc.CreateElement("Person");
            //        XmlAttribute Name = doc.CreateAttribute("Name");
            //        XmlElement Designation = doc.CreateElement("Designation");
            //        XmlElement Location = doc.CreateElement("Location");

            //        //Add the values for each nodes
            //        Name.Value = txtName.Text;
            //        Designation.InnerText = txtDesignation.Text;
            //        Location.InnerText = txtLocation.Text;

            //        //Construct the Person element
            //        person.Attributes.Append(Name);
            //        person.AppendChild(Designation);
            //        person.AppendChild(Location);

            //        //Add the New person element to the end of the root element
            //        root.AppendChild(person);

            //        //Save the document
            //        doc.Save(PATH);
            //    }

            //    //Show confirmation message
            //    MessageBox.Show("Details have been added to the XML File.");

            //    //Reset text fields for new input
            //    txtName.Text = String.Empty;
            //    txtDesignation.Text = String.Empty;
            //    txtLocation.Text = String.Empty;
            //}
            //else
            //{
            //    MessageBox.Show("All value are mandatory");
            //}       
        }

        private void txtDistTravebyConvencoderpulse_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtPropixSpeed_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtRequiredDistance_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        } 
    }
}