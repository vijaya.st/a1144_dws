﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class frmSorterDirection : Form
    {

        DataTable Armdt = new DataTable();
        DataTable SpecificArmdt = new DataTable();

        DataTable srtdt = new DataTable();
        DataSet dsPLCDiam = new DataSet();
        public static int cellId; 
        public frmSorterDirection()
        {
            InitializeComponent();
        }

        private void frmSorterDirection_Load(object sender, EventArgs e)
        {
            BindGrid();

            dsPLCDiam = clsAPI.getPLCDiemensionalData();
            srtdt = dsPLCDiam.Tables[0];
            int count = Convert.ToInt32(srtdt.Rows[0][1]);

            for (int i = 1; i <= count; i++)
            {
                drpSorter.Items.Add(i);
            }            
        }

        private void BindGrid()
        {
            int id=0;
            Armdt = clsAPI.getArmDirectionDataGrid(id);

            if (Armdt.Rows.Count > 0)
            {
                dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
                dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 12F, FontStyle.Bold, GraphicsUnit.Pixel);
                
                dataGridView1.RowHeadersVisible = false;

                dataGridView1.DataSource = Armdt;
                
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Width = 70;
               // dataGridView1.Columns[1].HeaderText = "Srt";
                dataGridView1.Columns[2].HeaderText = "Arm Direction";
                dataGridView1.Columns[5].HeaderText = "Distance From Earlier Start Point";                
                dataGridView1.Columns[2].Width = 80;
                dataGridView1.Columns[3].Width = 90;
                dataGridView1.Columns[4].Width = 95;
                dataGridView1.Columns[5].Width = 155;
                dataGridView1.Columns[6].Width = 60;
                dataGridView1.Columns[6].DefaultCellStyle.ForeColor = Color.Blue;
                dataGridView1.Columns[7].Width = 70;
                dataGridView1.Columns[7].DefaultCellStyle.ForeColor = Color.Blue;

                dataGridView1.Columns[6].HeaderCell.Style.BackColor = Color.Blue;
                 
               // dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                //DataGridViewColumn dataGridViewColumn = dataGridView1.Columns[1];
                //dataGridViewColumn.HeaderCell.Style.BackColor = Color.Magenta;

              //  cellId = Convert.ToInt32(Armdt.Rows[0]["SorterNo"]);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            clsAPI obj = new clsAPI();
            if (drpSorter.Text != "" && drpArmdirection.Text != "" && txtDistanceFrom.Text != "")
            {
                //if (drpArmdirection.Text == "Both")
                //{
                //    if (txtNoofBags1.Text == "")
                //    {
                //        MessageBox.Show("All values are mandatory");
                //        return;
                //    }
                //    else
                //    {
                //        obj.NoOfBags1 = Convert.ToInt32(txtNoofBags1.Text);
                //    }
                //}

                obj.SorterNo = Convert.ToInt32(drpSorter.Text);
                obj.ArmDirection = drpArmdirection.Text;

                if (drpArmdirection.Text == "Up")
                {
                    if (txtNoofBags.Text != "")
                    {
                        obj.NoOfBagsUp = Convert.ToInt32(txtNoofBags.Text);
                    }
                    else
                    {
                        MessageBox.Show("All values are mandatory");
                        return;
                    }
                }
                else if (drpArmdirection.Text == "Down")
                {
                    if (txtNoofBags1.Text != "")
                    {
                        obj.NoOfBagsDown = Convert.ToInt32(txtNoofBags1.Text);
                    }
                    else
                    {
                        MessageBox.Show("All values are mandatory");
                        return;
                    }
                }
                else
                {
                    if (txtNoofBags1.Text == "" && txtNoofBags.Text == "")
                    {
                        MessageBox.Show("All values are mandatory");
                        return;
                    }
                    else
                    {
                        obj.NoOfBagsUp = Convert.ToInt32(txtNoofBags.Text);
                        obj.NoOfBagsDown = Convert.ToInt32(txtNoofBags1.Text);
                    }                    
                }

                obj.Distance = Convert.ToInt32(txtDistanceFrom.Text);

                int i;
                if (btnSave.Text == "Update")
                {
                    obj.Id = cellId;
                    i = clsAPI.UpdateArmDirection(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Record successfully updated!!");

                        drpSorter.SelectedIndex = -1;
                        drpArmdirection.SelectedIndex = -1;
                        txtNoofBags.Text = "";
                        txtNoofBags1.Text = "";
                        txtNoofBags.Enabled = false;
                        txtNoofBags1.Enabled = false;
                        txtDistanceFrom.Text = "";
                        btnSave.Text = "Save";

                        BindGrid();
                    }                   
                }
                else
                {
                    i = clsAPI.InsertArmDirection(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Record successfully inserted!!");
                        drpSorter.SelectedIndex = -1;
                        drpArmdirection.SelectedIndex = -1;
                        txtNoofBags.Text = "";
                        txtNoofBags1.Text = "";
                        txtNoofBags.Enabled = false;
                        txtNoofBags1.Enabled = false;
                        txtDistanceFrom.Text = "";
                        btnSave.Text = "Save";
                        BindGrid();
                    }
                    else
                    {
                        MessageBox.Show("Sorter No. "+drpSorter.Text+" already existed!!");
                        drpSorter.SelectedIndex = -1;
                        drpArmdirection.SelectedIndex = -1;
                        txtNoofBags.Text = "";
                        txtNoofBags1.Text = "";
                        txtNoofBags.Enabled = false;
                        txtNoofBags1.Enabled = false;
                        txtDistanceFrom.Text = "";
                        btnSave.Text = "Save";
                    }
                }
            }
            else
            {
                MessageBox.Show("All values are mandatory");                
            }          
        }

        private void drpArmdirection_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (drpArmdirection.Text == "Both")
            //{
            //    txtNoofBags1.Visible = true;
            //}
            //else
            //{
            //    txtNoofBags1.Visible = false;
            //}

            if (drpArmdirection.Text == "Both")
            {
                txtNoofBags.Enabled = true;
                txtNoofBags1.Enabled = true;
                lblBag.Enabled = true;
                lblbag2.Enabled = true;
            }
            else if (drpArmdirection.Text == "Up")
            {
                txtNoofBags.Enabled = true;
                txtNoofBags1.Enabled = false;
                lblBag.Enabled = true;
                lblbag2.Enabled = false;
            }
            else if (drpArmdirection.Text == "Down")
            {
                txtNoofBags.Enabled = false;
                txtNoofBags1.Enabled = true;
                lblBag.Enabled = false;
                lblbag2.Enabled = true;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                    int id = Convert.ToInt32(row.Cells[0].Value);

                    //cellId = id;

                   SpecificArmdt = clsAPI.getArmDirectionDataGrid(id);
                   
                   if (SpecificArmdt.Rows.Count > 0)
                   {
                       cellId = Convert.ToInt32(SpecificArmdt.Rows[0]["SorterNo"]);
                       btnSave.Text = "Update";
                       drpSorter.Text = SpecificArmdt.Rows[0]["SorterNo"].ToString();
                       drpArmdirection.Text = SpecificArmdt.Rows[0]["ArmDirection"].ToString();
                       txtDistanceFrom.Text = SpecificArmdt.Rows[0]["DistanceFromEarlierStartPoint"].ToString();

                       if (SpecificArmdt.Rows[0]["No. of BagsUp"].ToString() != "0")
                       {
                           txtNoofBags.Text = SpecificArmdt.Rows[0]["No. of BagsUp"].ToString();
                       }

                       if (SpecificArmdt.Rows[0]["No. of BagsDown"].ToString() != "0")
                       {
                           txtNoofBags1.Visible = true;
                           txtNoofBags1.Text = SpecificArmdt.Rows[0]["No. of BagsDown"].ToString();
                       }
                   }
                }
            }
            if (e.ColumnIndex == 7)
            {
                if (e.RowIndex >= 0)
                {
                 DialogResult ch = MessageBox.Show("Are you sure you want to delete this record?","Delete", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                 if (ch == DialogResult.Yes)
                 {
                     int id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[1].Value);

                     //  int id = Convert.ToInt32(SpecificArmdt.Rows[0]["SorterNo"]);

                     int j = clsAPI.DeleteArmDirectionDataGrid(id);
                     if (j > 0)
                     {
                         MessageBox.Show("Record successfully deleted");
                         BindGrid();
                     }
                 }
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            drpSorter.SelectedIndex = -1;
            drpArmdirection.SelectedIndex = -1;
            txtNoofBags.Text = "";
            txtNoofBags1.Text = "";
            txtNoofBags.Enabled = false;
            txtNoofBags1.Enabled = false;
            txtDistanceFrom.Text = "";
            btnSave.Text = "Save";
        }

        private void txtDistanceFrom_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtNoofBags_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtNoofBags1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
