﻿namespace SorterAPI
{
    partial class BayAssign1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlfiltertext = new System.Windows.Forms.Panel();
            this.drpBranchFilter = new System.Windows.Forms.ComboBox();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.drpBinDown = new System.Windows.Forms.ComboBox();
            this.drpBinUp = new System.Windows.Forms.ComboBox();
            this.lblDown = new System.Windows.Forms.Label();
            this.lblUp = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.drpBranch = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblSuccesful = new System.Windows.Forms.Label();
            this.btnSaveLocation = new System.Windows.Forms.Button();
            this.dgvHubList = new System.Windows.Forms.DataGridView();
            this.BayAClose = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblClose = new System.Windows.Forms.Label();
            this.pnlfiltertext.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.bay_Changes;
            this.panel1.Location = new System.Drawing.Point(0, -250);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1200, 95);
            this.panel1.TabIndex = 10;
            // 
            // pnlfiltertext
            // 
            this.pnlfiltertext.BackgroundImage = global::SorterAPI.Properties.Resources.filterText;
            this.pnlfiltertext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlfiltertext.Controls.Add(this.drpBranchFilter);
            this.pnlfiltertext.Controls.Add(this.txtFilter);
            this.pnlfiltertext.Location = new System.Drawing.Point(0, -371);
            this.pnlfiltertext.Name = "pnlfiltertext";
            this.pnlfiltertext.Size = new System.Drawing.Size(1200, 119);
            this.pnlfiltertext.TabIndex = 9;
            // 
            // drpBranchFilter
            // 
            this.drpBranchFilter.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBranchFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranchFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBranchFilter.FormattingEnabled = true;
            this.drpBranchFilter.Location = new System.Drawing.Point(460, 41);
            this.drpBranchFilter.Name = "drpBranchFilter";
            this.drpBranchFilter.Size = new System.Drawing.Size(281, 33);
            this.drpBranchFilter.TabIndex = 12;
            // 
            // txtFilter
            // 
            this.txtFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilter.Location = new System.Drawing.Point(465, 41);
            this.txtFilter.MaxLength = 6;
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(270, 31);
            this.txtFilter.TabIndex = 17;
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackgroundImage = global::SorterAPI.Properties.Resources.bay_header;
            this.pnlHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlHeader.Location = new System.Drawing.Point(12, -470);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1177, 98);
            this.pnlHeader.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(4, 109);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1173, 500);
            this.panel3.TabIndex = 12;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel4.Controls.Add(this.panel2);
            this.panel4.Controls.Add(this.dgvHubList);
            this.panel4.Location = new System.Drawing.Point(11, 11);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1149, 475);
            this.panel4.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnExit);
            this.panel2.Controls.Add(this.drpBinDown);
            this.panel2.Controls.Add(this.drpBinUp);
            this.panel2.Controls.Add(this.lblDown);
            this.panel2.Controls.Add(this.lblUp);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.drpBranch);
            this.panel2.Controls.Add(this.txtSearch);
            this.panel2.Controls.Add(this.lblSuccesful);
            this.panel2.Controls.Add(this.btnSaveLocation);
            this.panel2.Location = new System.Drawing.Point(569, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(567, 441);
            this.panel2.TabIndex = 22;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(269, 336);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(121, 41);
            this.btnExit.TabIndex = 31;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // drpBinDown
            // 
            this.drpBinDown.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBinDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBinDown.FormattingEnabled = true;
            this.drpBinDown.Location = new System.Drawing.Point(166, 222);
            this.drpBinDown.Name = "drpBinDown";
            this.drpBinDown.Size = new System.Drawing.Size(120, 33);
            this.drpBinDown.TabIndex = 30;
            this.drpBinDown.Visible = false;
            // 
            // drpBinUp
            // 
            this.drpBinUp.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBinUp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBinUp.FormattingEnabled = true;
            this.drpBinUp.Location = new System.Drawing.Point(166, 161);
            this.drpBinUp.Name = "drpBinUp";
            this.drpBinUp.Size = new System.Drawing.Size(120, 33);
            this.drpBinUp.TabIndex = 27;
            this.drpBinUp.Visible = false;
            // 
            // lblDown
            // 
            this.lblDown.AutoSize = true;
            this.lblDown.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblDown.Location = new System.Drawing.Point(16, 230);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(138, 19);
            this.lblDown.TabIndex = 29;
            this.lblDown.Text = "Select Bin Down";
            this.lblDown.Visible = false;
            // 
            // lblUp
            // 
            this.lblUp.AutoSize = true;
            this.lblUp.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblUp.Location = new System.Drawing.Point(16, 169);
            this.lblUp.Name = "lblUp";
            this.lblUp.Size = new System.Drawing.Size(115, 19);
            this.lblUp.TabIndex = 28;
            this.lblUp.Text = "Select Bin Up";
            this.lblUp.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 19);
            this.label1.TabIndex = 26;
            this.label1.Text = "Select Bay";
            // 
            // drpBranch
            // 
            this.drpBranch.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBranch.FormattingEnabled = true;
            this.drpBranch.Location = new System.Drawing.Point(162, 101);
            this.drpBranch.Name = "drpBranch";
            this.drpBranch.Size = new System.Drawing.Size(270, 33);
            this.drpBranch.TabIndex = 25;
            this.drpBranch.SelectedIndexChanged += new System.EventHandler(this.drpBranch_SelectedIndexChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(162, 43);
            this.txtSearch.MaxLength = 6;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(270, 31);
            this.txtSearch.TabIndex = 24;
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // lblSuccesful
            // 
            this.lblSuccesful.AutoSize = true;
            this.lblSuccesful.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuccesful.ForeColor = System.Drawing.Color.Black;
            this.lblSuccesful.Location = new System.Drawing.Point(16, 51);
            this.lblSuccesful.Name = "lblSuccesful";
            this.lblSuccesful.Size = new System.Drawing.Size(117, 19);
            this.lblSuccesful.TabIndex = 23;
            this.lblSuccesful.Text = "Enter Pincode";
            // 
            // btnSaveLocation
            // 
            this.btnSaveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnSaveLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveLocation.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnSaveLocation.ForeColor = System.Drawing.Color.White;
            this.btnSaveLocation.Location = new System.Drawing.Point(87, 336);
            this.btnSaveLocation.Name = "btnSaveLocation";
            this.btnSaveLocation.Size = new System.Drawing.Size(121, 41);
            this.btnSaveLocation.TabIndex = 21;
            this.btnSaveLocation.Text = "Submit";
            this.btnSaveLocation.UseVisualStyleBackColor = false;
            this.btnSaveLocation.Click += new System.EventHandler(this.btnSaveLocation_Click);
            // 
            // dgvHubList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.dgvHubList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkOrange;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHubList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvHubList.ColumnHeadersHeight = 45;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(157)))), ((int)(((byte)(157)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHubList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvHubList.GridColor = System.Drawing.Color.White;
            this.dgvHubList.Location = new System.Drawing.Point(15, 12);
            this.dgvHubList.Name = "dgvHubList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHubList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvHubList.RowHeadersWidth = 45;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvHubList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvHubList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHubList.Size = new System.Drawing.Size(539, 441);
            this.dgvHubList.TabIndex = 12;
            this.dgvHubList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvHubList_CellPainting);
            // 
            // BayAClose
            // 
            this.BayAClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.BayAClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BayAClose.Location = new System.Drawing.Point(591, -493);
            this.BayAClose.Name = "BayAClose";
            this.BayAClose.Size = new System.Drawing.Size(18, 14);
            this.BayAClose.TabIndex = 13;
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::SorterAPI.Properties.Resources.bay_LocationHeader;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Location = new System.Drawing.Point(8, 29);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1169, 74);
            this.panel5.TabIndex = 14;
            // 
            // lblClose
            // 
            this.lblClose.AutoSize = true;
            this.lblClose.BackColor = System.Drawing.Color.Transparent;
            this.lblClose.Image = global::SorterAPI.Properties.Resources.close1;
            this.lblClose.Location = new System.Drawing.Point(1145, 9);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(19, 13);
            this.lblClose.TabIndex = 15;
            this.lblClose.Text = "    ";
            this.lblClose.Click += new System.EventHandler(this.lblClose_Click);
            // 
            // BayAssign1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 624);
            this.Controls.Add(this.lblClose);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlfiltertext);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.BayAClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BayAssign1";
            this.Text = "BayAssign1";
            this.Load += new System.EventHandler(this.BayAssign1_Load);
            this.pnlfiltertext.ResumeLayout(false);
            this.pnlfiltertext.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlfiltertext;
        private System.Windows.Forms.ComboBox drpBranchFilter;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgvHubList;
        private System.Windows.Forms.Panel BayAClose;
        private System.Windows.Forms.Button btnSaveLocation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblSuccesful;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox drpBranch;
        private System.Windows.Forms.ComboBox drpBinDown;
        private System.Windows.Forms.ComboBox drpBinUp;
        private System.Windows.Forms.Label lblDown;
        private System.Windows.Forms.Label lblUp;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblClose;
    }
}