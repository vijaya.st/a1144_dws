﻿namespace DynamicButton
{
    partial class frmBags
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBagInfo = new System.Windows.Forms.GroupBox();
            this.lblPic = new System.Windows.Forms.Label();
            this.grpoucode = new System.Windows.Forms.GroupBox();
            this.txtOUCode = new System.Windows.Forms.TextBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grpBagInfo.SuspendLayout();
            this.grpoucode.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBagInfo
            // 
            this.grpBagInfo.Controls.Add(this.btnExit);
            this.grpBagInfo.Controls.Add(this.btnEdit);
            this.grpBagInfo.Controls.Add(this.lblPic);
            this.grpBagInfo.Controls.Add(this.grpoucode);
            this.grpBagInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBagInfo.Location = new System.Drawing.Point(9, 12);
            this.grpBagInfo.Name = "grpBagInfo";
            this.grpBagInfo.Size = new System.Drawing.Size(465, 234);
            this.grpBagInfo.TabIndex = 1;
            this.grpBagInfo.TabStop = false;
            this.grpBagInfo.Text = "  Capture OU / Pincode";
            // 
            // lblPic
            // 
            this.lblPic.AutoSize = true;
            this.lblPic.Location = new System.Drawing.Point(305, 0);
            this.lblPic.Name = "lblPic";
            this.lblPic.Size = new System.Drawing.Size(57, 20);
            this.lblPic.TabIndex = 7;
            this.lblPic.Text = "label3";
            // 
            // grpoucode
            // 
            this.grpoucode.Controls.Add(this.txtOUCode);
            this.grpoucode.Location = new System.Drawing.Point(29, 36);
            this.grpoucode.Name = "grpoucode";
            this.grpoucode.Size = new System.Drawing.Size(406, 125);
            this.grpoucode.TabIndex = 1;
            this.grpoucode.TabStop = false;
            // 
            // txtOUCode
            // 
            this.txtOUCode.Enabled = false;
            this.txtOUCode.Location = new System.Drawing.Point(10, 19);
            this.txtOUCode.Multiline = true;
            this.txtOUCode.Name = "txtOUCode";
            this.txtOUCode.Size = new System.Drawing.Size(385, 94);
            this.txtOUCode.TabIndex = 0;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnEdit.ForeColor = System.Drawing.Color.White;
            this.btnEdit.Location = new System.Drawing.Point(103, 180);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(111, 37);
            this.btnEdit.TabIndex = 8;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(251, 180);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(111, 37);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmBags
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 261);
            this.Controls.Add(this.grpBagInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBags";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configure Pincodes to Bag";
            this.Load += new System.EventHandler(this.frmBags_Load);
            this.grpBagInfo.ResumeLayout(false);
            this.grpBagInfo.PerformLayout();
            this.grpoucode.ResumeLayout(false);
            this.grpoucode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBagInfo;
        private System.Windows.Forms.GroupBox grpoucode;
        private System.Windows.Forms.TextBox txtOUCode;
        private System.Windows.Forms.Label lblPic;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnExit;
    }
}