﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Data;
using System.Data.SqlClient;


namespace SorterAPI.Common
{
   public class clsDataAccess
    {
        static string databaseOwner = "dbo";
        public static string GetConnectionString()
        {
              //string con = @"Data Source=ANC6\SQLEXPRESS;Initial Catalog=GPO;Integrated Security=Yes";
              //string con = @"Data Source=ANC6\SQLEXPRESS;Initial Catalog=GPO;Integrated Security=No; UID=sa; Pwd=123;";
            string con = ConfigurationManager.ConnectionStrings["GPOConnectionString"].ConnectionString;
             // string con = @"Data Source=ANL1-PC\SQLEXPRESS;Initial Catalog=GPO;Integrated Security=Yes";
           // string con = @"Data Source=EPPC-PC\SQLEXPRESS;Initial Catalog=AMBPL_GPO;Integrated Security=Yes";
            //string con = @"Data Source=PROPIX-PC\SQLEXPRESS;Initial Catalog=GPO;Integrated Security=No; UID=sa; Pwd=sql;";
            return con;
        }
        
        public static DataTable GetDataTable(string ProcName, SqlCommand cmd)
        {
            ErrorLog oErrorLog = new ErrorLog();
            DataTable dt = new DataTable();
            try
            {
                SqlConnection cn = new SqlConnection(GetConnectionString());
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.Message);
            }
            return dt;
        }

        public static int ExecuteNonQuery(string ProcName, SqlCommand cmd)
        {
            int ResultExecuteNonQuery = 0;
            try
            {
                SqlConnection cn = new SqlConnection(GetConnectionString());
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                cn.Open();
                ResultExecuteNonQuery = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
            }
            return ResultExecuteNonQuery;
        }

        public static DataSet GetDataSet(string procedureName, SqlCommand Commond)
        {
            SqlConnection connection = new SqlConnection(GetConnectionString());
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                Commond.CommandText = databaseOwner + "." + procedureName;
                Commond.Connection = connection;

                //Mark As Stored Procedure
                Commond.CommandType = CommandType.StoredProcedure;

                da.SelectCommand = Commond;
                da.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
            }
            return ds;
        }
    }
}
