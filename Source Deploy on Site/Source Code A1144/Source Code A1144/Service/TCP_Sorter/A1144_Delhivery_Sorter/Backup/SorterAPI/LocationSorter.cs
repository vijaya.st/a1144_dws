﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using SorterAPI.Common;

using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace SorterAPI
{
    public partial class LocationSorter : Form
    {
        DataSet dsPLCDiam = new DataSet();
        DataSet dstResults = new DataSet();
        DataTable dtSorterInfo = new DataTable();
        DataView myView;// = new DataView();
        ErrorLog oErrorLog = new ErrorLog();
        DataTable dtbin = new DataTable();
        public LocationSorter()
        {
            InitializeComponent();
        }
        private void LocationSorter_Load(object sender, EventArgs e)
        {
            ReadData("select LocationId,Pincode,SorterNo as [BayNo] from LocationAddr",
                       ref dstResults, "LocationAddr");

            //Creates a DataView from our table's default view
            myView = ((DataTable)dstResults.Tables["LocationAddr"]).DefaultView;

            dgvHubList.RowHeadersVisible = false;
            dgvHubList.DataSource = myView;
            dgvHubList.Columns[0].Width = 100;
            dgvHubList.Columns[1].Width = 160;
            //Assigns the DataView to the grid
            dgvHubList.RowHeadersVisible = false;
            //  dgvHubList.DataSource = myView;

            DataTable dtCircle = new DataTable();
            dtCircle = clsAPI.GetCircle();
            //   DataRow dr;
            //  dr = dtCircle.NewRow();
            //   dr.ItemArray = new object[] { 0, "--Select--" };
            //  dtCircle.Rows.InsertAt(dr, 0);
            //  drpCircle.DisplayMember = "CircleName";
            //  drpCircle.ValueMember = "CircleId";
            //  drpCircle.DataSource = dtCircle;

            dsPLCDiam = clsAPI.getPLCDiemensionalData();
            dtSorterInfo = dsPLCDiam.Tables[0];

            int count = Convert.ToInt32(dtSorterInfo.Rows[0][1]);
            drpBranch.Items.Clear();
            for (int i = 1; i <= count; i++)
            {
                drpBranch.Items.Add(i);
            }

            drpBranchFilter.Items.Clear();
            for (int i = 1; i <= count; i++)
            {
                drpBranchFilter.Items.Add(i);
            }
            //drpBinUp.Items.Clear();
            //for (int i = 1; i <= 32; i++)
            //{
            //    drpBinUp.Items.Add(i);
            //}
           
        }

        //private void rdNationalHub_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DataTable dtNHub = new DataTable();
        //        if (rdNationalHub.Checked == true)
        //        {                    
        //            ////  dgvHubList.DataSource = dt;
        //            dstResults.Tables["CityPincode"].Clear();// = null;

        //            ReadData("select Pincode,City from CityPincode",
        //                ref dstResults, "CityPincode");

        //            //Creates a DataView from our table's default view
        //            myView = ((DataTable)dstResults.Tables["CityPincode"]).DefaultView;
                    
        //            dgvHubList.RowHeadersVisible = false;
        //            dgvHubList.DataSource = myView;
        //            dgvHubList.Columns[0].Width = 100;
        //            dgvHubList.Columns[1].Width = 160;
        //            //  dgvHubList.DataSource = dtNHub;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oErrorLog.WriteErrorLog(ex.ToString());
        //    }
        //}

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            string outputInfo = "";
            string[] keyWords = txtSearch.Text.Split(' ');

            foreach (string word in keyWords)
            {
                if (outputInfo.Length == 0)
                {
                    //outputInfo = "(Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo = "Convert(Pincode, 'System.String') LIKE '" + word + "%'";

                }
                else
                {
                    //outputInfo += " AND (Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo += " AND (Pincode LIKE '" + word + "%')";
                }
            }

            //Applies the filter to the DataView
            myView.RowFilter = outputInfo;
        }
        public void ReadData(string strSQL, ref DataSet dstMain, string strTableName)
        {
            try
            {
               // string connectionString = @"Data Source=ANC6\SQLEXPRESS;Initial Catalog=GPO;Integrated Security=Yes";
               string con = clsDataAccess.GetConnectionString();                
                SqlConnection cnn = new SqlConnection(con);
                SqlCommand cmd = new SqlCommand(strSQL, cnn);
                cnn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dstMain, strTableName);
                da.Dispose();
                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void btnSaveLocation_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Length == 3 || txtSearch.Text.Length == 6)
            {
                DataTable dtch = new DataTable();
                int pin = Convert.ToInt32(txtSearch.Text);
                dtch = clsAPI.CheckPin(pin);
                if (dtch.Rows.Count > 0)
                {
                    string srtSoter = dtch.Rows[0]["SorterNo"].ToString();
                    DialogResult ch = MessageBox.Show("This pincode already existing to Bay no. "+srtSoter+", do you want update it?? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ch == DialogResult.Yes)
                    {
                        if (drpBranch.Text != "" && (drpBinUp.Text != "" || drpBinDown.Text != ""))
                        {
                            clsAPI obj = new clsAPI();
                            obj.Pincode = Convert.ToInt32(txtSearch.Text);
                            obj.SorterNo = Convert.ToInt32(drpBranch.Text);
                            if (drpBinUp.Text != "")
                            {
                                obj.Binno = Convert.ToInt32(drpBinUp.Text);
                            }
                            if (drpBinDown.Text != "")
                            {
                                obj.Binno = Convert.ToInt32(drpBinDown.Text);
                            }
                        //    obj.PlanDetailId = 0;// when data insert from Assignbag and LoactionSorter form that time Planid will be 0

                            int j = clsAPI.UpdateLocationAddr(obj);//InsertLocationSorter(obj);
                            if (j > 0)
                            {
                                MessageBox.Show("Record successfully Saved!!");
                                txtSearch.Text = "";
                                drpBranch.Text = "";
                                drpBinUp.Text = "";
                                drpBinDown.Text = "";
                                drpBinUp.Visible = false;
                                drpBinDown.Visible = false;
                                lblDown.Visible = false;
                                lblUp.Visible = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("All values are mandatory!!");
                        }
                    }
                }
                else
                {
                    if (drpBranch.Text != "" && (drpBinUp.Text != "" || drpBinDown.Text != ""))
                    {
                        clsAPI obj = new clsAPI();
                        obj.Pincode = Convert.ToInt32(txtSearch.Text);
                        obj.SorterNo = Convert.ToInt32(drpBranch.Text);
                        if (drpBinUp.Text != "")
                        {
                            obj.Binno = Convert.ToInt32(drpBinUp.Text);
                        }
                        if (drpBinDown.Text != "")
                        {
                            obj.Binno = Convert.ToInt32(drpBinDown.Text);
                        }
                      //  obj.PlanDetailId = 0;// when data insert from Assignbag and LoactionSorter form that time Planid will be 0

                        int j = clsAPI.SaveLocationAddr(obj);  //InsertLocationSorter(obj);//
                        if (j > 0)
                        {
                            MessageBox.Show("Record successfully Saved!!");
                            txtSearch.Text = "";
                            drpBranch.Text = "";
                            drpBinUp.Text = "";
                            drpBinDown.Text = "";
                            drpBinUp.Visible = false;
                            drpBinDown.Visible = false;
                            lblDown.Visible = false;
                            lblUp.Visible = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("All values are mandatory!!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Only Three or Six digit pincode allowed!!");
            }
        }

        private void btnAddNewLocation_Click(object sender, EventArgs e)
        {
            frmAddNewLocation fAddLoc = new frmAddNewLocation();
            fAddLoc.ShowDialog();
        }

        private void btnLocationClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void drpBranch_SelectedIndexChanged(object sender, EventArgs e)
        {            
            int Branch;
            Branch = Convert.ToInt32(drpBranch.Text);
            dtbin = clsAPI.getBin(Branch);
            if (dtbin.Rows.Count > 0)
            {
                if (dtbin.Rows[0]["ArmDirection"].ToString() == "Up")
                {
                    int bin = Convert.ToInt32(dtbin.Rows[0]["NoofBags"]);
                    drpBinUp.Items.Clear();
                    for (int i = 1; i <= bin; i++)
                    {
                        drpBinUp.Items.Add(i);
                    }
                    lblUp.Visible = true;
                    drpBinUp.Visible = true;
                    lblDown.Visible = false;
                    drpBinDown.Visible = false;
                }
                else if (dtbin.Rows[0]["ArmDirection"].ToString() == "Down")
                {
                    int bin = Convert.ToInt32(dtbin.Rows[0]["NoofBags1"]);
                    drpBinDown.Items.Clear();
                    for (int i = 1; i <= bin; i++)
                    {
                        drpBinDown.Items.Add(i);
                    }
                    lblUp.Visible = false;
                    drpBinUp.Visible = false;
                    lblDown.Visible = true;
                    drpBinDown.Visible = true;
                }
                else
                {
                    int binup = Convert.ToInt32(dtbin.Rows[0]["NoofBags"]);
                    int bindown = Convert.ToInt32(dtbin.Rows[0]["NoofBags1"]);

                    drpBinDown.Items.Clear();
                    for (int i = 1; i <= bindown; i++)
                    {
                        drpBinDown.Items.Add(i);
                    }
                    drpBinUp.Items.Clear();
                    for (int i = 1; i <= binup; i++)
                    {
                        drpBinUp.Items.Add(i);
                    }
                    lblUp.Visible = true;
                    drpBinUp.Visible = true;
                    lblDown.Visible = true;
                    drpBinDown.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Bin not assign to sorter " + Branch, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblUp.Visible = false;
                drpBinUp.Visible = false;
                lblDown.Visible = false;
                drpBinDown.Visible = false;
                drpBinUp.Items.Clear();
                drpBinDown.Items.Clear();
            }
        }

        private void drpBranchFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
          string strBay = drpBranchFilter.Text;
          string outputInfo1 = string.Empty;
           // outputInfo1 = "SorterNo = '" + strBay + "')";
          outputInfo1 += "Convert(BayNo, 'System.String') LIKE '" + strBay + "%'";

        
            //Applies the filter to the DataView
            myView.RowFilter = outputInfo1;
        }

        private void txtFilter_KeyUp(object sender, KeyEventArgs e)
        {
            string outputInfo = "";
            string[] keyWords = txtSearch.Text.Split(' ');

            foreach (string word in keyWords)
            {
                if (outputInfo.Length == 0)
                {
                    //outputInfo = "(Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo = "Convert(Pincode, 'System.String') LIKE '" + word + "%'";

                }
                else
                {
                    //outputInfo += " AND (Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo += " AND (Pincode LIKE '" + word + "%')";
                }
            }

            //Applies the filter to the DataView
            myView.RowFilter = outputInfo;
        }

        private void txtFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
