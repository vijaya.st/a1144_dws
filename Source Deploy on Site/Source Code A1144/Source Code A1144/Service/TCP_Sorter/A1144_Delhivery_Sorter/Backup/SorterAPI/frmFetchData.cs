﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using System.IO.Ports;
using System.Threading;

namespace SorterAPI
{
    public partial class frmFetchData : Form
    {
        ManualResetEvent evt = new ManualResetEvent(true);
        ThreadServer t1 = null;
        ThreadServer t2 = null;
        ThreadServer t3 = null;​

        public frmFetchData()
        {
            InitializeComponent();
        }
        private static DataGridView[] dataGridView = new DataGridView[10];

        private void frmFetchData_Load(object sender, EventArgs e)
        {
            t1 = new ThreadServer(this, "L1");
            t1.RaiseMe += new ThreadHandler(RaiseMe);
            this.BeginInvoke(new Action(() => { t1.Start(); }));

            t2 = new ThreadServer(this, "L2");
            t2.RaiseMe += new ThreadHandler(RaiseMe);
            this.BeginInvoke(new Action(() => { t2.Start(); }));

            t3 = new ThreadServer(this, "L3");
            t3.RaiseMe += new ThreadHandler(RaiseMe);
            this.BeginInvoke(new Action(() => { t3.Start(); }));​ 
            
        }

        private void frmFetchData_FormClosing(object sender, FormClosingEventArgs e)
        {
            t1.Stop();
            t2.Stop();
            t3.Stop();
        }
​
        void RaiseMe(object data, string name)
        {
            while (true)
            {
                evt.Set();
                if (name == "L1")
                {
                    this.t1.RecDataScanner();
                }

                if (name == "L2")
                {
                    this.t2.RecDataVMS();
                }

                if (name == "L3")
                {
                    this.t3.RecDataWeighing();
                } 
            }
        }​
        private void timer1_Tick(object sender, EventArgs e)
        {
          //  DataTable dt = new DataTable();
           // dt = clsAPI.getWSData();
            //dgvMain.DataSource = dt;
           //// MessageBox.Show("00");
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }        ​
}

