﻿namespace SorterAPI
{
    partial class frmBatchOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvBatchOperation = new System.Windows.Forms.DataGridView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtBarCode = new System.Windows.Forms.TextBox();
            this.tmrBit = new System.Windows.Forms.Timer(this.components);
            this.tmrPLCHurtBit = new System.Windows.Forms.Timer(this.components);
            this.btnExporttoExl = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pnlGridData = new System.Windows.Forms.Panel();
            this.pnlSeparator = new System.Windows.Forms.Panel();
            this.pnlBatchOpnSummary = new System.Windows.Forms.Panel();
            this.btnCameraD = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.btnCameraE = new System.Windows.Forms.Button();
            this.lblExportCSV = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lbloneday = new System.Windows.Forms.Label();
            this.lblScanFail = new System.Windows.Forms.Label();
            this.lblScanSuccess = new System.Windows.Forms.Label();
            this.lblonehr = new System.Windows.Forms.Label();
            this.lblonemin = new System.Windows.Forms.Label();
            this.pnlBatchOpn = new System.Windows.Forms.Panel();
            this.tbData = new System.Windows.Forms.TextBox();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchOperation)).BeginInit();
            this.pnlGridData.SuspendLayout();
            this.pnlBatchOpnSummary.SuspendLayout();
            this.pnlBatchOpn.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvBatchOperation
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBatchOperation.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBatchOperation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBatchOperation.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBatchOperation.Location = new System.Drawing.Point(12, 12);
            this.dgvBatchOperation.Name = "dgvBatchOperation";
            this.dgvBatchOperation.Size = new System.Drawing.Size(1172, 455);
            this.dgvBatchOperation.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 4000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtBarCode
            // 
            this.txtBarCode.Location = new System.Drawing.Point(22, 556);
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Size = new System.Drawing.Size(100, 20);
            this.txtBarCode.TabIndex = 8;
            this.txtBarCode.Visible = false;
            // 
            // tmrBit
            // 
            this.tmrBit.Interval = 2000;
            // 
            // tmrPLCHurtBit
            // 
            this.tmrPLCHurtBit.Enabled = true;
            this.tmrPLCHurtBit.Interval = 2500;
            // 
            // btnExporttoExl
            // 
            this.btnExporttoExl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnExporttoExl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnExporttoExl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnExporttoExl.Location = new System.Drawing.Point(1052, 200);
            this.btnExporttoExl.Name = "btnExporttoExl";
            this.btnExporttoExl.Size = new System.Drawing.Size(121, 30);
            this.btnExporttoExl.TabIndex = 16;
            this.btnExporttoExl.Text = "Export to CSV";
            this.btnExporttoExl.UseVisualStyleBackColor = false;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // pnlGridData
            // 
            this.pnlGridData.AutoScroll = true;
            this.pnlGridData.Controls.Add(this.dgvBatchOperation);
            this.pnlGridData.Location = new System.Drawing.Point(2, 283);
            this.pnlGridData.Name = "pnlGridData";
            this.pnlGridData.Size = new System.Drawing.Size(1197, 478);
            this.pnlGridData.TabIndex = 21;
            // 
            // pnlSeparator
            // 
            this.pnlSeparator.BackgroundImage = global::SorterAPI.Properties.Resources.image_separator;
            this.pnlSeparator.Location = new System.Drawing.Point(-1, 269);
            this.pnlSeparator.Name = "pnlSeparator";
            this.pnlSeparator.Size = new System.Drawing.Size(1200, 15);
            this.pnlSeparator.TabIndex = 20;
            // 
            // pnlBatchOpnSummary
            // 
            this.pnlBatchOpnSummary.BackgroundImage = global::SorterAPI.Properties.Resources.batch_operation_summary;
            this.pnlBatchOpnSummary.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlBatchOpnSummary.Controls.Add(this.btnCameraD);
            this.pnlBatchOpnSummary.Controls.Add(this.lblError);
            this.pnlBatchOpnSummary.Controls.Add(this.btnCameraE);
            this.pnlBatchOpnSummary.Controls.Add(this.lblExportCSV);
            this.pnlBatchOpnSummary.Controls.Add(this.lblTotal);
            this.pnlBatchOpnSummary.Controls.Add(this.lbloneday);
            this.pnlBatchOpnSummary.Controls.Add(this.lblScanFail);
            this.pnlBatchOpnSummary.Controls.Add(this.lblScanSuccess);
            this.pnlBatchOpnSummary.Controls.Add(this.lblonehr);
            this.pnlBatchOpnSummary.Controls.Add(this.lblonemin);
            this.pnlBatchOpnSummary.Location = new System.Drawing.Point(2, 83);
            this.pnlBatchOpnSummary.Name = "pnlBatchOpnSummary";
            this.pnlBatchOpnSummary.Size = new System.Drawing.Size(1191, 190);
            this.pnlBatchOpnSummary.TabIndex = 19;
            // 
            // btnCameraD
            // 
            this.btnCameraD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnCameraD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCameraD.ForeColor = System.Drawing.Color.White;
            this.btnCameraD.Location = new System.Drawing.Point(1074, 21);
            this.btnCameraD.Name = "btnCameraD";
            this.btnCameraD.Size = new System.Drawing.Size(91, 38);
            this.btnCameraD.TabIndex = 21;
            this.btnCameraD.Text = "Camera Disabled";
            this.btnCameraD.UseVisualStyleBackColor = false;
            this.btnCameraD.Visible = false;
            this.btnCameraD.Click += new System.EventHandler(this.btnCameraD_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(1047, 150);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(102, 15);
            this.lblError.TabIndex = 11;
            this.lblError.Text = "Annunciation !!";
            this.lblError.Visible = false;
            // 
            // btnCameraE
            // 
            this.btnCameraE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnCameraE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCameraE.ForeColor = System.Drawing.Color.White;
            this.btnCameraE.Location = new System.Drawing.Point(983, 21);
            this.btnCameraE.Name = "btnCameraE";
            this.btnCameraE.Size = new System.Drawing.Size(91, 38);
            this.btnCameraE.TabIndex = 20;
            this.btnCameraE.Text = "Camera Enabled";
            this.btnCameraE.UseVisualStyleBackColor = false;
            this.btnCameraE.Visible = false;
            this.btnCameraE.Click += new System.EventHandler(this.btnCameraE_Click);
            // 
            // lblExportCSV
            // 
            this.lblExportCSV.AutoSize = true;
            this.lblExportCSV.BackColor = System.Drawing.Color.Transparent;
            this.lblExportCSV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExportCSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExportCSV.Location = new System.Drawing.Point(980, 77);
            this.lblExportCSV.Name = "lblExportCSV";
            this.lblExportCSV.Size = new System.Drawing.Size(181, 29);
            this.lblExportCSV.TabIndex = 9;
            this.lblExportCSV.Text = "                            ";
            this.lblExportCSV.Click += new System.EventHandler(this.lblExportCSV_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblTotal.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.Black;
            this.lblTotal.Location = new System.Drawing.Point(183, 25);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(39, 29);
            this.lblTotal.TabIndex = 2;
            this.lblTotal.Text = "00";
            // 
            // lbloneday
            // 
            this.lbloneday.AutoSize = true;
            this.lbloneday.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbloneday.ForeColor = System.Drawing.Color.Black;
            this.lbloneday.Location = new System.Drawing.Point(637, 138);
            this.lbloneday.Name = "lbloneday";
            this.lbloneday.Size = new System.Drawing.Size(39, 29);
            this.lbloneday.TabIndex = 8;
            this.lbloneday.Text = "00";
            // 
            // lblScanFail
            // 
            this.lblScanFail.AutoSize = true;
            this.lblScanFail.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScanFail.ForeColor = System.Drawing.Color.Black;
            this.lblScanFail.Location = new System.Drawing.Point(183, 138);
            this.lblScanFail.Name = "lblScanFail";
            this.lblScanFail.Size = new System.Drawing.Size(39, 29);
            this.lblScanFail.TabIndex = 4;
            this.lblScanFail.Text = "00";
            // 
            // lblScanSuccess
            // 
            this.lblScanSuccess.AutoSize = true;
            this.lblScanSuccess.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScanSuccess.ForeColor = System.Drawing.Color.Black;
            this.lblScanSuccess.Location = new System.Drawing.Point(183, 83);
            this.lblScanSuccess.Name = "lblScanSuccess";
            this.lblScanSuccess.Size = new System.Drawing.Size(39, 29);
            this.lblScanSuccess.TabIndex = 0;
            this.lblScanSuccess.Text = "00";
            // 
            // lblonehr
            // 
            this.lblonehr.AutoSize = true;
            this.lblonehr.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblonehr.ForeColor = System.Drawing.Color.Black;
            this.lblonehr.Location = new System.Drawing.Point(637, 83);
            this.lblonehr.Name = "lblonehr";
            this.lblonehr.Size = new System.Drawing.Size(39, 29);
            this.lblonehr.TabIndex = 6;
            this.lblonehr.Text = "00";
            // 
            // lblonemin
            // 
            this.lblonemin.AutoSize = true;
            this.lblonemin.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblonemin.ForeColor = System.Drawing.Color.Black;
            this.lblonemin.Location = new System.Drawing.Point(637, 25);
            this.lblonemin.Name = "lblonemin";
            this.lblonemin.Size = new System.Drawing.Size(39, 29);
            this.lblonemin.TabIndex = 5;
            this.lblonemin.Text = "00";
            // 
            // pnlBatchOpn
            // 
            this.pnlBatchOpn.BackgroundImage = global::SorterAPI.Properties.Resources.batch_operation;
            this.pnlBatchOpn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlBatchOpn.Controls.Add(this.tbData);
            this.pnlBatchOpn.Controls.Add(this.pnlClose);
            this.pnlBatchOpn.Controls.Add(this.btnStart);
            this.pnlBatchOpn.Controls.Add(this.btnStop);
            this.pnlBatchOpn.Controls.Add(this.button4);
            this.pnlBatchOpn.Controls.Add(this.btnExit);
            this.pnlBatchOpn.Controls.Add(this.btnUpload);
            this.pnlBatchOpn.Location = new System.Drawing.Point(2, 1);
            this.pnlBatchOpn.Name = "pnlBatchOpn";
            this.pnlBatchOpn.Size = new System.Drawing.Size(1196, 81);
            this.pnlBatchOpn.TabIndex = 18;
            // 
            // tbData
            // 
            this.tbData.Location = new System.Drawing.Point(933, 8);
            this.tbData.Multiline = true;
            this.tbData.Name = "tbData";
            this.tbData.Size = new System.Drawing.Size(183, 25);
            this.tbData.TabIndex = 19;
            this.tbData.Visible = false;
            // 
            // pnlClose
            // 
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.Location = new System.Drawing.Point(1155, 6);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(29, 27);
            this.pnlClose.TabIndex = 18;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnStart.Location = new System.Drawing.Point(144, 3);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(119, 30);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "Start Operation";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Visible = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnStop.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnStop.Location = new System.Drawing.Point(144, 39);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(119, 30);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop Operation";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Visible = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.Red;
            this.button4.Location = new System.Drawing.Point(269, 39);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 30);
            this.button4.TabIndex = 6;
            this.button4.Text = "Emergency Stop";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(537, 800);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(86, 30);
            this.btnExit.TabIndex = 17;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnUpload.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnUpload.Location = new System.Drawing.Point(269, 6);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(104, 30);
            this.btnUpload.TabIndex = 11;
            this.btnUpload.Text = "Upload Data";
            this.btnUpload.UseVisualStyleBackColor = false;
            this.btnUpload.Visible = false;
            // 
            // frmBatchOperation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1199, 765);
            this.Controls.Add(this.pnlGridData);
            this.Controls.Add(this.pnlSeparator);
            this.Controls.Add(this.pnlBatchOpnSummary);
            this.Controls.Add(this.pnlBatchOpn);
            this.Controls.Add(this.btnExporttoExl);
            this.Controls.Add(this.txtBarCode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(200, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBatchOperation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Batch Operation";
            this.Load += new System.EventHandler(this.frmBatchOperation_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBatchOperation_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBatchOperation)).EndInit();
            this.pnlGridData.ResumeLayout(false);
            this.pnlBatchOpnSummary.ResumeLayout(false);
            this.pnlBatchOpnSummary.PerformLayout();
            this.pnlBatchOpn.ResumeLayout(false);
            this.pnlBatchOpn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScanSuccess;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblScanFail;
        private System.Windows.Forms.DataGridView dgvBatchOperation;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtBarCode;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Timer tmrBit;
        private System.Windows.Forms.Timer tmrPLCHurtBit;
        private System.Windows.Forms.Button btnExporttoExl;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblonehr;
        private System.Windows.Forms.Label lblonemin;
        private System.Windows.Forms.Label lbloneday;
        private System.Windows.Forms.Panel pnlBatchOpn;
        private System.Windows.Forms.Panel pnlBatchOpnSummary;
        private System.Windows.Forms.Panel pnlSeparator;
        private System.Windows.Forms.Label lblExportCSV;
        private System.Windows.Forms.Panel pnlGridData;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.TextBox tbData;
        private System.Windows.Forms.Button btnCameraE;
        private System.Windows.Forms.Button btnCameraD;
    }
}