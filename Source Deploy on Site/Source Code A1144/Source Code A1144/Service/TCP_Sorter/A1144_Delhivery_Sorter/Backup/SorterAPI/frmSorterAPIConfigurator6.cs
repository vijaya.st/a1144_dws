﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SorterAPI
{
    public partial class frmSorterAPIConfigurator6 : Form
    {
        public frmSorterAPIConfigurator6()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                          Screen.PrimaryScreen.Bounds.Y + 100);

            //string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = System.Convert.ToInt32(screenWidth);
            //int H = System.Convert.ToInt32(screenHeight);

            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);

            //this.TopMost = true;
            //this.StartPosition = FormStartPosition.Manual;
          //  this.MaximumSize = new Size(1200, 1000);
           // this.AutoScroll = true;
            
        }

        private void frmSorterAPIConfigurator6_Load(object sender, EventArgs e)
        {
            //string str_directory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            //panel1.BackgroundImage = Image.FromFile(str_directory + "\\images\\bagassign_title.png");
            //panel2.BackgroundImage = Image.FromFile(str_directory + "\\images\\bagassign_details.png");
            //pnlClose.BackgroundImage = Image.FromFile(str_directory + "\\images\\close1.png"); 
        }
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblCustInfo_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator4 f4 = new frmSorterAPIConfigurator4();
            f4.Show();
            this.Close();
        }

        private void lblPLCData_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator5 f5 = new frmSorterAPIConfigurator5();
            f5.Show();
            this.Close();
        }

        private void lblLinkDB_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator5 f5 = new frmSorterAPIConfigurator5();
            f5.Show();
            this.Close();
        } 

        private void lblPTLSys_Click(object sender, EventArgs e)
        { 
            frmSorterAPIConfigurator3 f3 = new frmSorterAPIConfigurator3();
            f3.Show();
            this.Close();
        }

        private void lblAssignBag_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator6 f6 = new frmSorterAPIConfigurator6();
            f6.Show();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator1 f1 = new frmSorterAPIConfigurator1();
            f1.Show();
            this.Close();
        }

        private void lblBagAssignment_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator6 f6 = new frmSorterAPIConfigurator6();
            f6.Show();
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
 
         
    }
}