﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using System.IO.Ports;

namespace SorterAPI
{
    public partial class frmSorterAPIConfigurator1 : Form
    {
        SerialPortManager _spManager;
        string[] WmyPort;
        string[] ScannerPort;
        public frmSorterAPIConfigurator1()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                          Screen.PrimaryScreen.Bounds.Y + 100);
            //string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = System.Convert.ToInt32(screenWidth);
            //int H = System.Convert.ToInt32(screenHeight);

            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);

            //this.TopMost = true;
            //this.StartPosition = FormStartPosition.Manual;
            //this.MaximumSize = new Size(1200, 1500);            
        }

        private void frmSorterAPIConfigurator1_Load(object sender, EventArgs e)
        {
            // seriel port
            _spManager = new SerialPortManager();
            SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
            serialSettingsBindingSource.DataSource = mySerialSettings;
            portNameComboBox.DataSource = mySerialSettings.PortNameCollection;

            WmyPort = SerialPort.GetPortNames();
            drpWPortName.Items.AddRange(WmyPort);

            ScannerPort = SerialPort.GetPortNames();
            drpScanerPortName.Items.AddRange(ScannerPort);

            getConfiguredData();
        }
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblCustInfo_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator4"];

            if (fc == null)
            {
                frmSorterAPIConfigurator4 f4 = new frmSorterAPIConfigurator4();
                f4.Show();
                this.Close();
            }
        }

        private void lblPLCData_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator2"];

            if (fc == null)
            {
                frmSorterAPIConfigurator2 f2 = new frmSorterAPIConfigurator2();
               // f2.SendToBack();
                f2.Show();
                this.Close();
            }
        }

        private void lblPTLSys_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator3"];

            if (fc == null)
            {
                frmSorterAPIConfigurator3 f3 = new frmSorterAPIConfigurator3();
               // f3.SendToBack();
                f3.Show();
                this.Close();
            }
        }

        private void lblLinkDB_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator5"];

            if (fc == null)
            {
                frmSorterAPIConfigurator5 f5 = new frmSorterAPIConfigurator5();
               // f5.SendToBack();
                f5.Show();
                this.Close();
            }
        }

        private void lblAssignBag_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator5"];

            if (fc == null)
            {
                frmSorterAPIConfigurator6 f6 = new frmSorterAPIConfigurator6();
               // f6.SendToBack();
                f6.Show();
                this.Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator1"];

            if (fc == null)
            {
                frmSorterAPIConfigurator1 f1 = new frmSorterAPIConfigurator1();
               // f1.SendToBack();
                f1.Show();
                this.Close();
            }
        }

        private void pnlClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void getConfiguredData()
        {
            // seriel port
            _spManager = new SerialPortManager();
            SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
            serialSettingsBindingSource.DataSource = mySerialSettings;
            portNameComboBox.DataSource = mySerialSettings.PortNameCollection;

            WmyPort = SerialPort.GetPortNames();
            drpWPortName.Items.AddRange(WmyPort);

            ScannerPort = SerialPort.GetPortNames();
            drpScanerPortName.Items.AddRange(ScannerPort);

            // SerialSettings mySerialW = _spManager.CurrentSerialSettings;
            //drpWPortName.DataSource = mySerialW.WPortNameCollection;
            // baudRateComboBox.DataSource = mySerialSettings.BaudRateCollection;
            //dataBitsComboBox.DataSource = mySerialSettings.DataBitsCollection;
            // parityComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.Parity));
            // stopBitsComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.StopBits));

            //  _spManager.NewSerialDataRecieved += new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved);
     //       this.FormClosing += new FormClosingEventHandler(frmSorterAPIConfigurator_FormClosing);
            //
            DataSet dsPLCDiam = new DataSet();
            DataTable dtSorterInfo = new DataTable();
            DataTable dtDws = new DataTable();

            dsPLCDiam = clsAPI.getPLCDiemensionalData();
            dtSorterInfo = dsPLCDiam.Tables[0];
            dtDws = dsPLCDiam.Tables[1];

            if (dtSorterInfo.Rows.Count > 0 && dtDws.Rows.Count > 0)
            {
                btnSavePLCDiemensionalData.Text = " Update ";
                txtnumberofSorter.Text = dtSorterInfo.Rows[0][1].ToString();
                txtDataLoadSensor.Text = dtSorterInfo.Rows[0][2].ToString();
                if (Convert.ToInt32(dtSorterInfo.Rows[0][3]) == 1)
                {
                 //   rdDWSYes.Checked = true;
                //    rdDWSNo.Checked = false;
                }
                else
                {
                //    rdDWSNo.Checked = true;
                }
                if (Convert.ToInt32(dtSorterInfo.Rows[0][4]) == 1)
                {
                    rdPTLYes.Checked = true;
                    rdPTLNo.Checked = false;
                }
                else
                {
                    rdPTLNo.Checked = true;
                }

                if (Convert.ToInt32(dtSorterInfo.Rows[0][5]) == 1)
                {
                    rdSortsYes.Checked = true;
                    rdSortsNo.Checked = false;
                }
                else
                {
                    rdSortsNo.Checked = true;
                }

                portNameComboBox.Text = dtDws.Rows[0]["PortName"].ToString();
                if (Convert.ToInt32(dtDws.Rows[0]["DataExportType"]) == 1)
                {
                    rdXML.Checked = true;
                }
                else
                {
                    rdCSV.Checked = true;
                }
                txtFolderLocation.Text = dtDws.Rows[0]["FolderLocation"].ToString();
                drpWPortName.Text = dtDws.Rows[0]["WPortname"].ToString();
                txtPLCIP.Text = dtDws.Rows[0]["PLCIP"].ToString();
                txtScannerIP.Text = dtDws.Rows[0]["ScannerIP"].ToString();
                txtPLCPort.Text = dtDws.Rows[0]["PLCPort"].ToString();
                //txtScanPort.Text = dtDws.Rows[0]["ScannerPort"].ToString();

                baudRateComboBox.Text = dtDws.Rows[0]["BaudRate"].ToString();
                drpWBaud.Text = dtDws.Rows[0]["WBaudRate"].ToString();

                drpScanerPortName.Text = dtDws.Rows[0]["ScannerPort"].ToString();

                //string strBranches = "";
                //strBranches = dt.Rows[0][3].ToString();
                //string[] arr = strBranches.Split(',');
                //txtb1.Text = arr[0];
                //txtb2.Text = arr[1];                    
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            clsAPI objd = new clsAPI();
            if (txtDataLoadSensor.Text != "0" || txtnumberofSorter.Text != "0")
            {

               // // objd.StartPointSorter = Convert.ToInt32(txtStartPointSorter.Text);
               // if (rdDWSYes.Checked == true)
                //{
                    objd.IsDWS = 1;

                    if (txtFolderLocation.Text != "" && portNameComboBox.Text != "" && baudRateComboBox.Text != "" && txtPLCIP.Text != "" && txtPLCPort.Text != "" && drpWBaud.Text != "")
                    {

                        objd.FolderLocation = txtFolderLocation.Text;
                        objd.portname = portNameComboBox.Text;
                        objd.baudRate = Convert.ToInt32(baudRateComboBox.Text);
                        objd.WbaudRate = Convert.ToInt32(drpWBaud.Text);

                        objd.PLCIP = txtPLCIP.Text;
                        objd.PLCPort = Convert.ToInt32(txtPLCPort.Text);
                        objd.ScannerIP = txtScannerIP.Text;
                        //objd.ScannerPort = Convert.ToInt32(txtScanPort.Text);
                        //objd.dataBits = Convert.ToInt32(dataBitsComboBox.Text);
                        //objd.parity = parityComboBox.Text;
                        //objd.stopBits = stopBitsComboBox.Text;
                    }
                    else
                    {
                        MessageBox.Show("Enter DWS required data!!");
                        return;
                    }

                    if (rdXML.Checked == true)
                    {
                        objd.DataExportType = 1;
                    }
                    else
                    {
                        objd.DataExportType = 0;
                    }

                    if (portNameComboBox.Text == drpWPortName.Text || portNameComboBox.Text == drpScanerPortName.Text || drpScanerPortName.Text == drpWPortName.Text)
                    {
                        MessageBox.Show("VMS port name, weighing port name and Scanner port name should be different!!");
                        return;
                    }
                    else
                    {
                        objd.WPortName = drpWPortName.Text;

                        objd.ScannerPort = drpScanerPortName.Text;
                    }

                //}
                //else
                //{
                //    objd.IsDWS = 0;
                //}
                if (rdPTLYes.Checked == true)
                {
                    objd.IsPTL = 1;
                }
                else
                {
                    objd.IsPTL = 0;
                }
                if (rdSortsYes.Checked == true)
                {
                    objd.IsSorts = 1;
                    if(rdRobosort.Checked == true)
                    {
                        objd.SorterType = "Robosort";
                    }
                    else{
                        objd.SorterType = "Skat";
                    }
                    
                }
                else
                {
                    objd.IsSorts = 0;
                }

                objd.DataLoadSensor = Convert.ToInt32(txtDataLoadSensor.Text);
                objd.NumberofSorter = Convert.ToInt32(txtnumberofSorter.Text);

                //objd.Branches =txtb1.Text + "," + txtb2.Text + "," + txtb3.Text + "," + txtb4.Text
                //    + "," + txtb5.Text + "," + txtb6.Text + "," + txtb7.Text + "," + txtb8.Text
                //    + "," + txtb9.Text + "," + txtb10.Text + "," + txtb11.Text + "," + txtb12.Text
                //    + "," + txtb13.Text + ","+ txtb14.Text + "," + txtb15.Text + "," + txtb16.Text;

                if (btnSavePLCDiemensionalData.Text == " Update ")
                {
                    int i = clsAPI.UpdatePLCDiemensionalData(objd);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully Update");
                        //  ClearTextBoxes(this.Controls);
                        // this.Load += new EventHandler(frmSorterAPIConfigurator_Load); 

                    }
                }
                else
                {
                    int i = clsAPI.InsertPLCDiemensionalData(objd);
                    if (i > 0)
                    {

                        MessageBox.Show("Data successfully Insert");
                        //  ClearTextBoxes(this.Controls);
                        // this.Load += new EventHandler(frmSorterAPIConfigurator_Load);
                    }
                }
            }
            else
            {
                MessageBox.Show("Enter sorter information");
            } 
        }

        private void lblConfigureSorter_Click(object sender, EventArgs e)
        {
            frmSorterDirection sd = new frmSorterDirection();
            sd.ShowDialog();
            sd.BringToFront();
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            DialogResult result = this.folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFolderLocation.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void rdSortsYes_CheckedChanged(object sender, EventArgs e)
        {
            if (rdSortsYes.Checked == true)
            {
                pnlSorterOption.Visible = true;
            }
        }

        private void rdSortsNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rdSortsNo.Checked == true)
            {
                pnlSorterOption.Visible = false;
            }
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnTestDWS_Click(object sender, EventArgs e)
        {

        }           
    }
}