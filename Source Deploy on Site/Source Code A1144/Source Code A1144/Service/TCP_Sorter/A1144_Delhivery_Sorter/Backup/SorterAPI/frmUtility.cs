﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.Configuration;
using SorterAPI.Common;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;

namespace SorterAPI
{
    public partial class frmUtility : Form
    {
        ErrorLog oErrorLog = new ErrorLog();
        string conGPO = ConfigurationManager.ConnectionStrings["DWSConnectionString"].ConnectionString;
        public frmUtility()
        {
            InitializeComponent();
        }

        private void btnDBBackup_Click(object sender, EventArgs e)
        {
            //DateTime datetime = DateTime.Now;
            //string s = datetime.ToString("dd/MM/yyyy");
            //BackupDatabase(@"D:\bak\GPO_" + s + ".bak");
            saveFileDialog1.ShowDialog();
        }
        public static void TakeDBbackup(string backUpFile)
        {
            try
            {
                //ServerConnection con = new ServerConnection(@"PROPIX-PC\SQLEXPRESS");//(@"PROPIX-PC\SQLEXPRESS");
                //Server server = new Server(con);
                //Backup source = new Backup();
                //source.Action = BackupActionType.Database;
                //source.Database = "GPO";
                //BackupDeviceItem destination = new BackupDeviceItem(backUpFile, DeviceType.File);
                //source.Devices.Add(destination);
                //source.SqlBackup(server);
                //con.Disconnect();
                //MessageBox.Show("The backup of database 'GPO' completed successfully.");
            
             SqlConnection conBak = new SqlConnection(clsDataAccess.GetConnectionString());
             conBak.Open();
             string strB = "Use GPO;";
             string strB1 = "BACKUP DATABASE GPO TO DISK = '" + backUpFile + "' WITH FORMAT,MEDIANAME = 'Z_SQLServerBackup',NAME ='FULL Backup of GPO'";
             SqlCommand cmd1 = new SqlCommand(strB,conBak);
             SqlCommand cmd2 = new SqlCommand(strB1, conBak);
             cmd1.ExecuteNonQuery();
             cmd2.ExecuteNonQuery();
             MessageBox.Show("The backup of database 'GPO' completed successfully.");
             conBak.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string strFilePath = saveFileDialog1.FileName;
            TakeDBbackup(strFilePath + ".bak");
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clsAPI.getEMSINData();

                string sql = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sql = sql + "insert into EMSIN (ARTICLENUMBER, P_WEIGHT,P_LENGTH,P_BREADTH,P_HEIGHT,USERCODE,TDATE) values('"
                          + dt.Rows[i]["Barcode"].ToString().Trim() + "','"
                          + dt.Rows[i]["Weight"].ToString().Trim() + "','"
                          + dt.Rows[i]["Length"].ToString().Trim() + "','"
                          + dt.Rows[i]["Width"].ToString().Trim() + "','"
                          + dt.Rows[i]["Height"].ToString().Trim() + "','DWS',Getdate())";
                }
                string strInsert1 = sql;
              //  string con1 = clsDataAccess.GetConnectionString();
                SqlConnection cn1 = new SqlConnection(conGPO);
                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandType = CommandType.Text;
                cmd1.CommandText = strInsert1;
                cn1.Open();
                cmd1.Connection = cn1;
                cmd1.ExecuteNonQuery();
                cn1.Close();
                MessageBox.Show("Data successfuly transfer in EMSIN!!", "", MessageBoxButtons.OK, MessageBoxIcon.None);

            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDownloadLocationSortre_Click(object sender, EventArgs e)
        {            
           FDLocationBay.ShowDialog();
        }

        private void FDLocationBay_FileOk(object sender, CancelEventArgs e)
        {
            DataTable dtLocationBay = new DataTable();
            dtLocationBay = clsAPI.GetLocatioBay();

            string strpath = FDLocationBay.FileName;
            strpath = strpath + ".csv";

            CreateLocationCSVFile(dtLocationBay, strpath,1);
        }

        public void CreateLocationCSVFile(DataTable dtDataTablesList, string strFilePath, int chk)
        {
            // Create the CSV file to which grid data will be exported.

            StreamWriter sw = new StreamWriter(strFilePath, false);

            //First we will write the headers.

            int iColCount = dtDataTablesList.Columns.Count;

            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dtDataTablesList.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);

            // Now write all the rows.

            foreach (DataRow dr in dtDataTablesList.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();

            if (chk == 1)
            {
                MessageBox.Show("File Exported!!..");
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();

                string strInsert = "select * from  [DWS].[dbo].[EMSOUT] where convert(varchar(10),TDATE,120) >= convert(varchar(10),getdate(),120)"; // '2016-03-21 15:11:16.303'";//([ARTICLENUMBER],[P_WEIGHT],[P_LENGTH]";
                SqlConnection cn = new SqlConnection(conGPO);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = strInsert;
                cn.Open();
                cmd.Connection = cn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cn.Close();

                string sql = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sql = sql + "insert into EMSOUT (ARTICLENUMBER, PINCODE, TDATE) values('"
                          + dt.Rows[i]["ARTICLENUMBER"].ToString().Trim() + "','"
                          + dt.Rows[i]["PINCODE"].ToString().Trim() + "','"
                          + dt.Rows[i]["TDATE"].ToString().Trim() + "')";
                }
                string strInsert1 = sql;
                string con1 = clsDataAccess.GetConnectionString();
                SqlConnection cn1 = new SqlConnection(con1);
                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandType = CommandType.Text;
                cmd1.CommandText = strInsert1;
                cn1.Open();
                cmd1.Connection = cn1;
                cmd1.ExecuteNonQuery();
                cn1.Close();
                MessageBox.Show("Data successfuly import in EMSOUT!!", "", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message);
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void pnlClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBayxlsUpload_Click(object sender, EventArgs e)
        {
            DataTable dtLocationBay = new DataTable();
            dtLocationBay = clsAPI.GetLocatioBay();
            string dat = DateTime.Now.ToString("yyyy-MM-dd");

            string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            string strFile = path + "\\PathFile\\Path.txt";
            string text = System.IO.File.ReadAllText(strFile);
            string strpath = text + dat + ".csv";
            CreateLocationCSVFile(dtLocationBay, strpath, 0);      
    
            openFileDialog1.ShowDialog();
            
        }

        private void SaveFileToDatabase(string filePath)
        {
            try
            {
                //  String strConnection = "Data Source=.\\SQLEXPRESS;AttachDbFilename='C:\\Users\\Hemant\\documents\\visual studio 2010\\Projects\\CRMdata\\CRMdata\\App_Data\\Database1.mdf';Integrated Security=True;User Instance=True";
                String strConnection = clsDataAccess.GetConnectionString();

                string strTruncate = "truncate table Locationaddr";
                string con1 = clsDataAccess.GetConnectionString();
                SqlConnection cn1 = new SqlConnection(strConnection);
                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandType = CommandType.Text;
                cmd1.CommandText = strTruncate;
                cn1.Open();
                cmd1.Connection = cn1;
                cmd1.ExecuteNonQuery();
                cn1.Close();

                String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0\"", filePath);
                //Create Connection to Excel work book 
                using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                {
                    //Create OleDbCommand to fetch data from Excel 
                    using (OleDbCommand cmd = new OleDbCommand("Select [LocationId], [Pincode],[BayNo] from [Sheet1$]", excelConnection))
                    {
                        excelConnection.Open();
                        using (OleDbDataReader dReader = cmd.ExecuteReader())
                        {
                            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection))
                            {
                                //Give your Destination table name 
                                sqlBulk.DestinationTableName = "Locationaddr";
                                sqlBulk.WriteToServer(dReader);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {          
           string strUploadFile = openFileDialog1.FileName;
           SaveFileToDatabase(strUploadFile);
        }

        private void btnImportSortation_Click(object sender, EventArgs e)
        {

        }
    }
}
