﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SorterAPI.Common;

namespace DynamicButton
{
    public partial class frmPTL : Form
    {
        FlowLayoutPanel panelSkat;
        PictureBox pictureSkat; 
        FlowLayoutPanel  panel1, panel2, panel3, panel4, panel5, panel6, panel7, panel8, panel9, panel10, panel11, panel12, panel13, panel14, panel15, panel16, panel17;
        FlowLayoutPanel panel18, panel19, panel20, panel21, panel22, panel23, panel24, panel25, panel26, panel27, panel28, panel29, panel30, panel31, panel36, panel37;
        PictureBox pictureBox1, pictureBox2, pictureBox3, pictureBox4, pictureBox5, pictureBox6, pictureBox7, pictureBox8, pictureBox9, pictureBox10, pictureBox11, pictureBox12, pictureBox13, pictureBox14, pictureBox15;
        PictureBox pictureBox16, pictureBox17, pictureBox18, pictureBox19, pictureBox20, pictureBox21, pictureBox22, pictureBox23, pictureBox24, pictureBox25, pictureBox26, pictureBox27, pictureBox28, pictureBox29, pictureBox30;
        PictureBox pictureBox31, pictureBox36, pictureBox37;
        SqlConnection cn = new SqlConnection();
     //   string con = @"Data Source=PROPX-PC\SQLEXPRESS;Initial Catalog=Gati;Integrated Security=No; UID=sa; Pwd=sql;";
       // string con = @"Data Source=ANL4\SQLEXPRESSK;Initial Catalog=Gati;Integrated Security=Yes;UID=sa; Password=123";

        string con = clsDataAccess.GetConnectionString();

       
        private bool flagImg = true;

        string str;
        private String value1;

        public string value
        {
            get { return value1; }
            set { value1 = value; }
        }

        public frmPTL()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                       Screen.PrimaryScreen.Bounds.Y + 100);
            this.TopMost = true;
            this.StartPosition = FormStartPosition.Manual;
            this.MaximumSize = new Size(1200, 786);

            panelSkat = new FlowLayoutPanel();
            panelSkat.SuspendLayout();
            panelSkat.Size = new  Size(800, 160);
            panelSkat.Location = new Point(14, 300);
            panelSkat.BorderStyle = BorderStyle.None;
            panelSkat.FlowDirection = FlowDirection.RightToLeft;
            panelSkat.AutoScroll = false;
            panelSkat.WrapContents = false;
            this.Controls.Add(panelSkat);  

            panel1 = new FlowLayoutPanel();
            panel1.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel1.Size = new Size(22, 270);
            panel1.Location = new Point(290, 14);
            panel1.BorderStyle = BorderStyle.FixedSingle;
            panel1.FlowDirection = FlowDirection.TopDown;
            panel1.AutoScroll = true; // automatically add scrollbars if needed
            panel1.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel1);

            panel2 = new FlowLayoutPanel();
            panel2.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel2.Size = new Size(22, 270);
            panel2.Location = new Point(335, 14);
            panel2.BorderStyle = BorderStyle.FixedSingle;
            panel2.FlowDirection = FlowDirection.TopDown;
            panel2.AutoScroll = true; // automatically add scrollbars if needed
            panel2.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel2);

            panel3 = new FlowLayoutPanel();
            panel3.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel3.Size = new Size(22, 270);
            panel3.Location = new Point(355, 14);
            panel3.BorderStyle = BorderStyle.FixedSingle;
            panel3.FlowDirection = FlowDirection.TopDown;
            panel3.AutoScroll = true; // automatically add scrollbars if needed
            panel3.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel3);

            panel4 = new FlowLayoutPanel();
            panel4.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel4.Size = new Size(22, 270);
            panel4.Location = new Point(390, 14);
            panel4.BorderStyle = BorderStyle.FixedSingle;
            panel4.FlowDirection = FlowDirection.TopDown;
            panel4.AutoScroll = true; // automatically add scrollbars if needed
            panel4.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel4);

            panel5 = new FlowLayoutPanel();
            panel5.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel5.Size = new Size(22, 270);
            panel5.Location = new Point(410, 14);
            panel5.BorderStyle = BorderStyle.FixedSingle;
            panel5.FlowDirection = FlowDirection.TopDown;
            panel5.AutoScroll = true; // automatically add scrollbars if needed
            panel5.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel5);

            panel6 = new FlowLayoutPanel();
            panel6.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel6.Size = new Size(22, 270);
            panel6.Location = new Point(455, 14);
            panel6.BorderStyle = BorderStyle.FixedSingle;
            panel6.FlowDirection = FlowDirection.TopDown;
            panel6.AutoScroll = true; // automatically add scrollbars if needed
            panel6.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel6);

            panel7 = new FlowLayoutPanel();
            panel7.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel7.Size = new Size(22, 270);
            panel7.Location = new Point(475, 14);
            panel7.BorderStyle = BorderStyle.FixedSingle;
            panel7.FlowDirection = FlowDirection.TopDown;
            panel7.AutoScroll = true; // automatically add scrollbars if needed
            panel7.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel7);

            panel8 = new FlowLayoutPanel();
            panel8.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel8.Size = new Size(22, 270);
            panel8.Location = new Point(520, 14);
            panel8.BorderStyle = BorderStyle.FixedSingle;
            panel8.FlowDirection = FlowDirection.TopDown;
            panel8.AutoScroll = true; // automatically add scrollbars if needed
            panel8.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel8);

            panel9 = new FlowLayoutPanel();
            panel9.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel9.Size = new Size(22, 270);
            panel9.Location = new Point(540, 14);
            panel9.BorderStyle = BorderStyle.FixedSingle;
            panel9.FlowDirection = FlowDirection.TopDown;
            panel9.AutoScroll = true; // automatically add scrollbars if needed
            panel9.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel9);

            panel10 = new FlowLayoutPanel();
            panel10.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel10.Size = new Size(22, 270);
            panel10.Location = new Point(585, 14);
            panel10.BorderStyle = BorderStyle.FixedSingle;
            panel10.FlowDirection = FlowDirection.TopDown;
            panel10.AutoScroll = true; // automatically add scrollbars if needed
            panel10.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel10);

            panel11 = new FlowLayoutPanel();
            panel11.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel11.Size = new Size(22, 270);
            panel11.Location = new Point(605, 14);
            panel11.BorderStyle = BorderStyle.FixedSingle;
            panel11.FlowDirection = FlowDirection.TopDown;
            panel11.AutoScroll = true; // automatically add scrollbars if needed
            panel11.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel11);

            panel12 = new FlowLayoutPanel();
            panel12.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel12.Size = new Size(22, 270);
            panel12.Location = new Point(640, 14);
            panel12.BorderStyle = BorderStyle.FixedSingle;
            panel12.FlowDirection = FlowDirection.TopDown;
            panel12.AutoScroll = true; // automatically add scrollbars if needed
            panel12.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel12);

            panel13 = new FlowLayoutPanel();
            panel13.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel13.Size = new Size(22, 270);
            panel13.Location = new Point(660, 14);
            panel13.BorderStyle = BorderStyle.FixedSingle;
            panel13.FlowDirection = FlowDirection.TopDown;
            panel13.AutoScroll = true; // automatically add scrollbars if needed
            panel13.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel13);

            panel36 = new FlowLayoutPanel();
            panel36.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel36.Size = new Size(22, 270);
            panel36.Location = new Point(705, 14);
            panel36.BorderStyle = BorderStyle.FixedSingle;
            panel36.FlowDirection = FlowDirection.TopDown;
            panel36.AutoScroll = true; // automatically add scrollbars if needed
            panel36.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel36); 
            //====
            panel16 = new FlowLayoutPanel();
            panel16.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel16.Size = new Size(22, 270);
            panel16.Location = new Point(705, 474);
            panel16.BorderStyle = BorderStyle.FixedSingle;
            panel16.FlowDirection = FlowDirection.BottomUp; 
            panel16.AutoScroll = true; // automatically add scrollbars if needed
            panel16.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel16);

            panel17 = new FlowLayoutPanel();
            panel17.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel17.Size = new Size(22, 270);
            panel17.Location = new Point(665, 474);
            panel17.BorderStyle = BorderStyle.FixedSingle;
            panel17.FlowDirection = FlowDirection.BottomUp;
            panel17.AutoScroll = true; // automatically add scrollbars if needed
            panel17.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel17);

            panel18 = new FlowLayoutPanel();
            panel18.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel18.Size = new Size(22, 270);
            panel18.Location = new Point(645, 474);
            panel18.BorderStyle = BorderStyle.FixedSingle;
            panel18.FlowDirection = FlowDirection.BottomUp;
            panel18.AutoScroll = true; // automatically add scrollbars if needed
            panel18.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel18);

            panel19 = new FlowLayoutPanel();
            panel19.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel19.Size = new Size(22, 270);
            panel19.Location = new Point(605, 474);
            panel19.BorderStyle = BorderStyle.FixedSingle;
            panel19.FlowDirection = FlowDirection.BottomUp;
            panel19.AutoScroll = true; // automatically add scrollbars if needed
            panel19.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel19);

            panel20 = new FlowLayoutPanel();
            panel20.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel20.Size = new Size(22, 270);
            panel20.Location = new Point(585, 474);
            panel20.BorderStyle = BorderStyle.FixedSingle;
            panel20.FlowDirection = FlowDirection.BottomUp;
            panel20.AutoScroll = true; // automatically add scrollbars if needed
            panel20.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel20);

            panel21 = new FlowLayoutPanel();
            panel21.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel21.Size = new Size(22, 270);
            panel21.Location = new Point(540, 474);
            panel21.BorderStyle = BorderStyle.FixedSingle;
            panel21.FlowDirection = FlowDirection.BottomUp;
            panel21.AutoScroll = true; // automatically add scrollbars if needed
            panel21.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel21);

            panel22 = new FlowLayoutPanel();
            panel22.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel22.Size = new Size(22, 270);
            panel22.Location = new Point(520, 474);
            panel22.BorderStyle = BorderStyle.FixedSingle;
            panel22.FlowDirection = FlowDirection.BottomUp;
            panel22.AutoScroll = true; // automatically add scrollbars if needed
            panel22.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel22);

            panel23 = new FlowLayoutPanel();
            panel23.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel23.Size = new Size(22, 270);
            panel23.Location = new Point(475, 474);
            panel23.BorderStyle = BorderStyle.FixedSingle;
            panel23.FlowDirection = FlowDirection.BottomUp;
            panel23.AutoScroll = true; // automatically add scrollbars if needed
            panel23.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel23);

            panel24 = new FlowLayoutPanel();
            panel24.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel24.Size = new Size(22, 270);
            panel24.Location = new Point(455, 474);
            panel24.BorderStyle = BorderStyle.FixedSingle;
            panel24.FlowDirection = FlowDirection.BottomUp;
            panel24.AutoScroll = true; // automatically add scrollbars if needed
            panel24.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel24);

            panel25 = new FlowLayoutPanel();
            panel25.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel25.Size = new Size(22, 270);
            panel25.Location = new Point(420, 474);
            panel25.BorderStyle = BorderStyle.FixedSingle;
            panel25.FlowDirection = FlowDirection.BottomUp;
            panel25.AutoScroll = true; // automatically add scrollbars if needed
            panel25.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel25); 

            panel26 = new FlowLayoutPanel();
            panel26.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel26.Size = new Size(22, 270);
            panel26.Location = new Point(400, 474);
            panel26.BorderStyle = BorderStyle.FixedSingle;
            panel26.FlowDirection = FlowDirection.BottomUp;
            panel26.AutoScroll = true; // automatically add scrollbars if needed
            panel26.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel26);

            panel27 = new FlowLayoutPanel();
            panel27.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel27.Size = new Size(22, 270);
            panel27.Location = new Point(355, 474);
            panel27.BorderStyle = BorderStyle.FixedSingle;
            panel27.FlowDirection = FlowDirection.BottomUp;
            panel27.AutoScroll = true; // automatically add scrollbars if needed
            panel27.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel27);


            panel28 = new FlowLayoutPanel();
            panel28.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel28.Size = new Size(22, 270);
            panel28.Location = new Point(335, 474);
            panel28.BorderStyle = BorderStyle.FixedSingle;
            panel28.FlowDirection = FlowDirection.BottomUp;
            panel28.AutoScroll = true; // automatically add scrollbars if needed
            panel28.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel28);

            panel29 = new FlowLayoutPanel();
            panel29.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel29.Size = new Size(22, 270);
            panel29.Location = new Point(290, 474);
            panel29.BorderStyle = BorderStyle.FixedSingle;
            panel29.FlowDirection = FlowDirection.BottomUp;
            panel29.AutoScroll = true; // automatically add scrollbars if needed
            panel29.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel29);

            panel30 = new FlowLayoutPanel();
            panel30.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel30.Size = new Size(22, 270);
            panel30.Location = new Point(270, 474);
            panel30.BorderStyle = BorderStyle.FixedSingle;
            panel30.FlowDirection = FlowDirection.BottomUp;
            panel30.AutoScroll = true; // automatically add scrollbars if needed
            panel30.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel30);

            panel31 = new FlowLayoutPanel();
            panel31.SuspendLayout(); // don't calculate the layout before all picture boxes are added
            panel31.Size = new Size(22, 270);
            panel31.Location = new Point(225, 474);
            panel31.BorderStyle = BorderStyle.FixedSingle;
            panel31.FlowDirection = FlowDirection.BottomUp;
            panel31.AutoScroll = true; // automatically add scrollbars if needed
            panel31.WrapContents = false; // all picture boxes in a single row
            this.Controls.Add(panel31);  

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureSkat = new PictureBox();
            pictureSkat.Size = new Size(800, 170);
            pictureSkat.ImageLocation = Application.StartupPath + "\\skat.jpg";
            pictureSkat.BorderStyle = BorderStyle.None;
            panelSkat.Controls.Add(pictureSkat);

            for (int i = 12; i >= 1; i--)
            {
                int x = i;
                x = 24 - i + 1;
                #region PicBox15_Left
                pictureBox1 = new PictureBox();
                pictureBox1.Size = new Size(14,16);
                pictureBox1.Name = "SorterNo15_" + x.ToString();
                pictureBox1.BorderStyle = BorderStyle.FixedSingle;
                pictureBox1.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox1.MouseLeave += control_MouseLeave;
                pictureBox1.MouseEnter += control_MouseEnter;
                pictureBox1.MouseHover += control_MouseHover;
                pictureBox1.Click += control_Click;
                string sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo =15 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                SqlCommand cmd = new SqlCommand(sql, cn);
                SqlDataReader rd = cmd.ExecuteReader();
                //MessageBox.Show(rd.RecordsAffected.ToString());
                while (rd.Read())
                {
                        if (rd["CassetteAddr"].ToString() != "")
                        {
                            string pic1 = pictureBox1.Name.ToString().ToUpper();
                            if (pic1 == rd["CassetteAddr"].ToString())
                            {
                                pictureBox1.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                                flagImg = false;
                            }
                        }
                        else
                        {

                        }
                    }  
              
                cn.Close(); 
                panel1.Controls.Add(pictureBox1);
                #endregion

                #region PicBox15_Right
               
                pictureBox2 = new PictureBox();
                pictureBox2.Size = new Size(14, 16);
                pictureBox2.Name = "SorterNo15_" + i.ToString();
                pictureBox2.BorderStyle = BorderStyle.FixedSingle;
                pictureBox2.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox2.MouseLeave += control_MouseLeave;
                pictureBox2.MouseEnter += control_MouseEnter;
                pictureBox2.Click += control_Click;

                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo =15 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open(); 
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic2 = pictureBox2.Name.ToString().ToUpper(); 
                        if (pic2 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox2.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                } 
                cn.Close();  
                panel2.Controls.Add(pictureBox2);
                #endregion

                #region PicBox14_Left
                pictureBox3 = new PictureBox();
                pictureBox3.Size = new Size(14, 16);
                pictureBox3.Name = "SorterNo14_" + x.ToString();
                pictureBox3.BorderStyle = BorderStyle.FixedSingle;
                pictureBox3.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox3.MouseLeave += control_MouseLeave;
                pictureBox3.MouseEnter += control_MouseEnter;
                pictureBox3.Click += control_Click;

                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 14 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic3 = pictureBox3.Name.ToString().ToUpper();
                        if (pic3 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox3.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                } 
                cn.Close();  
                panel3.Controls.Add(pictureBox3);
                #endregion PicBox3

                #region PicBox14_Right
                pictureBox4 = new PictureBox();
                pictureBox4.Size = new Size(14, 16);
                pictureBox4.Name = "SorterNo14_" + i.ToString();
                pictureBox4.BorderStyle = BorderStyle.FixedSingle;
                pictureBox4.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox4.MouseLeave += control_MouseLeave;
                pictureBox4.MouseEnter += control_MouseEnter;
                pictureBox4.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 14 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                { 
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic4 = pictureBox4.Name.ToString().ToUpper();
                        if (pic4 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox4.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                } 
                cn.Close();  
                panel4.Controls.Add(pictureBox4);
                #endregion PicBox1

                #region PicBox13_Left
                pictureBox5 = new PictureBox();
                pictureBox5.Size = new Size(14, 16);
                pictureBox5.Name = "SorterNo13_" +x.ToString();
                pictureBox5.BorderStyle = BorderStyle.FixedSingle;
                pictureBox5.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox5.MouseLeave += control_MouseLeave;
                pictureBox5.MouseEnter += control_MouseEnter;
                pictureBox5.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 13 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic5 = pictureBox5.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic5 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox5.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                } 
                cn.Close();  
                panel5.Controls.Add(pictureBox5);
                #endregion PicBox5

                #region PicBox13_Right
                pictureBox6 = new PictureBox();
                pictureBox6.Size = new Size(14, 16);
                pictureBox6.Name = "SorterNo13_" + i.ToString();
                pictureBox6.BorderStyle = BorderStyle.FixedSingle;
                pictureBox6.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox6.MouseLeave += control_MouseLeave;
                pictureBox6.MouseEnter += control_MouseEnter;
                pictureBox6.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 13 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic6 = pictureBox6.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic6 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox6.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel6.Controls.Add(pictureBox6);
                #endregion PicBox6

                #region PicBox12_Left
                pictureBox7 = new PictureBox();
                pictureBox7.Size = new Size(14, 16);
                pictureBox7.Name = "SorterNo12_" + x.ToString();
                pictureBox7.BorderStyle = BorderStyle.FixedSingle;
                pictureBox7.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox7.MouseLeave += control_MouseLeave;
                pictureBox7.MouseEnter += control_MouseEnter;
                pictureBox7.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 12 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic7 = pictureBox7.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic7 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox7.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel7.Controls.Add(pictureBox7);
                #endregion PicBox7

                #region PicBox12_Right
                pictureBox8 = new PictureBox();
                pictureBox8.Size = new Size(14, 16);
                pictureBox8.Name = "SorterNo12_" + i.ToString();
                pictureBox8.BorderStyle = BorderStyle.FixedSingle;
                pictureBox8.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox8.MouseLeave += control_MouseLeave;
                pictureBox8.MouseEnter += control_MouseEnter;
                pictureBox8.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 12 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic8 = pictureBox8.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic8 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox8.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel8.Controls.Add(pictureBox8);
                #endregion PicBox8

                #region PicBox11_Left
                pictureBox9 = new PictureBox();
                pictureBox9.Size = new Size(14, 16);
                pictureBox9.Name = "SorterNo11_" + x.ToString();
                pictureBox9.BorderStyle = BorderStyle.FixedSingle;
                pictureBox9.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox9.MouseLeave += control_MouseLeave;
                pictureBox9.MouseEnter += control_MouseEnter;
                pictureBox9.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 11 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic9 = pictureBox9.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic9 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox9.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel9.Controls.Add(pictureBox9);
                #endregion PicBox9

                #region PicBox11_Right
                pictureBox10 = new PictureBox();
                pictureBox10.Size = new Size(14, 16);
                pictureBox10.Name = "SorterNo11_" + i.ToString();
                pictureBox10.BorderStyle = BorderStyle.FixedSingle;
                pictureBox10.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox10.MouseLeave += control_MouseLeave;
                pictureBox10.MouseEnter += control_MouseEnter;
                pictureBox10.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 11 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic10 = pictureBox10.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic10 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox10.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel10.Controls.Add(pictureBox10);
                #endregion PicBox10

                #region PicBox10_Left
                pictureBox11 = new PictureBox();
                pictureBox11.Size = new Size(14, 16);
                pictureBox11.Name = "SorterNo10_" + x.ToString();
                pictureBox11.BorderStyle = BorderStyle.FixedSingle;
                pictureBox11.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox11.MouseLeave += control_MouseLeave;
                pictureBox11.MouseEnter += control_MouseEnter;
                pictureBox11.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 10 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic11 = pictureBox11.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic11 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox11.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel11.Controls.Add(pictureBox11);
                #endregion PicBox11

                #region PicBox10_Right
                pictureBox12 = new PictureBox();
                pictureBox12.Size = new Size(14, 16);
                pictureBox12.Name = "SorterNo10_" + i.ToString();
                pictureBox12.BorderStyle = BorderStyle.FixedSingle;
                pictureBox12.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox12.MouseLeave += control_MouseLeave;
                pictureBox12.MouseEnter += control_MouseEnter;
                pictureBox12.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 10 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic12 = pictureBox12.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic12 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox12.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel12.Controls.Add(pictureBox12);
                #endregion PicBox12

                #region PicBox9_Left
                pictureBox13 = new PictureBox();
                pictureBox13.Size = new Size(14, 16);
                pictureBox13.Name = "SorterNo9_" + x.ToString();
                pictureBox13.BorderStyle = BorderStyle.FixedSingle;
                pictureBox13.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox13.MouseLeave += control_MouseLeave;
                pictureBox13.MouseEnter += control_MouseEnter;
                pictureBox13.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 9 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic13 = pictureBox13.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic13 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox13.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel13.Controls.Add(pictureBox13);
                #endregion PicBox13

                #region PicBox36(9)_Right
                pictureBox36 = new PictureBox();
                pictureBox36.Size = new Size(14, 16);
                pictureBox36.Name = "SorterNo9_" + i.ToString();
                pictureBox36.BorderStyle = BorderStyle.FixedSingle;
                pictureBox36.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox36.MouseLeave += control_MouseLeave;
                pictureBox36.MouseEnter += control_MouseEnter;
                pictureBox36.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 9 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic36 = pictureBox36.Name.ToString().ToUpper();
                        //Debug.Print("Pic1 : " + pic1 + "\n");
                        if (pic36 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox36.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel36.Controls.Add(pictureBox36);
                #endregion PicBox36

                #region PicBox8_Left
                pictureBox16 = new PictureBox();
                pictureBox16.Size = new Size(14, 16);
                pictureBox16.Name = "SorterNo8_" + x.ToString();
                pictureBox16.BorderStyle = BorderStyle.FixedSingle;
                pictureBox16.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox16.MouseLeave += control_MouseLeave;
                pictureBox16.MouseEnter += control_MouseEnter;
                pictureBox16.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 8 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic16 = pictureBox16.Name.ToString().ToUpper(); 
                        if (pic16 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox16.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel16.Controls.Add(pictureBox16);
                #endregion PicBox16

                #region PicBox8_Right
                pictureBox17 = new PictureBox();
                pictureBox17.Size = new Size(14, 16);
                pictureBox17.Name = "SorterNo8_" + i.ToString();
                pictureBox17.BorderStyle = BorderStyle.FixedSingle;
                pictureBox17.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox17.MouseLeave += control_MouseLeave;
                pictureBox17.MouseEnter += control_MouseEnter;
                pictureBox17.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 8 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic17 = pictureBox17.Name.ToString().ToUpper();
                        if (pic17 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox17.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel17.Controls.Add(pictureBox17);
                #endregion PicBox17

                #region PicBox7_Left
                pictureBox18 = new PictureBox();
                pictureBox18.Size = new Size(14, 16);
                pictureBox18.Name = "SorterNo7_" + x.ToString();
                pictureBox18.BorderStyle = BorderStyle.FixedSingle;
                pictureBox18.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox18.MouseLeave += control_MouseLeave;
                pictureBox18.MouseEnter += control_MouseEnter;
                pictureBox18.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 7 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic18 = pictureBox18.Name.ToString().ToUpper();
                        if (pic18 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox18.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel18.Controls.Add(pictureBox18);
                #endregion PicBox18

                #region PicBox7_Right
                pictureBox19 = new PictureBox();
                pictureBox19.Size = new Size(14, 16);
                pictureBox19.Name = "SorterNo7_" + i.ToString();
                pictureBox19.BorderStyle = BorderStyle.FixedSingle;
                pictureBox19.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox19.MouseLeave += control_MouseLeave;
                pictureBox19.MouseEnter += control_MouseEnter;
                pictureBox19.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 7 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic19 = pictureBox19.Name.ToString().ToUpper();
                        if (pic19 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox19.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel19.Controls.Add(pictureBox19);
                #endregion PicBox19

                #region PicBox6_Left
                pictureBox20 = new PictureBox();
                pictureBox20.Size = new Size(14, 16);
                pictureBox20.Name = "SorterNo6_" + x.ToString();
                pictureBox20.BorderStyle = BorderStyle.FixedSingle;
                pictureBox20.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox20.MouseLeave += control_MouseLeave;
                pictureBox20.MouseEnter += control_MouseEnter;
                pictureBox20.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 6 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic20 = pictureBox20.Name.ToString().ToUpper();
                        if (pic20 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox20.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel20.Controls.Add(pictureBox20);
                #endregion PicBox20

                #region PicBox6_Right
                pictureBox21 = new PictureBox();
                pictureBox21.Size = new Size(14, 16);
                pictureBox21.Name = "SorterNo6_" + i.ToString();
                pictureBox21.BorderStyle = BorderStyle.FixedSingle;
                pictureBox21.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox21.MouseLeave += control_MouseLeave;
                pictureBox21.MouseEnter += control_MouseEnter;
                pictureBox21.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 6 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic21 = pictureBox21.Name.ToString().ToUpper();
                        if (pic21 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox21.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel21.Controls.Add(pictureBox21);
                #endregion PicBox21

                #region PicBox5_Left
                pictureBox22 = new PictureBox();
                pictureBox22.Size = new Size(14, 16);
                pictureBox22.Name = "SorterNo5_" + x.ToString();
                pictureBox22.BorderStyle = BorderStyle.FixedSingle;
                pictureBox22.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox22.MouseLeave += control_MouseLeave;
                pictureBox22.MouseEnter += control_MouseEnter;
                pictureBox22.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 5 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic22 = pictureBox22.Name.ToString().ToUpper();
                        if (pic22 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox22.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel22.Controls.Add(pictureBox22);
                #endregion PicBox22 

                #region PicBox5_Right
                pictureBox23 = new PictureBox();
                pictureBox23.Size = new Size(14, 16);
                pictureBox23.Name = "SorterNo5_" + i.ToString();
                pictureBox23.BorderStyle = BorderStyle.FixedSingle;
                pictureBox23.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox23.MouseLeave += control_MouseLeave;
                pictureBox23.MouseEnter += control_MouseEnter;
                pictureBox23.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 5 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic23 = pictureBox23.Name.ToString().ToUpper();
                        if (pic23 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox23.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel23.Controls.Add(pictureBox23);
                #endregion PicBox23 

                #region PicBox4_Left
                pictureBox24 = new PictureBox();
                pictureBox24.Size = new Size(14, 16);
                pictureBox24.Name = "SorterNo4_" + x.ToString();
                pictureBox24.BorderStyle = BorderStyle.FixedSingle;
                pictureBox24.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox24.MouseLeave += control_MouseLeave;
                pictureBox24.MouseEnter += control_MouseEnter;
                pictureBox24.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 4 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic24 = pictureBox24.Name.ToString().ToUpper();
                        if (pic24 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox24.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel24.Controls.Add(pictureBox24);
                #endregion PicBox24

                #region PicBox4_Right
                pictureBox25 = new PictureBox();
                pictureBox25.Size = new Size(14, 16);
                pictureBox25.Name = "SorterNo4_" + i.ToString();
                pictureBox25.BorderStyle = BorderStyle.FixedSingle;
                pictureBox25.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox25.MouseLeave += control_MouseLeave;
                pictureBox25.MouseEnter += control_MouseEnter;
                pictureBox25.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 4 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic25 = pictureBox25.Name.ToString().ToUpper();
                        if (pic25 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox25.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel25.Controls.Add(pictureBox25);
                #endregion PicBox25 

                #region PicBox3_Left
                pictureBox26 = new PictureBox();
                pictureBox26.Size = new Size(14, 16);
                pictureBox26.Name = "SorterNo3_" + x.ToString();
                pictureBox26.BorderStyle = BorderStyle.FixedSingle;
                pictureBox26.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox26.MouseLeave += control_MouseLeave;
                pictureBox26.MouseEnter += control_MouseEnter;
                pictureBox26.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 3 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    Debug.Print(rd["CassetteAddr"].ToString());
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic26 = pictureBox26.Name.ToString().ToUpper();
                        if (pic26 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox26.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel26.Controls.Add(pictureBox26);
                #endregion PicBox26 

                #region PicBox3_Right
                pictureBox27 = new PictureBox();
                pictureBox27.Size = new Size(14, 16);
                pictureBox27.Name = "SorterNo3_" + i.ToString();
                pictureBox27.BorderStyle = BorderStyle.FixedSingle;
                pictureBox27.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox27.MouseLeave += control_MouseLeave;
                pictureBox27.MouseEnter += control_MouseEnter;
                pictureBox27.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 3 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic27 = pictureBox27.Name.ToString().ToUpper();
                        if (pic27 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox27.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel27.Controls.Add(pictureBox27);
                #endregion PicBox27 

                #region PicBox2_Left
                pictureBox28 = new PictureBox();
                pictureBox28.Size = new Size(14, 16);
                pictureBox28.Name = "SorterNo2_" + x.ToString();
                pictureBox28.BorderStyle = BorderStyle.FixedSingle;
                pictureBox28.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox28.MouseLeave += control_MouseLeave;
                pictureBox28.MouseEnter += control_MouseEnter;
                pictureBox28.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 2 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic28 = pictureBox28.Name.ToString().ToUpper();
                        if (pic28 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox28.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel28.Controls.Add(pictureBox28);
                #endregion PicBox28

                #region PicBox2_Right
                pictureBox29 = new PictureBox();
                pictureBox29.Size = new Size(14, 16);
                pictureBox29.Name = "SorterNo2_" + i.ToString();
                pictureBox29.BorderStyle = BorderStyle.FixedSingle;
                pictureBox29.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox29.MouseLeave += control_MouseLeave;
                pictureBox29.MouseEnter += control_MouseEnter;
                pictureBox29.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 2 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic29 = pictureBox29.Name.ToString().ToUpper();
                        if (pic29 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox29.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel29.Controls.Add(pictureBox29);
                #endregion PicBox29

                #region PicBox1_Left
                pictureBox30 = new PictureBox();
                pictureBox30.Size = new Size(14, 16);
                pictureBox30.Name = "SorterNo1_" + x.ToString();
                pictureBox30.BorderStyle = BorderStyle.FixedSingle;
                pictureBox30.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox30.MouseLeave += control_MouseLeave;
                pictureBox30.MouseEnter += control_MouseEnter;
                pictureBox30.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 1 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic30 = pictureBox30.Name.ToString().ToUpper();
                        if (pic30 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox30.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel30.Controls.Add(pictureBox30);
                #endregion PicBox30

                #region PicBox1_Right
                pictureBox31 = new PictureBox();
                pictureBox31.Size = new Size(14, 16);
                pictureBox31.Name = "SorterNo1_" + i.ToString();
                pictureBox31.BorderStyle = BorderStyle.FixedSingle;
                pictureBox31.ImageLocation = Application.StartupPath + "\\bag.jpg";
                pictureBox31.MouseLeave += control_MouseLeave;
                pictureBox31.MouseEnter += control_MouseEnter;
                pictureBox31.Click += control_Click;
                sql = "";
                sql = "Select distinct CassetteAddr from LocationAddr where SorterNo = 1 and  CassetteAddr  IS NOT NULL";
                cn.ConnectionString = con;
                cn.Open();
                cmd = new SqlCommand(sql, cn);
                rd = cmd.ExecuteReader(); ;
                while (rd.Read())
                {
                    if (rd["CassetteAddr"].ToString() != "")
                    {
                        string pic31 = pictureBox31.Name.ToString().ToUpper();
                        if (pic31 == rd["CassetteAddr"].ToString())
                        {
                            pictureBox31.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                            flagImg = false;
                        }
                    }
                    else
                    {

                    }
                }
                cn.Close();
                panel31.Controls.Add(pictureBox31);
                #endregion PicBox31

            }

            panel1.ResumeLayout();
            panel2.ResumeLayout();
            panel3.ResumeLayout();
            panel4.ResumeLayout();
            panel5.ResumeLayout();
            panel6.ResumeLayout();
            panel7.ResumeLayout();
            panel8.ResumeLayout();
            panel9.ResumeLayout();
            panel10.ResumeLayout();
            panel11.ResumeLayout();
            panel12.ResumeLayout();
            panel13.ResumeLayout();
          //  panel14.ResumeLayout();
            //panel15.ResumeLayout();
            panel36.ResumeLayout();
            panel16.ResumeLayout();
            panel17.ResumeLayout();
            panel18.ResumeLayout();
            panel19.ResumeLayout();
            panel20.ResumeLayout();
            panel21.ResumeLayout();
            panel22.ResumeLayout();
            panel23.ResumeLayout();
            panel24.ResumeLayout();
            panel25.ResumeLayout();
            panel26.ResumeLayout();
            panel27.ResumeLayout();
            panel28.ResumeLayout();
            panel29.ResumeLayout();
            panel30.ResumeLayout();
            panel31.ResumeLayout();
            panelSkat.ResumeLayout();
        }
        void control_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            int rowCount = 0;
            string sql = "Select Ou_Code, CassetteAddr from LocationAddr where CassetteAddr  ='" + pb.Name.ToUpper() + "' and BagType is not null";
            SqlConnection cnpb = new SqlConnection(con);
            SqlDataReader rdpb = null;
            try
            {
                cnpb.Open();    
                SqlCommand cmdpb = new SqlCommand(sql, cnpb);
                rdpb = cmdpb.ExecuteReader(); ;
                while (rdpb.Read())
                {
                    if (rdpb["CassetteAddr"].ToString() != "")
                    {
                        rowCount++;
                        pb.ImageLocation = Application.StartupPath + "\\bagGreen.jpg";
                        ToolTip tt = new ToolTip();
                        string conCat = rdpb["Ou_Code"].ToString();
                        tt.UseFading = true;
                        tt.UseAnimation = true;
                        tt.IsBalloon = true;
                        tt.ShowAlways = true;
                        tt.AutoPopDelay = 1000;
                        tt.InitialDelay = 1000;
                        tt.ReshowDelay = 500;
                        tt.SetToolTip(pb, conCat);
                    }
                    else
                    {
                        pb.ImageLocation = Application.StartupPath + "\\bag.jpg";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); 
            }
            finally
            {
                ///Debug.Print(rowCount.ToString());
                if (rowCount < 1)
                {
                    pb.ImageLocation = Application.StartupPath + "\\bag.jpg";
                }
                rdpb.Close();
                cnpb.Close();
            }
        }
        void control_MouseHover(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if (pb != null)
                pb.ImageLocation = Application.StartupPath + "\\bag.jpg";
        }
        void control_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if (pb != null)
                pb.ImageLocation = Application.StartupPath + "\\bagYellow.jpg";
                System.Threading.Thread.Sleep(100);
                //pb.ImageLocation = Application.StartupPath + "\\bag.jpg";
        }

        void control_Click(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox) sender;
            frmBags fb = new frmBags(pb.Name);
            fb.ShowDialog(this);
        }  
    }
}
