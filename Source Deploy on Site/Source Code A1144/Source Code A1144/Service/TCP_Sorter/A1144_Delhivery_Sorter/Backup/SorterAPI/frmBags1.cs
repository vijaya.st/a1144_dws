﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using SorterAPI.Common;

namespace GatiProject
{
    public partial class frmBags1 : Form
    {
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd, _cmd;
        DataSet dsPLCDiam = new DataSet();
        DataSet dstResults = new DataSet();
        DataTable dtSorterInfo = new DataTable();
        DataView myView;// = new DataView();
        ErrorLog oErrorLog = new ErrorLog();
        DataTable dtbin = new DataTable();
        string str_directory = "";
       //  string con = @"Data Source=PROPX-PC\SQLEXPRESS;Initial Catalog=Gati;Integrated Security=No; UID=sa; Pwd=sql;";
       // string con = @"Data Source=ANL4\SQLEXPRESSK;Initial Catalog=Gati;Integrated Security=Yes;UID=sa; Password=123";
        string con = clsDataAccess.GetConnectionString();
        string sql = "";

        public frmBags1()
        {
            InitializeComponent();
        }
         private void frmBags1_Load(object sender, EventArgs e)
        {
            BindMasterOUCodes();

            dsPLCDiam = clsAPI.getPLCDiemensionalData();
            dtSorterInfo = dsPLCDiam.Tables[0];

            int count = Convert.ToInt32(dtSorterInfo.Rows[0][1]);
            drpBranch.Items.Clear();
            for (int i = 1; i <= count; i++)
            {
                drpBranch.Items.Add(i);
            }
            drpBag.Items.Clear();
            for (int i = 1; i <= 24; i++)
            {
                drpBag.Items.Add(i);
            }
        }

        private void rdD_CheckedChanged(object sender, EventArgs e)
        {
            if (rdD.Checked == true)
            {
                grpSingle.Visible = true;
                grpMix.Visible = false;
               // grpOURemove.Visible = false;
                btnSave.Visible = true;
                btnCancel.Visible = true;

                txtOuRemove.Visible = false;
                btnOuRemove.Visible = false;
                lblRemoveOU.Visible = false;
            }
        }

        private void rdI_CheckedChanged(object sender, EventArgs e)
        {
            if (rdI.Checked == true)
            {
                grpMix.Visible = true;
                grpSingle.Visible = false;
              //  grpOURemove.Visible = false;
                btnSave.Visible = true;
                btnCancel.Visible = true;

                txtOuRemove.Visible = false;
                btnOuRemove.Visible = false;

                cmdMstOUCODE.Visible = true;
                txtOUCodeI.Visible = true;
                lblouMix.Visible = true;
                lbloutxt.Visible = true;
                lblRemoveOU.Visible = false;
            }
        }

        private void rdRemove_CheckedChanged(object sender, EventArgs e)
        {
            if (rdRemove.Checked == true)
            {
               // grpOURemove.Visible = true;
                grpSingle.Visible = false;
                grpMix.Visible = true;
                btnSave.Visible = false;
                btnCancel.Visible = false;

                txtOuRemove.Visible = true;
                btnOuRemove.Visible = true;

                cmdMstOUCODE.Visible = false;
                txtOUCodeI.Visible = false;
                lblouMix.Visible = false;
                lbloutxt.Visible = false;
                lblRemoveOU.Visible = true;
            }
        }  

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (rdD.Checked == true)
            {
                if (txtOUCodeD.Text != "" && drpBranch.Text != "" & drpBag.Text != "")
                {
                    clsAPI obj1 = new clsAPI();
                    DataTable dtch = new DataTable();
                    obj1.OuCode = txtOUCodeD.Text.ToString();
                    obj1.SorterNo = Convert.ToInt32(drpBranch.Text);
                    obj1.Binno = Convert.ToInt32(drpBag.Text);

                    dtch = clsAPI.CheckOu(obj1);

                    if (dtch.Rows.Count > 0)
                    {
                        string chk = dtch.Rows[0]["chk"].ToString();
                        if (chk == "0")
                        {
                            string srtSoter = dtch.Rows[0]["SorterNo"].ToString();
                            string srtBinNo = dtch.Rows[0]["Binno"].ToString();
                            if (srtSoter == "" && srtBinNo == "")
                            {
                                clsAPI obj = new clsAPI();
                               
                                obj.BagType = "D";
                                obj.OuCode = txtOUCodeD.Text.ToString();
                                obj.IsMst = 1;
                                obj.MstOUCode = null;                               
                                obj.SorterNo = Convert.ToInt32(drpBranch.Text);
                                obj.Binno = Convert.ToInt32(drpBag.Text);

                                int k = clsAPI.InsertOuCode(obj);

                                if (k > 0)
                                {
                                    MessageBox.Show("Record successfully Saved!!");
                                    txtOUCodeD.Text = "";
                                    txtOUCodeI.Text = "";
                                    drpBranch.SelectedIndex = -1;
                                    drpBag.SelectedIndex = -1;
                                    drpBranch.Text = "";
                                    drpBag.Text = "";
                                }
                            }
                            else
                            {
                                DialogResult ch = MessageBox.Show("This Ou Code already existing to Gate no. " + srtSoter + " and Bag no. " + srtBinNo + ", do you want update it?? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (ch == DialogResult.Yes)
                                {
                                    clsAPI obj = new clsAPI();
                                    
                                    obj.BagType = "D";
                                    obj.OuCode = txtOUCodeD.Text.ToString();
                                    obj.IsMst = 1;
                                    obj.MstOUCode = null;
                                    
                                    obj.SorterNo = Convert.ToInt32(drpBranch.Text);
                                    obj.Binno = Convert.ToInt32(drpBag.Text);

                                    int k = clsAPI.InsertOuCode(obj);
                                    if (k > 0)
                                    {
                                        MessageBox.Show("Record successfully Saved!!");
                                        txtOUCodeD.Text = "";
                                        txtOUCodeI.Text = "";
                                        drpBranch.SelectedIndex = -1;
                                        drpBag.SelectedIndex = -1;
                                        drpBranch.Text = "";
                                        drpBag.Text = "";
                                    }
                                }
                            }
                           
                        }
                        else
                        {
                            //string srtSoter = dtch.Rows[0]["SorterNo"].ToString();
                            string srtBinNo = dtch.Rows[0]["Binno"].ToString();
                            MessageBox.Show("Selected Bag no. " + srtBinNo + ", Already assigned to " + dtch.Rows[0]["OUCode"].ToString() + " OU, Please remove existing OU from this location and assign new OU !!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        clsAPI obj = new clsAPI();
                       
                            obj.BagType = "D";
                            obj.OuCode = txtOUCodeD.Text.ToString();
                            obj.IsMst = 1;
                            obj.MstOUCode = null; 
                        obj.SorterNo = Convert.ToInt32(drpBranch.Text);
                        obj.Binno = Convert.ToInt32(drpBag.Text);

                        int k = clsAPI.InsertOuCode(obj);

                        if (k > 0)
                        {
                            MessageBox.Show("Record successfully Saved!!");
                            txtOUCodeD.Text = "";
                            txtOUCodeI.Text = "";
                            drpBranch.SelectedIndex = -1;
                            drpBag.SelectedIndex = -1;
                            drpBranch.Text = "";
                            drpBag.Text = "";                           
                        }
                    }
                }
                else
                {
                    MessageBox.Show("All values are mandatory!!");
                }

            }
           else if (rdI.Checked == true)
            {
                if (txtOUCodeI.Text != "" && cmdMstOUCODE.Text != "")
                {
                    clsAPI obj1 = new clsAPI();
                    DataTable dtch = new DataTable();
                    obj1.OuCode = txtOUCodeI.Text.ToString();
                  // obj1.SorterNo = Convert.ToInt32(drpBranch.Text);
                  // obj1.Binno = Convert.ToInt32(drpBag.Text);

                    dtch = clsAPI.CheckOu(obj1);
                    if (dtch.Rows.Count > 0)
                    {
                        string srtSoter = dtch.Rows[0]["SorterNo"].ToString();
                        string srtBinNo = dtch.Rows[0]["Binno"].ToString();
                         DialogResult ch = MessageBox.Show("This Ou Code already existing to Gate no. " + srtSoter + " and Bag no. " + srtBinNo + ", do you want update it?? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                         if (ch == DialogResult.Yes)
                         {
                             clsAPI obj = new clsAPI();
                             obj.BagType = null;
                             obj.OuCode = txtOUCodeI.Text.ToString();
                             obj.IsMst = 0;
                             obj.MstOUCode = cmdMstOUCODE.Text;
                             obj.MstOUCodeId = Convert.ToInt32(cmdMstOUCODE.SelectedValue);

                             int k = clsAPI.InsertOuCode(obj);

                             if (k > 0)
                             {
                                 MessageBox.Show("Record successfully Saved!!");
                                 txtOUCodeD.Text = "";
                                 txtOUCodeI.Text = "";
                                 drpBranch.SelectedIndex = -1;
                                 drpBag.SelectedIndex = -1;
                                 cmdMstOUCODE.SelectedIndex = -1;
                                 drpBranch.Text = "";
                                 drpBag.Text = "";
                             }
                         }
                    }
                    else
                    {
                        clsAPI obj = new clsAPI();
                        obj.BagType = null;
                        obj.OuCode = txtOUCodeI.Text.ToString();
                        obj.IsMst = 0;
                        obj.MstOUCode = cmdMstOUCODE.Text;
                        obj.MstOUCodeId = Convert.ToInt32(cmdMstOUCODE.SelectedValue);

                        int k = clsAPI.InsertOuCode(obj);

                        if (k > 0)
                        {
                            MessageBox.Show("Record successfully Saved!!");
                            txtOUCodeD.Text = "";
                            txtOUCodeI.Text = "";
                            drpBranch.SelectedIndex = -1;
                            drpBag.SelectedIndex = -1;
                            cmdMstOUCODE.SelectedIndex = -1;
                            drpBranch.Text = "";
                            drpBag.Text = "";
                        }
                    }
                }
                else
                {
                    MessageBox.Show("All values are mandatory!!");
                }
            }           
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BindMasterOUCodes()
        {
            string sql = string.Empty;
            cn.ConnectionString = con;
            cn.Open();
            //MessageBox.Show(cn.State.ToString());
            sql = "select distinct t.ID_OUMST, t.OU_CODEMST from tblOUMaster t INNER JOIN LocationAddr L ON t.ID_OUMST = L.ID_OUMST AND t.flag = 'Y'";
            cmd = new SqlCommand(sql, cn);
            DataRow dr;            
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);           

            dr = dt.NewRow();
            dr.ItemArray = new object[] { 0, "--Select--" };
            dt.Rows.InsertAt(dr, 0);
            cmdMstOUCODE.ValueMember = "ID_OUMST";
            cmdMstOUCODE.DisplayMember = "OU_CODEMST";
            cmdMstOUCODE.DataSource = dt;  

           // SqlDataReader rd = cmd.ExecuteReader();

           // while (rd.Read())
           // {
           //     cmdMstOUCODE.Items.Add(rd["OU_CODEMST"].ToString());
           //     cmdMstOUCODE.ValueMember
           //     cmdMstOUCODE.ValueMember = rd["ID_OUMST"].ToString();
           //   //  cmdMstOUCODE.ValueMember = rd["FleetID"].ToString();
           //    // cmdMstOUCODE.DisplayMember = rd["FleetName"].ToString();

           //  //   this.cmdMstOUCODE.Text = rd.GetString(1);
           // }
           //// OUCodeSel = this.cmdMstOUCODE.Text;
          //  rd.Close();
            cn.Close();
        }

        private void txtOUCodeD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back)
                e.Handled = true;            
        }

        private void txtOUCodeI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back)
                e.Handled = true;            
        }

        private void btnOuRemove_Click(object sender, EventArgs e)
        {
            DialogResult ch = MessageBox.Show("Are you sure you want to delete " + txtOuRemove.Text + " OU? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ch == DialogResult.Yes)
            {
                string ou = txtOuRemove.Text;
                DataTable dtRemoveOU = new DataTable();
                dtRemoveOU = clsAPI.DeleteOu(ou);
                int re = Convert.ToInt32(dtRemoveOU.Rows[0]["MstOu"]);

                if (re == 0)
                {
                    MessageBox.Show("Sorry you cant delete " + txtOuRemove.Text + " OU!!, because this is Mix OU!!..", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtOuRemove.Text = "";
                }
                else
                {
                    MessageBox.Show("OU successfully deleted!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtOuRemove.Text = "";
                    rdD.Checked = true;
                }
            }
        }                    
    }
}
