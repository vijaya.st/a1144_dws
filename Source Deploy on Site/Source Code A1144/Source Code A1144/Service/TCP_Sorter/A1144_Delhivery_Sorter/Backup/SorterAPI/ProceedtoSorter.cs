﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class ProceedtoSorter : Form
    {
        DataSet dsPLCDiam = new DataSet();
        ErrorLog oErrorLog = new ErrorLog();
        DataTable dtSorterInfo = new DataTable();
        Label[] lblArray = new Label[32];
        static int Sorterx;
        static int Sortery;
        static int btnx;
        static int lblx;
        int xValue;
        int yValue;
        private int _ArrowDir;
        static string Temppath = "";
        static string imagepath = @"G:\Kedar\Projects\SorterAPI_RnD_Menu_new\SorterAPI\image\Conveyor1.bmp";
        static string path = @"G:\Kedar\Projects\SorterAPI_RnD_Menu_new\SorterAPI\image";
        static int posX, posY = 100;
        Bitmap bmp;

        public int RackNo1 { get; set; }

        public int ArrowDir
        {
            get
            {
                return _ArrowDir;
            }
            set
            {
                _ArrowDir = value;
            }
        }
        private int _PosDistance;
        public int PosDistance
        {
            get
            {
                return _PosDistance;
            }
            set
            {
                _PosDistance = value;
            }
        }
        System.Drawing.RectangleF rect;
        public int ArrowPosition { get; set; }

        Button[] buttonArray = new Button[32];
        Button[] buttonArray1 = new Button[32];
        DataTable dtBininSorter = new DataTable();
        Label[] srtr = new Label[32];

        public ProceedtoSorter()
        {
            InitializeComponent();

            if (Temppath == "")
            {
                bmp = new Bitmap(imagepath);
                imageMap1.Image = bmp;
            }
            else
            {
                bmp = new Bitmap(Temppath);
                imageMap1.Image = bmp;
            }
        }

        private void ProceedtoSorter_Load(object sender, EventArgs e)
        {
           dsPLCDiam = clsAPI.getPLCDiemensionalData();

           dtSorterInfo = dsPLCDiam.Tables[0];

           dtBininSorter = clsAPI.getBininSorterData();


            //Button button = sender as Button;
            //if (button != null)
            //{
            //    int srtr = (int)button.Tag;
            //  //  frmRackInfo rf = new frmRackInfo();
            //  //  rf.mSorterNo = srtr;
            //  //  rf.ShowDialog();
            //    //// now you know the button that was clicked
            //    for(int i = 0; i<= dtBininSorter.Rows.Count; i++)
            //    {
            //        int btncheck = Convert.ToInt32(dtBininSorter.Rows[i]["bin"]);
            //    //switch ((int)button.Tag)
            //        switch(btncheck)
            //        {
            //           case 1:
            //                buttonArray
            //                button.Text = dtBininSorter.Rows[i]["bin"].ToString();
            //                //this.Hide();
            //                //frmRackInfo rf1 = new frmRackInfo();
            //                //rf1.mSorterNo = 1;
            //                //rf1.ShowDialog();
            //                break; 
            //           case 2:                            
            //                button.Text = dtBininSorter.Rows[i]["bin"].ToString();                    
            //            break;
            //           case 3:
            //                button.Text = dtBininSorter.Rows[i]["bin"].ToString();
            //            break;
            //           case 4:
            //                button.Text = dtBininSorter.Rows[i]["bin"].ToString();
            //            break;
            //           case 5:
            //                button.Text = dtBininSorter.Rows[i]["bin"].ToString();
            //            break;
            //            // ...
            //        }
            //    }
            //}// int srtr = (int)button.Tag;

        }

        void ButtonClickOneEvent(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                int srtr = (int)button.Tag;
                frmRackInfo rf = new frmRackInfo();
                rf.mSorterNo = srtr;
                rf.ShowDialog();
                //// now you know the button that was clicked
                //switch ((int)button.Tag)
                //{
                //    case 1:
                //        this.Hide();
                //        frmRackInfo rf1 = new frmRackInfo();
                //        rf1.mSorterNo = 1;
                //        rf1.ShowDialog();
                //        break;               
                //    // ...
                //}
            }
        }

        private void DrawSidewaysText(Graphics gr, Font font, Brush brush, Rectangle bounds, StringFormat string_format, string txt)
        {
            // Make a rotated rectangle at the origin.
            Rectangle rotated_bounds = new Rectangle(0, 0, bounds.Height, bounds.Width);
            // Rotate.
            gr.ResetTransform();
            gr.RotateTransform(-90);
            // Translate to move the rectangle to the correct position.
            gr.TranslateTransform(bounds.Left, bounds.Bottom, System.Drawing.Drawing2D.MatrixOrder.Append);
            // Draw the text.
            gr.DrawString(txt, font, brush, rotated_bounds, string_format);
        }

        private void imageMap1_RegionClick(int index, string key)
        {
            //this.Hide();
            frmSelDirection f = new frmSelDirection();
            f.ArrowPosition1 = Convert.ToInt32(key);
            f.ShowDialog();
        }

        private void imageMap1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                int ynew;
                int lblPo;

                int srtno = Convert.ToInt32(dtSorterInfo.Rows[0][1]);
                yValue = 1131 / srtno;    // sorter no set yValue         

                //if (ArrowPosition == 1)
                //{
                //    ynew = yValue * ArrowPosition - 200;
                //    lblPo = yValue * ArrowPosition - 200;
                //}
                //else
                //{
                //    ynew = yValue * ArrowPosition - yValue - 100;
                //    lblPo = yValue * ArrowPosition - yValue -100;
                //}

                if (srtno <= 5)
                {
                    ynew = yValue * ArrowPosition - 155;
                    lblPo = yValue * ArrowPosition - 155;
                }
                else if (srtno <= 10)
                {
                    ynew = yValue * ArrowPosition - 135;
                    lblPo = yValue * ArrowPosition - 135;
                }
                else
                {
                    ynew = yValue * ArrowPosition - 125;
                    lblPo = yValue * ArrowPosition - 125;
                }

                if (ArrowDir == 1)
                {
                    foreach (Control ctn in this.Controls)
                    {
                        if (ctn is Button)
                        {
                            if (ctn.Name == "btnR1")
                            {
                                //MessageBox.Show(ctn.Name.ToString());
                                ctn.Visible = false;
                            }
                        }
                    }
                }
                

                Graphics er = Graphics.FromImage(imageMap1.Image);
                if (ArrowDir == 3)
                {
                    er.TranslateTransform(100.0F, 0.0F);
                    er.RotateTransform(-90);
                    Pen mypen = new Pen(Color.White);
                    mypen.Width = 50;
                    er.DrawLine(mypen, -80, ynew, -20, ynew);//clear
                    er.DrawString("(" + lblRack1.Text + ")", new Font("Arial", 8), Brushes.Black, -65, lblPo);
                    er.DrawLine(Pens.Black, -80, ynew, -20, ynew);//vertical line

                    er.DrawLine(Pens.Black, -30, ynew + 10, -20, ynew);//for up Arrow
                    er.DrawLine(Pens.Black, -30, ynew - 10, -20, ynew);//for up arrow

                    er.DrawLine(Pens.Black, -80, ynew, -70, ynew - 10);//for down Arrow
                    er.DrawLine(Pens.Black, -80, ynew, -70, ynew + 10);//for down Arrow
                    SaveImage();
                }
                else if (ArrowDir == 1)
                {
                    er.TranslateTransform(100.0F, 0.0F);
                    er.RotateTransform(-90);
                    Pen mypen = new Pen(Color.White);
                    mypen.Width = 50;
                    er.DrawLine(mypen, -80, ynew, -20, ynew);//clear 
                    lblRack1.Text = PosDistance.ToString();
                    er.DrawString("(" + lblRack1.Text + ")", new Font("Arial", 8), Brushes.Black, -65, lblPo);
                    er.DrawLine(Pens.Black, -80, ynew, -20, ynew);//vertical line

                    er.DrawLine(Pens.Black, -30, ynew + 10, -20, ynew);//for up Arrow
                    er.DrawLine(Pens.Black, -30, ynew - 10, -20, ynew);//for up arrow

                  //  buttonArray.
                    SaveImage();
                }
                else if (ArrowDir == 2)
                {
                    er.TranslateTransform(100.0F, 0.0F);
                    er.RotateTransform(-90);
                    Pen mypen = new Pen(Color.White);
                    mypen.Width = 50;
                    er.DrawLine(mypen, -80, ynew, -20, ynew);//clear
                    er.DrawString("(" + lblRack1.Text + ")", new Font("Arial", 8), Brushes.Black, -65, lblPo);
                    er.DrawLine(Pens.Black, -80, ynew, -20, ynew);//vertical line
                    er.DrawLine(Pens.Black, -80, ynew, -70, ynew - 10);//for down Arrow
                    er.DrawLine(Pens.Black, -80, ynew, -70, ynew + 10);//for down Arrow
                    SaveImage();
                }
                er.Dispose();
                // this.Invalidate();
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());   
            }
        }

        public void SaveImage()
        {
            string picname = imagepath.Substring(imagepath.LastIndexOf('\\'));
            Bitmap imgImage = new Bitmap(imageMap1.Image);
            string UniqueName = DateTime.Now.ToString("hh:mm:ss.fff");
            UniqueName = UniqueName.Replace("/", "");
            UniqueName = UniqueName.Replace(":", "");
            UniqueName = UniqueName.Replace(" ", "");
            UniqueName = UniqueName.Replace(".", "");
            string[] arr = new string[2];
            arr = picname.Split('.');
            String picname1 = arr[0] + UniqueName + "." + arr[1];
            imgImage.Save(path + "\\temp\\" + picname1);

            String p = @"" + path + picname;

            String p1 = @"" + path + "\\temp" + picname1;

            FileInfo imgFile = new FileInfo(p1);

            clsAPI objimg = new clsAPI();
            objimg.imgPath = p1;
            DataTable dtimg = new DataTable();
            dtimg = clsAPI.UpdateGetImgPath(objimg);
            string imgnewPath = dtimg.Rows[0]["ImagePath"].ToString();

            // pictureBox1.Image = new Bitmap(p1);
            // pictureBox1.Image = new Bitmap(imgnewPath);
            Graphics graphic = Graphics.FromImage(imageMap1.Image);
            graphic.Clear(Color.White);

            // imageMap1.Image = new Bitmap(p1);
            imageMap1.Image = new Bitmap(imgnewPath);
            Temppath = p1;

            //int srtcount = Convert.ToInt32(dtSorterInfo.Rows[0][1]);
            //for(int i = 1; i<= srtcount; i++)
            //{
            //lblArray[i].Visible = false;
            //}
            // lblClickHere.Visible = false;
            // File.Delete(
        }

        private void DrawRotatedTextAt(Graphics gr, float angle, string txt, int x, int y, Font the_font, Brush the_brush)
        {
            // Save the graphics state.
            GraphicsState state = gr.Save();
            gr.ResetTransform();
            // Rotate.
            gr.RotateTransform(angle);
            // Translate to desired position. Be sure to append
            // the rotation so it occurs after the rotation.
            gr.TranslateTransform(x, y, MatrixOrder.Append);
            // Draw the text at the origin.
            gr.DrawString(txt, the_font, the_brush, 0, 0);
            // Restore the graphics state.
            gr.Restore(state);
        }

        private void ProceedtoSorter_Paint(object sender, PaintEventArgs e)
        {
            int strsorter = Convert.ToInt32(dtSorterInfo.Rows[0][1]);
            int i;

            yValue = 1131 / strsorter;

            Sorterx = yValue * ArrowPosition + yValue;
            Sortery = yValue * ArrowPosition + yValue;
            btnx = yValue * ArrowPosition + yValue;
            lblx = yValue * ArrowPosition + yValue;


            // Debug.Print(btnx + " " + Sorterx);
            for (i = 1; i <= strsorter; i++)
            {
                if (strsorter <= 5)
                {
                    Sorterx = yValue * i - 50;
                    Sortery = yValue * i - 50;
                    btnx = yValue * i - 50;
                    lblx = yValue * i - 50;
                }
                else if (strsorter <= 10)
                {
                    Sorterx = yValue * i - 40;
                    Sortery = yValue * i - 40;
                    btnx = yValue * i - 40;
                    lblx = yValue * i - 40;
                }

                else
                {
                    Sorterx = yValue * i - 30;
                    Sortery = yValue * i - 30;
                    btnx = yValue * i - 30;
                    lblx = yValue * i - 30;
                }
                e.Graphics.FillRectangle(Brushes.Red, Sorterx - 5, 60, 42f, 140f); // button background red up
                e.Graphics.FillRectangle(Brushes.Red, Sorterx - 5, 204, 42f, 30f); // button background red down
               // e.Graphics.FillRectangle(Brushes.Red, Sorterx - 5, 200, 42f, 140f);
                imageMap1.AddRectangle(i.ToString(), Sorterx, 10, Sortery + 30, 80);
                
                lblRack1.Text = PosDistance.ToString();

                srtr[i] = new Label();
                srtr[i].BackColor = Color.Transparent;
                srtr[i].ForeColor = Color.White;
                srtr[i].Size = new Size(30, 20);
                srtr[i].Location = new Point(btnx+5, 40);
                srtr[i].Text = i.ToString();


                buttonArray[i] = new Button();
                buttonArray[i].Size = new Size(34, 23);
                buttonArray[i].Name = "btnR" + i;

                // buttonArray[i].Text = "0";
                if (dtBininSorter.Rows.Count >= i)
                {
                    int ch = Convert.ToInt32(dtBininSorter.Rows[i - 1]["SorterNo"]);
                    if (ch == i)
                    {
                        buttonArray[i].Text = dtBininSorter.Rows[i - 1]["bin"].ToString();
                    }
                    else
                    {
                        buttonArray[i].Text = "0";
                    }
                    
                }
                else
                {
                    buttonArray[i].Text = "0";
                }

                buttonArray[i].BackColor = Color.White;
                buttonArray[i].Location = new Point(btnx, 66);
                buttonArray[i].Click += new EventHandler(ButtonClickOneEvent);
                buttonArray[i].Tag = i;

                buttonArray1[i] = new Button();
                buttonArray1[i].Size = new Size(34, 23);
                buttonArray1[i].Name = "btnL" + i;

                // buttonArray[i].Text = "0";
                if (dtBininSorter.Rows.Count >= i)
                {
                    int ch = Convert.ToInt32(dtBininSorter.Rows[i - 1]["SorterNo"]);
                    if (ch == i)
                    {
                        buttonArray1[i].Text = dtBininSorter.Rows[i - 1]["bin"].ToString();
                    }
                    else
                    {
                        buttonArray1[i].Text = "0";
                    }                    
                }
                else
                {
                    buttonArray1[i].Text = "0";
                }

                buttonArray1[i].BackColor = Color.White;
                buttonArray1[i].Location = new Point(btnx, 204);
                buttonArray1[i].Click += new EventHandler(ButtonClickOneEvent);
                buttonArray1[i].Tag = i;


                lblArray[i] = new Label();
                lblArray[i].Name = "lable" + i;
                //lblArray[i].Text = "Cl\ni\nc\nk\nHe\nr\ne ";
                lblArray[i].Text = "S\ne\ntDi\nre\nc\nt\ni\non ";
                lblArray[i].Location = new Point(lblx + 10, 90);
                lblArray[i].BackColor = Color.LightYellow;
                // lblArray[i].BackColor = Color.Transparent;
                lblArray[i].ForeColor = Color.Blue;
                lblArray[i].Font = new Font("Arial", 5.5f);
                lblArray[i].Height = 110;
                lblArray[i].Width = 10;
                if (ArrowDir == 0)
                {
                    lblArray[i].Visible = true;
                }
                else
                {
                    lblArray[i].Visible = false;
                }
                // buttonArray[i].Location = new Point(40, 20 + (i * 20));
                this.Controls.Add(buttonArray[i]);
                this.Controls.Add(buttonArray1[i]);
                this.Controls.Add(lblArray[i]);
                this.Controls.Add(srtr[i]);
                // tabPage1.Controls.Add(buttonArray[i]);
                // tabPage1.Controls.Add(lblArray[i]);


                //yValue = 800 / strsorter;

                // else
                // {
                Sorterx = yValue * i;
                Sortery = yValue * i;
                btnx = yValue * i;
                lblx = yValue * i;
                // }

                // Sorterx = Sorterx + 100;
                //Sortery = Sortery + 100;
                //btnx = btnx + 100;
                // lblx = lblx + 100;

                imageMap1.SendToBack();
            }
        }

        private void btnDiscard_Click(object sender, EventArgs e)
        {
            //string filePath = @"G:\Kedar\Projects\SorterAPI\image\";
            //this.Close();
            //this.Dispose();
            //imageMap1.Dispose();
            //bmp = new Bitmap(imagepath);
            //imageMap1.Image = bmp;

            //frmSorterAPIConfigurator S = new frmSorterAPIConfigurator();
            //{
            //    S.IsDelete = 1;
            //    S.Show();                
            //}

            //imageMap1.Dispose();
            //bmp = new Bitmap(imagepath);
            //imageMap1.Image = bmp;

            string[] filePaths = Directory.GetFiles(@"G:\Kedar\Projects\SorterAPI\image\temp");
            foreach (string filePath in filePaths)
                if (filePath != "")
                {
                    using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        byte[] buffer = new byte[fs.Length];
                        fs.Read(buffer, 0, (int)fs.Length);
                        using (MemoryStream ms = new MemoryStream(buffer))
                            this.imageMap1.Image = Image.FromStream(ms);
                    }
                    File.Delete(filePath);
                }

            // string pa = @"G:\Kedar\Projects\SorterAPI\image\temp";            

        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            using (var brush = new LinearGradientBrush
                       (DisplayRectangle, Color.FromArgb(4, 20, 21), Color.FromArgb(29, 143, 150), LinearGradientMode.Vertical))
            {
                e.Graphics.FillRectangle(brush, DisplayRectangle);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            Invalidate(); // Force repainting on resize
        }       
    }
}
