﻿namespace SorterAPI
{
    partial class BayAssign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgvHubList = new System.Windows.Forms.DataGridView();
            this.BayAClose = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnLocationClose = new System.Windows.Forms.Button();
            this.btnSaveLocation = new System.Windows.Forms.Button();
            this.drpBinDown = new System.Windows.Forms.ComboBox();
            this.drpBinUp = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.drpBranch = new System.Windows.Forms.ComboBox();
            this.lblDown = new System.Windows.Forms.Label();
            this.lblUp = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlfiltertext = new System.Windows.Forms.Panel();
            this.drpBranchFilter = new System.Windows.Forms.ComboBox();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnlfiltertext.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(0, 754);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1200, 500);
            this.panel3.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel4.Controls.Add(this.dgvHubList);
            this.panel4.Location = new System.Drawing.Point(12, 11);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1158, 475);
            this.panel4.TabIndex = 0;
            // 
            // dgvHubList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.dgvHubList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkOrange;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHubList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvHubList.ColumnHeadersHeight = 45;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(157)))), ((int)(((byte)(157)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHubList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvHubList.GridColor = System.Drawing.Color.White;
            this.dgvHubList.Location = new System.Drawing.Point(280, 11);
            this.dgvHubList.Name = "dgvHubList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHubList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvHubList.RowHeadersWidth = 45;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvHubList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvHubList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHubList.Size = new System.Drawing.Size(598, 441);
            this.dgvHubList.TabIndex = 12;
            // 
            // BayAClose
            // 
            this.BayAClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.BayAClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BayAClose.Location = new System.Drawing.Point(1176, 5);
            this.BayAClose.Name = "BayAClose";
            this.BayAClose.Size = new System.Drawing.Size(18, 14);
            this.BayAClose.TabIndex = 6;
            this.BayAClose.Click += new System.EventHandler(this.BayAClose_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::SorterAPI.Properties.Resources.bay_Entry;
            this.panel2.Controls.Add(this.btnLocationClose);
            this.panel2.Controls.Add(this.btnSaveLocation);
            this.panel2.Controls.Add(this.drpBinDown);
            this.panel2.Controls.Add(this.drpBinUp);
            this.panel2.Controls.Add(this.txtSearch);
            this.panel2.Controls.Add(this.drpBranch);
            this.panel2.Controls.Add(this.lblDown);
            this.panel2.Controls.Add(this.lblUp);
            this.panel2.Location = new System.Drawing.Point(0, 345);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1200, 313);
            this.panel2.TabIndex = 3;
            // 
            // btnLocationClose
            // 
            this.btnLocationClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnLocationClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLocationClose.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.btnLocationClose.ForeColor = System.Drawing.Color.White;
            this.btnLocationClose.Location = new System.Drawing.Point(622, 196);
            this.btnLocationClose.Name = "btnLocationClose";
            this.btnLocationClose.Size = new System.Drawing.Size(182, 64);
            this.btnLocationClose.TabIndex = 22;
            this.btnLocationClose.Text = "Close";
            this.btnLocationClose.UseVisualStyleBackColor = false;
            this.btnLocationClose.Click += new System.EventHandler(this.btnLocationClose_Click);
            // 
            // btnSaveLocation
            // 
            this.btnSaveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnSaveLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveLocation.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.btnSaveLocation.ForeColor = System.Drawing.Color.White;
            this.btnSaveLocation.Location = new System.Drawing.Point(397, 196);
            this.btnSaveLocation.Name = "btnSaveLocation";
            this.btnSaveLocation.Size = new System.Drawing.Size(182, 64);
            this.btnSaveLocation.TabIndex = 21;
            this.btnSaveLocation.Text = "Submit";
            this.btnSaveLocation.UseVisualStyleBackColor = false;
            this.btnSaveLocation.Click += new System.EventHandler(this.btnSaveLocation_Click);
            // 
            // drpBinDown
            // 
            this.drpBinDown.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBinDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBinDown.FormattingEnabled = true;
            this.drpBinDown.Location = new System.Drawing.Point(975, 123);
            this.drpBinDown.Name = "drpBinDown";
            this.drpBinDown.Size = new System.Drawing.Size(120, 33);
            this.drpBinDown.TabIndex = 25;
            this.drpBinDown.Visible = false;
            // 
            // drpBinUp
            // 
            this.drpBinUp.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBinUp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBinUp.FormattingEnabled = true;
            this.drpBinUp.Location = new System.Drawing.Point(690, 120);
            this.drpBinUp.Name = "drpBinUp";
            this.drpBinUp.Size = new System.Drawing.Size(120, 33);
            this.drpBinUp.TabIndex = 20;
            this.drpBinUp.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(265, 52);
            this.txtSearch.MaxLength = 6;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(270, 31);
            this.txtSearch.TabIndex = 18;
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // drpBranch
            // 
            this.drpBranch.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBranch.FormattingEnabled = true;
            this.drpBranch.Location = new System.Drawing.Point(265, 124);
            this.drpBranch.Name = "drpBranch";
            this.drpBranch.Size = new System.Drawing.Size(270, 33);
            this.drpBranch.TabIndex = 19;
            this.drpBranch.SelectedIndexChanged += new System.EventHandler(this.drpBranch_SelectedIndexChanged);
            // 
            // lblDown
            // 
            this.lblDown.AutoSize = true;
            this.lblDown.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblDown.Location = new System.Drawing.Point(831, 128);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(138, 19);
            this.lblDown.TabIndex = 24;
            this.lblDown.Text = "Select Bin Down";
            this.lblDown.Visible = false;
            // 
            // lblUp
            // 
            this.lblUp.AutoSize = true;
            this.lblUp.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblUp.Location = new System.Drawing.Point(569, 128);
            this.lblUp.Name = "lblUp";
            this.lblUp.Size = new System.Drawing.Size(115, 19);
            this.lblUp.TabIndex = 23;
            this.lblUp.Text = "Select Bin Up";
            this.lblUp.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.bay_Changes;
            this.panel1.Location = new System.Drawing.Point(0, 248);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1200, 95);
            this.panel1.TabIndex = 2;
            // 
            // pnlfiltertext
            // 
            this.pnlfiltertext.BackgroundImage = global::SorterAPI.Properties.Resources.filterText;
            this.pnlfiltertext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlfiltertext.Controls.Add(this.drpBranchFilter);
            this.pnlfiltertext.Controls.Add(this.txtFilter);
            this.pnlfiltertext.Location = new System.Drawing.Point(0, 127);
            this.pnlfiltertext.Name = "pnlfiltertext";
            this.pnlfiltertext.Size = new System.Drawing.Size(1200, 119);
            this.pnlfiltertext.TabIndex = 1;
            // 
            // drpBranchFilter
            // 
            this.drpBranchFilter.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBranchFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranchFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBranchFilter.FormattingEnabled = true;
            this.drpBranchFilter.Location = new System.Drawing.Point(265, 41);
            this.drpBranchFilter.Name = "drpBranchFilter";
            this.drpBranchFilter.Size = new System.Drawing.Size(281, 33);
            this.drpBranchFilter.TabIndex = 12;
            this.drpBranchFilter.SelectedIndexChanged += new System.EventHandler(this.drpBranchFilter_SelectedIndexChanged);
            // 
            // txtFilter
            // 
            this.txtFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilter.Location = new System.Drawing.Point(793, 41);
            this.txtFilter.MaxLength = 6;
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(270, 31);
            this.txtFilter.TabIndex = 17;
            this.txtFilter.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilter_KeyUp);
            this.txtFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilter_KeyPress);
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackgroundImage = global::SorterAPI.Properties.Resources.bay_header;
            this.pnlHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlHeader.Location = new System.Drawing.Point(0, 28);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1177, 98);
            this.pnlHeader.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.BackgroundImage = global::SorterAPI.Properties.Resources.bay_LocationHeader;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Location = new System.Drawing.Point(329, 673);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(585, 67);
            this.panel5.TabIndex = 7;
            // 
            // BayAssign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1252, 683);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.BayAClose);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnlfiltertext);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BayAssign";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "BayAssign";
            this.Load += new System.EventHandler(this.BayAssign_Load);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlfiltertext.ResumeLayout(false);
            this.pnlfiltertext.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel pnlfiltertext;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox drpBranchFilter;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ComboBox drpBranch;
        private System.Windows.Forms.ComboBox drpBinUp;
        private System.Windows.Forms.Button btnSaveLocation;
        private System.Windows.Forms.Button btnLocationClose;
        private System.Windows.Forms.Label lblUp;
        private System.Windows.Forms.ComboBox drpBinDown;
        private System.Windows.Forms.Label lblDown;
        private System.Windows.Forms.DataGridView dgvHubList;
        private System.Windows.Forms.Panel BayAClose;
        private System.Windows.Forms.Panel panel5;
    }
}