﻿namespace SorterAPI
{
    partial class frmSorterAPIConfigurator3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.testDWSSetting = new System.Windows.Forms.Button();
            this.btnUpdateDWS = new System.Windows.Forms.Button();
            this.txtDataTrakingZone = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtIdDataLoadingZone = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtRequiredDistance = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtPropixSpeed = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDistTravebyConvencoderpulse = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPTLSystem = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBagClosurePrefix = new System.Windows.Forms.TextBox();
            this.txtMinNoArticle = new System.Windows.Forms.TextBox();
            this.txtMaxNoArticle = new System.Windows.Forms.TextBox();
            this.txtMaxWeightArticle = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAssignBag = new System.Windows.Forms.Label();
            this.lblLinkDB = new System.Windows.Forms.Label();
            this.lblPTLSys = new System.Windows.Forms.Label();
            this.lblPLCData = new System.Windows.Forms.Label();
            this.lblCustInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PTLPanel = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PTLPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(3, 143);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1180, 404);
            this.panel3.TabIndex = 9;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel4.Controls.Add(this.groupBox2);
            this.panel4.Controls.Add(this.groupBox1);
            this.panel4.Location = new System.Drawing.Point(5, 7);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1170, 388);
            this.panel4.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.testDWSSetting);
            this.groupBox2.Controls.Add(this.btnUpdateDWS);
            this.groupBox2.Controls.Add(this.txtDataTrakingZone);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.txtIdDataLoadingZone);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.txtRequiredDistance);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.txtPropixSpeed);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtDistTravebyConvencoderpulse);
            this.groupBox2.Location = new System.Drawing.Point(11, 198);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1152, 181);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DWS";
            // 
            // testDWSSetting
            // 
            this.testDWSSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.testDWSSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testDWSSetting.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testDWSSetting.ForeColor = System.Drawing.Color.White;
            this.testDWSSetting.Location = new System.Drawing.Point(567, 136);
            this.testDWSSetting.Name = "testDWSSetting";
            this.testDWSSetting.Size = new System.Drawing.Size(159, 36);
            this.testDWSSetting.TabIndex = 51;
            this.testDWSSetting.Text = "Test DWS Setting";
            this.testDWSSetting.UseVisualStyleBackColor = false;
            this.testDWSSetting.Click += new System.EventHandler(this.testDWSSetting_Click);
            // 
            // btnUpdateDWS
            // 
            this.btnUpdateDWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnUpdateDWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateDWS.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateDWS.ForeColor = System.Drawing.Color.White;
            this.btnUpdateDWS.Location = new System.Drawing.Point(427, 136);
            this.btnUpdateDWS.Name = "btnUpdateDWS";
            this.btnUpdateDWS.Size = new System.Drawing.Size(118, 36);
            this.btnUpdateDWS.TabIndex = 50;
            this.btnUpdateDWS.Text = "Update";
            this.btnUpdateDWS.UseVisualStyleBackColor = false;
            this.btnUpdateDWS.Click += new System.EventHandler(this.btnUpdateDWS_Click);
            // 
            // txtDataTrakingZone
            // 
            this.txtDataTrakingZone.Location = new System.Drawing.Point(938, 97);
            this.txtDataTrakingZone.Multiline = true;
            this.txtDataTrakingZone.Name = "txtDataTrakingZone";
            this.txtDataTrakingZone.Size = new System.Drawing.Size(162, 29);
            this.txtDataTrakingZone.TabIndex = 49;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(583, 104);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(349, 19);
            this.label28.TabIndex = 48;
            this.label28.Text = "Max data update zone in data tracking zone :";
            // 
            // txtIdDataLoadingZone
            // 
            this.txtIdDataLoadingZone.Location = new System.Drawing.Point(938, 57);
            this.txtIdDataLoadingZone.Multiline = true;
            this.txtIdDataLoadingZone.Name = "txtIdDataLoadingZone";
            this.txtIdDataLoadingZone.Size = new System.Drawing.Size(162, 29);
            this.txtIdDataLoadingZone.TabIndex = 46;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(583, 64);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(194, 19);
            this.label26.TabIndex = 45;
            this.label26.Text = "ID of data loading zone :\r\n";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label23.Location = new System.Drawing.Point(508, 81);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 16);
            this.label23.TabIndex = 44;
            this.label23.Text = "(mm)";
            // 
            // txtRequiredDistance
            // 
            this.txtRequiredDistance.Location = new System.Drawing.Point(345, 77);
            this.txtRequiredDistance.Multiline = true;
            this.txtRequiredDistance.Name = "txtRequiredDistance";
            this.txtRequiredDistance.Size = new System.Drawing.Size(162, 29);
            this.txtRequiredDistance.TabIndex = 43;
            this.txtRequiredDistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRequiredDistance_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(16, 76);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(315, 38);
            this.label24.TabIndex = 42;
            this.label24.Text = "Required distance of dimesnion delivery\r\n point from dimesioning sensor :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label22.Location = new System.Drawing.Point(1103, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 16);
            this.label22.TabIndex = 41;
            this.label22.Text = "(mpm)";
            // 
            // txtPropixSpeed
            // 
            this.txtPropixSpeed.Location = new System.Drawing.Point(938, 17);
            this.txtPropixSpeed.Multiline = true;
            this.txtPropixSpeed.Name = "txtPropixSpeed";
            this.txtPropixSpeed.Size = new System.Drawing.Size(162, 29);
            this.txtPropixSpeed.TabIndex = 40;
            this.txtPropixSpeed.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPropixSpeed_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(583, 24);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(151, 19);
            this.label21.TabIndex = 39;
            this.label21.Text = "Set Propix Speed :\r\n";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label19.Location = new System.Drawing.Point(509, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 16);
            this.label19.TabIndex = 38;
            this.label19.Text = "(mm)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(16, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(295, 38);
            this.label20.TabIndex = 37;
            this.label20.Text = "Define distance travelled by conveyor\r\n for each encoder pulse:";
            // 
            // txtDistTravebyConvencoderpulse
            // 
            this.txtDistTravebyConvencoderpulse.Location = new System.Drawing.Point(345, 17);
            this.txtDistTravebyConvencoderpulse.Multiline = true;
            this.txtDistTravebyConvencoderpulse.Name = "txtDistTravebyConvencoderpulse";
            this.txtDistTravebyConvencoderpulse.Size = new System.Drawing.Size(162, 29);
            this.txtDistTravebyConvencoderpulse.TabIndex = 36;
            this.txtDistTravebyConvencoderpulse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDistTravebyConvencoderpulse_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PTLPanel);
            this.groupBox1.Location = new System.Drawing.Point(11, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1152, 182);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PTL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(567, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(166, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "Bag Closure Prefix : ";
            // 
            // btnPTLSystem
            // 
            this.btnPTLSystem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnPTLSystem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPTLSystem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPTLSystem.ForeColor = System.Drawing.Color.White;
            this.btnPTLSystem.Location = new System.Drawing.Point(492, 108);
            this.btnPTLSystem.Name = "btnPTLSystem";
            this.btnPTLSystem.Size = new System.Drawing.Size(118, 36);
            this.btnPTLSystem.TabIndex = 11;
            this.btnPTLSystem.Text = "Update";
            this.btnPTLSystem.UseVisualStyleBackColor = false;
            this.btnPTLSystem.Click += new System.EventHandler(this.btnPTLSystem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(272, 19);
            this.label4.TabIndex = 1;
            this.label4.Text = "Min No. of Articles Allowed in Bag :";
            // 
            // txtBagClosurePrefix
            // 
            this.txtBagClosurePrefix.Location = new System.Drawing.Point(918, 67);
            this.txtBagClosurePrefix.MaxLength = 5;
            this.txtBagClosurePrefix.Multiline = true;
            this.txtBagClosurePrefix.Name = "txtBagClosurePrefix";
            this.txtBagClosurePrefix.Size = new System.Drawing.Size(162, 29);
            this.txtBagClosurePrefix.TabIndex = 8;
            // 
            // txtMinNoArticle
            // 
            this.txtMinNoArticle.Location = new System.Drawing.Point(325, 11);
            this.txtMinNoArticle.Multiline = true;
            this.txtMinNoArticle.Name = "txtMinNoArticle";
            this.txtMinNoArticle.Size = new System.Drawing.Size(162, 29);
            this.txtMinNoArticle.TabIndex = 3;
            this.txtMinNoArticle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMinNoArticle_KeyPress);
            // 
            // txtMaxNoArticle
            // 
            this.txtMaxNoArticle.Location = new System.Drawing.Point(918, 11);
            this.txtMaxNoArticle.Multiline = true;
            this.txtMaxNoArticle.Name = "txtMaxNoArticle";
            this.txtMaxNoArticle.Size = new System.Drawing.Size(162, 29);
            this.txtMaxNoArticle.TabIndex = 7;
            this.txtMaxNoArticle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxNoArticle_KeyPress);
            // 
            // txtMaxWeightArticle
            // 
            this.txtMaxWeightArticle.Location = new System.Drawing.Point(325, 67);
            this.txtMaxWeightArticle.Multiline = true;
            this.txtMaxWeightArticle.Name = "txtMaxWeightArticle";
            this.txtMaxWeightArticle.Size = new System.Drawing.Size(162, 29);
            this.txtMaxWeightArticle.TabIndex = 4;
            this.txtMaxWeightArticle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxWeightArticle_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(563, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(303, 19);
            this.label7.TabIndex = 6;
            this.label7.Text = "Max Weight of Articles Allowed in Bag :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(15, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(276, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "Max No. of Articles Allowed in Bag :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(217, 31);
            this.label3.TabIndex = 1;
            this.label3.Text = "                             ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pnlClose);
            this.panel2.Location = new System.Drawing.Point(1, 59);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1180, 81);
            this.panel2.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label8.Location = new System.Drawing.Point(586, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(150, 29);
            this.label8.TabIndex = 21;
            this.label8.Text = "PTL System";
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::SorterAPI.Properties.Resources.PTLTitle;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Location = new System.Drawing.Point(461, 5);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(74, 71);
            this.panel5.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "                             ";
            // 
            // pnlClose
            // 
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.Location = new System.Drawing.Point(1154, 5);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(28, 24);
            this.pnlClose.TabIndex = 0;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.PTL_DWS_banner1;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblAssignBag);
            this.panel1.Controls.Add(this.lblLinkDB);
            this.panel1.Controls.Add(this.lblPTLSys);
            this.panel1.Controls.Add(this.lblPLCData);
            this.panel1.Controls.Add(this.lblCustInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1182, 54);
            this.panel1.TabIndex = 10;
            // 
            // lblAssignBag
            // 
            this.lblAssignBag.AutoSize = true;
            this.lblAssignBag.BackColor = System.Drawing.Color.Transparent;
            this.lblAssignBag.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssignBag.Location = new System.Drawing.Point(939, 13);
            this.lblAssignBag.Name = "lblAssignBag";
            this.lblAssignBag.Size = new System.Drawing.Size(187, 29);
            this.lblAssignBag.TabIndex = 5;
            this.lblAssignBag.Text = "                             ";
            this.lblAssignBag.Click += new System.EventHandler(this.lblAssignBag_Click);
            // 
            // lblLinkDB
            // 
            this.lblLinkDB.AutoSize = true;
            this.lblLinkDB.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkDB.Location = new System.Drawing.Point(771, 14);
            this.lblLinkDB.Name = "lblLinkDB";
            this.lblLinkDB.Size = new System.Drawing.Size(151, 29);
            this.lblLinkDB.TabIndex = 4;
            this.lblLinkDB.Text = "                       ";
            this.lblLinkDB.Click += new System.EventHandler(this.lblLinkDB_Click);
            // 
            // lblPTLSys
            // 
            this.lblPTLSys.AutoSize = true;
            this.lblPTLSys.BackColor = System.Drawing.Color.Transparent;
            this.lblPTLSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPTLSys.Location = new System.Drawing.Point(604, 17);
            this.lblPTLSys.Name = "lblPTLSys";
            this.lblPTLSys.Size = new System.Drawing.Size(145, 29);
            this.lblPTLSys.TabIndex = 3;
            this.lblPTLSys.Text = "                      ";
            this.lblPTLSys.Click += new System.EventHandler(this.lblPTLSys_Click);
            // 
            // lblPLCData
            // 
            this.lblPLCData.AutoSize = true;
            this.lblPLCData.BackColor = System.Drawing.Color.Transparent;
            this.lblPLCData.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLCData.Location = new System.Drawing.Point(419, 13);
            this.lblPLCData.Name = "lblPLCData";
            this.lblPLCData.Size = new System.Drawing.Size(163, 29);
            this.lblPLCData.TabIndex = 2;
            this.lblPLCData.Text = "                         ";
            this.lblPLCData.Click += new System.EventHandler(this.lblPLCData_Click);
            // 
            // lblCustInfo
            // 
            this.lblCustInfo.AutoSize = true;
            this.lblCustInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCustInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustInfo.Location = new System.Drawing.Point(234, 14);
            this.lblCustInfo.Name = "lblCustInfo";
            this.lblCustInfo.Size = new System.Drawing.Size(145, 29);
            this.lblCustInfo.TabIndex = 1;
            this.lblCustInfo.Text = "                      ";
            this.lblCustInfo.Click += new System.EventHandler(this.lblCustInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(67, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "                                   ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // PTLPanel
            // 
            this.PTLPanel.Controls.Add(this.btnPTLSystem);
            this.PTLPanel.Controls.Add(this.label5);
            this.PTLPanel.Controls.Add(this.txtMaxNoArticle);
            this.PTLPanel.Controls.Add(this.txtBagClosurePrefix);
            this.PTLPanel.Controls.Add(this.label7);
            this.PTLPanel.Controls.Add(this.label6);
            this.PTLPanel.Controls.Add(this.label4);
            this.PTLPanel.Controls.Add(this.txtMinNoArticle);
            this.PTLPanel.Controls.Add(this.txtMaxWeightArticle);
            this.PTLPanel.Location = new System.Drawing.Point(20, 19);
            this.PTLPanel.Name = "PTLPanel";
            this.PTLPanel.Size = new System.Drawing.Size(1101, 157);
            this.PTLPanel.TabIndex = 12;
            // 
            // frmSorterAPIConfigurator3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 562);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterAPIConfigurator3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Sorter Configurator";
            this.Load += new System.EventHandler(this.frmSorterAPIConfigurator3_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PTLPanel.ResumeLayout(false);
            this.PTLPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblAssignBag;
        private System.Windows.Forms.Label lblLinkDB;
        private System.Windows.Forms.Label lblPTLSys;
        private System.Windows.Forms.Label lblPLCData;
        private System.Windows.Forms.Label lblCustInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnPTLSystem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMaxWeightArticle;
        private System.Windows.Forms.TextBox txtMinNoArticle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBagClosurePrefix;
        private System.Windows.Forms.TextBox txtMaxNoArticle;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDataTrakingZone;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtIdDataLoadingZone;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtRequiredDistance;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtPropixSpeed;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDistTravebyConvencoderpulse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button testDWSSetting;
        private System.Windows.Forms.Button btnUpdateDWS;
        private System.Windows.Forms.Panel PTLPanel;
    }
}