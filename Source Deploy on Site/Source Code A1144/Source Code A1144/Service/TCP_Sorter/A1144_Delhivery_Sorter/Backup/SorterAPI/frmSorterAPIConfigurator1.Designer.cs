﻿namespace SorterAPI
{
    partial class frmSorterAPIConfigurator1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlSorterOption = new System.Windows.Forms.Panel();
            this.rdRobosort = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.panel23 = new System.Windows.Forms.Panel();
            this.rdSortsYes = new System.Windows.Forms.RadioButton();
            this.rdSortsNo = new System.Windows.Forms.RadioButton();
            this.panel21 = new System.Windows.Forms.Panel();
            this.rdPTLYes = new System.Windows.Forms.RadioButton();
            this.rdPTLNo = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.btnSavePLCDiemensionalData = new System.Windows.Forms.Button();
            this.btnTestDWS = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.baudRateComboBox = new System.Windows.Forms.ComboBox();
            this.portNameComboBox = new System.Windows.Forms.ComboBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.txtScannerIP = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.rdCSV = new System.Windows.Forms.RadioButton();
            this.rdXML = new System.Windows.Forms.RadioButton();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtFolderLocation = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.txtPLCPort = new System.Windows.Forms.TextBox();
            this.drpScanerPortName = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPLCIP = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.drpWBaud = new System.Windows.Forms.ComboBox();
            this.drpWPortName = new System.Windows.Forms.ComboBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnlGridData = new System.Windows.Forms.Panel();
            this.lblConfigureSorter = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtDataLoadSensor = new System.Windows.Forms.TextBox();
            this.txtnumberofSorter = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAssignBag = new System.Windows.Forms.Label();
            this.lblLinkDB = new System.Windows.Forms.Label();
            this.lblPTLSys = new System.Windows.Forms.Label();
            this.lblPLCData = new System.Windows.Forms.Label();
            this.lblCustInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.serialSettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel4.SuspendLayout();
            this.pnlSorterOption.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel13.SuspendLayout();
            this.pnlGridData.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serialSettingsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel4.Controls.Add(this.pnlSorterOption);
            this.panel4.Controls.Add(this.panel23);
            this.panel4.Controls.Add(this.panel21);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(17, 7);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(409, 251);
            this.panel4.TabIndex = 9;
            // 
            // pnlSorterOption
            // 
            this.pnlSorterOption.Controls.Add(this.rdRobosort);
            this.pnlSorterOption.Controls.Add(this.radioButton2);
            this.pnlSorterOption.Location = new System.Drawing.Point(65, 162);
            this.pnlSorterOption.Name = "pnlSorterOption";
            this.pnlSorterOption.Size = new System.Drawing.Size(200, 34);
            this.pnlSorterOption.TabIndex = 13;
            this.pnlSorterOption.Visible = false;
            // 
            // rdRobosort
            // 
            this.rdRobosort.AutoSize = true;
            this.rdRobosort.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdRobosort.Location = new System.Drawing.Point(19, 7);
            this.rdRobosort.Name = "rdRobosort";
            this.rdRobosort.Size = new System.Drawing.Size(82, 20);
            this.rdRobosort.TabIndex = 3;
            this.rdRobosort.TabStop = true;
            this.rdRobosort.Text = "Robosort";
            this.rdRobosort.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(126, 7);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(54, 20);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Skat";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.rdSortsYes);
            this.panel23.Controls.Add(this.rdSortsNo);
            this.panel23.Location = new System.Drawing.Point(65, 115);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(200, 34);
            this.panel23.TabIndex = 11;
            // 
            // rdSortsYes
            // 
            this.rdSortsYes.AutoSize = true;
            this.rdSortsYes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSortsYes.Location = new System.Drawing.Point(19, 7);
            this.rdSortsYes.Name = "rdSortsYes";
            this.rdSortsYes.Size = new System.Drawing.Size(47, 20);
            this.rdSortsYes.TabIndex = 3;
            this.rdSortsYes.TabStop = true;
            this.rdSortsYes.Text = "Yes";
            this.rdSortsYes.UseVisualStyleBackColor = true;
            this.rdSortsYes.CheckedChanged += new System.EventHandler(this.rdSortsYes_CheckedChanged);
            // 
            // rdSortsNo
            // 
            this.rdSortsNo.AutoSize = true;
            this.rdSortsNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSortsNo.Location = new System.Drawing.Point(126, 7);
            this.rdSortsNo.Name = "rdSortsNo";
            this.rdSortsNo.Size = new System.Drawing.Size(43, 20);
            this.rdSortsNo.TabIndex = 4;
            this.rdSortsNo.TabStop = true;
            this.rdSortsNo.Text = "No";
            this.rdSortsNo.UseVisualStyleBackColor = true;
            this.rdSortsNo.CheckedChanged += new System.EventHandler(this.rdSortsNo_CheckedChanged);
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.rdPTLYes);
            this.panel21.Controls.Add(this.rdPTLNo);
            this.panel21.Location = new System.Drawing.Point(65, 41);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(200, 34);
            this.panel21.TabIndex = 9;
            // 
            // rdPTLYes
            // 
            this.rdPTLYes.AutoSize = true;
            this.rdPTLYes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdPTLYes.Location = new System.Drawing.Point(19, 7);
            this.rdPTLYes.Name = "rdPTLYes";
            this.rdPTLYes.Size = new System.Drawing.Size(47, 20);
            this.rdPTLYes.TabIndex = 3;
            this.rdPTLYes.TabStop = true;
            this.rdPTLYes.Text = "Yes";
            this.rdPTLYes.UseVisualStyleBackColor = true;
            // 
            // rdPTLNo
            // 
            this.rdPTLNo.AutoSize = true;
            this.rdPTLNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdPTLNo.Location = new System.Drawing.Point(126, 7);
            this.rdPTLNo.Name = "rdPTLNo";
            this.rdPTLNo.Size = new System.Drawing.Size(43, 20);
            this.rdPTLNo.TabIndex = 4;
            this.rdPTLNo.TabStop = true;
            this.rdPTLNo.Text = "No";
            this.rdPTLNo.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(61, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 19);
            this.label5.TabIndex = 2;
            this.label5.Text = "Do you have Sorts system?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(61, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(201, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Do you have PTL System";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.panel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel14.Controls.Add(this.btnLogOut);
            this.panel14.Controls.Add(this.btnSavePLCDiemensionalData);
            this.panel14.Controls.Add(this.btnTestDWS);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Controls.Add(this.panel16);
            this.panel14.Controls.Add(this.panel17);
            this.panel14.Controls.Add(this.panel18);
            this.panel14.Controls.Add(this.panel19);
            this.panel14.Controls.Add(this.panel20);
            this.panel14.Location = new System.Drawing.Point(7, 576);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1182, 443);
            this.panel14.TabIndex = 10;
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnLogOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOut.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.ForeColor = System.Drawing.Color.White;
            this.btnLogOut.Location = new System.Drawing.Point(621, 396);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(149, 41);
            this.btnLogOut.TabIndex = 10;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // btnSavePLCDiemensionalData
            // 
            this.btnSavePLCDiemensionalData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnSavePLCDiemensionalData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSavePLCDiemensionalData.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePLCDiemensionalData.ForeColor = System.Drawing.Color.White;
            this.btnSavePLCDiemensionalData.Location = new System.Drawing.Point(415, 396);
            this.btnSavePLCDiemensionalData.Name = "btnSavePLCDiemensionalData";
            this.btnSavePLCDiemensionalData.Size = new System.Drawing.Size(149, 41);
            this.btnSavePLCDiemensionalData.TabIndex = 9;
            this.btnSavePLCDiemensionalData.Text = "Update";
            this.btnSavePLCDiemensionalData.UseVisualStyleBackColor = false;
            this.btnSavePLCDiemensionalData.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnTestDWS
            // 
            this.btnTestDWS.BackColor = System.Drawing.Color.DimGray;
            this.btnTestDWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTestDWS.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestDWS.ForeColor = System.Drawing.Color.White;
            this.btnTestDWS.Location = new System.Drawing.Point(9, 355);
            this.btnTestDWS.Name = "btnTestDWS";
            this.btnTestDWS.Size = new System.Drawing.Size(1164, 34);
            this.btnTestDWS.TabIndex = 8;
            this.btnTestDWS.Text = "Test DWS System";
            this.btnTestDWS.UseVisualStyleBackColor = false;
            this.btnTestDWS.Click += new System.EventHandler(this.btnTestDWS_Click);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel15.Controls.Add(this.label17);
            this.panel15.Controls.Add(this.label18);
            this.panel15.Controls.Add(this.baudRateComboBox);
            this.panel15.Controls.Add(this.portNameComboBox);
            this.panel15.Location = new System.Drawing.Point(329, 11);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(844, 63);
            this.panel15.TabIndex = 4;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(492, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 16);
            this.label17.TabIndex = 5;
            this.label17.Text = "Baud Rate";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(38, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 16);
            this.label18.TabIndex = 4;
            this.label18.Text = "Port Name";
            // 
            // baudRateComboBox
            // 
            this.baudRateComboBox.FormattingEnabled = true;
            this.baudRateComboBox.Items.AddRange(new object[] {
            "9600",
            "14400",
            "19200",
            "48400",
            "56000",
            "57600"});
            this.baudRateComboBox.Location = new System.Drawing.Point(623, 24);
            this.baudRateComboBox.Name = "baudRateComboBox";
            this.baudRateComboBox.Size = new System.Drawing.Size(162, 21);
            this.baudRateComboBox.TabIndex = 2;
            // 
            // portNameComboBox
            // 
            this.portNameComboBox.FormattingEnabled = true;
            this.portNameComboBox.Location = new System.Drawing.Point(180, 24);
            this.portNameComboBox.Name = "portNameComboBox";
            this.portNameComboBox.Size = new System.Drawing.Size(164, 21);
            this.portNameComboBox.TabIndex = 0;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel16.Controls.Add(this.txtScannerIP);
            this.panel16.Controls.Add(this.label28);
            this.panel16.Controls.Add(this.rdCSV);
            this.panel16.Controls.Add(this.rdXML);
            this.panel16.Controls.Add(this.btnBrowseFolder);
            this.panel16.Controls.Add(this.label19);
            this.panel16.Controls.Add(this.txtFolderLocation);
            this.panel16.Location = new System.Drawing.Point(462, 151);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(711, 199);
            this.panel16.TabIndex = 7;
            // 
            // txtScannerIP
            // 
            this.txtScannerIP.Location = new System.Drawing.Point(585, 27);
            this.txtScannerIP.MaxLength = 15;
            this.txtScannerIP.Name = "txtScannerIP";
            this.txtScannerIP.Size = new System.Drawing.Size(121, 20);
            this.txtScannerIP.TabIndex = 13;
            this.txtScannerIP.Text = "111 . 111 .111 . 111";
            this.txtScannerIP.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(80, 154);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(95, 16);
            this.label28.TabIndex = 12;
            this.label28.Text = "Export Type. :";
            // 
            // rdCSV
            // 
            this.rdCSV.AutoSize = true;
            this.rdCSV.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdCSV.Location = new System.Drawing.Point(287, 152);
            this.rdCSV.Name = "rdCSV";
            this.rdCSV.Size = new System.Drawing.Size(53, 20);
            this.rdCSV.TabIndex = 11;
            this.rdCSV.TabStop = true;
            this.rdCSV.Text = "CSV";
            this.rdCSV.UseVisualStyleBackColor = true;
            // 
            // rdXML
            // 
            this.rdXML.AutoSize = true;
            this.rdXML.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdXML.Location = new System.Drawing.Point(186, 152);
            this.rdXML.Name = "rdXML";
            this.rdXML.Size = new System.Drawing.Size(54, 20);
            this.rdXML.TabIndex = 10;
            this.rdXML.TabStop = true;
            this.rdXML.Text = "XML";
            this.rdXML.UseVisualStyleBackColor = true;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.btnBrowseFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowseFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseFolder.ForeColor = System.Drawing.Color.White;
            this.btnBrowseFolder.Location = new System.Drawing.Point(325, 87);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(177, 29);
            this.btnBrowseFolder.TabIndex = 9;
            this.btnBrowseFolder.Text = "Browse Folder";
            this.btnBrowseFolder.UseVisualStyleBackColor = false;
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(73, 25);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(443, 19);
            this.label19.TabIndex = 4;
            this.label19.Text = "Set folder location were you want to save XML or CSV file";
            // 
            // txtFolderLocation
            // 
            this.txtFolderLocation.Location = new System.Drawing.Point(77, 53);
            this.txtFolderLocation.Multiline = true;
            this.txtFolderLocation.Name = "txtFolderLocation";
            this.txtFolderLocation.Size = new System.Drawing.Size(439, 31);
            this.txtFolderLocation.TabIndex = 8;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel17.Controls.Add(this.txtPLCPort);
            this.panel17.Controls.Add(this.drpScanerPortName);
            this.panel17.Controls.Add(this.label20);
            this.panel17.Controls.Add(this.txtPLCIP);
            this.panel17.Controls.Add(this.label21);
            this.panel17.Controls.Add(this.label22);
            this.panel17.Controls.Add(this.label23);
            this.panel17.Location = new System.Drawing.Point(9, 151);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(447, 201);
            this.panel17.TabIndex = 6;
            // 
            // txtPLCPort
            // 
            this.txtPLCPort.Location = new System.Drawing.Point(215, 105);
            this.txtPLCPort.Multiline = true;
            this.txtPLCPort.Name = "txtPLCPort";
            this.txtPLCPort.Size = new System.Drawing.Size(164, 31);
            this.txtPLCPort.TabIndex = 11;
            // 
            // drpScanerPortName
            // 
            this.drpScanerPortName.FormattingEnabled = true;
            this.drpScanerPortName.Location = new System.Drawing.Point(215, 155);
            this.drpScanerPortName.Name = "drpScanerPortName";
            this.drpScanerPortName.Size = new System.Drawing.Size(140, 21);
            this.drpScanerPortName.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(24, 156);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(179, 16);
            this.label20.TabIndex = 9;
            this.label20.Text = "Select scanner port name :";
            // 
            // txtPLCIP
            // 
            this.txtPLCIP.Location = new System.Drawing.Point(217, 61);
            this.txtPLCIP.Multiline = true;
            this.txtPLCIP.Name = "txtPLCIP";
            this.txtPLCIP.Size = new System.Drawing.Size(164, 31);
            this.txtPLCIP.TabIndex = 7;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(24, 106);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 16);
            this.label21.TabIndex = 6;
            this.label21.Text = "Port No. :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(24, 61);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 16);
            this.label22.TabIndex = 5;
            this.label22.Text = "Enter PLC IP :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(179, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(89, 19);
            this.label23.TabIndex = 4;
            this.label23.Text = "Capture IP";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel18.Controls.Add(this.label24);
            this.panel18.Controls.Add(this.label25);
            this.panel18.Controls.Add(this.drpWBaud);
            this.panel18.Controls.Add(this.drpWPortName);
            this.panel18.Location = new System.Drawing.Point(329, 82);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(844, 63);
            this.panel18.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(492, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(74, 16);
            this.label24.TabIndex = 5;
            this.label24.Text = "Baud Rate";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(38, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 16);
            this.label25.TabIndex = 4;
            this.label25.Text = "Port Name";
            // 
            // drpWBaud
            // 
            this.drpWBaud.FormattingEnabled = true;
            this.drpWBaud.Items.AddRange(new object[] {
            "9600",
            "14400",
            "19200",
            "48400",
            "56000",
            "57600"});
            this.drpWBaud.Location = new System.Drawing.Point(623, 22);
            this.drpWBaud.Name = "drpWBaud";
            this.drpWBaud.Size = new System.Drawing.Size(162, 21);
            this.drpWBaud.TabIndex = 2;
            // 
            // drpWPortName
            // 
            this.drpWPortName.FormattingEnabled = true;
            this.drpWPortName.Location = new System.Drawing.Point(180, 22);
            this.drpWPortName.Name = "drpWPortName";
            this.drpWPortName.Size = new System.Drawing.Size(162, 21);
            this.drpWPortName.TabIndex = 1;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel19.Controls.Add(this.label26);
            this.panel19.Location = new System.Drawing.Point(9, 80);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(302, 65);
            this.panel19.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(26, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(232, 19);
            this.label26.TabIndex = 4;
            this.label26.Text = "Weinghing Serial Port Setting";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panel20.Controls.Add(this.label27);
            this.panel20.Location = new System.Drawing.Point(9, 9);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(302, 65);
            this.panel20.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(26, 25);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(185, 19);
            this.label27.TabIndex = 3;
            this.label27.Text = "VMS Serial Port Setting";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.panel13.Controls.Add(this.panel4);
            this.panel13.Location = new System.Drawing.Point(7, 234);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(442, 266);
            this.panel13.TabIndex = 10;
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::SorterAPI.Properties.Resources.dwssys;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Location = new System.Drawing.Point(16, 504);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1167, 69);
            this.panel5.TabIndex = 7;
            // 
            // pnlGridData
            // 
            this.pnlGridData.BackgroundImage = global::SorterAPI.Properties.Resources.ptlimage;
            this.pnlGridData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlGridData.Controls.Add(this.lblConfigureSorter);
            this.pnlGridData.Location = new System.Drawing.Point(462, 234);
            this.pnlGridData.Name = "pnlGridData";
            this.pnlGridData.Size = new System.Drawing.Size(723, 267);
            this.pnlGridData.TabIndex = 6;
            // 
            // lblConfigureSorter
            // 
            this.lblConfigureSorter.AutoSize = true;
            this.lblConfigureSorter.BackColor = System.Drawing.Color.Transparent;
            this.lblConfigureSorter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfigureSorter.Location = new System.Drawing.Point(240, 213);
            this.lblConfigureSorter.Name = "lblConfigureSorter";
            this.lblConfigureSorter.Size = new System.Drawing.Size(229, 29);
            this.lblConfigureSorter.TabIndex = 1;
            this.lblConfigureSorter.Text = "                                    ";
            this.lblConfigureSorter.Click += new System.EventHandler(this.lblConfigureSorter_Click);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::SorterAPI.Properties.Resources.numberofsorters;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.txtDataLoadSensor);
            this.panel3.Controls.Add(this.txtnumberofSorter);
            this.panel3.Location = new System.Drawing.Point(8, 129);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1177, 102);
            this.panel3.TabIndex = 4;
            // 
            // txtDataLoadSensor
            // 
            this.txtDataLoadSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDataLoadSensor.Location = new System.Drawing.Point(954, 40);
            this.txtDataLoadSensor.Name = "txtDataLoadSensor";
            this.txtDataLoadSensor.Size = new System.Drawing.Size(164, 31);
            this.txtDataLoadSensor.TabIndex = 5;
            // 
            // txtnumberofSorter
            // 
            this.txtnumberofSorter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnumberofSorter.Location = new System.Drawing.Point(271, 40);
            this.txtnumberofSorter.Name = "txtnumberofSorter";
            this.txtnumberofSorter.Size = new System.Drawing.Size(164, 31);
            this.txtnumberofSorter.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::SorterAPI.Properties.Resources.SysDefine;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pnlClose);
            this.panel2.Location = new System.Drawing.Point(2, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1187, 71);
            this.panel2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "                             ";
            // 
            // pnlClose
            // 
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.Location = new System.Drawing.Point(1162, 3);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(27, 23);
            this.pnlClose.TabIndex = 0;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.System;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblAssignBag);
            this.panel1.Controls.Add(this.lblLinkDB);
            this.panel1.Controls.Add(this.lblPTLSys);
            this.panel1.Controls.Add(this.lblPLCData);
            this.panel1.Controls.Add(this.lblCustInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1191, 54);
            this.panel1.TabIndex = 2;
            // 
            // lblAssignBag
            // 
            this.lblAssignBag.AutoSize = true;
            this.lblAssignBag.BackColor = System.Drawing.Color.Transparent;
            this.lblAssignBag.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssignBag.Location = new System.Drawing.Point(941, 12);
            this.lblAssignBag.Name = "lblAssignBag";
            this.lblAssignBag.Size = new System.Drawing.Size(187, 29);
            this.lblAssignBag.TabIndex = 5;
            this.lblAssignBag.Text = "                             ";
            this.lblAssignBag.Click += new System.EventHandler(this.lblAssignBag_Click);
            // 
            // lblLinkDB
            // 
            this.lblLinkDB.AutoSize = true;
            this.lblLinkDB.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkDB.Location = new System.Drawing.Point(769, 11);
            this.lblLinkDB.Name = "lblLinkDB";
            this.lblLinkDB.Size = new System.Drawing.Size(145, 29);
            this.lblLinkDB.TabIndex = 4;
            this.lblLinkDB.Text = "                      ";
            this.lblLinkDB.Click += new System.EventHandler(this.lblLinkDB_Click);
            // 
            // lblPTLSys
            // 
            this.lblPTLSys.AutoSize = true;
            this.lblPTLSys.BackColor = System.Drawing.Color.Transparent;
            this.lblPTLSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPTLSys.Location = new System.Drawing.Point(607, 12);
            this.lblPTLSys.Name = "lblPTLSys";
            this.lblPTLSys.Size = new System.Drawing.Size(133, 29);
            this.lblPTLSys.TabIndex = 3;
            this.lblPTLSys.Text = "                    ";
            this.lblPTLSys.Click += new System.EventHandler(this.lblPTLSys_Click);
            // 
            // lblPLCData
            // 
            this.lblPLCData.AutoSize = true;
            this.lblPLCData.BackColor = System.Drawing.Color.Transparent;
            this.lblPLCData.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLCData.Location = new System.Drawing.Point(419, 11);
            this.lblPLCData.Name = "lblPLCData";
            this.lblPLCData.Size = new System.Drawing.Size(163, 29);
            this.lblPLCData.TabIndex = 2;
            this.lblPLCData.Text = "                         ";
            this.lblPLCData.Click += new System.EventHandler(this.lblPLCData_Click);
            // 
            // lblCustInfo
            // 
            this.lblCustInfo.AutoSize = true;
            this.lblCustInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCustInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustInfo.Location = new System.Drawing.Point(238, 12);
            this.lblCustInfo.Name = "lblCustInfo";
            this.lblCustInfo.Size = new System.Drawing.Size(145, 29);
            this.lblCustInfo.TabIndex = 1;
            this.lblCustInfo.Text = "                      ";
            this.lblCustInfo.Click += new System.EventHandler(this.lblCustInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "                           ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmSorterAPIConfigurator1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1203, 776);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pnlGridData);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterAPIConfigurator1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Sorter Configurator";
            this.Load += new System.EventHandler(this.frmSorterAPIConfigurator1_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnlSorterOption.ResumeLayout(false);
            this.pnlSorterOption.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.pnlGridData.ResumeLayout(false);
            this.pnlGridData.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serialSettingsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtnumberofSorter;
        private System.Windows.Forms.TextBox txtDataLoadSensor;
        private System.Windows.Forms.Panel pnlGridData;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Label lblCustInfo;
        private System.Windows.Forms.Label lblPLCData;
        private System.Windows.Forms.Label lblPTLSys;
        private System.Windows.Forms.Label lblLinkDB;
        private System.Windows.Forms.Label lblAssignBag;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rdPTLNo;
        private System.Windows.Forms.RadioButton rdPTLYes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button btnSavePLCDiemensionalData;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Button btnTestDWS;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox baudRateComboBox;
        private System.Windows.Forms.ComboBox portNameComboBox;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.RadioButton rdCSV;
        private System.Windows.Forms.RadioButton rdXML;
        private System.Windows.Forms.Button btnBrowseFolder;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtFolderLocation;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox txtPLCPort;
        private System.Windows.Forms.ComboBox drpScanerPortName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtPLCIP;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox drpWBaud;
        private System.Windows.Forms.ComboBox drpWPortName;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblConfigureSorter;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.RadioButton rdSortsYes;
        private System.Windows.Forms.RadioButton rdSortsNo;
        private System.Windows.Forms.BindingSource serialSettingsBindingSource;
        private System.Windows.Forms.TextBox txtScannerIP;
        private System.Windows.Forms.Panel pnlSorterOption;
        private System.Windows.Forms.RadioButton rdRobosort;
        private System.Windows.Forms.RadioButton radioButton2;
    }
}