﻿namespace SorterAPI
{
    partial class frmSorterPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblHeading = new System.Windows.Forms.Label();
            this.dgvHubList = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.rdNationalHub = new System.Windows.Forms.RadioButton();
            this.rdStateHub = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.drpBinDown = new System.Windows.Forms.ComboBox();
            this.lblDown = new System.Windows.Forms.Label();
            this.drpBinUp = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblUp = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Teal;
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(687, 429);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Set Sorter Plan";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel1.Controls.Add(this.lblHeading);
            this.panel1.Controls.Add(this.dgvHubList);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(18, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(652, 399);
            this.panel1.TabIndex = 20;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblHeading.Location = new System.Drawing.Point(451, 12);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(0, 18);
            this.lblHeading.TabIndex = 67;
            // 
            // dgvHubList
            // 
            this.dgvHubList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHubList.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHubList.Location = new System.Drawing.Point(7, 12);
            this.dgvHubList.Name = "dgvHubList";
            this.dgvHubList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHubList.Size = new System.Drawing.Size(266, 374);
            this.dgvHubList.TabIndex = 66;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel3.Controls.Add(this.radioButton3);
            this.panel3.Controls.Add(this.rdNationalHub);
            this.panel3.Controls.Add(this.rdStateHub);
            this.panel3.Location = new System.Drawing.Point(319, 42);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(298, 52);
            this.panel3.TabIndex = 63;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.Transparent;
            this.radioButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.ForeColor = System.Drawing.Color.Black;
            this.radioButton3.Location = new System.Drawing.Point(224, 12);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(54, 20);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.Text = "Hub";
            this.radioButton3.UseVisualStyleBackColor = false;
            // 
            // rdNationalHub
            // 
            this.rdNationalHub.AutoSize = true;
            this.rdNationalHub.BackColor = System.Drawing.Color.Transparent;
            this.rdNationalHub.Checked = true;
            this.rdNationalHub.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdNationalHub.ForeColor = System.Drawing.Color.Black;
            this.rdNationalHub.Location = new System.Drawing.Point(10, 12);
            this.rdNationalHub.Name = "rdNationalHub";
            this.rdNationalHub.Size = new System.Drawing.Size(116, 20);
            this.rdNationalHub.TabIndex = 1;
            this.rdNationalHub.TabStop = true;
            this.rdNationalHub.Text = "National Hub";
            this.rdNationalHub.UseVisualStyleBackColor = false;
            this.rdNationalHub.CheckedChanged += new System.EventHandler(this.rdNationalHub_CheckedChanged);
            // 
            // rdStateHub
            // 
            this.rdStateHub.AutoSize = true;
            this.rdStateHub.BackColor = System.Drawing.Color.Transparent;
            this.rdStateHub.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdStateHub.ForeColor = System.Drawing.Color.Black;
            this.rdStateHub.Location = new System.Drawing.Point(127, 12);
            this.rdStateHub.Name = "rdStateHub";
            this.rdStateHub.Size = new System.Drawing.Size(94, 20);
            this.rdStateHub.TabIndex = 2;
            this.rdStateHub.Text = "State Hub";
            this.rdStateHub.UseVisualStyleBackColor = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.drpBinDown);
            this.groupBox2.Controls.Add(this.lblDown);
            this.groupBox2.Controls.Add(this.drpBinUp);
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.lblUp);
            this.groupBox2.Controls.Add(this.txtSearch);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(299, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(337, 257);
            this.groupBox2.TabIndex = 70;
            this.groupBox2.TabStop = false;
            // 
            // drpBinDown
            // 
            this.drpBinDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinDown.FormattingEnabled = true;
            this.drpBinDown.Location = new System.Drawing.Point(171, 154);
            this.drpBinDown.Name = "drpBinDown";
            this.drpBinDown.Size = new System.Drawing.Size(121, 23);
            this.drpBinDown.TabIndex = 70;
            this.drpBinDown.Visible = false;
            // 
            // lblDown
            // 
            this.lblDown.AutoSize = true;
            this.lblDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.lblDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDown.ForeColor = System.Drawing.Color.Black;
            this.lblDown.Location = new System.Drawing.Point(17, 157);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(120, 16);
            this.lblDown.TabIndex = 71;
            this.lblDown.Text = "Select Bin Down";
            this.lblDown.Visible = false;
            // 
            // drpBinUp
            // 
            this.drpBinUp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinUp.FormattingEnabled = true;
            this.drpBinUp.Location = new System.Drawing.Point(171, 122);
            this.drpBinUp.Name = "drpBinUp";
            this.drpBinUp.Size = new System.Drawing.Size(121, 23);
            this.drpBinUp.TabIndex = 68;
            this.drpBinUp.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnExit.Location = new System.Drawing.Point(177, 205);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(82, 29);
            this.btnExit.TabIndex = 62;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblUp
            // 
            this.lblUp.AutoSize = true;
            this.lblUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.lblUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUp.ForeColor = System.Drawing.Color.Black;
            this.lblUp.Location = new System.Drawing.Point(17, 125);
            this.lblUp.Name = "lblUp";
            this.lblUp.Size = new System.Drawing.Size(102, 16);
            this.lblUp.TabIndex = 69;
            this.lblUp.Text = "Select Bin Up";
            this.lblUp.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(171, 88);
            this.txtSearch.MaxLength = 6;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(147, 21);
            this.txtSearch.TabIndex = 64;
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnSave.Location = new System.Drawing.Point(71, 205);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(82, 29);
            this.btnSave.TabIndex = 61;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(17, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 16);
            this.label9.TabIndex = 65;
            this.label9.Text = "Assign city to sorter";
            // 
            // frmSorterPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SorterAPI.Properties.Resources.diemension_HihgResolution;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(706, 448);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterPlan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sorter Plan";
            this.Load += new System.EventHandler(this.frmSorterPlan_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton rdNationalHub;
        private System.Windows.Forms.RadioButton rdStateHub;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView dgvHubList;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox drpBinUp;
        private System.Windows.Forms.Label lblUp;
        private System.Windows.Forms.ComboBox drpBinDown;
        private System.Windows.Forms.Label lblDown;

    }
}