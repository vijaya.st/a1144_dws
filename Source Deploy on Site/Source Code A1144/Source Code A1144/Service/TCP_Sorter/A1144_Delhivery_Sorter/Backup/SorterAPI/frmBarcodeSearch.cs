﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using System.Data.SqlClient;
using System.Configuration;

namespace SorterAPI
{
    public partial class frmBarcodeSearch : Form
    {
        public frmBarcodeSearch()
        {
            InitializeComponent();
            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 300, //should be (0,0)
            //           Screen.PrimaryScreen.Bounds.Y + 200);
            //this.TopMost = true;
            //this.StartPosition = FormStartPosition.Manual;
            //string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = System.Convert.ToInt32(screenWidth);
            //int H = System.Convert.ToInt32(screenHeight);

            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);
        }
        DataTable dtArmstrong = new DataTable();
      //  DataTable dtEMSOut = new DataTable();
        DataTable dtEMSIn = new DataTable();

        string conGPO = ConfigurationManager.ConnectionStrings["DWSConnectionString"].ConnectionString;
        private void btnSearchBarcode_Click(object sender, EventArgs e)
        {
            clsAPI obj = new clsAPI();
            obj.barcode = txtBarcodeSearch.Text;
            dtArmstrong = clsAPI.GetSearchBarcodeData(obj);

            //string strInsert = "select * from  [DWS].[dbo].[EMSOUT] where ARTICLENUMBER = '" + txtBarcodeSearch.Text + "'";//([ARTICLENUMBER],[P_WEIGHT],[P_LENGTH]";
            //SqlConnection cn = new SqlConnection(conGPO);
            //SqlCommand cmd = new SqlCommand();
            //cmd.CommandType = CommandType.Text;
            //cmd.CommandText = strInsert;
            //cn.Open();
            //cmd.Connection = cn;
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(dtEMSOut);
            //cn.Close();

            string strInsert1 = "select * from  [DWS].[dbo].[EMSIn] where ARTICLENUMBER = '" + txtBarcodeSearch.Text + "'";//([ARTICLENUMBER],[P_WEIGHT],[P_LENGTH]";
            SqlConnection cn1 = new SqlConnection(conGPO);
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandType = CommandType.Text;
            cmd1.CommandText = strInsert1;
            cn1.Open();
            cmd1.Connection = cn1;
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            da1.Fill(dtEMSIn);
            cn1.Close();
            
            dgvArmstrong.RowsDefaultCellStyle.BackColor = Color.White;
            dgvArmstrong.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(115, 205, 212);           
            Font f = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
            dgvArmstrong.DefaultCellStyle.Font = f;            
            dgvArmstrong.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Bold);
            dgvArmstrong.AutoResizeColumns();
            dgvArmstrong.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgvArmstrong.RowHeadersVisible = false;
            dgvArmstrong.DataSource = dtArmstrong;

           // dgvEMSOut.RowsDefaultCellStyle.BackColor = Color.White;
           // dgvEMSOut.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(115, 205, 212);
           //// Font f = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
           // dgvEMSOut.DefaultCellStyle.Font = f;
           // dgvEMSOut.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Bold);
           // dgvEMSOut.AutoResizeColumns();
           // dgvEMSOut.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
           // dgvEMSOut.RowHeadersVisible = false;
           // dgvEMSOut.DataSource = dtEMSOut;

            dgvEMSIn.RowsDefaultCellStyle.BackColor = Color.White;
            dgvEMSIn.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(115, 205, 212);
            // Font f = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
            dgvEMSIn.DefaultCellStyle.Font = f;
            dgvEMSIn.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Bold);
            dgvEMSIn.AutoResizeColumns();
            dgvEMSIn.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgvEMSIn.RowHeadersVisible = false;
            dgvEMSIn.DataSource = dtEMSIn;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
