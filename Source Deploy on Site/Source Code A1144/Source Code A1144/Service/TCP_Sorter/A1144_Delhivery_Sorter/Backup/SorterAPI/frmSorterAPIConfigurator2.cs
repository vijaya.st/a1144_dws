﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class frmSorterAPIConfigurator2 : Form
    {
        DataTable dtPLCData = new DataTable();

        public frmSorterAPIConfigurator2()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                          Screen.PrimaryScreen.Bounds.Y + 100);

            //string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = System.Convert.ToInt32(screenWidth);
            //int H = System.Convert.ToInt32(screenHeight);

            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);

            //this.TopMost = true;
            //this.StartPosition = FormStartPosition.Manual;
            //this.MaximumSize = new Size(1200, 900);
        }

        private void frmSorterAPIConfigurator2_Load(object sender, EventArgs e)
        {
          //  string str_directory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
           // panel1.BackgroundImage = Image.FromFile(str_directory + "\\images\\plc_data_banner.png");
            //panel2.BackgroundImage = Image.FromFile(str_directory + "\\images\\plc_data_middle.png"); 
           // pnlClose.BackgroundImage = Image.FromFile(str_directory + "\\images\\close1.png"); 

            getPLCData();
        }
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblCustInfo_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator4 f4 = new frmSorterAPIConfigurator4();
            f4.Show();
            this.Close();
        }

        private void lblPLCData_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator2 f2 = new frmSorterAPIConfigurator2();
            f2.Show();
            this.Close();
        }

        private void lblPTLSys_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator3 f3 = new frmSorterAPIConfigurator3();
            f3.Show();
            this.Close();
        }

        private void lblLinkDB_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator5 f5 = new frmSorterAPIConfigurator5();
            f5.Show();
            this.Close();
        }

        private void lblBagAssignment_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator6 f6 = new frmSorterAPIConfigurator6();
            f6.Show();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator1 f1 = new frmSorterAPIConfigurator1();
            f1.Show();
            this.Close();
        }

        private void pnlClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void getPLCData()
        {
            dtPLCData = clsAPI.getPLCData();
            if (dtPLCData.Rows.Count > 0)
            {
                btnPLCDynamicDataSave.Text = "Update";

                txtDistanceTravelled.Text = dtPLCData.Rows[0]["PullyDistance"].ToString();
                txtEncoderPPR.Text = dtPLCData.Rows[0]["PPREncoder"].ToString();
                txtSpeedofConveyor.Text = dtPLCData.Rows[0]["ConveyorSpeed"].ToString();
                txtArmOpeningTime.Text = dtPLCData.Rows[0]["ArmActivationTime"].ToString();
                txtLinearDistance.Text = dtPLCData.Rows[0]["LinearDistance"].ToString();
                txtEnterActivationDelay.Text = dtPLCData.Rows[0]["ActConstantDistance"].ToString();
                txtEnterDeactivationDelay.Text = dtPLCData.Rows[0]["DeactConstantDistance"].ToString();
                txtPLCScanTime.Text = dtPLCData.Rows[0]["PLCScanTime"].ToString();
            }
        }

        private void btnPLCDynamicDataSave_Click(object sender, EventArgs e)
        {
            clsAPI obj = new clsAPI();
            if (txtDistanceTravelled.Text == "" || txtEncoderPPR.Text == "" || txtSpeedofConveyor.Text == ""
                || txtArmOpeningTime.Text == "" || txtLinearDistance.Text == "" || txtEnterActivationDelay.Text == ""
            || txtEnterDeactivationDelay.Text == "" || txtPLCScanTime.Text == "")
            {
                MessageBox.Show("All value are mandatory");
                //txtDistanceTravelled.Focus();
                // txtDistanceTravelled.BackColor = Color.Red;

            }
            else
            {
                obj.PullyDistance = Convert.ToInt32(txtDistanceTravelled.Text);
                obj.PPREncoder = Convert.ToInt32(txtEncoderPPR.Text);
                obj.ConveyorSpeed = Convert.ToInt32(txtSpeedofConveyor.Text);
                obj.ActivetionTime = Convert.ToInt32(txtArmOpeningTime.Text);
                obj.LinearDistance = Convert.ToInt32(txtLinearDistance.Text);
                obj.ActConstantDistance = Convert.ToInt32(txtEnterActivationDelay.Text);
                obj.DeactConstantDistance = Convert.ToInt32(txtEnterDeactivationDelay.Text);
                obj.PLCScanTime = Convert.ToInt32(txtPLCScanTime.Text);
                if (btnPLCDynamicDataSave.Text == "Update")
                {
                    int i = clsAPI.UpdatePLCDynamicData(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully updated");
                        //txtDistanceTravelled.Text = "";
                        //txtEncoderPPR.Text = "";
                        //txtSpeedofConveyor.Text = "";
                        //txtArmOpeningTime.Text = "";
                        //txtLinearDistance.Text = "";
                        //txtEnterActivationDelay.Text = "";
                        //txtEnterDeactivationDelay.Text = "";
                        //txtPLCScanTime.Text = "";
                    }
                }
                else
                {
                    int i = clsAPI.InsertPLCDynamicData(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully Saved");
                        //txtDistanceTravelled.Text = "";
                        //txtEncoderPPR.Text = "";
                        //txtSpeedofConveyor.Text = "";
                        //txtArmOpeningTime.Text = "";
                        //txtLinearDistance.Text = "";
                        //txtEnterActivationDelay.Text = "";
                        //txtEnterDeactivationDelay.Text = "";
                        //txtPLCScanTime.Text = "";
                    }
                }
            }
        }

        private void txtDistanceTravelled_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtEncoderPPR_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtSpeedofConveyor_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtArmOpeningTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtLinearDistance_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtEnterActivationDelay_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtEnterDeactivationDelay_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtPLCScanTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btnReset2_Click(object sender, EventArgs e)
        {
            txtDistanceTravelled.Text = "";
            txtEncoderPPR.Text = "";
            txtSpeedofConveyor.Text = "";
            txtArmOpeningTime.Text = "";
            txtLinearDistance.Text = "";
            txtEnterActivationDelay.Text = "";
            txtEnterDeactivationDelay.Text = "";
            txtPLCScanTime.Text = "";
        }

    }
}