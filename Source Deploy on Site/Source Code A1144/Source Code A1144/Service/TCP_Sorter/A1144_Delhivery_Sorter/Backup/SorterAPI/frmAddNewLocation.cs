﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class frmAddNewLocation : Form
    {
        public frmAddNewLocation()
        {
            InitializeComponent();
        }

        private void frmAddNewLocation_Load(object sender, EventArgs e)
        {
            DataTable dtCircle = new DataTable();
           
            dtCircle = clsAPI.GetCircle();
            DataRow dr;
            dr = dtCircle.NewRow();
            dr.ItemArray = new object[] { 0, "--Select--" };
            dtCircle.Rows.InsertAt(dr, 0);
            drpCircle.DisplayMember = "CircleName";
            drpCircle.ValueMember = "CircleId";
            drpCircle.DataSource = dtCircle;
        }

        private void drpCircle_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtRegion = new DataTable();
            clsAPI obj = new clsAPI();

            obj.CircleId = Convert.ToInt32(drpCircle.SelectedValue);
            dtRegion = clsAPI.GetRegion(obj);
            DataRow dr;
            dr = dtRegion.NewRow();
            dr.ItemArray = new object[] { 0, "--Select--" };
            dtRegion.Rows.InsertAt(dr, 0);

            drpRegion.DisplayMember = "Region";
            drpRegion.ValueMember = "RegionId";
            drpRegion.DataSource = dtRegion;
        }

        private void drpRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtDivision = new DataTable();
            clsAPI obj = new clsAPI();

            obj.RegionId = Convert.ToInt32(drpRegion.SelectedValue);
            dtDivision = clsAPI.GetDivision(obj);
            DataRow dr;
            dr = dtDivision.NewRow();
            dr.ItemArray = new object[] { 0, "--Select--" };
            dtDivision.Rows.InsertAt(dr, 0);

            drpDivision.DisplayMember = "Division";
            drpDivision.ValueMember = "DivisionId";
            drpDivision.DataSource = dtDivision;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (drpCircle.Text != "--Select--" && drpRegion.Text != "--Select--" && drpDivision.Text != "--Select--" && txtOffice.Text != "" && txtPincode.Text != "")
            {
                clsAPI obj = new clsAPI();
                obj.DivisionId = Convert.ToInt32(drpDivision.SelectedValue);
                obj.DestinationOffice = txtOffice.Text;
                obj.Pincode = Convert.ToInt32(txtPincode.Text);
                int i = clsAPI.SaveNewLocation(obj);
                if (i > 0)
                {
                    MessageBox.Show("Data successfully Insert");
                }
            }
            else
            {
                MessageBox.Show("All value are mandatory","",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
        }

        private void txtPincode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
