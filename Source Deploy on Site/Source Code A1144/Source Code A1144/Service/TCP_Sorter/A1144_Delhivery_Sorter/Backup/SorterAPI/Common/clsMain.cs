﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SorterAPI.Common;

namespace GPOMumbai
{
   public class clsMain
    {
        public string id { get; set; }
        public string RackNo { get; set; }
        public string Location { get; set; }
        public string Pin { get; set; }
        public int SorterNo { get; set; }
        public int BayNo { get; set; }

        public static DataTable GetCmbLocation(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add("@SorterNo", SqlDbType.Int).Value = obj.RackNo.ToString();
            return clsDataAccess.GetDataTable("sp_CmbLocation_get", cmd);          
        }

        public static int UpdateRackNo(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();
            //  cmd.Parameters.Add("@Pin", SqlDbType.VarChar).Value = obj.Pin.ToString();
            cmd.Parameters.Add("@RackNo", SqlDbType.VarChar).Value = obj.RackNo.ToString();
            cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = obj.Location.ToString();
            return clsDataAccess.ExecuteNonQuery("sp_RackNo_Update", cmd);
        }

        public static DataTable GetValidateRackNo(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add("@RackNo", SqlDbType.VarChar).Value = obj.RackNo.ToString();
            cmd.Parameters.Add("@SorterNo", SqlDbType.Int).Value = obj.SorterNo;
            return clsDataAccess.GetDataTable("sp_ValidateRackNo", cmd);
        }

        public static int DeleteLocation(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = obj.Location.ToString();
            return clsDataAccess.ExecuteNonQuery("sp_Location_Delete", cmd);
        }

        public static DataTable GetPinCode(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add("@Id", SqlDbType.Int).Value = obj.id;
            return clsDataAccess.GetDataTable("sp_Pincode_get", cmd);
        }

        public static int InsertCityPinSorter(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add("@SorterNo", SqlDbType.Int).Value = obj.SorterNo;
            cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = obj.Location;
            cmd.Parameters.Add("@Pin", SqlDbType.Int).Value = obj.Pin;
            cmd.Parameters.Add("@RackNo", SqlDbType.VarChar).Value = obj.RackNo;

            return clsDataAccess.ExecuteNonQuery("sp_CityPinSorter_Insert", cmd);
        }


        public static DataTable ValidateCityPin(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();
           // cmd.Parameters.Add("@SorterNo", SqlDbType.Int).Value = obj.SorterNo;
            cmd.Parameters.Add("@PinCode", SqlDbType.Int).Value = obj.Pin;
            cmd.Parameters.Add("@RackNo", SqlDbType.VarChar).Value = obj.RackNo;

            return clsDataAccess.GetDataTable("sp_ValidateCityPin_get", cmd);
        }

        public static int UpdateBayNo(clsMain obj)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.Add("@SorterNo", SqlDbType.Int).Value = obj.BayNo;
            cmd.Parameters.Add("@PinCode", SqlDbType.Int).Value = Convert.ToInt32(obj.Pin);

            return clsDataAccess.ExecuteNonQuery("sp_BayNo_Update", cmd);
        } 
    }
}
