﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class frmUserCreation : Form
    {
        //Button[] buttonArray = new Button[32];
        LinkLabel[] lnklbl = new LinkLabel[30];
        DataTable dtUserList = new DataTable();
        int mainId;
        ErrorLog oErrorLog = new ErrorLog();
        public frmUserCreation()
        {
            InitializeComponent();
        }

        private void frmUserCreation_Load(object sender, EventArgs e)
        {
            
        }

        void ButtonClickOneEvent(object sender, EventArgs e)
        {
            LinkLabel lbl = sender as LinkLabel;
            if (lbl != null)
            {
                int UId = (int)lbl.Tag;
                BindUserData(UId);

                //switch ((int)lbl.Tag)
                //switch (UId)
                //{
                //    case 1:
                //        BindUserData(UId);
                //       // clsAPI obj = new clsAPI();
                //        //obj.Id = UId;
                        
                //        break;
                //    case 1:
                //        BindUserData(UId);
                //        // clsAPI obj = new clsAPI();
                //        //obj.Id = UId;

                //        break;
                //}
            }           
        }

        private void BindUserData(int getid)
        {
            try
            {
                dtUserList = clsAPI.getUserList(getid);
                mainId = getid;
                for (int j = 0; j < dtUserList.Rows.Count; j++)
                {
                    btnUserSave.Text = "Update";

                    txtUser.Text = dtUserList.Rows[j]["User_LoginName"].ToString();
                    txtPass.Text = "";
                    if (Convert.ToInt32(dtUserList.Rows[j]["User_IsActive"]) == 1)
                    {
                        chkActive.Checked = true;
                    }
                    int menu1 = Convert.ToInt32(dtUserList.Rows[j]["MenuId"]);
                    if (menu1 == 1)
                    {
                        if (Convert.ToInt32(dtUserList.Rows[j]["CanView"]) == 1)
                        {
                            chkBatchView.Checked = true;
                        }
                        else { chkBatchView.Checked = false; }
                        if (Convert.ToInt32(dtUserList.Rows[j]["CanEdit"]) == 1)
                        {
                            chkBatchEdit.Checked = true;
                        }
                        else { chkBatchEdit.Checked = false; }
                    }
                    int menu2 = Convert.ToInt32(dtUserList.Rows[j]["MenuId"]);
                    if (menu2 == 2)
                    {
                        if (Convert.ToInt32(dtUserList.Rows[j]["CanView"]) == 1)
                        {
                            chkLocationView.Checked = true;
                        }
                        else { chkLocationView.Checked = false; }
                        if (Convert.ToInt32(dtUserList.Rows[j]["CanEdit"]) == 1)
                        {
                            chkLocationEdit.Checked = true;
                        }
                        else { chkLocationEdit.Checked = false; }
                    }
                    int menu3 = Convert.ToInt32(dtUserList.Rows[j]["MenuId"]);
                    if (menu3 == 3)
                    {
                        if (Convert.ToInt32(dtUserList.Rows[j]["CanView"]) == 1)
                        {
                            chkReportsView.Checked = true;
                        }
                        else { chkReportsView.Checked = false; }
                        if (Convert.ToInt32(dtUserList.Rows[j]["CanEdit"]) == 1)
                        {
                            chkReportsEdit.Checked = true;
                        }
                        else { chkReportsEdit.Checked = false; }
                    }
                }
            }
            catch(Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void btnUserSave_Click(object sender, EventArgs e)
        {
            try
            {
                clsAPI obj = new clsAPI();
                if (btnUserSave.Text != "Update")
                {
                    if (txtUser.Text != "" && txtPass.Text != "")
                    {

                        if (txtPass.Text == txtPassconfirmation.Text)
                        {
                            string EncryptionKey = string.Empty;
                            EncryptionKey = SSTCryptographer.Encrypt(txtPass.Text.ToString().Trim(), "");

                            obj.UserName = txtUser.Text;
                            obj.UserPass = EncryptionKey;
                            if (chkActive.Checked == true)
                            {
                                obj.IsActive = 1;
                            }
                            else
                            {
                                obj.IsActive = 0;
                            }
                            if (chkBatchEdit.Checked == true)
                            {
                                obj.BatchEdit = 1;
                            }
                            else
                            {
                                obj.BatchEdit = 0;
                            }
                            if (chkBatchView.Checked == true)
                            {
                                obj.BatchView = 1;
                            }
                            else
                            {
                                obj.BatchView = 0;
                            }
                            if (chkLocationEdit.Checked == true)
                            {
                                obj.LocationEdit = 1;
                            }
                            else
                            {
                                obj.LocationEdit = 0;
                            }
                            if (chkLocationView.Checked == true)
                            {
                                obj.LocationView = 1;
                            }
                            else
                            {
                                obj.LocationView = 0;
                            }

                            if (chkReportsView.Checked == true)
                            {
                                obj.ReportView = 1;
                            }
                            else
                            {
                                obj.ReportView = 0;
                            }

                            int i = clsAPI.InsertNewUser(obj);
                            if (i > 0)
                            {
                                MessageBox.Show("Data successfully inserted..");
                                this.Close();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Passwords do not match.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Username and Password are mandatory fields..");
                    }
                }
                else
                {
                    if (txtUser.Text != "")
                    {
                        obj.Id = mainId;

                        obj.UserName = txtUser.Text;
                        if (txtPass.Text != "")
                        {
                            if (txtPass.Text == txtPassconfirmation.Text)
                            {
                                string EncryptionKey = string.Empty;
                                EncryptionKey = SSTCryptographer.Encrypt(txtPass.Text.ToString().Trim(), "");
                                obj.UserPass = EncryptionKey;
                            }
                            else
                            {
                                MessageBox.Show("Passwords do not match.");
                                return;
                            }

                        }
                        if (chkActive.Checked == true)
                        {
                            obj.IsActive = 1;
                        }
                        else
                        {
                            obj.IsActive = 0;
                        }
                        if (chkBatchEdit.Checked == true)
                        {
                            obj.BatchEdit = 1;
                        }
                        else
                        {
                            obj.BatchEdit = 0;
                        }
                        if (chkBatchView.Checked == true)
                        {
                            obj.BatchView = 1;
                        }
                        else
                        {
                            obj.BatchView = 0;
                        }
                        if (chkLocationEdit.Checked == true)
                        {
                            obj.LocationEdit = 1;
                        }
                        else
                        {
                            obj.LocationEdit = 0;
                        }
                        if (chkLocationView.Checked == true)
                        {
                            obj.LocationView = 1;
                        }
                        else
                        {
                            obj.LocationView = 0;
                        }

                        if (chkReportsView.Checked == true)
                        {
                            obj.ReportView = 1;
                        }
                        else
                        {
                            obj.ReportView = 0;
                        }
                        int j = clsAPI.UpdateNewUser(obj);
                        if (j > 0)
                        {
                            MessageBox.Show("Data successfully updated..");
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Username is mandatory..");
                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmUserCreation_Paint(object sender, PaintEventArgs e)
        {
            int passid = 0;
            dtUserList = clsAPI.getUserList(passid);
            int lblx = 25;
            for (int i = 0; i < dtUserList.Rows.Count; i++)
            {
                lnklbl[i] = new LinkLabel();
                lnklbl[i].Name = "lblU" + i;
                lnklbl[i].ForeColor = Color.Black;                
                lnklbl[i].Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Bold);
                lnklbl[i].Location = new Point(10, lblx);
                lnklbl[i].Text = dtUserList.Rows[i]["User_LoginName"].ToString();
                lnklbl[i].Click += new EventHandler(ButtonClickOneEvent);
                lnklbl[i].Tag = Convert.ToInt32(dtUserList.Rows[i]["User_Id"]);

                grpUserList.Controls.Add(lnklbl[i]);
               
                lblx = lblx + 25;
            }
        }     
    }
}
