﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI_Member;
using SorterAPI.Common;
using System.IO.Ports;
using System.Data.SqlClient;
using System.IO;

namespace SorterAPI
{
    public partial class frmLogin : Form
    {
        #region Local Variables
        string UserName = string.Empty;
        string UserPwd = string.Empty;
        string sPwd = string.Empty;
        string str_directory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
        //bool PropixChk;

        //SerialPort VMSPort = new SerialPort();
        //SerialPort MTPort = new SerialPort();
        //SerialPort BarcodePort = new SerialPort();

        public static class LoginInfo // Login User id use anywhere like session
        {
            public static int UserID;
        }


        #endregion

        #region Form & Window Event
        public frmLogin()
        {
            InitializeComponent();

            //btnLogin.Image = Image.FromFile(str_directory + "\\images\\LoginBtn.png");
            //btnLogin.TabStop = false;
            //btnLogin.FlatStyle = FlatStyle.Flat;
            //btnLogin.FlatAppearance.BorderSize = 0;


            //btnForgetPwd.Image = Image.FromFile(str_directory + "\\images\\ForgetPwdBtn.png");
            //btnForgetPwd.TabStop = false;
            //btnForgetPwd.FlatStyle = FlatStyle.Flat;
            //btnForgetPwd.FlatAppearance.BorderSize = 0;


            //btnExit.Image = Image.FromFile(str_directory + "\\images\\ExitBtn.png");
            //btnExit.TabStop = false;
            //btnExit.FlatStyle = FlatStyle.Flat;
            //btnExit.FlatAppearance.BorderSize = 0;


            this.txtUsername.Size = new System.Drawing.Size(383, 45);
            this.txtUsername.MinimumSize = new Size(383, 45);
            this.txtPassword.Size = new System.Drawing.Size(383, 45);            

           //PropixChk = API.PropixAPI.StartApplication(@"E:\Propix_data\Release");
           //if (PropixChk == true)
           //{
           //    MessageBox.Show("Propix Start");
           //}
           //else
           //{
           //    MessageBox.Show("Propix Error");
           //}
           // API.PropixAPI.StartApplication(@"E:\Propix\cam\cam\Release"); // start Propix exe
            txtPassword.PasswordChar = '*';
            txtPassword.MaxLength = 10;
           // this.txtPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(CheckEnter);
        }

        private void frmLogin_Paint(object sender, PaintEventArgs e)
        {
            System.Drawing.Rectangle rect = new Rectangle(txtUsername.Location.X, txtUsername.Location.Y, txtUsername.ClientSize.Width, txtUsername.ClientSize.Height);

            rect.Inflate(1, 1); // border thickness
            System.Windows.Forms.ControlPaint.DrawBorder(e.Graphics, rect, Color.Red, ButtonBorderStyle.Solid);
        }


        private void frmLogin_Load(object sender, EventArgs e)
        {
            try
            {

            SqlConnection cn = new SqlConnection(clsDataAccess.GetConnectionString());
            cn.Open();
            
            DataTable dtport = new DataTable();
            dtport = clsAPI.getPort();

            //VMSPort.PortName = "COM7";
            //   // VMSPort.PortName = dtport.Rows[0]["PortName"].ToString();
            //    VMSPort.BaudRate = 9600;
            //    VMSPort.Parity = Parity.None;
            //    VMSPort.StopBits = StopBits.One;
            //    VMSPort.Open();

            //    MTPort.PortName = dtport.Rows[0]["WPortname"].ToString();
            //    MTPort.BaudRate = 19200;
            //    MTPort.Parity = Parity.None;
            //    MTPort.StopBits = StopBits.One;
            //    MTPort.Open();

            //    BarcodePort.PortName = dtport.Rows[0]["ScannerPort"].ToString();
            //    BarcodePort.BaudRate = 57600;
            //    BarcodePort.Parity = Parity.None;
            //    BarcodePort.StopBits = StopBits.One;
            //    BarcodePort.Open();


                //if (VMSPort.IsOpen == false)
                //{
                //}
                //if (MTPort.IsOpen == false)
                //{
                //}
                //if (BarcodePort.IsOpen == false)
                //{
                //}

                //foreach (string portname in SerialPort.GetPortNames())
                //{
                //    var sp = new SerialPort(portname, 4800, Parity.Odd, 8, StopBits.One);
                //    try
                //    {
                //        sp.Open();
                //        sp.Write("Your known command to device");
                //       // Thread.Sleep(500);
                //        string received = sp.ReadLine();

                //        if (received == "expected response")
                //        {
                //            Console.WriteLine("device connected to: " + portname);
                //            break;
                //        }
                //    }
                //    catch (Exception)
                //    {
                //        Console.WriteLine("device NOT connected to: " + portname);
                //    }
                //    finally
                //    {
                //        sp.Close();
                //    }
                //}


                btnLogin.Enabled = true;
                cn.Close();

                DateTime date = DateTime.Now;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);                 
                string lastDayOfMonth = firstDayOfMonth.AddMonths(-1).AddDays(-1).ToString("yyyy-MM-dd");

              int rtn = clsAPI.TakeBAKnDeleteLastMonthBeforeData();
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    MessageBox.Show("There is some problem in sql server connection establish !!","",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(ex.Message);
                }                
            }
          
        }

        protected override void WndProc(ref Message m)
        {
            const int WM_SYSCommonD = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (m.Msg)
            {
                case WM_SYSCommonD:
                    int Commond = m.WParam.ToInt32() & 0xfff0;
                    if (Commond == SC_MOVE)
                        return;
                    break;
            }
            base.WndProc(ref m);
        }
        #endregion

        #region Button Press Events
        private void btnLogin_Click(object sender, EventArgs e)
        {
            //if (PropixChk == true)
            //{

                string EncryptionKey = string.Empty;
                EncryptionKey = SSTCryptographer.Encrypt(txtPassword.Text.ToString().Trim(), "");
                //Debug.Write(" Password " + EncryptionKey);

                UserName = txtUsername.Text.ToString().Trim();
                UserPwd = EncryptionKey;
                //sPwd = SSTCryptographer.Encrypt(UserPwd,"");
                //sPwd = SSTCryptographer.Encrypt(UserPwd, "");
                //ValidateLoginUser(UserName, SSTCryptographer.Decrypt(sPwd, ""));
                ValidateLoginUser(UserName, UserPwd);

                //DirectoryInfo source = new DirectoryInfo(@"D:\imgdelete");

                //// Get info of each file into the directory
                //foreach (FileInfo fi in source.GetFiles())
                //{
                //    var creationTime = fi.CreationTime;

                //    if (creationTime < (DateTime.Now - new TimeSpan(0, 0, 3, 0)))    //(90, 0, 0, 0)))
                //    {
                //        fi.Delete();
                //    }
                //}
            //}
            //else
            //{
            //    MessageBox.Show("There is some problem in system please contact with administrator!!");
            //}
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
            Application.Exit();
            System.Environment.Exit(1);
        }
        #endregion

        #region Text box enter key event
        private void txtUsername_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
            {
                MessageBox.Show("Please enter username...");
            }
            if (e.KeyCode.ToString() == "Escape")
            {
                if (MessageBox.Show("Quit Y/N?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
                    Application.Exit();
                System.Environment.Exit(1);
            }
        }
        #endregion

        //#region Check enter key press event
        //private void CheckEnter(object sender, System.Windows.Forms.KeyPressEventArgs e)
        //{
        //    if (e.KeyChar == (char)Keys.Return)
        //    {
        //        UserName = txtUsername.Text.ToString().Trim();
        //        UserPwd = txtPassword.Text.ToString().Trim();
        //        sPwd = SSTCryptographer.Encrypt(UserPwd, "");
        //        ValidateLoginUser(UserName, SSTCryptographer.Decrypt(sPwd, ""));
        //    }
        //    else if (e.KeyChar == (char)Keys.Escape)
        //    {
        //        this.txtPassword.Text = "";
        //    }
        //}
        //#endregion

        #region Validate User
        private void ValidateLoginUser(string UserName, string UserPwd)
        {
            try
            {
                if (UserName == string.Empty)
                {
                    MessageBox.Show("Please enter username...");
                    txtUsername.Focus();
                }
                else if (UserPwd == string.Empty)
                {
                    MessageBox.Show("Please enter password...");
                    txtPassword.Focus();
                }
                else
                {
                    Member m = new Member();
                    m.UserName = UserName;
                    m.Password = UserPwd;
                    DataTable mCnt = (DataTable)Member.ValidateLogin(m);

                    if (mCnt.Rows.Count != 0)
                    {
                        this.Hide(); 
 
                        LoginInfo.UserID = Convert.ToInt32(mCnt.Rows[0]["User_id"]);
                        MDIParent1 mdi = new MDIParent1();
                                              mdi.Show();
                                              
                    }
                    else
                    {
                        MessageBox.Show("No record found");
                    }
                }
            }
            catch (Exception ex)
            {

            }
        #endregion

        }

        #region Check enter key press event
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // check press key is enter
            {
                btnLogin.PerformClick(); // call loginButton click event                
            }
        }
        #endregion 

      
    }
}

