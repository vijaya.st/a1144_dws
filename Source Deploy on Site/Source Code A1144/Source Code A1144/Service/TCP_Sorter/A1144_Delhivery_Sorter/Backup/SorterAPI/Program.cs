﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace SorterAPI
{
    static class Program
    {
        private static Mutex m_Mutex;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            bool CreatedNew;
            m_Mutex = new Mutex(true, "GPO Mumbai Application", out CreatedNew);
            if (CreatedNew)
            {
               // Application.Run(new frmBatchOperation());
              //  Application.Run(new MDIParent1());
                Application.Run(new frmLogin());
                //Application.Run(new BayAssign1());
            }
            else
            {
                MessageBox.Show("The application is already running.",Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
           
          //  Application.Run(new MDIParent1());
            //Application.Run(new frmBatchOperation());
           // Application.Run(new frmReport());
        }
    }
}
