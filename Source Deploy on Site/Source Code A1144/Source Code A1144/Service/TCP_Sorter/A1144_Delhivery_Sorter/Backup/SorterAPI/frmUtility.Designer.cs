﻿namespace SorterAPI
{
    partial class frmUtility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDBBackup = new System.Windows.Forms.Button();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnImportSortation = new System.Windows.Forms.Button();
            this.btnDownloadLocationSortre = new System.Windows.Forms.Button();
            this.FDLocationBay = new System.Windows.Forms.SaveFileDialog();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.btnBayxlsUpload = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btnDBBackup
            // 
            this.btnDBBackup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnDBBackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnDBBackup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnDBBackup.Location = new System.Drawing.Point(171, 81);
            this.btnDBBackup.Name = "btnDBBackup";
            this.btnDBBackup.Size = new System.Drawing.Size(236, 31);
            this.btnDBBackup.TabIndex = 9;
            this.btnDBBackup.Text = "Backup DWS Database";
            this.btnDBBackup.UseVisualStyleBackColor = false;
            this.btnDBBackup.Click += new System.EventHandler(this.btnDBBackup_Click);
            // 
            // btnTransfer
            // 
            this.btnTransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnTransfer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnTransfer.Location = new System.Drawing.Point(171, 181);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(236, 31);
            this.btnTransfer.TabIndex = 17;
            this.btnTransfer.Text = "Transfer Data to EMSIN";
            this.btnTransfer.UseVisualStyleBackColor = false;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // btnImport
            // 
            this.btnImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnImport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnImport.Location = new System.Drawing.Point(171, 131);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(236, 31);
            this.btnImport.TabIndex = 16;
            this.btnImport.Text = "Import data from EMSOUT";
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Green;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(453, 299);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(113, 31);
            this.btnExit.TabIndex = 18;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Visible = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnImportSortation
            // 
            this.btnImportSortation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnImportSortation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnImportSortation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnImportSortation.Location = new System.Drawing.Point(171, 231);
            this.btnImportSortation.Name = "btnImportSortation";
            this.btnImportSortation.Size = new System.Drawing.Size(236, 31);
            this.btnImportSortation.TabIndex = 19;
            this.btnImportSortation.Text = "Import Pincode Bay Wise Data";
            this.btnImportSortation.UseVisualStyleBackColor = false;
            this.btnImportSortation.Click += new System.EventHandler(this.btnImportSortation_Click);
            // 
            // btnDownloadLocationSortre
            // 
            this.btnDownloadLocationSortre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnDownloadLocationSortre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnDownloadLocationSortre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnDownloadLocationSortre.Location = new System.Drawing.Point(171, 281);
            this.btnDownloadLocationSortre.Name = "btnDownloadLocationSortre";
            this.btnDownloadLocationSortre.Size = new System.Drawing.Size(236, 31);
            this.btnDownloadLocationSortre.TabIndex = 20;
            this.btnDownloadLocationSortre.Text = "Download Pincode Bay Wise Data";
            this.btnDownloadLocationSortre.UseVisualStyleBackColor = false;
            this.btnDownloadLocationSortre.Click += new System.EventHandler(this.btnDownloadLocationSortre_Click);
            // 
            // FDLocationBay
            // 
            this.FDLocationBay.FileOk += new System.ComponentModel.CancelEventHandler(this.FDLocationBay_FileOk);
            // 
            // pnlClose
            // 
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.CloseMenu;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlClose.Location = new System.Drawing.Point(505, 14);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(59, 58);
            this.pnlClose.TabIndex = 21;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // btnBayxlsUpload
            // 
            this.btnBayxlsUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnBayxlsUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnBayxlsUpload.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnBayxlsUpload.Location = new System.Drawing.Point(171, 31);
            this.btnBayxlsUpload.Name = "btnBayxlsUpload";
            this.btnBayxlsUpload.Size = new System.Drawing.Size(236, 31);
            this.btnBayxlsUpload.TabIndex = 22;
            this.btnBayxlsUpload.Text = "Upload Pincode Bay Wise Data";
            this.btnBayxlsUpload.UseVisualStyleBackColor = false;
            this.btnBayxlsUpload.Click += new System.EventHandler(this.btnBayxlsUpload_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // frmUtility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.BackgroundImage = global::SorterAPI.Properties.Resources.BackColor1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(578, 342);
            this.Controls.Add(this.btnBayxlsUpload);
            this.Controls.Add(this.pnlClose);
            this.Controls.Add(this.btnDownloadLocationSortre);
            this.Controls.Add(this.btnImportSortation);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnTransfer);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnDBBackup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUtility";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmUtility";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDBBackup;
        private System.Windows.Forms.Button btnTransfer;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnImportSortation;
        private System.Windows.Forms.Button btnDownloadLocationSortre;
        private System.Windows.Forms.SaveFileDialog FDLocationBay;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnBayxlsUpload;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}