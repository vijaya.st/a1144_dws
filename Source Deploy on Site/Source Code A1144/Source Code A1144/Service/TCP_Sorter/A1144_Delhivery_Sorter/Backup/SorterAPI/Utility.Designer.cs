﻿namespace SorterAPI
{
    partial class Utility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlDwnldPincodeBayWise = new System.Windows.Forms.Panel();
            this.pnlImportBayWise = new System.Windows.Forms.Panel();
            this.pnlTransferDataEMSIN = new System.Windows.Forms.Panel();
            this.pnlImportEMSOut = new System.Windows.Forms.Panel();
            this.pnlDBBak = new System.Windows.Forms.Panel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.FDLocationBay = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Black;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(551, 443);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(93, 34);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlDwnldPincodeBayWise
            // 
            this.pnlDwnldPincodeBayWise.BackgroundImage = global::SorterAPI.Properties.Resources.Uti_DwnPinBayWise;
            this.pnlDwnldPincodeBayWise.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDwnldPincodeBayWise.Location = new System.Drawing.Point(936, 99);
            this.pnlDwnldPincodeBayWise.Name = "pnlDwnldPincodeBayWise";
            this.pnlDwnldPincodeBayWise.Size = new System.Drawing.Size(225, 300);
            this.pnlDwnldPincodeBayWise.TabIndex = 4;
            this.pnlDwnldPincodeBayWise.Click += new System.EventHandler(this.pnlDwnldPincodeBayWise_Click);
            // 
            // pnlImportBayWise
            // 
            this.pnlImportBayWise.BackgroundImage = global::SorterAPI.Properties.Resources.Uti_ImportPinBayWise;
            this.pnlImportBayWise.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlImportBayWise.Location = new System.Drawing.Point(705, 99);
            this.pnlImportBayWise.Name = "pnlImportBayWise";
            this.pnlImportBayWise.Size = new System.Drawing.Size(225, 300);
            this.pnlImportBayWise.TabIndex = 3;
            this.pnlImportBayWise.Click += new System.EventHandler(this.pnlImportBayWise_Click);
            // 
            // pnlTransferDataEMSIN
            // 
            this.pnlTransferDataEMSIN.BackgroundImage = global::SorterAPI.Properties.Resources.Uti_TranDataEmsIn;
            this.pnlTransferDataEMSIN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTransferDataEMSIN.Location = new System.Drawing.Point(474, 99);
            this.pnlTransferDataEMSIN.Name = "pnlTransferDataEMSIN";
            this.pnlTransferDataEMSIN.Size = new System.Drawing.Size(225, 300);
            this.pnlTransferDataEMSIN.TabIndex = 2;
            this.pnlTransferDataEMSIN.Click += new System.EventHandler(this.pnlTransferDataEMSIN_Click);
            // 
            // pnlImportEMSOut
            // 
            this.pnlImportEMSOut.BackgroundImage = global::SorterAPI.Properties.Resources.uti_ImpDataEMSOut;
            this.pnlImportEMSOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlImportEMSOut.Location = new System.Drawing.Point(243, 99);
            this.pnlImportEMSOut.Name = "pnlImportEMSOut";
            this.pnlImportEMSOut.Size = new System.Drawing.Size(225, 300);
            this.pnlImportEMSOut.TabIndex = 1;
            this.pnlImportEMSOut.Click += new System.EventHandler(this.pnlImportEMSOut_Click);
            // 
            // pnlDBBak
            // 
            this.pnlDBBak.BackgroundImage = global::SorterAPI.Properties.Resources.Uti_DBBackup;
            this.pnlDBBak.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDBBak.Location = new System.Drawing.Point(12, 99);
            this.pnlDBBak.Name = "pnlDBBak";
            this.pnlDBBak.Size = new System.Drawing.Size(225, 300);
            this.pnlDBBak.TabIndex = 0;
            this.pnlDBBak.Click += new System.EventHandler(this.pnlDBBak_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // FDLocationBay
            // 
            this.FDLocationBay.FileOk += new System.ComponentModel.CancelEventHandler(this.FDLocationBay_FileOk);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // Utility
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1176, 559);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pnlDwnldPincodeBayWise);
            this.Controls.Add(this.pnlImportBayWise);
            this.Controls.Add(this.pnlTransferDataEMSIN);
            this.Controls.Add(this.pnlImportEMSOut);
            this.Controls.Add(this.pnlDBBak);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Utility";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utility";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDBBak;
        private System.Windows.Forms.Panel pnlImportEMSOut;
        private System.Windows.Forms.Panel pnlTransferDataEMSIN;
        private System.Windows.Forms.Panel pnlImportBayWise;
        private System.Windows.Forms.Panel pnlDwnldPincodeBayWise;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.SaveFileDialog FDLocationBay;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}