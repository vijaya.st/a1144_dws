﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class frmFormula : Form
    {
        public string fvalue { get; set; }
        public int Id { get; set; }
        public frmFormula()
        {
            InitializeComponent();
        }

        private void frmFormula_Load(object sender, EventArgs e)
        {
            
          //  lblHeading.Text = "Formula - " + fvalue;
         
           // txtFormula.Text += "if(" + fvalue;
        }      

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.TextLength != 0)
            {
                txtFormulaArea.Text = txtFormulaArea.Text.Remove(txtFormulaArea.Text.Length - 1, 1); //backspace
            }
        }

        private void txtvalue_TextChanged(object sender, EventArgs e)
        {
            ////txtvalue.Text = "";
            //if (txtFormula.Text.Contains("if("))
            //{
            //txtFormula.Text += txtvalue.Text;
            //txtvalue.Clear();
            //}
            //else
            //{
            //    MessageBox.Show("Please set if condition");
            //}
        }

        private void btnFormulaSave_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator frs = new frmSorterAPIConfigurator();

            if (txtFormulaArea.Text != "")
            {
                clsAPI obj = new clsAPI();
                obj.Id = Id;
                obj.Formula = txtFormulaArea.Text;
               
                clsAPI.InsertFormula(obj);
                this.Close();
            }
            else
            {
                MessageBox.Show("Please Enter formula");
            }
        }      

        private void btnUndo_Click(object sender, EventArgs e)
        {
            txtFormulaArea.Undo();            
        }

        private void btnif_Click(object sender, EventArgs e)
        {
            txtFormulaArea.Text += "if("+fvalue;
        }

        private void btnOpenB_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "(";
            }
        }

        private void btnClosingB_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += ")";
            }
        }

        private void btnOpenC_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "{";
            }
        }

        private void btnCosingC_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "}";
            }
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "==";
            }
        }

        private void btnNotEqual_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "!=";
            }
        }

        private void btnLess_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "<";
            }
        }

        private void btnGreater_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += ">";
            }
        }

        private void btnLessEqual_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "<=";
            }
        }

        private void btnGreaterEqual_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += ">=";
            }

          //  if (txtFormulaArea.Text.Contains("")) { MessageBox.Show("ds"); }
        }

        private void btnMsg_Click(object sender, EventArgs e)
        {
            if (txtFormulaArea.Text.Contains("if"))
            {
                txtFormulaArea.Text += "{MessageBox.Show();}";                
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region button color
        private void btnFormulaSave_MouseHover(object sender, EventArgs e)
        {
            btnFormulaSave.BackColor = Color.FromArgb(62, 46, 52);
            btnFormulaSave.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnFormulaSave_MouseLeave(object sender, EventArgs e)
        {
            btnFormulaSave.BackColor = Color.FromArgb(255, 190, 0);
            btnFormulaSave.ForeColor = Color.FromArgb(62, 46, 52);
        }

        private void btnUndo_MouseHover(object sender, EventArgs e)
        {
            btnUndo.BackColor = Color.FromArgb(62, 46, 52);
            btnUndo.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnUndo_MouseLeave(object sender, EventArgs e)
        {
            btnUndo.BackColor = Color.FromArgb(255, 190, 0);
            btnUndo.ForeColor = Color.FromArgb(62, 46, 52);
        }

        private void btnCancel_MouseHover(object sender, EventArgs e)
        {
            btnCancel.BackColor = Color.FromArgb(62, 46, 52);
            btnCancel.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnCancel_MouseLeave(object sender, EventArgs e)
        {
            btnCancel.BackColor = Color.FromArgb(255, 190, 0);
            btnCancel.ForeColor = Color.FromArgb(62, 46, 52);
        }

        private void btnEvalFormula_MouseHover(object sender, EventArgs e)
        {
            btnEvalFormula.BackColor = Color.FromArgb(62, 46, 52);
            btnEvalFormula.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnEvalFormula_MouseLeave(object sender, EventArgs e)
        {
            btnEvalFormula.BackColor = Color.FromArgb(255, 190, 0);
            btnEvalFormula.ForeColor = Color.FromArgb(62, 46, 52);
        }

        private void btnExit_MouseMove(object sender, MouseEventArgs e)
        {
            btnExit.BackColor = Color.FromArgb(62, 46, 52);
            btnExit.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.BackColor = Color.FromArgb(255, 190, 0);
            btnExit.ForeColor = Color.FromArgb(62, 46, 52);
        }        
        #endregion       

        private void btnEvalFormula_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = true;
            
        }       

        private void btnCheck_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
        }
        
    }
}
