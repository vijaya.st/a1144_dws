﻿namespace SorterAPI
{
    partial class frmReport1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.chkBay = new System.Windows.Forms.CheckBox();
            this.chkPin = new System.Windows.Forms.CheckBox();
            this.txtPincode = new System.Windows.Forms.TextBox();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.drpBaywise = new System.Windows.Forms.ComboBox();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlDWSTitle = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.pnlDWSFilter = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnExporttoExcel = new System.Windows.Forms.Button();
            this.lblSearch = new System.Windows.Forms.Label();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlDWSStatistics = new System.Windows.Forms.Panel();
            this.lblSuccesful = new System.Windows.Forms.Label();
            this.lblNA = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.pnlExit = new System.Windows.Forms.Panel();
            this.pnlDWSTitle.SuspendLayout();
            this.pnlDWSFilter.SuspendLayout();
            this.pnlGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.panel2.SuspendLayout();
            this.pnlDWSStatistics.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkBay
            // 
            this.chkBay.AutoSize = true;
            this.chkBay.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBay.Location = new System.Drawing.Point(683, 66);
            this.chkBay.Name = "chkBay";
            this.chkBay.Size = new System.Drawing.Size(15, 14);
            this.chkBay.TabIndex = 0;
            this.chkBay.UseVisualStyleBackColor = true;
            // 
            // chkPin
            // 
            this.chkPin.AutoSize = true;
            this.chkPin.Location = new System.Drawing.Point(156, 69);
            this.chkPin.Name = "chkPin";
            this.chkPin.Size = new System.Drawing.Size(15, 14);
            this.chkPin.TabIndex = 1;
            this.chkPin.UseVisualStyleBackColor = true;
            this.chkPin.CheckedChanged += new System.EventHandler(this.chkPin_CheckedChanged);
            // 
            // txtPincode
            // 
            this.txtPincode.Enabled = false;
            this.txtPincode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPincode.Location = new System.Drawing.Point(250, 59);
            this.txtPincode.MaxLength = 6;
            this.txtPincode.Name = "txtPincode";
            this.txtPincode.Size = new System.Drawing.Size(230, 31);
            this.txtPincode.TabIndex = 16;
            // 
            // dtTo
            // 
            this.dtTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtTo.Location = new System.Drawing.Point(713, 15);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(283, 31);
            this.dtTo.TabIndex = 12;
            // 
            // drpBaywise
            // 
            this.drpBaywise.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBaywise.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBaywise.Enabled = false;
            this.drpBaywise.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBaywise.FormattingEnabled = true;
            this.drpBaywise.Location = new System.Drawing.Point(713, 57);
            this.drpBaywise.Name = "drpBaywise";
            this.drpBaywise.Size = new System.Drawing.Size(281, 33);
            this.drpBaywise.TabIndex = 11;
            // 
            // dtFrom
            // 
            this.dtFrom.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFrom.Location = new System.Drawing.Point(250, 14);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(283, 31);
            this.dtFrom.TabIndex = 5;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // pnlDWSTitle
            // 
            this.pnlDWSTitle.BackColor = System.Drawing.Color.White;
            this.pnlDWSTitle.Controls.Add(this.label1);
            this.pnlDWSTitle.Controls.Add(this.panel1);
            this.pnlDWSTitle.Controls.Add(this.pnlClose);
            this.pnlDWSTitle.Location = new System.Drawing.Point(2, 1);
            this.pnlDWSTitle.Name = "pnlDWSTitle";
            this.pnlDWSTitle.Size = new System.Drawing.Size(1198, 78);
            this.pnlDWSTitle.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label1.Location = new System.Drawing.Point(528, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 29);
            this.label1.TabIndex = 24;
            this.label1.Text = "DWS Article Reports";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.ReportTitle;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(434, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(61, 63);
            this.panel1.TabIndex = 20;
            // 
            // pnlClose
            // 
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.Location = new System.Drawing.Point(1169, 5);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(23, 20);
            this.pnlClose.TabIndex = 19;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // pnlDWSFilter
            // 
            this.pnlDWSFilter.BackColor = System.Drawing.Color.White;
            this.pnlDWSFilter.Controls.Add(this.btnSearch);
            this.pnlDWSFilter.Controls.Add(this.drpBaywise);
            this.pnlDWSFilter.Controls.Add(this.label11);
            this.pnlDWSFilter.Controls.Add(this.chkBay);
            this.pnlDWSFilter.Controls.Add(this.label8);
            this.pnlDWSFilter.Controls.Add(this.label9);
            this.pnlDWSFilter.Controls.Add(this.label7);
            this.pnlDWSFilter.Controls.Add(this.label6);
            this.pnlDWSFilter.Controls.Add(this.btnExporttoExcel);
            this.pnlDWSFilter.Controls.Add(this.lblSearch);
            this.pnlDWSFilter.Controls.Add(this.chkPin);
            this.pnlDWSFilter.Controls.Add(this.dtFrom);
            this.pnlDWSFilter.Controls.Add(this.dtTo);
            this.pnlDWSFilter.Controls.Add(this.txtPincode);
            this.pnlDWSFilter.Location = new System.Drawing.Point(11, 7);
            this.pnlDWSFilter.Name = "pnlDWSFilter";
            this.pnlDWSFilter.Size = new System.Drawing.Size(1176, 104);
            this.pnlDWSFilter.TabIndex = 15;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(1008, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(153, 40);
            this.btnSearch.TabIndex = 19;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(3, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 19);
            this.label11.TabIndex = 28;
            this.label11.Text = "Pincode/OU Wise:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(639, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 19);
            this.label8.TabIndex = 25;
            this.label8.Text = "To :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(587, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 19);
            this.label9.TabIndex = 26;
            this.label9.Text = "Bay Wise :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(185, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 19);
            this.label7.TabIndex = 24;
            this.label7.Text = "From :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(3, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 19);
            this.label6.TabIndex = 23;
            this.label6.Text = "Date Range :";
            // 
            // btnExporttoExcel
            // 
            this.btnExporttoExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.btnExporttoExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExporttoExcel.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExporttoExcel.ForeColor = System.Drawing.Color.White;
            this.btnExporttoExcel.Location = new System.Drawing.Point(1008, 54);
            this.btnExporttoExcel.Name = "btnExporttoExcel";
            this.btnExporttoExcel.Size = new System.Drawing.Size(153, 38);
            this.btnExporttoExcel.TabIndex = 18;
            this.btnExporttoExcel.Text = "Export To CSV";
            this.btnExporttoExcel.UseVisualStyleBackColor = false;
            this.btnExporttoExcel.Click += new System.EventHandler(this.btnExporttoExcel_Click);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.BackColor = System.Drawing.Color.Transparent;
            this.lblSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearch.Location = new System.Drawing.Point(1032, 15);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(134, 73);
            this.lblSearch.TabIndex = 17;
            this.lblSearch.Text = "      ";
            this.lblSearch.Visible = false;
            this.lblSearch.Click += new System.EventHandler(this.lblSearch_Click);
            // 
            // pnlGrid
            // 
            this.pnlGrid.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlGrid.Controls.Add(this.dgvReport);
            this.pnlGrid.Location = new System.Drawing.Point(2, 282);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(1200, 459);
            this.pnlGrid.TabIndex = 17;
            // 
            // dgvReport
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvReport.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvReport.Location = new System.Drawing.Point(10, 18);
            this.dgvReport.Name = "dgvReport";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReport.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvReport.Size = new System.Drawing.Size(1176, 428);
            this.dgvReport.TabIndex = 18;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Controls.Add(this.pnlDWSFilter);
            this.panel2.Location = new System.Drawing.Point(2, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1200, 116);
            this.panel2.TabIndex = 18;
            // 
            // pnlDWSStatistics
            // 
            this.pnlDWSStatistics.BackgroundImage = global::SorterAPI.Properties.Resources.dws_statistics;
            this.pnlDWSStatistics.Controls.Add(this.lblSuccesful);
            this.pnlDWSStatistics.Controls.Add(this.lblNA);
            this.pnlDWSStatistics.Controls.Add(this.lblTotal);
            this.pnlDWSStatistics.Location = new System.Drawing.Point(2, 204);
            this.pnlDWSStatistics.Name = "pnlDWSStatistics";
            this.pnlDWSStatistics.Size = new System.Drawing.Size(1200, 78);
            this.pnlDWSStatistics.TabIndex = 16;
            // 
            // lblSuccesful
            // 
            this.lblSuccesful.AutoSize = true;
            this.lblSuccesful.BackColor = System.Drawing.Color.Transparent;
            this.lblSuccesful.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuccesful.ForeColor = System.Drawing.Color.Black;
            this.lblSuccesful.Location = new System.Drawing.Point(162, 22);
            this.lblSuccesful.Name = "lblSuccesful";
            this.lblSuccesful.Size = new System.Drawing.Size(52, 29);
            this.lblSuccesful.TabIndex = 22;
            this.lblSuccesful.Text = "000";
            // 
            // lblNA
            // 
            this.lblNA.AutoSize = true;
            this.lblNA.BackColor = System.Drawing.Color.Transparent;
            this.lblNA.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNA.ForeColor = System.Drawing.Color.Black;
            this.lblNA.Location = new System.Drawing.Point(511, 22);
            this.lblNA.Name = "lblNA";
            this.lblNA.Size = new System.Drawing.Size(52, 29);
            this.lblNA.TabIndex = 23;
            this.lblNA.Text = "000";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(917, 22);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(52, 29);
            this.lblTotal.TabIndex = 24;
            this.lblTotal.Text = "000";
            // 
            // pnlExit
            // 
            this.pnlExit.BackgroundImage = global::SorterAPI.Properties.Resources.CloseMenu;
            this.pnlExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlExit.Location = new System.Drawing.Point(1153, 6);
            this.pnlExit.Name = "pnlExit";
            this.pnlExit.Size = new System.Drawing.Size(75, 67);
            this.pnlExit.TabIndex = 13;
            this.pnlExit.Click += new System.EventHandler(this.pnlExit_Click);
            // 
            // frmReport1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1200, 747);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlGrid);
            this.Controls.Add(this.pnlDWSStatistics);
            this.Controls.Add(this.pnlDWSTitle);
            this.Controls.Add(this.pnlExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmReport1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "DWS Articles Reports";
            this.Load += new System.EventHandler(this.frmReport1_Load);
            this.pnlDWSTitle.ResumeLayout(false);
            this.pnlDWSTitle.PerformLayout();
            this.pnlDWSFilter.ResumeLayout(false);
            this.pnlDWSFilter.PerformLayout();
            this.pnlGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.panel2.ResumeLayout(false);
            this.pnlDWSStatistics.ResumeLayout(false);
            this.pnlDWSStatistics.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.ComboBox drpBaywise;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.TextBox txtPincode;
        private System.Windows.Forms.Label lblNA;
        private System.Windows.Forms.Label lblSuccesful;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.CheckBox chkBay;
        private System.Windows.Forms.CheckBox chkPin;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel pnlExit;
        private System.Windows.Forms.Panel pnlDWSTitle;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Panel pnlDWSFilter;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Panel pnlDWSStatistics;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.Button btnExporttoExcel;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;

        //  private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}