﻿namespace SorterAPI
{
    partial class frmSorterAPIConfigurator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label baudRateLabel;
            System.Windows.Forms.Label portNameLabel;
            System.Windows.Forms.Label label39;
            System.Windows.Forms.Label label107;
            System.Windows.Forms.Label label110;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSorterAPIConfigurator));
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lnkNewUser = new System.Windows.Forms.LinkLabel();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtOurContactNumber = new System.Windows.Forms.TextBox();
            this.txtOurContactEmail = new System.Windows.Forms.TextBox();
            this.txtOurContactPerson = new System.Windows.Forms.TextBox();
            this.txtContactNumber = new System.Windows.Forms.TextBox();
            this.txtContactEmail = new System.Windows.Forms.TextBox();
            this.txtContactPerson = new System.Windows.Forms.TextBox();
            this.txtAddr = new System.Windows.Forms.TextBox();
            this.txtComapanyName = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.btnLogoUpload = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.btnSaveGeneralSetting = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.drpBranchFilter = new System.Windows.Forms.ComboBox();
            this.dgvHubList = new System.Windows.Forms.DataGridView();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lblUp = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblDown = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnLocationClose = new System.Windows.Forms.Button();
            this.btnSaveLocation = new System.Windows.Forms.Button();
            this.drpBinDown = new System.Windows.Forms.ComboBox();
            this.drpBinUp = new System.Windows.Forms.ComboBox();
            this.drpBranch = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnAddNewLocation = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtMaxWe = new System.Windows.Forms.TextBox();
            this.txtMinWe = new System.Windows.Forms.TextBox();
            this.txtMaxL = new System.Windows.Forms.TextBox();
            this.txtMinL = new System.Windows.Forms.TextBox();
            this.txtMaxW = new System.Windows.Forms.TextBox();
            this.txtMinW = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaxH = new System.Windows.Forms.TextBox();
            this.txtMinH = new System.Windows.Forms.TextBox();
            this.txtOurColumn4 = new System.Windows.Forms.TextBox();
            this.txtOurColumn3 = new System.Windows.Forms.TextBox();
            this.txtOurColumn2 = new System.Windows.Forms.TextBox();
            this.txtOurColumn1 = new System.Windows.Forms.TextBox();
            this.txtOurTableName = new System.Windows.Forms.TextBox();
            this.txtOurDBName = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnConnTest = new System.Windows.Forms.Button();
            this.btnTestOurConn = new System.Windows.Forms.Button();
            this.pnlMySQL = new System.Windows.Forms.Panel();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.txtServerMy = new System.Windows.Forms.TextBox();
            this.txtDBIdMy = new System.Windows.Forms.TextBox();
            this.txtDBPassMy = new System.Windows.Forms.TextBox();
            this.pnlOurMySql = new System.Windows.Forms.Panel();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.txtOurServerMy = new System.Windows.Forms.TextBox();
            this.txtOurDBPassMy = new System.Windows.Forms.TextBox();
            this.txtOurDBIdMy = new System.Windows.Forms.TextBox();
            this.pnlOurAccess = new System.Windows.Forms.Panel();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.txtOurDBProviderA = new System.Windows.Forms.TextBox();
            this.txtOurDBSourcePathA = new System.Windows.Forms.TextBox();
            this.txtOurDBPassworA = new System.Windows.Forms.TextBox();
            this.pnlOurSql = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtOurDBIpAddr = new System.Windows.Forms.TextBox();
            this.txtOurDBUsername = new System.Windows.Forms.TextBox();
            this.txtOurDBPassword = new System.Windows.Forms.TextBox();
            this.pnlAccess = new System.Windows.Forms.Panel();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.txtDBProviderA = new System.Windows.Forms.TextBox();
            this.txtDBSourcePathA = new System.Windows.Forms.TextBox();
            this.txtDBPassworA = new System.Windows.Forms.TextBox();
            this.pnlOurOracle = new System.Windows.Forms.Panel();
            this.drpOurIntegratedSecurity = new System.Windows.Forms.ComboBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.txtOurDBIdOracle = new System.Windows.Forms.TextBox();
            this.txtOurDBPassOracle = new System.Windows.Forms.TextBox();
            this.pnlOracle = new System.Windows.Forms.Panel();
            this.drpIntegratedSecurity = new System.Windows.Forms.ComboBox();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.txtDBIdOracle = new System.Windows.Forms.TextBox();
            this.txtDBPassOracle = new System.Windows.Forms.TextBox();
            this.pnlSql = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtDBIpAddr = new System.Windows.Forms.TextBox();
            this.txtDBUsername = new System.Windows.Forms.TextBox();
            this.txtDBPassword = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.drpColumnName4 = new System.Windows.Forms.ComboBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.drpColumnName3 = new System.Windows.Forms.ComboBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.drpColumnName2 = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.drpOurDataSource = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.drpColumnName1 = new System.Windows.Forms.ComboBox();
            this.drpTableName = new System.Windows.Forms.ComboBox();
            this.drpDBName = new System.Windows.Forms.ComboBox();
            this.drpDataSource = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.btnSaveDBInfo = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PTLPanel = new System.Windows.Forms.Panel();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtBagClosurePrefix = new System.Windows.Forms.TextBox();
            this.txtMaxWeightArticle = new System.Windows.Forms.TextBox();
            this.txtMaxNoArticle = new System.Windows.Forms.TextBox();
            this.txtMinNoArticle = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.btnSorterwiseCassetteConfig = new System.Windows.Forms.Button();
            this.btnPTLCancel = new System.Windows.Forms.Button();
            this.btnPTLSystem = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label72 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.txtPLCScanTime = new System.Windows.Forms.TextBox();
            this.txtEnterDeactivationDelay = new System.Windows.Forms.TextBox();
            this.txtEnterActivationDelay = new System.Windows.Forms.TextBox();
            this.txtLinearDistance = new System.Windows.Forms.TextBox();
            this.txtArmOpeningTime = new System.Windows.Forms.TextBox();
            this.txtSpeedofConveyor = new System.Windows.Forms.TextBox();
            this.txtEncoderPPR = new System.Windows.Forms.TextBox();
            this.txtDistanceTravelled = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.btnReset2 = new System.Windows.Forms.Button();
            this.btnPLCDynamicDataSave = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.grpVMSSystem = new System.Windows.Forms.GroupBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtScanPort = new System.Windows.Forms.TextBox();
            this.btnTestDWS = new System.Windows.Forms.Button();
            this.label108 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.drpWBaud = new System.Windows.Forms.ComboBox();
            this.drpWPortName = new System.Windows.Forms.ComboBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.drpScanerPortName = new System.Windows.Forms.ComboBox();
            this.txtPLCPort = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtPLCIP = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.grpFolderLocation = new System.Windows.Forms.GroupBox();
            this.rdCSV = new System.Windows.Forms.RadioButton();
            this.btnFldBrowser = new System.Windows.Forms.Button();
            this.rdXML = new System.Windows.Forms.RadioButton();
            this.txtFolderLocation = new System.Windows.Forms.TextBox();
            this.txtScannerIP = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.baudRateComboBox = new System.Windows.Forms.ComboBox();
            this.portNameComboBox = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnProceedtoSorter = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.rdSortsNo = new System.Windows.Forms.RadioButton();
            this.rdSortsYes = new System.Windows.Forms.RadioButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.rdDWSNo = new System.Windows.Forms.RadioButton();
            this.rdDWSYes = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rdPTLNo = new System.Windows.Forms.RadioButton();
            this.rdPTLYes = new System.Windows.Forms.RadioButton();
            this.label106 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtnumberofSorter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDataLoadSensor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSavePLCDiemensionalData = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabCntrl = new System.Windows.Forms.TabControl();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDatetime = new System.Windows.Forms.ToolStripStatusLabel();
            this.serialSettingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            baudRateLabel = new System.Windows.Forms.Label();
            portNameLabel = new System.Windows.Forms.Label();
            label39 = new System.Windows.Forms.Label();
            label107 = new System.Windows.Forms.Label();
            label110 = new System.Windows.Forms.Label();
            this.tabPage6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.pnlMySQL.SuspendLayout();
            this.pnlOurMySql.SuspendLayout();
            this.pnlOurAccess.SuspendLayout();
            this.pnlOurSql.SuspendLayout();
            this.pnlAccess.SuspendLayout();
            this.pnlOurOracle.SuspendLayout();
            this.pnlOracle.SuspendLayout();
            this.pnlSql.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.PTLPanel.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grpVMSSystem.SuspendLayout();
            this.panel11.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.grpFolderLocation.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabCntrl.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serialSettingsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // baudRateLabel
            // 
            baudRateLabel.AutoSize = true;
            baudRateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            baudRateLabel.ForeColor = System.Drawing.Color.Black;
            baudRateLabel.Location = new System.Drawing.Point(6, 49);
            baudRateLabel.Name = "baudRateLabel";
            baudRateLabel.Size = new System.Drawing.Size(85, 16);
            baudRateLabel.TabIndex = 1;
            baudRateLabel.Text = "Baud Rate:";
            // 
            // portNameLabel
            // 
            portNameLabel.AutoSize = true;
            portNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            portNameLabel.ForeColor = System.Drawing.Color.Black;
            portNameLabel.Location = new System.Drawing.Point(6, 22);
            portNameLabel.Name = "portNameLabel";
            portNameLabel.Size = new System.Drawing.Size(85, 16);
            portNameLabel.TabIndex = 7;
            portNameLabel.Text = "Port Name:";
            // 
            // label39
            // 
            label39.AutoSize = true;
            label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            label39.ForeColor = System.Drawing.Color.Black;
            label39.Location = new System.Drawing.Point(279, 52);
            label39.Name = "label39";
            label39.Size = new System.Drawing.Size(96, 16);
            label39.TabIndex = 24;
            label39.Text = "Export Type:";
            // 
            // label107
            // 
            label107.AutoSize = true;
            label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            label107.ForeColor = System.Drawing.Color.Black;
            label107.Location = new System.Drawing.Point(6, 49);
            label107.Name = "label107";
            label107.Size = new System.Drawing.Size(85, 16);
            label107.TabIndex = 1;
            label107.Text = "Baud Rate:";
            // 
            // label110
            // 
            label110.AutoSize = true;
            label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            label110.ForeColor = System.Drawing.Color.Black;
            label110.Location = new System.Drawing.Point(6, 22);
            label110.Name = "label110";
            label110.Size = new System.Drawing.Size(85, 16);
            label110.TabIndex = 7;
            label110.Text = "Port Name:";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage6.BackgroundImage = global::SorterAPI.Properties.Resources.diemensions21211;
            this.tabPage6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage6.Controls.Add(this.groupBox5);
            this.tabPage6.Controls.Add(this.button9);
            this.tabPage6.Controls.Add(this.btnSaveGeneralSetting);
            this.tabPage6.Location = new System.Drawing.Point(4, 41);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(989, 598);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Customer Info  ";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Teal;
            this.groupBox5.Controls.Add(this.panel5);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(4, 14);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(818, 448);
            this.groupBox5.TabIndex = 62;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "General Setting";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel5.Controls.Add(this.lnkNewUser);
            this.panel5.Controls.Add(this.label79);
            this.panel5.Controls.Add(this.label78);
            this.panel5.Controls.Add(this.label77);
            this.panel5.Controls.Add(this.label76);
            this.panel5.Controls.Add(this.label75);
            this.panel5.Controls.Add(this.label74);
            this.panel5.Controls.Add(this.label73);
            this.panel5.Controls.Add(this.label41);
            this.panel5.Controls.Add(this.txtOurContactNumber);
            this.panel5.Controls.Add(this.txtOurContactEmail);
            this.panel5.Controls.Add(this.txtOurContactPerson);
            this.panel5.Controls.Add(this.txtContactNumber);
            this.panel5.Controls.Add(this.txtContactEmail);
            this.panel5.Controls.Add(this.txtContactPerson);
            this.panel5.Controls.Add(this.txtAddr);
            this.panel5.Controls.Add(this.txtComapanyName);
            this.panel5.Controls.Add(this.label63);
            this.panel5.Controls.Add(this.label62);
            this.panel5.Controls.Add(this.label61);
            this.panel5.Controls.Add(this.btnLogoUpload);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Controls.Add(this.label60);
            this.panel5.Controls.Add(this.label59);
            this.panel5.Controls.Add(this.label58);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.label56);
            this.panel5.Controls.Add(this.label55);
            this.panel5.Location = new System.Drawing.Point(13, 24);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(794, 399);
            this.panel5.TabIndex = 0;
            // 
            // lnkNewUser
            // 
            this.lnkNewUser.AutoSize = true;
            this.lnkNewUser.Location = new System.Drawing.Point(607, 178);
            this.lnkNewUser.Name = "lnkNewUser";
            this.lnkNewUser.Size = new System.Drawing.Size(109, 16);
            this.lnkNewUser.TabIndex = 104;
            this.lnkNewUser.TabStop = true;
            this.lnkNewUser.Text = "Manage Users";
            this.lnkNewUser.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkNewUser_LinkClicked);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.BackColor = System.Drawing.Color.Transparent;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Red;
            this.label79.Location = new System.Drawing.Point(761, 117);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(12, 15);
            this.label79.TabIndex = 103;
            this.label79.Text = "*";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.Transparent;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(761, 68);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(12, 15);
            this.label78.TabIndex = 102;
            this.label78.Text = "*";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.BackColor = System.Drawing.Color.Transparent;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Red;
            this.label77.Location = new System.Drawing.Point(761, 12);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(12, 15);
            this.label77.TabIndex = 101;
            this.label77.Text = "*";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.BackColor = System.Drawing.Color.Transparent;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Red;
            this.label76.Location = new System.Drawing.Point(397, 207);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(12, 15);
            this.label76.TabIndex = 100;
            this.label76.Text = "*";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.BackColor = System.Drawing.Color.Transparent;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Red;
            this.label75.Location = new System.Drawing.Point(397, 163);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(12, 15);
            this.label75.TabIndex = 99;
            this.label75.Text = "*";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Red;
            this.label74.Location = new System.Drawing.Point(397, 117);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(12, 15);
            this.label74.TabIndex = 98;
            this.label74.Text = "*";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.BackColor = System.Drawing.Color.Transparent;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Red;
            this.label73.Location = new System.Drawing.Point(397, 48);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(12, 15);
            this.label73.TabIndex = 97;
            this.label73.Text = "*";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(397, 12);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(12, 15);
            this.label41.TabIndex = 96;
            this.label41.Text = "*";
            // 
            // txtOurContactNumber
            // 
            this.txtOurContactNumber.BackColor = System.Drawing.Color.LightGray;
            this.txtOurContactNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurContactNumber.ForeColor = System.Drawing.Color.Black;
            this.txtOurContactNumber.Location = new System.Drawing.Point(558, 127);
            this.txtOurContactNumber.MaxLength = 11;
            this.txtOurContactNumber.Name = "txtOurContactNumber";
            this.txtOurContactNumber.Size = new System.Drawing.Size(201, 22);
            this.txtOurContactNumber.TabIndex = 95;
            // 
            // txtOurContactEmail
            // 
            this.txtOurContactEmail.BackColor = System.Drawing.Color.LightGray;
            this.txtOurContactEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurContactEmail.ForeColor = System.Drawing.Color.Black;
            this.txtOurContactEmail.Location = new System.Drawing.Point(558, 75);
            this.txtOurContactEmail.MaxLength = 50;
            this.txtOurContactEmail.Name = "txtOurContactEmail";
            this.txtOurContactEmail.Size = new System.Drawing.Size(201, 22);
            this.txtOurContactEmail.TabIndex = 94;
            // 
            // txtOurContactPerson
            // 
            this.txtOurContactPerson.BackColor = System.Drawing.Color.LightGray;
            this.txtOurContactPerson.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurContactPerson.ForeColor = System.Drawing.Color.Black;
            this.txtOurContactPerson.Location = new System.Drawing.Point(558, 22);
            this.txtOurContactPerson.MaxLength = 50;
            this.txtOurContactPerson.Name = "txtOurContactPerson";
            this.txtOurContactPerson.Size = new System.Drawing.Size(201, 22);
            this.txtOurContactPerson.TabIndex = 93;
            // 
            // txtContactNumber
            // 
            this.txtContactNumber.BackColor = System.Drawing.Color.LightGray;
            this.txtContactNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContactNumber.ForeColor = System.Drawing.Color.Black;
            this.txtContactNumber.Location = new System.Drawing.Point(187, 216);
            this.txtContactNumber.MaxLength = 11;
            this.txtContactNumber.Name = "txtContactNumber";
            this.txtContactNumber.Size = new System.Drawing.Size(208, 22);
            this.txtContactNumber.TabIndex = 88;
            // 
            // txtContactEmail
            // 
            this.txtContactEmail.BackColor = System.Drawing.Color.LightGray;
            this.txtContactEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContactEmail.ForeColor = System.Drawing.Color.Black;
            this.txtContactEmail.Location = new System.Drawing.Point(187, 172);
            this.txtContactEmail.MaxLength = 50;
            this.txtContactEmail.Name = "txtContactEmail";
            this.txtContactEmail.Size = new System.Drawing.Size(208, 22);
            this.txtContactEmail.TabIndex = 87;
            // 
            // txtContactPerson
            // 
            this.txtContactPerson.BackColor = System.Drawing.Color.LightGray;
            this.txtContactPerson.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtContactPerson.ForeColor = System.Drawing.Color.Black;
            this.txtContactPerson.Location = new System.Drawing.Point(187, 127);
            this.txtContactPerson.MaxLength = 50;
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.Size = new System.Drawing.Size(208, 22);
            this.txtContactPerson.TabIndex = 86;
            // 
            // txtAddr
            // 
            this.txtAddr.BackColor = System.Drawing.Color.LightGray;
            this.txtAddr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddr.ForeColor = System.Drawing.Color.Black;
            this.txtAddr.Location = new System.Drawing.Point(187, 58);
            this.txtAddr.MaxLength = 100;
            this.txtAddr.Multiline = true;
            this.txtAddr.Name = "txtAddr";
            this.txtAddr.Size = new System.Drawing.Size(208, 54);
            this.txtAddr.TabIndex = 85;
            // 
            // txtComapanyName
            // 
            this.txtComapanyName.BackColor = System.Drawing.Color.LightGray;
            this.txtComapanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComapanyName.ForeColor = System.Drawing.Color.Black;
            this.txtComapanyName.Location = new System.Drawing.Point(187, 22);
            this.txtComapanyName.MaxLength = 50;
            this.txtComapanyName.Name = "txtComapanyName";
            this.txtComapanyName.Size = new System.Drawing.Size(208, 22);
            this.txtComapanyName.TabIndex = 84;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(429, 130);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(112, 16);
            this.label63.TabIndex = 92;
            this.label63.Text = "Contact Phone:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(429, 78);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(107, 16);
            this.label62.TabIndex = 91;
            this.label62.Text = "Contact Email:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(429, 25);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(106, 32);
            this.label61.TabIndex = 90;
            this.label61.Text = "Our Contact \r\nPerson Name:";
            // 
            // btnLogoUpload
            // 
            this.btnLogoUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnLogoUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogoUpload.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnLogoUpload.Location = new System.Drawing.Point(463, 341);
            this.btnLogoUpload.Name = "btnLogoUpload";
            this.btnLogoUpload.Size = new System.Drawing.Size(88, 27);
            this.btnLogoUpload.TabIndex = 89;
            this.btnLogoUpload.Text = "Upload Logo";
            this.btnLogoUpload.UseVisualStyleBackColor = false;
            this.btnLogoUpload.Click += new System.EventHandler(this.btnLogoUpload_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(187, 268);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(270, 100);
            this.pictureBox1.TabIndex = 83;
            this.pictureBox1.TabStop = false;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(22, 268);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(116, 16);
            this.label60.TabIndex = 82;
            this.label60.Text = "Company Logo:";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(22, 219);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(122, 16);
            this.label59.TabIndex = 81;
            this.label59.Text = "Contact Number:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(22, 175);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(107, 16);
            this.label58.TabIndex = 80;
            this.label58.Text = "Contact Email:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(22, 130);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(162, 16);
            this.label57.TabIndex = 79;
            this.label57.Text = "Contact Person Name:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(22, 61);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(70, 16);
            this.label56.TabIndex = 78;
            this.label56.Text = "Address:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(22, 25);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(162, 16);
            this.label55.TabIndex = 77;
            this.label55.Text = "Enter Company Name:";
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.button9.Location = new System.Drawing.Point(438, 478);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 27);
            this.button9.TabIndex = 61;
            this.button9.Text = "Cancel";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // btnSaveGeneralSetting
            // 
            this.btnSaveGeneralSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnSaveGeneralSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveGeneralSetting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnSaveGeneralSetting.Location = new System.Drawing.Point(336, 478);
            this.btnSaveGeneralSetting.Name = "btnSaveGeneralSetting";
            this.btnSaveGeneralSetting.Size = new System.Drawing.Size(75, 27);
            this.btnSaveGeneralSetting.TabIndex = 60;
            this.btnSaveGeneralSetting.Text = "Save";
            this.btnSaveGeneralSetting.UseVisualStyleBackColor = false;
            this.btnSaveGeneralSetting.Click += new System.EventHandler(this.btnSaveGeneralSetting_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage5.BackgroundImage = global::SorterAPI.Properties.Resources.diemensions21211;
            this.tabPage5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage5.Controls.Add(this.groupBox4);
            this.tabPage5.Location = new System.Drawing.Point(4, 41);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(989, 598);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Assign bag address   ";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Teal;
            this.groupBox4.Controls.Add(this.panel4);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(19, 26);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(841, 547);
            this.groupBox4.TabIndex = 74;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Locations";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel4.Controls.Add(this.groupBox12);
            this.panel4.Controls.Add(this.dgvHubList);
            this.panel4.Controls.Add(this.groupBox10);
            this.panel4.Controls.Add(this.btnAddNewLocation);
            this.panel4.ForeColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(19, 19);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 511);
            this.panel4.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label24);
            this.groupBox12.Controls.Add(this.txtFilter);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this.drpBranchFilter);
            this.groupBox12.Location = new System.Drawing.Point(439, 39);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(348, 89);
            this.groupBox12.TabIndex = 65;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Filter";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(20, 59);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 16);
            this.label24.TabIndex = 21;
            this.label24.Text = "Enter Pincode";
            // 
            // txtFilter
            // 
            this.txtFilter.Location = new System.Drawing.Point(143, 56);
            this.txtFilter.MaxLength = 6;
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(147, 24);
            this.txtFilter.TabIndex = 20;
            this.txtFilter.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilter_KeyUp);
            this.txtFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilter_KeyPress);
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Black;
            this.label112.Location = new System.Drawing.Point(20, 29);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(104, 16);
            this.label112.TabIndex = 19;
            this.label112.Text = "Select Branch";
            // 
            // drpBranchFilter
            // 
            this.drpBranchFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranchFilter.FormattingEnabled = true;
            this.drpBranchFilter.Location = new System.Drawing.Point(143, 26);
            this.drpBranchFilter.Name = "drpBranchFilter";
            this.drpBranchFilter.Size = new System.Drawing.Size(121, 26);
            this.drpBranchFilter.TabIndex = 18;
            this.drpBranchFilter.SelectedIndexChanged += new System.EventHandler(this.drpBranchFilter_SelectedIndexChanged);
            // 
            // dgvHubList
            // 
            this.dgvHubList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHubList.GridColor = System.Drawing.SystemColors.Control;
            this.dgvHubList.Location = new System.Drawing.Point(10, 9);
            this.dgvHubList.Name = "dgvHubList";
            this.dgvHubList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHubList.Size = new System.Drawing.Size(386, 486);
            this.dgvHubList.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.lblUp);
            this.groupBox10.Controls.Add(this.label10);
            this.groupBox10.Controls.Add(this.lblDown);
            this.groupBox10.Controls.Add(this.label9);
            this.groupBox10.Controls.Add(this.btnLocationClose);
            this.groupBox10.Controls.Add(this.btnSaveLocation);
            this.groupBox10.Controls.Add(this.drpBinDown);
            this.groupBox10.Controls.Add(this.drpBinUp);
            this.groupBox10.Controls.Add(this.drpBranch);
            this.groupBox10.Controls.Add(this.txtSearch);
            this.groupBox10.Location = new System.Drawing.Point(439, 134);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(348, 243);
            this.groupBox10.TabIndex = 60;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Assign Bay";
            // 
            // lblUp
            // 
            this.lblUp.AutoSize = true;
            this.lblUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUp.ForeColor = System.Drawing.Color.Black;
            this.lblUp.Location = new System.Drawing.Point(20, 109);
            this.lblUp.Name = "lblUp";
            this.lblUp.Size = new System.Drawing.Size(102, 16);
            this.lblUp.TabIndex = 10;
            this.lblUp.Text = "Select Bin Up";
            this.lblUp.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 16);
            this.label10.TabIndex = 9;
            this.label10.Text = "Select Branch";
            // 
            // lblDown
            // 
            this.lblDown.AutoSize = true;
            this.lblDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDown.ForeColor = System.Drawing.Color.Black;
            this.lblDown.Location = new System.Drawing.Point(20, 144);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(120, 16);
            this.lblDown.TabIndex = 12;
            this.lblDown.Text = "Select Bin Down";
            this.lblDown.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(20, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 16);
            this.label9.TabIndex = 8;
            this.label9.Text = "Enter Pincode";
            // 
            // btnLocationClose
            // 
            this.btnLocationClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnLocationClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLocationClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnLocationClose.Location = new System.Drawing.Point(199, 194);
            this.btnLocationClose.Name = "btnLocationClose";
            this.btnLocationClose.Size = new System.Drawing.Size(79, 30);
            this.btnLocationClose.TabIndex = 59;
            this.btnLocationClose.Text = "Close";
            this.btnLocationClose.UseVisualStyleBackColor = false;
            this.btnLocationClose.Click += new System.EventHandler(this.btnLocationClose_Click);
            // 
            // btnSaveLocation
            // 
            this.btnSaveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnSaveLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveLocation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnSaveLocation.Location = new System.Drawing.Point(97, 194);
            this.btnSaveLocation.Name = "btnSaveLocation";
            this.btnSaveLocation.Size = new System.Drawing.Size(79, 30);
            this.btnSaveLocation.TabIndex = 58;
            this.btnSaveLocation.Text = "Save";
            this.btnSaveLocation.UseVisualStyleBackColor = false;
            this.btnSaveLocation.Click += new System.EventHandler(this.btnSaveLocation_Click);
            // 
            // drpBinDown
            // 
            this.drpBinDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinDown.FormattingEnabled = true;
            this.drpBinDown.Location = new System.Drawing.Point(149, 141);
            this.drpBinDown.Name = "drpBinDown";
            this.drpBinDown.Size = new System.Drawing.Size(121, 26);
            this.drpBinDown.TabIndex = 11;
            this.drpBinDown.Visible = false;
            // 
            // drpBinUp
            // 
            this.drpBinUp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinUp.FormattingEnabled = true;
            this.drpBinUp.Location = new System.Drawing.Point(149, 104);
            this.drpBinUp.Name = "drpBinUp";
            this.drpBinUp.Size = new System.Drawing.Size(121, 26);
            this.drpBinUp.TabIndex = 7;
            this.drpBinUp.Visible = false;
            // 
            // drpBranch
            // 
            this.drpBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranch.FormattingEnabled = true;
            this.drpBranch.Location = new System.Drawing.Point(149, 63);
            this.drpBranch.Name = "drpBranch";
            this.drpBranch.Size = new System.Drawing.Size(121, 26);
            this.drpBranch.TabIndex = 6;
            this.drpBranch.SelectedIndexChanged += new System.EventHandler(this.drpBranch_SelectedIndexChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(149, 32);
            this.txtSearch.MaxLength = 6;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(147, 22);
            this.txtSearch.TabIndex = 5;
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // btnAddNewLocation
            // 
            this.btnAddNewLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnAddNewLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNewLocation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnAddNewLocation.Location = new System.Drawing.Point(568, 402);
            this.btnAddNewLocation.Name = "btnAddNewLocation";
            this.btnAddNewLocation.Size = new System.Drawing.Size(161, 30);
            this.btnAddNewLocation.TabIndex = 28;
            this.btnAddNewLocation.Text = "Add New Location";
            this.btnAddNewLocation.UseVisualStyleBackColor = false;
            this.btnAddNewLocation.Visible = false;
            this.btnAddNewLocation.Click += new System.EventHandler(this.btnAddNewLocation_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.BackColor = System.Drawing.Color.Teal;
            this.tabPage4.BackgroundImage = global::SorterAPI.Properties.Resources.diemension_HihgResolution;
            this.tabPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage4.Controls.Add(this.panel6);
            this.tabPage4.Controls.Add(this.button11);
            this.tabPage4.Controls.Add(this.btnSaveDBInfo);
            this.tabPage4.Controls.Add(this.groupBox9);
            this.tabPage4.Location = new System.Drawing.Point(4, 41);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(989, 598);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Link Database   ";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel6.Controls.Add(this.txtMaxWe);
            this.panel6.Controls.Add(this.txtMinWe);
            this.panel6.Controls.Add(this.txtMaxL);
            this.panel6.Controls.Add(this.txtMinL);
            this.panel6.Controls.Add(this.txtMaxW);
            this.panel6.Controls.Add(this.txtMinW);
            this.panel6.Controls.Add(this.pnlMySQL);
            this.panel6.Controls.Add(this.pnlAccess);
            this.panel6.Controls.Add(this.pnlOracle);
            this.panel6.Controls.Add(this.pnlOurSql);
            this.panel6.Controls.Add(this.pnlOurAccess);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.txtMaxH);
            this.panel6.Controls.Add(this.txtMinH);
            this.panel6.Controls.Add(this.txtOurColumn4);
            this.panel6.Controls.Add(this.txtOurColumn3);
            this.panel6.Controls.Add(this.txtOurColumn2);
            this.panel6.Controls.Add(this.txtOurColumn1);
            this.panel6.Controls.Add(this.txtOurTableName);
            this.panel6.Controls.Add(this.txtOurDBName);
            this.panel6.Controls.Add(this.label103);
            this.panel6.Controls.Add(this.label102);
            this.panel6.Controls.Add(this.groupBox6);
            this.panel6.Controls.Add(this.btnConnTest);
            this.panel6.Controls.Add(this.btnTestOurConn);
            this.panel6.Controls.Add(this.pnlOurOracle);
            this.panel6.Controls.Add(this.pnlSql);
            this.panel6.Controls.Add(this.label53);
            this.panel6.Controls.Add(this.drpColumnName4);
            this.panel6.Controls.Add(this.label54);
            this.panel6.Controls.Add(this.label51);
            this.panel6.Controls.Add(this.drpColumnName3);
            this.panel6.Controls.Add(this.label52);
            this.panel6.Controls.Add(this.label49);
            this.panel6.Controls.Add(this.drpColumnName2);
            this.panel6.Controls.Add(this.label50);
            this.panel6.Controls.Add(this.drpOurDataSource);
            this.panel6.Controls.Add(this.label42);
            this.panel6.Controls.Add(this.label43);
            this.panel6.Controls.Add(this.label44);
            this.panel6.Controls.Add(this.label48);
            this.panel6.Controls.Add(this.drpColumnName1);
            this.panel6.Controls.Add(this.drpTableName);
            this.panel6.Controls.Add(this.drpDBName);
            this.panel6.Controls.Add(this.drpDataSource);
            this.panel6.Controls.Add(this.label38);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Controls.Add(this.label36);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Location = new System.Drawing.Point(17, 32);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(953, 474);
            this.panel6.TabIndex = 58;
            // 
            // txtMaxWe
            // 
            this.txtMaxWe.Location = new System.Drawing.Point(883, 418);
            this.txtMaxWe.MaxLength = 4;
            this.txtMaxWe.Name = "txtMaxWe";
            this.txtMaxWe.Size = new System.Drawing.Size(57, 24);
            this.txtMaxWe.TabIndex = 169;
            // 
            // txtMinWe
            // 
            this.txtMinWe.Location = new System.Drawing.Point(815, 418);
            this.txtMinWe.MaxLength = 4;
            this.txtMinWe.Name = "txtMinWe";
            this.txtMinWe.Size = new System.Drawing.Size(57, 24);
            this.txtMinWe.TabIndex = 168;
            // 
            // txtMaxL
            // 
            this.txtMaxL.Location = new System.Drawing.Point(883, 390);
            this.txtMaxL.MaxLength = 4;
            this.txtMaxL.Name = "txtMaxL";
            this.txtMaxL.Size = new System.Drawing.Size(57, 24);
            this.txtMaxL.TabIndex = 167;
            // 
            // txtMinL
            // 
            this.txtMinL.Location = new System.Drawing.Point(815, 390);
            this.txtMinL.MaxLength = 4;
            this.txtMinL.Name = "txtMinL";
            this.txtMinL.Size = new System.Drawing.Size(57, 24);
            this.txtMinL.TabIndex = 166;
            // 
            // txtMaxW
            // 
            this.txtMaxW.Location = new System.Drawing.Point(883, 362);
            this.txtMaxW.MaxLength = 4;
            this.txtMaxW.Name = "txtMaxW";
            this.txtMaxW.Size = new System.Drawing.Size(57, 24);
            this.txtMaxW.TabIndex = 165;
            // 
            // txtMinW
            // 
            this.txtMinW.Location = new System.Drawing.Point(815, 362);
            this.txtMinW.MaxLength = 4;
            this.txtMinW.Name = "txtMinW";
            this.txtMinW.Size = new System.Drawing.Size(57, 24);
            this.txtMinW.TabIndex = 164;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(896, 315);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 16);
            this.label4.TabIndex = 163;
            this.label4.Text = "Max";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(828, 315);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 16);
            this.label3.TabIndex = 162;
            this.label3.Text = "Min";
            // 
            // txtMaxH
            // 
            this.txtMaxH.Location = new System.Drawing.Point(883, 334);
            this.txtMaxH.MaxLength = 4;
            this.txtMaxH.Name = "txtMaxH";
            this.txtMaxH.Size = new System.Drawing.Size(57, 24);
            this.txtMaxH.TabIndex = 161;
            // 
            // txtMinH
            // 
            this.txtMinH.Location = new System.Drawing.Point(815, 334);
            this.txtMinH.MaxLength = 4;
            this.txtMinH.Name = "txtMinH";
            this.txtMinH.Size = new System.Drawing.Size(57, 24);
            this.txtMinH.TabIndex = 160;
            // 
            // txtOurColumn4
            // 
            this.txtOurColumn4.BackColor = System.Drawing.Color.LightGray;
            this.txtOurColumn4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurColumn4.Enabled = false;
            this.txtOurColumn4.Location = new System.Drawing.Point(225, 416);
            this.txtOurColumn4.Name = "txtOurColumn4";
            this.txtOurColumn4.Size = new System.Drawing.Size(173, 24);
            this.txtOurColumn4.TabIndex = 159;
            this.txtOurColumn4.Text = "Weight";
            // 
            // txtOurColumn3
            // 
            this.txtOurColumn3.BackColor = System.Drawing.Color.LightGray;
            this.txtOurColumn3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurColumn3.Enabled = false;
            this.txtOurColumn3.Location = new System.Drawing.Point(225, 386);
            this.txtOurColumn3.Name = "txtOurColumn3";
            this.txtOurColumn3.Size = new System.Drawing.Size(173, 24);
            this.txtOurColumn3.TabIndex = 158;
            this.txtOurColumn3.Text = "Length";
            // 
            // txtOurColumn2
            // 
            this.txtOurColumn2.BackColor = System.Drawing.Color.LightGray;
            this.txtOurColumn2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurColumn2.Enabled = false;
            this.txtOurColumn2.Location = new System.Drawing.Point(225, 356);
            this.txtOurColumn2.Name = "txtOurColumn2";
            this.txtOurColumn2.Size = new System.Drawing.Size(173, 24);
            this.txtOurColumn2.TabIndex = 157;
            this.txtOurColumn2.Text = "Width";
            // 
            // txtOurColumn1
            // 
            this.txtOurColumn1.BackColor = System.Drawing.Color.LightGray;
            this.txtOurColumn1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurColumn1.Enabled = false;
            this.txtOurColumn1.Location = new System.Drawing.Point(225, 327);
            this.txtOurColumn1.Name = "txtOurColumn1";
            this.txtOurColumn1.Size = new System.Drawing.Size(173, 24);
            this.txtOurColumn1.TabIndex = 156;
            this.txtOurColumn1.Text = "Height";
            // 
            // txtOurTableName
            // 
            this.txtOurTableName.BackColor = System.Drawing.Color.LightGray;
            this.txtOurTableName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurTableName.Enabled = false;
            this.txtOurTableName.Location = new System.Drawing.Point(225, 287);
            this.txtOurTableName.Name = "txtOurTableName";
            this.txtOurTableName.Size = new System.Drawing.Size(173, 24);
            this.txtOurTableName.TabIndex = 155;
            this.txtOurTableName.Text = "Product";
            // 
            // txtOurDBName
            // 
            this.txtOurDBName.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBName.Enabled = false;
            this.txtOurDBName.Location = new System.Drawing.Point(225, 250);
            this.txtOurDBName.Name = "txtOurDBName";
            this.txtOurDBName.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBName.TabIndex = 154;
            this.txtOurDBName.Text = "GPO";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label103.Location = new System.Drawing.Point(562, 11);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(127, 16);
            this.label103.TabIndex = 153;
            this.label103.Text = "Customer DB Info";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label102.Location = new System.Drawing.Point(136, 11);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(132, 16);
            this.label102.TabIndex = 152;
            this.label102.Text = "Armstrong DB Info";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Black;
            this.groupBox6.Location = new System.Drawing.Point(413, 11);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(2, 460);
            this.groupBox6.TabIndex = 151;
            this.groupBox6.TabStop = false;
            // 
            // btnConnTest
            // 
            this.btnConnTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnTest.Location = new System.Drawing.Point(776, 220);
            this.btnConnTest.Name = "btnConnTest";
            this.btnConnTest.Size = new System.Drawing.Size(38, 23);
            this.btnConnTest.TabIndex = 141;
            this.btnConnTest.Text = "Test";
            this.btnConnTest.UseVisualStyleBackColor = true;
            this.btnConnTest.Click += new System.EventHandler(this.btnConnTest_Click);
            // 
            // btnTestOurConn
            // 
            this.btnTestOurConn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestOurConn.Location = new System.Drawing.Point(360, 224);
            this.btnTestOurConn.Name = "btnTestOurConn";
            this.btnTestOurConn.Size = new System.Drawing.Size(38, 23);
            this.btnTestOurConn.TabIndex = 150;
            this.btnTestOurConn.Text = "Test";
            this.btnTestOurConn.UseVisualStyleBackColor = true;
            // 
            // pnlMySQL
            // 
            this.pnlMySQL.Controls.Add(this.label87);
            this.pnlMySQL.Controls.Add(this.label88);
            this.pnlMySQL.Controls.Add(this.label89);
            this.pnlMySQL.Controls.Add(this.txtServerMy);
            this.pnlMySQL.Controls.Add(this.txtDBIdMy);
            this.pnlMySQL.Controls.Add(this.txtDBPassMy);
            this.pnlMySQL.Location = new System.Drawing.Point(429, 58);
            this.pnlMySQL.Name = "pnlMySQL";
            this.pnlMySQL.Size = new System.Drawing.Size(399, 149);
            this.pnlMySQL.TabIndex = 144;
            this.pnlMySQL.Visible = false;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(6, 21);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(94, 16);
            this.label87.TabIndex = 1;
            this.label87.Text = "Enter Server";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(6, 66);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(133, 16);
            this.label88.TabIndex = 2;
            this.label88.Text = "Enter Database Id";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(6, 111);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(188, 16);
            this.label89.TabIndex = 3;
            this.label89.Text = "Enter Database Password";
            // 
            // txtServerMy
            // 
            this.txtServerMy.BackColor = System.Drawing.Color.LightGray;
            this.txtServerMy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServerMy.Location = new System.Drawing.Point(210, 18);
            this.txtServerMy.Name = "txtServerMy";
            this.txtServerMy.Size = new System.Drawing.Size(173, 24);
            this.txtServerMy.TabIndex = 11;
            // 
            // txtDBIdMy
            // 
            this.txtDBIdMy.BackColor = System.Drawing.Color.LightGray;
            this.txtDBIdMy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBIdMy.Location = new System.Drawing.Point(210, 63);
            this.txtDBIdMy.Name = "txtDBIdMy";
            this.txtDBIdMy.Size = new System.Drawing.Size(173, 24);
            this.txtDBIdMy.TabIndex = 12;
            // 
            // txtDBPassMy
            // 
            this.txtDBPassMy.BackColor = System.Drawing.Color.LightGray;
            this.txtDBPassMy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBPassMy.Location = new System.Drawing.Point(210, 108);
            this.txtDBPassMy.Name = "txtDBPassMy";
            this.txtDBPassMy.Size = new System.Drawing.Size(173, 24);
            this.txtDBPassMy.TabIndex = 13;
            // 
            // pnlOurMySql
            // 
            this.pnlOurMySql.Controls.Add(this.label99);
            this.pnlOurMySql.Controls.Add(this.label100);
            this.pnlOurMySql.Controls.Add(this.label101);
            this.pnlOurMySql.Controls.Add(this.txtOurServerMy);
            this.pnlOurMySql.Controls.Add(this.txtOurDBPassMy);
            this.pnlOurMySql.Controls.Add(this.txtOurDBIdMy);
            this.pnlOurMySql.Location = new System.Drawing.Point(3, 3);
            this.pnlOurMySql.Name = "pnlOurMySql";
            this.pnlOurMySql.Size = new System.Drawing.Size(396, 155);
            this.pnlOurMySql.TabIndex = 149;
            this.pnlOurMySql.Visible = false;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(6, 20);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(94, 16);
            this.label99.TabIndex = 1;
            this.label99.Text = "Enter Server";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(6, 65);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(133, 16);
            this.label100.TabIndex = 2;
            this.label100.Text = "Enter Database Id";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(6, 110);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(188, 16);
            this.label101.TabIndex = 3;
            this.label101.Text = "Enter Database Password";
            // 
            // txtOurServerMy
            // 
            this.txtOurServerMy.BackColor = System.Drawing.Color.LightGray;
            this.txtOurServerMy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurServerMy.Location = new System.Drawing.Point(220, 16);
            this.txtOurServerMy.Name = "txtOurServerMy";
            this.txtOurServerMy.Size = new System.Drawing.Size(173, 24);
            this.txtOurServerMy.TabIndex = 11;
            // 
            // txtOurDBPassMy
            // 
            this.txtOurDBPassMy.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBPassMy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBPassMy.Location = new System.Drawing.Point(220, 61);
            this.txtOurDBPassMy.Name = "txtOurDBPassMy";
            this.txtOurDBPassMy.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBPassMy.TabIndex = 12;
            // 
            // txtOurDBIdMy
            // 
            this.txtOurDBIdMy.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBIdMy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBIdMy.Location = new System.Drawing.Point(220, 106);
            this.txtOurDBIdMy.Name = "txtOurDBIdMy";
            this.txtOurDBIdMy.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBIdMy.TabIndex = 13;
            // 
            // pnlOurAccess
            // 
            this.pnlOurAccess.Controls.Add(this.label96);
            this.pnlOurAccess.Controls.Add(this.label97);
            this.pnlOurAccess.Controls.Add(this.label98);
            this.pnlOurAccess.Controls.Add(this.txtOurDBProviderA);
            this.pnlOurAccess.Controls.Add(this.txtOurDBSourcePathA);
            this.pnlOurAccess.Controls.Add(this.txtOurDBPassworA);
            this.pnlOurAccess.Controls.Add(this.pnlOurMySql);
            this.pnlOurAccess.Location = new System.Drawing.Point(3, 60);
            this.pnlOurAccess.Name = "pnlOurAccess";
            this.pnlOurAccess.Size = new System.Drawing.Size(402, 155);
            this.pnlOurAccess.TabIndex = 147;
            this.pnlOurAccess.Visible = false;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(6, 32);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(179, 16);
            this.label96.TabIndex = 1;
            this.label96.Text = "Enter Database Provider";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(6, 77);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(169, 16);
            this.label97.TabIndex = 2;
            this.label97.Text = "Enter Data Source Path";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(6, 122);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(187, 16);
            this.label98.TabIndex = 3;
            this.label98.Text = "Enter Database password";
            // 
            // txtOurDBProviderA
            // 
            this.txtOurDBProviderA.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBProviderA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBProviderA.Location = new System.Drawing.Point(217, 29);
            this.txtOurDBProviderA.Name = "txtOurDBProviderA";
            this.txtOurDBProviderA.ReadOnly = true;
            this.txtOurDBProviderA.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBProviderA.TabIndex = 11;
            this.txtOurDBProviderA.Text = "Microsoft.ACE.OLEDB.12.0";
            // 
            // txtOurDBSourcePathA
            // 
            this.txtOurDBSourcePathA.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBSourcePathA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBSourcePathA.Location = new System.Drawing.Point(217, 74);
            this.txtOurDBSourcePathA.Name = "txtOurDBSourcePathA";
            this.txtOurDBSourcePathA.ReadOnly = true;
            this.txtOurDBSourcePathA.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBSourcePathA.TabIndex = 12;
            // 
            // txtOurDBPassworA
            // 
            this.txtOurDBPassworA.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBPassworA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBPassworA.Location = new System.Drawing.Point(217, 119);
            this.txtOurDBPassworA.Name = "txtOurDBPassworA";
            this.txtOurDBPassworA.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBPassworA.TabIndex = 13;
            // 
            // pnlOurSql
            // 
            this.pnlOurSql.Controls.Add(this.label45);
            this.pnlOurSql.Controls.Add(this.label46);
            this.pnlOurSql.Controls.Add(this.label47);
            this.pnlOurSql.Controls.Add(this.txtOurDBIpAddr);
            this.pnlOurSql.Controls.Add(this.txtOurDBUsername);
            this.pnlOurSql.Controls.Add(this.txtOurDBPassword);
            this.pnlOurSql.Location = new System.Drawing.Point(3, 60);
            this.pnlOurSql.Name = "pnlOurSql";
            this.pnlOurSql.Size = new System.Drawing.Size(402, 155);
            this.pnlOurSql.TabIndex = 148;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(6, 32);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(94, 16);
            this.label45.TabIndex = 1;
            this.label45.Text = "Enter Server";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(6, 77);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(133, 16);
            this.label46.TabIndex = 2;
            this.label46.Text = "Enter Database Id";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(6, 122);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(188, 16);
            this.label47.TabIndex = 3;
            this.label47.Text = "Enter Database Password";
            // 
            // txtOurDBIpAddr
            // 
            this.txtOurDBIpAddr.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBIpAddr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBIpAddr.Location = new System.Drawing.Point(217, 29);
            this.txtOurDBIpAddr.Name = "txtOurDBIpAddr";
            this.txtOurDBIpAddr.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBIpAddr.TabIndex = 11;
            // 
            // txtOurDBUsername
            // 
            this.txtOurDBUsername.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBUsername.Location = new System.Drawing.Point(217, 74);
            this.txtOurDBUsername.Name = "txtOurDBUsername";
            this.txtOurDBUsername.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBUsername.TabIndex = 12;
            // 
            // txtOurDBPassword
            // 
            this.txtOurDBPassword.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBPassword.Location = new System.Drawing.Point(217, 119);
            this.txtOurDBPassword.Name = "txtOurDBPassword";
            this.txtOurDBPassword.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBPassword.TabIndex = 13;
            // 
            // pnlAccess
            // 
            this.pnlAccess.Controls.Add(this.label84);
            this.pnlAccess.Controls.Add(this.label85);
            this.pnlAccess.Controls.Add(this.label86);
            this.pnlAccess.Controls.Add(this.txtDBProviderA);
            this.pnlAccess.Controls.Add(this.txtDBSourcePathA);
            this.pnlAccess.Controls.Add(this.txtDBPassworA);
            this.pnlAccess.Location = new System.Drawing.Point(430, 61);
            this.pnlAccess.Name = "pnlAccess";
            this.pnlAccess.Size = new System.Drawing.Size(394, 155);
            this.pnlAccess.TabIndex = 143;
            this.pnlAccess.Visible = false;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(6, 32);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(179, 16);
            this.label84.TabIndex = 1;
            this.label84.Text = "Enter Database Provider";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(6, 77);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(169, 16);
            this.label85.TabIndex = 2;
            this.label85.Text = "Enter Data Source Path";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(6, 122);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(187, 16);
            this.label86.TabIndex = 3;
            this.label86.Text = "Enter Database password";
            // 
            // txtDBProviderA
            // 
            this.txtDBProviderA.BackColor = System.Drawing.Color.LightGray;
            this.txtDBProviderA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBProviderA.Location = new System.Drawing.Point(217, 29);
            this.txtDBProviderA.Name = "txtDBProviderA";
            this.txtDBProviderA.ReadOnly = true;
            this.txtDBProviderA.Size = new System.Drawing.Size(173, 24);
            this.txtDBProviderA.TabIndex = 11;
            this.txtDBProviderA.Text = "Microsoft.ACE.OLEDB.12.0";
            // 
            // txtDBSourcePathA
            // 
            this.txtDBSourcePathA.BackColor = System.Drawing.Color.LightGray;
            this.txtDBSourcePathA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBSourcePathA.Location = new System.Drawing.Point(217, 74);
            this.txtDBSourcePathA.Name = "txtDBSourcePathA";
            this.txtDBSourcePathA.ReadOnly = true;
            this.txtDBSourcePathA.Size = new System.Drawing.Size(173, 24);
            this.txtDBSourcePathA.TabIndex = 12;
            // 
            // txtDBPassworA
            // 
            this.txtDBPassworA.BackColor = System.Drawing.Color.LightGray;
            this.txtDBPassworA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBPassworA.Location = new System.Drawing.Point(217, 119);
            this.txtDBPassworA.Name = "txtDBPassworA";
            this.txtDBPassworA.Size = new System.Drawing.Size(173, 24);
            this.txtDBPassworA.TabIndex = 13;
            // 
            // pnlOurOracle
            // 
            this.pnlOurOracle.Controls.Add(this.drpOurIntegratedSecurity);
            this.pnlOurOracle.Controls.Add(this.label93);
            this.pnlOurOracle.Controls.Add(this.label94);
            this.pnlOurOracle.Controls.Add(this.label95);
            this.pnlOurOracle.Controls.Add(this.txtOurDBIdOracle);
            this.pnlOurOracle.Controls.Add(this.txtOurDBPassOracle);
            this.pnlOurOracle.Location = new System.Drawing.Point(3, 63);
            this.pnlOurOracle.Name = "pnlOurOracle";
            this.pnlOurOracle.Size = new System.Drawing.Size(402, 155);
            this.pnlOurOracle.TabIndex = 146;
            this.pnlOurOracle.Visible = false;
            // 
            // drpOurIntegratedSecurity
            // 
            this.drpOurIntegratedSecurity.BackColor = System.Drawing.Color.LightGray;
            this.drpOurIntegratedSecurity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpOurIntegratedSecurity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpOurIntegratedSecurity.FormattingEnabled = true;
            this.drpOurIntegratedSecurity.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.drpOurIntegratedSecurity.Location = new System.Drawing.Point(217, 112);
            this.drpOurIntegratedSecurity.Name = "drpOurIntegratedSecurity";
            this.drpOurIntegratedSecurity.Size = new System.Drawing.Size(144, 26);
            this.drpOurIntegratedSecurity.TabIndex = 53;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(6, 114);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(138, 16);
            this.label93.TabIndex = 52;
            this.label93.Text = "Integrated Security";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(6, 23);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(133, 16);
            this.label94.TabIndex = 2;
            this.label94.Text = "Enter Database Id";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(6, 68);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(188, 16);
            this.label95.TabIndex = 3;
            this.label95.Text = "Enter Database Password";
            // 
            // txtOurDBIdOracle
            // 
            this.txtOurDBIdOracle.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBIdOracle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBIdOracle.Location = new System.Drawing.Point(217, 12);
            this.txtOurDBIdOracle.Name = "txtOurDBIdOracle";
            this.txtOurDBIdOracle.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBIdOracle.TabIndex = 12;
            // 
            // txtOurDBPassOracle
            // 
            this.txtOurDBPassOracle.BackColor = System.Drawing.Color.LightGray;
            this.txtOurDBPassOracle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurDBPassOracle.Location = new System.Drawing.Point(217, 64);
            this.txtOurDBPassOracle.Name = "txtOurDBPassOracle";
            this.txtOurDBPassOracle.Size = new System.Drawing.Size(173, 24);
            this.txtOurDBPassOracle.TabIndex = 13;
            // 
            // pnlOracle
            // 
            this.pnlOracle.Controls.Add(this.drpIntegratedSecurity);
            this.pnlOracle.Controls.Add(this.label90);
            this.pnlOracle.Controls.Add(this.label91);
            this.pnlOracle.Controls.Add(this.label92);
            this.pnlOracle.Controls.Add(this.txtDBIdOracle);
            this.pnlOracle.Controls.Add(this.txtDBPassOracle);
            this.pnlOracle.Location = new System.Drawing.Point(430, 63);
            this.pnlOracle.Name = "pnlOracle";
            this.pnlOracle.Size = new System.Drawing.Size(402, 155);
            this.pnlOracle.TabIndex = 145;
            this.pnlOracle.Visible = false;
            // 
            // drpIntegratedSecurity
            // 
            this.drpIntegratedSecurity.BackColor = System.Drawing.Color.LightGray;
            this.drpIntegratedSecurity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpIntegratedSecurity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpIntegratedSecurity.FormattingEnabled = true;
            this.drpIntegratedSecurity.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.drpIntegratedSecurity.Location = new System.Drawing.Point(217, 112);
            this.drpIntegratedSecurity.Name = "drpIntegratedSecurity";
            this.drpIntegratedSecurity.Size = new System.Drawing.Size(144, 26);
            this.drpIntegratedSecurity.TabIndex = 53;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(6, 114);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(138, 16);
            this.label90.TabIndex = 52;
            this.label90.Text = "Integrated Security";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(6, 23);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(133, 16);
            this.label91.TabIndex = 2;
            this.label91.Text = "Enter Database Id";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(6, 68);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(188, 16);
            this.label92.TabIndex = 3;
            this.label92.Text = "Enter Database Password";
            // 
            // txtDBIdOracle
            // 
            this.txtDBIdOracle.BackColor = System.Drawing.Color.LightGray;
            this.txtDBIdOracle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBIdOracle.Location = new System.Drawing.Point(217, 12);
            this.txtDBIdOracle.Name = "txtDBIdOracle";
            this.txtDBIdOracle.Size = new System.Drawing.Size(173, 24);
            this.txtDBIdOracle.TabIndex = 12;
            // 
            // txtDBPassOracle
            // 
            this.txtDBPassOracle.BackColor = System.Drawing.Color.LightGray;
            this.txtDBPassOracle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBPassOracle.Location = new System.Drawing.Point(217, 64);
            this.txtDBPassOracle.Name = "txtDBPassOracle";
            this.txtDBPassOracle.Size = new System.Drawing.Size(173, 24);
            this.txtDBPassOracle.TabIndex = 13;
            // 
            // pnlSql
            // 
            this.pnlSql.Controls.Add(this.label33);
            this.pnlSql.Controls.Add(this.label34);
            this.pnlSql.Controls.Add(this.label35);
            this.pnlSql.Controls.Add(this.txtDBIpAddr);
            this.pnlSql.Controls.Add(this.txtDBUsername);
            this.pnlSql.Controls.Add(this.txtDBPassword);
            this.pnlSql.Location = new System.Drawing.Point(430, 64);
            this.pnlSql.Name = "pnlSql";
            this.pnlSql.Size = new System.Drawing.Size(402, 155);
            this.pnlSql.TabIndex = 142;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(6, 32);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(166, 32);
            this.label33.TabIndex = 1;
            this.label33.Text = "Enter Database Server\r\nIP";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(6, 77);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(191, 16);
            this.label34.TabIndex = 2;
            this.label34.Text = "Enter Database Username";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(6, 122);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(187, 16);
            this.label35.TabIndex = 3;
            this.label35.Text = "Enter Database password";
            // 
            // txtDBIpAddr
            // 
            this.txtDBIpAddr.BackColor = System.Drawing.Color.LightGray;
            this.txtDBIpAddr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBIpAddr.Location = new System.Drawing.Point(217, 25);
            this.txtDBIpAddr.Name = "txtDBIpAddr";
            this.txtDBIpAddr.Size = new System.Drawing.Size(173, 24);
            this.txtDBIpAddr.TabIndex = 11;
            // 
            // txtDBUsername
            // 
            this.txtDBUsername.BackColor = System.Drawing.Color.LightGray;
            this.txtDBUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBUsername.Location = new System.Drawing.Point(217, 73);
            this.txtDBUsername.Name = "txtDBUsername";
            this.txtDBUsername.Size = new System.Drawing.Size(173, 24);
            this.txtDBUsername.TabIndex = 12;
            // 
            // txtDBPassword
            // 
            this.txtDBPassword.BackColor = System.Drawing.Color.LightGray;
            this.txtDBPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBPassword.Location = new System.Drawing.Point(217, 119);
            this.txtDBPassword.Name = "txtDBPassword";
            this.txtDBPassword.Size = new System.Drawing.Size(173, 24);
            this.txtDBPassword.TabIndex = 13;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(31, 420);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(168, 16);
            this.label53.TabIndex = 135;
            this.label53.Text = "Select Columns Names";
            // 
            // drpColumnName4
            // 
            this.drpColumnName4.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpColumnName4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName4.FormattingEnabled = true;
            this.drpColumnName4.Location = new System.Drawing.Point(634, 418);
            this.drpColumnName4.Name = "drpColumnName4";
            this.drpColumnName4.Size = new System.Drawing.Size(174, 26);
            this.drpColumnName4.TabIndex = 134;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(427, 425);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(168, 16);
            this.label54.TabIndex = 133;
            this.label54.Text = "Select Columns Names";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(31, 390);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(168, 16);
            this.label51.TabIndex = 131;
            this.label51.Text = "Select Columns Names";
            // 
            // drpColumnName3
            // 
            this.drpColumnName3.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpColumnName3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName3.FormattingEnabled = true;
            this.drpColumnName3.Location = new System.Drawing.Point(634, 390);
            this.drpColumnName3.Name = "drpColumnName3";
            this.drpColumnName3.Size = new System.Drawing.Size(174, 26);
            this.drpColumnName3.TabIndex = 130;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(427, 395);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(168, 16);
            this.label52.TabIndex = 129;
            this.label52.Text = "Select Columns Names";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(30, 360);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(168, 16);
            this.label49.TabIndex = 127;
            this.label49.Text = "Select Columns Names";
            // 
            // drpColumnName2
            // 
            this.drpColumnName2.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName2.FormattingEnabled = true;
            this.drpColumnName2.Location = new System.Drawing.Point(634, 362);
            this.drpColumnName2.Name = "drpColumnName2";
            this.drpColumnName2.Size = new System.Drawing.Size(173, 26);
            this.drpColumnName2.TabIndex = 126;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(427, 365);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(168, 16);
            this.label50.TabIndex = 125;
            this.label50.Text = "Select Columns Names";
            // 
            // drpOurDataSource
            // 
            this.drpOurDataSource.BackColor = System.Drawing.Color.LightGray;
            this.drpOurDataSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpOurDataSource.FormattingEnabled = true;
            this.drpOurDataSource.Items.AddRange(new object[] {
            "SQLServer",
            "MSAccess",
            "MySql",
            "Oracle"});
            this.drpOurDataSource.Location = new System.Drawing.Point(222, 34);
            this.drpOurDataSource.Name = "drpOurDataSource";
            this.drpOurDataSource.Size = new System.Drawing.Size(175, 26);
            this.drpOurDataSource.TabIndex = 121;
            this.drpOurDataSource.SelectedIndexChanged += new System.EventHandler(this.drpOurDataSource_SelectedIndexChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(30, 331);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(168, 16);
            this.label42.TabIndex = 120;
            this.label42.Text = "Select Columns Names";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(30, 291);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(142, 16);
            this.label43.TabIndex = 119;
            this.label43.Text = "Select Table Name";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(30, 254);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(169, 16);
            this.label44.TabIndex = 118;
            this.label44.Text = "Select Database Name";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(8, 39);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(192, 16);
            this.label48.TabIndex = 117;
            this.label48.Text = "Select Our Database Type";
            // 
            // drpColumnName1
            // 
            this.drpColumnName1.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpColumnName1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName1.FormattingEnabled = true;
            this.drpColumnName1.Location = new System.Drawing.Point(634, 334);
            this.drpColumnName1.Name = "drpColumnName1";
            this.drpColumnName1.Size = new System.Drawing.Size(173, 26);
            this.drpColumnName1.TabIndex = 116;
            // 
            // drpTableName
            // 
            this.drpTableName.BackColor = System.Drawing.Color.LightGray;
            this.drpTableName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpTableName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpTableName.FormattingEnabled = true;
            this.drpTableName.Location = new System.Drawing.Point(635, 293);
            this.drpTableName.Name = "drpTableName";
            this.drpTableName.Size = new System.Drawing.Size(173, 26);
            this.drpTableName.TabIndex = 115;
            this.drpTableName.SelectedIndexChanged += new System.EventHandler(this.drpTableName_SelectedIndexChanged);
            // 
            // drpDBName
            // 
            this.drpDBName.BackColor = System.Drawing.Color.LightGray;
            this.drpDBName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpDBName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpDBName.FormattingEnabled = true;
            this.drpDBName.Location = new System.Drawing.Point(635, 249);
            this.drpDBName.Name = "drpDBName";
            this.drpDBName.Size = new System.Drawing.Size(173, 26);
            this.drpDBName.TabIndex = 114;
            this.drpDBName.SelectedIndexChanged += new System.EventHandler(this.drpDBName_SelectedIndexChanged);
            // 
            // drpDataSource
            // 
            this.drpDataSource.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpDataSource.BackColor = System.Drawing.Color.LightGray;
            this.drpDataSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpDataSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpDataSource.FormattingEnabled = true;
            this.drpDataSource.Location = new System.Drawing.Point(632, 32);
            this.drpDataSource.Name = "drpDataSource";
            this.drpDataSource.Size = new System.Drawing.Size(185, 26);
            this.drpDataSource.TabIndex = 113;
            this.drpDataSource.SelectedIndexChanged += new System.EventHandler(this.drpDataSource_SelectedIndexChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(427, 336);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(168, 16);
            this.label38.TabIndex = 112;
            this.label38.Text = "Select Columns Names";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(427, 296);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(142, 16);
            this.label37.TabIndex = 111;
            this.label37.Text = "Select Table Name";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(427, 254);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(169, 16);
            this.label36.TabIndex = 110;
            this.label36.Text = "Select Database Name";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(426, 38);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(164, 16);
            this.label32.TabIndex = 109;
            this.label32.Text = "Select Database Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(872, 339);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 16);
            this.label5.TabIndex = 170;
            this.label5.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(872, 423);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 16);
            this.label8.TabIndex = 173;
            this.label8.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(872, 394);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 16);
            this.label7.TabIndex = 172;
            this.label7.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(872, 367);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 16);
            this.label6.TabIndex = 171;
            this.label6.Text = ":";
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.button11.Location = new System.Drawing.Point(446, 520);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 25);
            this.button11.TabIndex = 57;
            this.button11.Text = "Cancel";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // btnSaveDBInfo
            // 
            this.btnSaveDBInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnSaveDBInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveDBInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnSaveDBInfo.Location = new System.Drawing.Point(344, 520);
            this.btnSaveDBInfo.Name = "btnSaveDBInfo";
            this.btnSaveDBInfo.Size = new System.Drawing.Size(75, 25);
            this.btnSaveDBInfo.TabIndex = 56;
            this.btnSaveDBInfo.Text = "Save";
            this.btnSaveDBInfo.UseVisualStyleBackColor = false;
            this.btnSaveDBInfo.Click += new System.EventHandler(this.btnSaveDBInfo_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.White;
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(983, 554);
            this.groupBox9.TabIndex = 174;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Database Info";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage3.BackgroundImage = global::SorterAPI.Properties.Resources.diemensions21211;
            this.tabPage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Controls.Add(this.btnSorterwiseCassetteConfig);
            this.tabPage3.Controls.Add(this.btnPTLCancel);
            this.tabPage3.Controls.Add(this.btnPTLSystem);
            this.tabPage3.Location = new System.Drawing.Point(4, 41);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(989, 598);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "PTL System  ";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Teal;
            this.groupBox2.Controls.Add(this.PTLPanel);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(17, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(714, 329);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PTL System";
            // 
            // PTLPanel
            // 
            this.PTLPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.PTLPanel.Controls.Add(this.label83);
            this.PTLPanel.Controls.Add(this.label82);
            this.PTLPanel.Controls.Add(this.label81);
            this.PTLPanel.Controls.Add(this.label80);
            this.PTLPanel.Controls.Add(this.label31);
            this.PTLPanel.Controls.Add(this.txtBagClosurePrefix);
            this.PTLPanel.Controls.Add(this.txtMaxWeightArticle);
            this.PTLPanel.Controls.Add(this.txtMaxNoArticle);
            this.PTLPanel.Controls.Add(this.txtMinNoArticle);
            this.PTLPanel.Controls.Add(this.label30);
            this.PTLPanel.Controls.Add(this.label28);
            this.PTLPanel.Controls.Add(this.label26);
            this.PTLPanel.Controls.Add(this.label25);
            this.PTLPanel.Location = new System.Drawing.Point(24, 30);
            this.PTLPanel.Name = "PTLPanel";
            this.PTLPanel.Size = new System.Drawing.Size(665, 274);
            this.PTLPanel.TabIndex = 24;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.BackColor = System.Drawing.Color.Transparent;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Red;
            this.label83.Location = new System.Drawing.Point(451, 200);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(12, 15);
            this.label83.TabIndex = 91;
            this.label83.Text = "*";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.BackColor = System.Drawing.Color.Transparent;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Red;
            this.label82.Location = new System.Drawing.Point(451, 147);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(12, 15);
            this.label82.TabIndex = 90;
            this.label82.Text = "*";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.BackColor = System.Drawing.Color.Transparent;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Red;
            this.label81.Location = new System.Drawing.Point(451, 91);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(12, 15);
            this.label81.TabIndex = 89;
            this.label81.Text = "*";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.BackColor = System.Drawing.Color.Transparent;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Red;
            this.label80.Location = new System.Drawing.Point(451, 37);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(12, 15);
            this.label80.TabIndex = 88;
            this.label80.Text = "*";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label31.Location = new System.Drawing.Point(479, 212);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(126, 16);
            this.label31.TabIndex = 87;
            this.label31.Text = "Ex. : \'L1_\' or\'R1_\'";
            // 
            // txtBagClosurePrefix
            // 
            this.txtBagClosurePrefix.BackColor = System.Drawing.Color.LightGray;
            this.txtBagClosurePrefix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBagClosurePrefix.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBagClosurePrefix.ForeColor = System.Drawing.Color.Black;
            this.txtBagClosurePrefix.Location = new System.Drawing.Point(350, 208);
            this.txtBagClosurePrefix.MaxLength = 3;
            this.txtBagClosurePrefix.Name = "txtBagClosurePrefix";
            this.txtBagClosurePrefix.Size = new System.Drawing.Size(101, 21);
            this.txtBagClosurePrefix.TabIndex = 86;
            // 
            // txtMaxWeightArticle
            // 
            this.txtMaxWeightArticle.BackColor = System.Drawing.Color.LightGray;
            this.txtMaxWeightArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxWeightArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaxWeightArticle.ForeColor = System.Drawing.Color.Black;
            this.txtMaxWeightArticle.Location = new System.Drawing.Point(350, 154);
            this.txtMaxWeightArticle.MaxLength = 3;
            this.txtMaxWeightArticle.Name = "txtMaxWeightArticle";
            this.txtMaxWeightArticle.Size = new System.Drawing.Size(101, 21);
            this.txtMaxWeightArticle.TabIndex = 85;
            // 
            // txtMaxNoArticle
            // 
            this.txtMaxNoArticle.BackColor = System.Drawing.Color.LightGray;
            this.txtMaxNoArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxNoArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaxNoArticle.ForeColor = System.Drawing.Color.Black;
            this.txtMaxNoArticle.Location = new System.Drawing.Point(350, 100);
            this.txtMaxNoArticle.MaxLength = 3;
            this.txtMaxNoArticle.Name = "txtMaxNoArticle";
            this.txtMaxNoArticle.Size = new System.Drawing.Size(101, 21);
            this.txtMaxNoArticle.TabIndex = 84;
            // 
            // txtMinNoArticle
            // 
            this.txtMinNoArticle.BackColor = System.Drawing.Color.LightGray;
            this.txtMinNoArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMinNoArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinNoArticle.ForeColor = System.Drawing.Color.Black;
            this.txtMinNoArticle.Location = new System.Drawing.Point(350, 46);
            this.txtMinNoArticle.MaxLength = 3;
            this.txtMinNoArticle.Name = "txtMinNoArticle";
            this.txtMinNoArticle.Size = new System.Drawing.Size(101, 21);
            this.txtMinNoArticle.TabIndex = 83;
            this.txtMinNoArticle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMinNoArticle_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(19, 211);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(140, 16);
            this.label30.TabIndex = 82;
            this.label30.Text = "Bag Closure Prefix:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(19, 156);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(276, 16);
            this.label28.TabIndex = 81;
            this.label28.Text = "Max. Weight of Articles Allowed in Bag:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(19, 102);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(282, 16);
            this.label26.TabIndex = 80;
            this.label26.Text = "Max. Number of Articles Allowed in Bag:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(19, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(278, 16);
            this.label25.TabIndex = 79;
            this.label25.Text = "Min. Number of Articles Allowed in Bag:";
            // 
            // btnSorterwiseCassetteConfig
            // 
            this.btnSorterwiseCassetteConfig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(99)))), ((int)(((byte)(173)))));
            this.btnSorterwiseCassetteConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSorterwiseCassetteConfig.ForeColor = System.Drawing.Color.LightGray;
            this.btnSorterwiseCassetteConfig.Location = new System.Drawing.Point(471, 465);
            this.btnSorterwiseCassetteConfig.Name = "btnSorterwiseCassetteConfig";
            this.btnSorterwiseCassetteConfig.Size = new System.Drawing.Size(248, 25);
            this.btnSorterwiseCassetteConfig.TabIndex = 22;
            this.btnSorterwiseCassetteConfig.Text = "Sorter wise Cassette Configuration";
            this.btnSorterwiseCassetteConfig.UseVisualStyleBackColor = false;
            this.btnSorterwiseCassetteConfig.Visible = false;
            this.btnSorterwiseCassetteConfig.Click += new System.EventHandler(this.btnSorterwiseCassetteConfig_Click);
            // 
            // btnPTLCancel
            // 
            this.btnPTLCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnPTLCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPTLCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnPTLCancel.Location = new System.Drawing.Point(391, 389);
            this.btnPTLCancel.Name = "btnPTLCancel";
            this.btnPTLCancel.Size = new System.Drawing.Size(75, 25);
            this.btnPTLCancel.TabIndex = 21;
            this.btnPTLCancel.Text = "Cancel";
            this.btnPTLCancel.UseVisualStyleBackColor = false;
            this.btnPTLCancel.Click += new System.EventHandler(this.btnPTLCancel_Click);
            // 
            // btnPTLSystem
            // 
            this.btnPTLSystem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnPTLSystem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPTLSystem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnPTLSystem.Location = new System.Drawing.Point(251, 389);
            this.btnPTLSystem.Name = "btnPTLSystem";
            this.btnPTLSystem.Size = new System.Drawing.Size(75, 25);
            this.btnPTLSystem.TabIndex = 20;
            this.btnPTLSystem.Text = "Save";
            this.btnPTLSystem.UseVisualStyleBackColor = false;
            this.btnPTLSystem.Click += new System.EventHandler(this.btnPTLSystem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(134)))), ((int)(((byte)(132)))), ((int)(((byte)(113)))));
            this.tabPage2.BackgroundImage = global::SorterAPI.Properties.Resources.diemensions21211;
            this.tabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.btnReset2);
            this.tabPage2.Controls.Add(this.btnPLCDynamicDataSave);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.ForeColor = System.Drawing.Color.White;
            this.tabPage2.Location = new System.Drawing.Point(4, 41);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(989, 598);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "PLC Dynamic Data   ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Teal;
            this.groupBox3.Controls.Add(this.panel2);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(17, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(792, 452);
            this.groupBox3.TabIndex = 49;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "  PLC Data  ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel2.Controls.Add(this.label72);
            this.panel2.Controls.Add(this.label67);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label71);
            this.panel2.Controls.Add(this.label70);
            this.panel2.Controls.Add(this.label69);
            this.panel2.Controls.Add(this.label68);
            this.panel2.Controls.Add(this.label66);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label65);
            this.panel2.Controls.Add(this.label64);
            this.panel2.Controls.Add(this.txtPLCScanTime);
            this.panel2.Controls.Add(this.txtEnterDeactivationDelay);
            this.panel2.Controls.Add(this.txtEnterActivationDelay);
            this.panel2.Controls.Add(this.txtLinearDistance);
            this.panel2.Controls.Add(this.txtArmOpeningTime);
            this.panel2.Controls.Add(this.txtSpeedofConveyor);
            this.panel2.Controls.Add(this.txtEncoderPPR);
            this.panel2.Controls.Add(this.txtDistanceTravelled);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label29);
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(17, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(758, 408);
            this.panel2.TabIndex = 79;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label72.Location = new System.Drawing.Point(702, 357);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(14, 16);
            this.label72.TabIndex = 110;
            this.label72.Text = "*";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label67.Location = new System.Drawing.Point(702, 298);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(14, 16);
            this.label67.TabIndex = 109;
            this.label67.Text = "*";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label27.Location = new System.Drawing.Point(702, 174);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(14, 16);
            this.label27.TabIndex = 107;
            this.label27.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label22.Location = new System.Drawing.Point(416, 121);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 16);
            this.label22.TabIndex = 106;
            this.label22.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(416, 90);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 16);
            this.label21.TabIndex = 105;
            this.label21.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label19.Location = new System.Drawing.Point(416, 56);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 16);
            this.label19.TabIndex = 104;
            this.label19.Text = "*";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label18.Location = new System.Drawing.Point(416, 12);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 16);
            this.label18.TabIndex = 103;
            this.label18.Text = "*";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(706, 366);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(32, 15);
            this.label71.TabIndex = 102;
            this.label71.Text = "(ms)";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(706, 307);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(37, 15);
            this.label70.TabIndex = 101;
            this.label70.Text = "(mm)";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(706, 243);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(37, 15);
            this.label69.TabIndex = 100;
            this.label69.Text = "(mm)";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(706, 182);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(37, 15);
            this.label68.TabIndex = 99;
            this.label68.Text = "(mm)";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(418, 130);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(32, 15);
            this.label66.TabIndex = 98;
            this.label66.Text = "(ms)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(418, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 15);
            this.label13.TabIndex = 97;
            this.label13.Text = "(mpm)";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(418, 66);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(33, 15);
            this.label65.TabIndex = 96;
            this.label65.Text = "(ppr)";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(418, 21);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(37, 15);
            this.label64.TabIndex = 95;
            this.label64.Text = "(mm)";
            // 
            // txtPLCScanTime
            // 
            this.txtPLCScanTime.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtPLCScanTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPLCScanTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPLCScanTime.Location = new System.Drawing.Point(600, 363);
            this.txtPLCScanTime.MaxLength = 5;
            this.txtPLCScanTime.Name = "txtPLCScanTime";
            this.txtPLCScanTime.Size = new System.Drawing.Size(100, 21);
            this.txtPLCScanTime.TabIndex = 94;
            // 
            // txtEnterDeactivationDelay
            // 
            this.txtEnterDeactivationDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtEnterDeactivationDelay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEnterDeactivationDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnterDeactivationDelay.Location = new System.Drawing.Point(600, 304);
            this.txtEnterDeactivationDelay.MaxLength = 5;
            this.txtEnterDeactivationDelay.Name = "txtEnterDeactivationDelay";
            this.txtEnterDeactivationDelay.Size = new System.Drawing.Size(100, 21);
            this.txtEnterDeactivationDelay.TabIndex = 93;
            // 
            // txtEnterActivationDelay
            // 
            this.txtEnterActivationDelay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtEnterActivationDelay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEnterActivationDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnterActivationDelay.Location = new System.Drawing.Point(600, 240);
            this.txtEnterActivationDelay.MaxLength = 5;
            this.txtEnterActivationDelay.Name = "txtEnterActivationDelay";
            this.txtEnterActivationDelay.Size = new System.Drawing.Size(100, 21);
            this.txtEnterActivationDelay.TabIndex = 92;
            // 
            // txtLinearDistance
            // 
            this.txtLinearDistance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtLinearDistance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLinearDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLinearDistance.Location = new System.Drawing.Point(600, 179);
            this.txtLinearDistance.MaxLength = 5;
            this.txtLinearDistance.Name = "txtLinearDistance";
            this.txtLinearDistance.Size = new System.Drawing.Size(100, 21);
            this.txtLinearDistance.TabIndex = 91;
            // 
            // txtArmOpeningTime
            // 
            this.txtArmOpeningTime.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtArmOpeningTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtArmOpeningTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArmOpeningTime.Location = new System.Drawing.Point(312, 128);
            this.txtArmOpeningTime.MaxLength = 5;
            this.txtArmOpeningTime.Name = "txtArmOpeningTime";
            this.txtArmOpeningTime.Size = new System.Drawing.Size(100, 21);
            this.txtArmOpeningTime.TabIndex = 90;
            // 
            // txtSpeedofConveyor
            // 
            this.txtSpeedofConveyor.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtSpeedofConveyor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpeedofConveyor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpeedofConveyor.Location = new System.Drawing.Point(312, 97);
            this.txtSpeedofConveyor.MaxLength = 5;
            this.txtSpeedofConveyor.Name = "txtSpeedofConveyor";
            this.txtSpeedofConveyor.Size = new System.Drawing.Size(100, 21);
            this.txtSpeedofConveyor.TabIndex = 89;
            // 
            // txtEncoderPPR
            // 
            this.txtEncoderPPR.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtEncoderPPR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEncoderPPR.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEncoderPPR.Location = new System.Drawing.Point(312, 64);
            this.txtEncoderPPR.MaxLength = 5;
            this.txtEncoderPPR.Name = "txtEncoderPPR";
            this.txtEncoderPPR.Size = new System.Drawing.Size(100, 21);
            this.txtEncoderPPR.TabIndex = 88;
            // 
            // txtDistanceTravelled
            // 
            this.txtDistanceTravelled.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtDistanceTravelled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDistanceTravelled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDistanceTravelled.Location = new System.Drawing.Point(312, 19);
            this.txtDistanceTravelled.MaxLength = 5;
            this.txtDistanceTravelled.Name = "txtDistanceTravelled";
            this.txtDistanceTravelled.Size = new System.Drawing.Size(100, 21);
            this.txtDistanceTravelled.TabIndex = 87;
            this.txtDistanceTravelled.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDistanceTravelled_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(15, 366);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(275, 16);
            this.label23.TabIndex = 86;
            this.label23.Text = "Cross check the PLC Scan Time & enter:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(15, 294);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(561, 48);
            this.label20.TabIndex = 85;
            this.label20.Text = resources.GetString("label20.Text");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 227);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(565, 48);
            this.label17.TabIndex = 84;
            this.label17.Text = resources.GetString("label17.Text");
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 175);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(407, 32);
            this.label16.TabIndex = 83;
            this.label16.Text = "Linear distance travelled by arm tip between open & closed\r\npositions in the dire" +
                "ctions of conveyor belt:\r\n";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(15, 131);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(221, 16);
            this.label15.TabIndex = 82;
            this.label15.Text = "Arm Opening / Activation Time:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(15, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(190, 16);
            this.label14.TabIndex = 81;
            this.label14.Text = "Speed of conveyor (mpm):";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 67);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(237, 16);
            this.label12.TabIndex = 80;
            this.label12.Text = "Pulses per revolution of Encoder:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(287, 32);
            this.label11.TabIndex = 79;
            this.label11.Text = "Distance travelled by belt in one rotation\r\nof the drive sprocket / pulley:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label29.Location = new System.Drawing.Point(702, 236);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(14, 16);
            this.label29.TabIndex = 108;
            this.label29.Text = "*";
            // 
            // btnReset2
            // 
            this.btnReset2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnReset2.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.btnReset2.FlatAppearance.BorderSize = 5;
            this.btnReset2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReset2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnReset2.Location = new System.Drawing.Point(424, 486);
            this.btnReset2.Name = "btnReset2";
            this.btnReset2.Size = new System.Drawing.Size(75, 25);
            this.btnReset2.TabIndex = 48;
            this.btnReset2.Text = "Reset";
            this.btnReset2.UseVisualStyleBackColor = false;
            this.btnReset2.Click += new System.EventHandler(this.btnReset2_Click);
            // 
            // btnPLCDynamicDataSave
            // 
            this.btnPLCDynamicDataSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnPLCDynamicDataSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnPLCDynamicDataSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPLCDynamicDataSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPLCDynamicDataSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnPLCDynamicDataSave.Location = new System.Drawing.Point(322, 486);
            this.btnPLCDynamicDataSave.Name = "btnPLCDynamicDataSave";
            this.btnPLCDynamicDataSave.Size = new System.Drawing.Size(75, 25);
            this.btnPLCDynamicDataSave.TabIndex = 47;
            this.btnPLCDynamicDataSave.Text = "Save";
            this.btnPLCDynamicDataSave.UseVisualStyleBackColor = false;
            this.btnPLCDynamicDataSave.Click += new System.EventHandler(this.btnPLCDynamicDataSave_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.grpVMSSystem);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.btnSavePLCDiemensionalData);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 41);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(8, 8, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(989, 598);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "System Define   ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // grpVMSSystem
            // 
            this.grpVMSSystem.BackColor = System.Drawing.Color.Teal;
            this.grpVMSSystem.Controls.Add(this.panel11);
            this.grpVMSSystem.ForeColor = System.Drawing.Color.White;
            this.grpVMSSystem.Location = new System.Drawing.Point(19, 283);
            this.grpVMSSystem.Name = "grpVMSSystem";
            this.grpVMSSystem.Size = new System.Drawing.Size(915, 210);
            this.grpVMSSystem.TabIndex = 48;
            this.grpVMSSystem.TabStop = false;
            this.grpVMSSystem.Text = "DWS System Data";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel11.Controls.Add(this.txtScanPort);
            this.panel11.Controls.Add(this.btnTestDWS);
            this.panel11.Controls.Add(this.label108);
            this.panel11.Controls.Add(this.groupBox7);
            this.panel11.Controls.Add(this.groupBox11);
            this.panel11.Controls.Add(this.grpFolderLocation);
            this.panel11.Controls.Add(this.txtScannerIP);
            this.panel11.Controls.Add(this.groupBox8);
            this.panel11.Location = new System.Drawing.Point(6, 21);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(903, 182);
            this.panel11.TabIndex = 1;
            // 
            // txtScanPort
            // 
            this.txtScanPort.Location = new System.Drawing.Point(813, 153);
            this.txtScanPort.MaxLength = 4;
            this.txtScanPort.Name = "txtScanPort";
            this.txtScanPort.Size = new System.Drawing.Size(69, 22);
            this.txtScanPort.TabIndex = 27;
            this.txtScanPort.Visible = false;
            this.txtScanPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtScnPort_KeyPress);
            // 
            // btnTestDWS
            // 
            this.btnTestDWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnTestDWS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestDWS.Location = new System.Drawing.Point(624, 94);
            this.btnTestDWS.Name = "btnTestDWS";
            this.btnTestDWS.Size = new System.Drawing.Size(173, 53);
            this.btnTestDWS.TabIndex = 26;
            this.btnTestDWS.Text = "   Test DWS System   ";
            this.btnTestDWS.UseVisualStyleBackColor = false;
            this.btnTestDWS.Click += new System.EventHandler(this.btnTestDWS_Click);
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(751, 156);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(64, 16);
            this.label108.TabIndex = 5;
            this.label108.Text = "Port No.";
            this.label108.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.drpWBaud);
            this.groupBox7.Controls.Add(label107);
            this.groupBox7.Controls.Add(this.drpWPortName);
            this.groupBox7.Controls.Add(label110);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(251, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(242, 80);
            this.groupBox7.TabIndex = 24;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "  Weighing Serial Port Settings  ";
            // 
            // drpWBaud
            // 
            this.drpWBaud.Enabled = false;
            this.drpWBaud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpWBaud.FormattingEnabled = true;
            this.drpWBaud.Location = new System.Drawing.Point(105, 46);
            this.drpWBaud.Name = "drpWBaud";
            this.drpWBaud.Size = new System.Drawing.Size(121, 24);
            this.drpWBaud.TabIndex = 2;
            this.drpWBaud.Text = "9600";
            // 
            // drpWPortName
            // 
            this.drpWPortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpWPortName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpWPortName.FormattingEnabled = true;
            this.drpWPortName.Location = new System.Drawing.Point(105, 19);
            this.drpWPortName.Name = "drpWPortName";
            this.drpWPortName.Size = new System.Drawing.Size(121, 24);
            this.drpWPortName.TabIndex = 8;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.drpScanerPortName);
            this.groupBox11.Controls.Add(this.txtPLCPort);
            this.groupBox11.Controls.Add(this.label40);
            this.groupBox11.Controls.Add(this.txtPLCIP);
            this.groupBox11.Controls.Add(this.label109);
            this.groupBox11.Controls.Add(this.label111);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(503, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(394, 80);
            this.groupBox11.TabIndex = 25;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "  Capture IP  ";
            // 
            // drpScanerPortName
            // 
            this.drpScanerPortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpScanerPortName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpScanerPortName.FormattingEnabled = true;
            this.drpScanerPortName.Location = new System.Drawing.Point(209, 46);
            this.drpScanerPortName.Name = "drpScanerPortName";
            this.drpScanerPortName.Size = new System.Drawing.Size(121, 24);
            this.drpScanerPortName.TabIndex = 28;
            // 
            // txtPLCPort
            // 
            this.txtPLCPort.Location = new System.Drawing.Point(310, 16);
            this.txtPLCPort.MaxLength = 4;
            this.txtPLCPort.Name = "txtPLCPort";
            this.txtPLCPort.Size = new System.Drawing.Size(69, 22);
            this.txtPLCPort.TabIndex = 27;
            this.txtPLCPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPLCPort_KeyPress);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(248, 18);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(56, 16);
            this.label40.TabIndex = 4;
            this.label40.Text = "Port No.";
            // 
            // txtPLCIP
            // 
            this.txtPLCIP.Location = new System.Drawing.Point(121, 16);
            this.txtPLCIP.MaxLength = 15;
            this.txtPLCIP.Name = "txtPLCIP";
            this.txtPLCIP.Size = new System.Drawing.Size(121, 22);
            this.txtPLCIP.TabIndex = 2;
            this.txtPLCIP.Text = "111 . 111 .111 . 111";
            this.txtPLCIP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPLCIP_KeyPress);
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(15, 49);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(188, 16);
            this.label109.TabIndex = 1;
            this.label109.Text = "Select SCANNER Port Name :";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(15, 22);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(88, 16);
            this.label111.TabIndex = 0;
            this.label111.Text = "Enter PLC IP :";
            // 
            // grpFolderLocation
            // 
            this.grpFolderLocation.Controls.Add(label39);
            this.grpFolderLocation.Controls.Add(this.rdCSV);
            this.grpFolderLocation.Controls.Add(this.btnFldBrowser);
            this.grpFolderLocation.Controls.Add(this.rdXML);
            this.grpFolderLocation.Controls.Add(this.txtFolderLocation);
            this.grpFolderLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpFolderLocation.Location = new System.Drawing.Point(3, 94);
            this.grpFolderLocation.Name = "grpFolderLocation";
            this.grpFolderLocation.Size = new System.Drawing.Size(490, 79);
            this.grpFolderLocation.TabIndex = 23;
            this.grpFolderLocation.TabStop = false;
            this.grpFolderLocation.Text = "Set Folder location were you want to save XMl or CSV file";
            // 
            // rdCSV
            // 
            this.rdCSV.AutoSize = true;
            this.rdCSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdCSV.ForeColor = System.Drawing.Color.Black;
            this.rdCSV.Location = new System.Drawing.Point(427, 49);
            this.rdCSV.Name = "rdCSV";
            this.rdCSV.Size = new System.Drawing.Size(48, 19);
            this.rdCSV.TabIndex = 1;
            this.rdCSV.TabStop = true;
            this.rdCSV.Text = "CSV";
            this.rdCSV.UseVisualStyleBackColor = true;
            // 
            // btnFldBrowser
            // 
            this.btnFldBrowser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnFldBrowser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFldBrowser.ForeColor = System.Drawing.Color.Black;
            this.btnFldBrowser.Location = new System.Drawing.Point(10, 49);
            this.btnFldBrowser.Name = "btnFldBrowser";
            this.btnFldBrowser.Size = new System.Drawing.Size(122, 23);
            this.btnFldBrowser.TabIndex = 24;
            this.btnFldBrowser.Text = "Browse Folder";
            this.btnFldBrowser.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnFldBrowser.UseVisualStyleBackColor = false;
            this.btnFldBrowser.Click += new System.EventHandler(this.btnFldBrowser_Click);
            // 
            // rdXML
            // 
            this.rdXML.AutoSize = true;
            this.rdXML.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdXML.ForeColor = System.Drawing.Color.Black;
            this.rdXML.Location = new System.Drawing.Point(373, 49);
            this.rdXML.Name = "rdXML";
            this.rdXML.Size = new System.Drawing.Size(51, 19);
            this.rdXML.TabIndex = 0;
            this.rdXML.TabStop = true;
            this.rdXML.Text = "XML";
            this.rdXML.UseVisualStyleBackColor = true;
            // 
            // txtFolderLocation
            // 
            this.txtFolderLocation.Enabled = false;
            this.txtFolderLocation.Location = new System.Drawing.Point(9, 21);
            this.txtFolderLocation.Name = "txtFolderLocation";
            this.txtFolderLocation.Size = new System.Drawing.Size(465, 22);
            this.txtFolderLocation.TabIndex = 0;
            // 
            // txtScannerIP
            // 
            this.txtScannerIP.Location = new System.Drawing.Point(624, 153);
            this.txtScannerIP.MaxLength = 15;
            this.txtScannerIP.Name = "txtScannerIP";
            this.txtScannerIP.Size = new System.Drawing.Size(121, 22);
            this.txtScannerIP.TabIndex = 3;
            this.txtScannerIP.Text = "111 . 111 .111 . 111";
            this.txtScannerIP.Visible = false;
            this.txtScannerIP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtScannerIP_KeyPress);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.baudRateComboBox);
            this.groupBox8.Controls.Add(baudRateLabel);
            this.groupBox8.Controls.Add(this.portNameComboBox);
            this.groupBox8.Controls.Add(portNameLabel);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(242, 80);
            this.groupBox8.TabIndex = 21;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "  VMS Serial Port Settings  ";
            // 
            // baudRateComboBox
            // 
            this.baudRateComboBox.Enabled = false;
            this.baudRateComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baudRateComboBox.FormattingEnabled = true;
            this.baudRateComboBox.Location = new System.Drawing.Point(105, 46);
            this.baudRateComboBox.Name = "baudRateComboBox";
            this.baudRateComboBox.Size = new System.Drawing.Size(121, 24);
            this.baudRateComboBox.TabIndex = 2;
            this.baudRateComboBox.Text = "9600";
            // 
            // portNameComboBox
            // 
            this.portNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.portNameComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.portNameComboBox.FormattingEnabled = true;
            this.portNameComboBox.Location = new System.Drawing.Point(105, 19);
            this.portNameComboBox.Name = "portNameComboBox";
            this.portNameComboBox.Size = new System.Drawing.Size(121, 24);
            this.portNameComboBox.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(528, 517);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 44);
            this.button2.TabIndex = 17;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.MouseLeave += new System.EventHandler(this.button2_MouseLeave);
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.MouseHover += new System.EventHandler(this.button2_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Teal;
            this.groupBox1.Controls.Add(this.btnProceedtoSorter);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Controls.Add(this.panel7);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(19, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(916, 258);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "System Definition";
            // 
            // btnProceedtoSorter
            // 
            this.btnProceedtoSorter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(99)))), ((int)(((byte)(173)))));
            this.btnProceedtoSorter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProceedtoSorter.ForeColor = System.Drawing.Color.LightGray;
            this.btnProceedtoSorter.Location = new System.Drawing.Point(490, 176);
            this.btnProceedtoSorter.Name = "btnProceedtoSorter";
            this.btnProceedtoSorter.Size = new System.Drawing.Size(261, 60);
            this.btnProceedtoSorter.TabIndex = 44;
            this.btnProceedtoSorter.Text = "Proceed to Configure sorter";
            this.btnProceedtoSorter.UseVisualStyleBackColor = false;
            this.btnProceedtoSorter.Click += new System.EventHandler(this.btnProceedtoSorter_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Image = global::SorterAPI.Properties.Resources.Sketch_new1;
            this.pictureBox3.Location = new System.Drawing.Point(350, 19);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(547, 135);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 47;
            this.pictureBox3.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Controls.Add(this.panel9);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.label106);
            this.panel7.Controls.Add(this.label104);
            this.panel7.Controls.Add(this.label105);
            this.panel7.Location = new System.Drawing.Point(8, 126);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(313, 126);
            this.panel7.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.rdSortsNo);
            this.panel10.Controls.Add(this.rdSortsYes);
            this.panel10.Location = new System.Drawing.Point(199, 74);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(111, 30);
            this.panel10.TabIndex = 28;
            // 
            // rdSortsNo
            // 
            this.rdSortsNo.AutoSize = true;
            this.rdSortsNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSortsNo.ForeColor = System.Drawing.Color.Black;
            this.rdSortsNo.Location = new System.Drawing.Point(63, 6);
            this.rdSortsNo.Name = "rdSortsNo";
            this.rdSortsNo.Size = new System.Drawing.Size(41, 19);
            this.rdSortsNo.TabIndex = 3;
            this.rdSortsNo.TabStop = true;
            this.rdSortsNo.Text = "No";
            this.rdSortsNo.UseVisualStyleBackColor = true;
            // 
            // rdSortsYes
            // 
            this.rdSortsYes.AutoSize = true;
            this.rdSortsYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdSortsYes.ForeColor = System.Drawing.Color.Black;
            this.rdSortsYes.Location = new System.Drawing.Point(6, 5);
            this.rdSortsYes.Name = "rdSortsYes";
            this.rdSortsYes.Size = new System.Drawing.Size(45, 19);
            this.rdSortsYes.TabIndex = 2;
            this.rdSortsYes.TabStop = true;
            this.rdSortsYes.Text = "Yes";
            this.rdSortsYes.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.rdDWSNo);
            this.panel9.Controls.Add(this.rdDWSYes);
            this.panel9.Location = new System.Drawing.Point(199, 43);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(111, 30);
            this.panel9.TabIndex = 28;
            // 
            // rdDWSNo
            // 
            this.rdDWSNo.AutoSize = true;
            this.rdDWSNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDWSNo.ForeColor = System.Drawing.Color.Black;
            this.rdDWSNo.Location = new System.Drawing.Point(63, 6);
            this.rdDWSNo.Name = "rdDWSNo";
            this.rdDWSNo.Size = new System.Drawing.Size(41, 19);
            this.rdDWSNo.TabIndex = 3;
            this.rdDWSNo.TabStop = true;
            this.rdDWSNo.Text = "No";
            this.rdDWSNo.UseVisualStyleBackColor = true;
            this.rdDWSNo.CheckedChanged += new System.EventHandler(this.rdDWSNo_CheckedChanged);
            // 
            // rdDWSYes
            // 
            this.rdDWSYes.AutoSize = true;
            this.rdDWSYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDWSYes.ForeColor = System.Drawing.Color.Black;
            this.rdDWSYes.Location = new System.Drawing.Point(6, 5);
            this.rdDWSYes.Name = "rdDWSYes";
            this.rdDWSYes.Size = new System.Drawing.Size(45, 19);
            this.rdDWSYes.TabIndex = 2;
            this.rdDWSYes.TabStop = true;
            this.rdDWSYes.Text = "Yes";
            this.rdDWSYes.UseVisualStyleBackColor = true;
            this.rdDWSYes.CheckedChanged += new System.EventHandler(this.rdDWSYes_CheckedChanged);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.rdPTLNo);
            this.panel8.Controls.Add(this.rdPTLYes);
            this.panel8.Location = new System.Drawing.Point(199, 12);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(111, 30);
            this.panel8.TabIndex = 27;
            // 
            // rdPTLNo
            // 
            this.rdPTLNo.AutoSize = true;
            this.rdPTLNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdPTLNo.ForeColor = System.Drawing.Color.Black;
            this.rdPTLNo.Location = new System.Drawing.Point(61, 6);
            this.rdPTLNo.Name = "rdPTLNo";
            this.rdPTLNo.Size = new System.Drawing.Size(41, 19);
            this.rdPTLNo.TabIndex = 1;
            this.rdPTLNo.TabStop = true;
            this.rdPTLNo.Text = "No";
            this.rdPTLNo.UseVisualStyleBackColor = true;
            // 
            // rdPTLYes
            // 
            this.rdPTLYes.AutoSize = true;
            this.rdPTLYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdPTLYes.ForeColor = System.Drawing.Color.Black;
            this.rdPTLYes.Location = new System.Drawing.Point(4, 5);
            this.rdPTLYes.Name = "rdPTLYes";
            this.rdPTLYes.Size = new System.Drawing.Size(45, 19);
            this.rdPTLYes.TabIndex = 0;
            this.rdPTLYes.TabStop = true;
            this.rdPTLYes.Text = "Yes";
            this.rdPTLYes.UseVisualStyleBackColor = true;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Black;
            this.label106.Location = new System.Drawing.Point(3, 81);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(196, 16);
            this.label106.TabIndex = 24;
            this.label106.Text = "Do you have Sorts system?";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(3, 17);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(188, 16);
            this.label104.TabIndex = 19;
            this.label104.Text = "Do you have PTL system?";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(3, 50);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(195, 16);
            this.label105.TabIndex = 16;
            this.label105.Text = "Do you have DWS system?";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.panel1.Controls.Add(this.txtnumberofSorter);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtDataLoadSensor);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(8, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(313, 131);
            this.panel1.TabIndex = 0;
            // 
            // txtnumberofSorter
            // 
            this.txtnumberofSorter.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtnumberofSorter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtnumberofSorter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnumberofSorter.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtnumberofSorter.Location = new System.Drawing.Point(216, 15);
            this.txtnumberofSorter.MaxLength = 2;
            this.txtnumberofSorter.Name = "txtnumberofSorter";
            this.txtnumberofSorter.Size = new System.Drawing.Size(75, 21);
            this.txtnumberofSorter.TabIndex = 44;
            this.txtnumberofSorter.Text = "0";
            this.txtnumberofSorter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnumberofSorter_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "Number of Sorts (nos.)";
            // 
            // txtDataLoadSensor
            // 
            this.txtDataLoadSensor.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtDataLoadSensor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDataLoadSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDataLoadSensor.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtDataLoadSensor.Location = new System.Drawing.Point(216, 61);
            this.txtDataLoadSensor.MaxLength = 5;
            this.txtDataLoadSensor.Name = "txtDataLoadSensor";
            this.txtDataLoadSensor.Size = new System.Drawing.Size(75, 21);
            this.txtDataLoadSensor.TabIndex = 18;
            this.txtDataLoadSensor.Text = "0";
            this.txtDataLoadSensor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDataLoadSensor_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 32);
            this.label2.TabIndex = 16;
            this.label2.Text = "Distance between start point \r\nand data load sensor (mm)";
            // 
            // btnSavePLCDiemensionalData
            // 
            this.btnSavePLCDiemensionalData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnSavePLCDiemensionalData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePLCDiemensionalData.Location = new System.Drawing.Point(364, 517);
            this.btnSavePLCDiemensionalData.Name = "btnSavePLCDiemensionalData";
            this.btnSavePLCDiemensionalData.Size = new System.Drawing.Size(132, 44);
            this.btnSavePLCDiemensionalData.TabIndex = 8;
            this.btnSavePLCDiemensionalData.Text = "Save";
            this.btnSavePLCDiemensionalData.UseVisualStyleBackColor = false;
            this.btnSavePLCDiemensionalData.MouseLeave += new System.EventHandler(this.btnSavePLCDiemensionalData_MouseLeave);
            this.btnSavePLCDiemensionalData.Click += new System.EventHandler(this.btnSavePLCDiemensionalData_Click);
            this.btnSavePLCDiemensionalData.MouseHover += new System.EventHandler(this.btnSavePLCDiemensionalData_MouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(989, 598);
            this.pictureBox2.TabIndex = 45;
            this.pictureBox2.TabStop = false;
            // 
            // tabCntrl
            // 
            this.tabCntrl.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tabCntrl.Controls.Add(this.tabPage1);
            this.tabCntrl.Controls.Add(this.tabPage6);
            this.tabCntrl.Controls.Add(this.tabPage2);
            this.tabCntrl.Controls.Add(this.tabPage3);
            this.tabCntrl.Controls.Add(this.tabPage4);
            this.tabCntrl.Controls.Add(this.tabPage5);
            this.tabCntrl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabCntrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCntrl.Location = new System.Drawing.Point(6, 12);
            this.tabCntrl.Name = "tabCntrl";
            this.tabCntrl.Padding = new System.Drawing.Point(7, 10);
            this.tabCntrl.SelectedIndex = 0;
            this.tabCntrl.Size = new System.Drawing.Size(997, 643);
            this.tabCntrl.TabIndex = 0;
            this.tabCntrl.SelectedIndexChanged += new System.EventHandler(this.tabCntrl_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblDatetime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 586);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(876, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.Visible = false;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(296, 17);
            this.toolStripStatusLabel1.Text = "Developed By Armstrong Machine and Builders Pvt Ltd";
            // 
            // lblDatetime
            // 
            this.lblDatetime.Name = "lblDatetime";
            this.lblDatetime.Size = new System.Drawing.Size(0, 17);
            // 
            // frmSorterAPIConfigurator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1015, 656);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabCntrl);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterAPIConfigurator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SorterAPIConfigurator";
            this.Load += new System.EventHandler(this.frmSorterAPIConfigurator_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSorterAPIConfigurator_FormClosing);
            this.tabPage6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.pnlMySQL.ResumeLayout(false);
            this.pnlMySQL.PerformLayout();
            this.pnlOurMySql.ResumeLayout(false);
            this.pnlOurMySql.PerformLayout();
            this.pnlOurAccess.ResumeLayout(false);
            this.pnlOurAccess.PerformLayout();
            this.pnlOurSql.ResumeLayout(false);
            this.pnlOurSql.PerformLayout();
            this.pnlAccess.ResumeLayout(false);
            this.pnlAccess.PerformLayout();
            this.pnlOurOracle.ResumeLayout(false);
            this.pnlOurOracle.PerformLayout();
            this.pnlOracle.ResumeLayout(false);
            this.pnlOracle.PerformLayout();
            this.pnlSql.ResumeLayout(false);
            this.pnlSql.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.PTLPanel.ResumeLayout(false);
            this.PTLPanel.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.grpVMSSystem.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.grpFolderLocation.ResumeLayout(false);
            this.grpFolderLocation.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabCntrl.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serialSettingsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button btnSaveGeneralSetting;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnLocationClose;
        private System.Windows.Forms.Button btnSaveLocation;
        private System.Windows.Forms.Button btnAddNewLocation;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button btnSaveDBInfo;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnSorterwiseCassetteConfig;
        private System.Windows.Forms.Button btnPTLCancel;
        private System.Windows.Forms.Button btnPTLSystem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnReset2;
        private System.Windows.Forms.Button btnPLCDynamicDataSave;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnProceedtoSorter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtnumberofSorter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDataLoadSensor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSavePLCDiemensionalData;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblDatetime;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox txtPLCScanTime;
        private System.Windows.Forms.TextBox txtEnterDeactivationDelay;
        private System.Windows.Forms.TextBox txtEnterActivationDelay;
        private System.Windows.Forms.TextBox txtLinearDistance;
        private System.Windows.Forms.TextBox txtArmOpeningTime;
        private System.Windows.Forms.TextBox txtSpeedofConveyor;
        private System.Windows.Forms.TextBox txtEncoderPPR;
        private System.Windows.Forms.TextBox txtDistanceTravelled;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel PTLPanel;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtOurContactNumber;
        private System.Windows.Forms.TextBox txtOurContactEmail;
        private System.Windows.Forms.TextBox txtOurContactPerson;
        private System.Windows.Forms.TextBox txtContactNumber;
        private System.Windows.Forms.TextBox txtContactEmail;
        private System.Windows.Forms.TextBox txtContactPerson;
        private System.Windows.Forms.TextBox txtAddr;
        private System.Windows.Forms.TextBox txtComapanyName;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Button btnLogoUpload;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.LinkLabel lnkNewUser;
        private System.Windows.Forms.TextBox txtBagClosurePrefix;
        private System.Windows.Forms.TextBox txtMaxWeightArticle;
        private System.Windows.Forms.TextBox txtMaxNoArticle;
        private System.Windows.Forms.TextBox txtMinNoArticle;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnTestOurConn;
        private System.Windows.Forms.Panel pnlMySQL;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox txtServerMy;
        private System.Windows.Forms.TextBox txtDBIdMy;
        private System.Windows.Forms.TextBox txtDBPassMy;
        private System.Windows.Forms.Panel pnlOurMySql;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox txtOurServerMy;
        private System.Windows.Forms.TextBox txtOurDBPassMy;
        private System.Windows.Forms.TextBox txtOurDBIdMy;
        private System.Windows.Forms.Panel pnlOurAccess;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox txtOurDBProviderA;
        private System.Windows.Forms.TextBox txtOurDBSourcePathA;
        private System.Windows.Forms.TextBox txtOurDBPassworA;
        private System.Windows.Forms.Panel pnlOurSql;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtOurDBIpAddr;
        private System.Windows.Forms.TextBox txtOurDBUsername;
        private System.Windows.Forms.TextBox txtOurDBPassword;
        private System.Windows.Forms.Panel pnlAccess;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox txtDBProviderA;
        private System.Windows.Forms.TextBox txtDBSourcePathA;
        private System.Windows.Forms.TextBox txtDBPassworA;
        private System.Windows.Forms.Panel pnlOurOracle;
        private System.Windows.Forms.ComboBox drpOurIntegratedSecurity;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox txtOurDBIdOracle;
        private System.Windows.Forms.TextBox txtOurDBPassOracle;
        private System.Windows.Forms.Button btnConnTest;
        private System.Windows.Forms.Panel pnlOracle;
        private System.Windows.Forms.ComboBox drpIntegratedSecurity;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox txtDBIdOracle;
        private System.Windows.Forms.TextBox txtDBPassOracle;
        private System.Windows.Forms.Panel pnlSql;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtDBIpAddr;
        private System.Windows.Forms.TextBox txtDBUsername;
        private System.Windows.Forms.TextBox txtDBPassword;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ComboBox drpColumnName4;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox drpColumnName3;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.ComboBox drpColumnName2;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox drpOurDataSource;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox drpColumnName1;
        private System.Windows.Forms.ComboBox drpTableName;
        private System.Windows.Forms.ComboBox drpDBName;
        private System.Windows.Forms.ComboBox drpDataSource;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.RadioButton rdPTLNo;
        private System.Windows.Forms.RadioButton rdPTLYes;
        private System.Windows.Forms.RadioButton rdSortsNo;
        private System.Windows.Forms.RadioButton rdSortsYes;
        private System.Windows.Forms.RadioButton rdDWSNo;
        private System.Windows.Forms.RadioButton rdDWSYes;
        private System.Windows.Forms.TextBox txtOurColumn4;
        private System.Windows.Forms.TextBox txtOurColumn3;
        private System.Windows.Forms.TextBox txtOurColumn2;
        private System.Windows.Forms.TextBox txtOurColumn1;
        private System.Windows.Forms.TextBox txtOurTableName;
        private System.Windows.Forms.TextBox txtOurDBName;
        private System.Windows.Forms.DataGridView dgvHubList;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ComboBox drpBranch;
        private System.Windows.Forms.ComboBox drpBinUp;
        private System.Windows.Forms.TextBox txtMaxH;
        private System.Windows.Forms.TextBox txtMinH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMaxWe;
        private System.Windows.Forms.TextBox txtMinWe;
        private System.Windows.Forms.TextBox txtMaxL;
        private System.Windows.Forms.TextBox txtMinL;
        private System.Windows.Forms.TextBox txtMaxW;
        private System.Windows.Forms.TextBox txtMinW;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox grpVMSSystem;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox baudRateComboBox;
        private System.Windows.Forms.ComboBox portNameComboBox;
        private System.Windows.Forms.RadioButton rdCSV;
        private System.Windows.Forms.RadioButton rdXML;
        private System.Windows.Forms.GroupBox grpFolderLocation;
        private System.Windows.Forms.TextBox txtFolderLocation;
        private System.Windows.Forms.BindingSource serialSettingsBindingSource;
        private System.Windows.Forms.Label lblUp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button btnFldBrowser;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox drpWBaud;
        private System.Windows.Forms.ComboBox drpWPortName;
        private System.Windows.Forms.Button btnTestDWS;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtScannerIP;
        private System.Windows.Forms.TextBox txtPLCIP;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox txtPLCPort;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtScanPort;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lblDown;
        private System.Windows.Forms.ComboBox drpBinDown;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.ComboBox drpBranchFilter;
        private System.Windows.Forms.ComboBox drpScanerPortName;
        private System.Windows.Forms.TabControl tabCntrl;

    }
}