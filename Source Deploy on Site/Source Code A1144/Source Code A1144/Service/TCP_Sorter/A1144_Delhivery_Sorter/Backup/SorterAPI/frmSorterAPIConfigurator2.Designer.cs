﻿namespace SorterAPI
{
    partial class frmSorterAPIConfigurator2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSorterAPIConfigurator2));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnTestPLC = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.btnReset2 = new System.Windows.Forms.Button();
            this.btnPLCDynamicDataSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDistanceTravelled = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtEncoderPPR = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSpeedofConveyor = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtArmOpeningTime = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLinearDistance = new System.Windows.Forms.TextBox();
            this.txtEnterActivationDelay = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEnterDeactivationDelay = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPLCScanTime = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblBagAssignment = new System.Windows.Forms.Label();
            this.lblLinkDB = new System.Windows.Forms.Label();
            this.lblPTLSys = new System.Windows.Forms.Label();
            this.lblPLCData = new System.Windows.Forms.Label();
            this.lblCustInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnTestPLC);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.btnReset2);
            this.panel2.Controls.Add(this.btnPLCDynamicDataSave);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Location = new System.Drawing.Point(4, 61);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1159, 553);
            this.panel2.TabIndex = 3;
            // 
            // btnTestPLC
            // 
            this.btnTestPLC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnTestPLC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTestPLC.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestPLC.ForeColor = System.Drawing.Color.White;
            this.btnTestPLC.Location = new System.Drawing.Point(751, 496);
            this.btnTestPLC.Name = "btnTestPLC";
            this.btnTestPLC.Size = new System.Drawing.Size(128, 40);
            this.btnTestPLC.TabIndex = 39;
            this.btnTestPLC.Text = "Test With PLC";
            this.btnTestPLC.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.pnlClose);
            this.panel3.Location = new System.Drawing.Point(-2, -4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1158, 75);
            this.panel3.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label3.Location = new System.Drawing.Point(590, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 29);
            this.label3.TabIndex = 20;
            this.label3.Text = "PLC Data";
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::SorterAPI.Properties.Resources.PLC_Dynamicico;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(464, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(88, 71);
            this.panel4.TabIndex = 1;
            // 
            // pnlClose
            // 
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.Location = new System.Drawing.Point(1128, 5);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(29, 23);
            this.pnlClose.TabIndex = 0;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // btnReset2
            // 
            this.btnReset2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnReset2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset2.ForeColor = System.Drawing.Color.White;
            this.btnReset2.Location = new System.Drawing.Point(511, 496);
            this.btnReset2.Name = "btnReset2";
            this.btnReset2.Size = new System.Drawing.Size(128, 40);
            this.btnReset2.TabIndex = 11;
            this.btnReset2.Text = "Reset";
            this.btnReset2.UseVisualStyleBackColor = false;
            this.btnReset2.Click += new System.EventHandler(this.btnReset2_Click);
            // 
            // btnPLCDynamicDataSave
            // 
            this.btnPLCDynamicDataSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnPLCDynamicDataSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPLCDynamicDataSave.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPLCDynamicDataSave.ForeColor = System.Drawing.Color.White;
            this.btnPLCDynamicDataSave.Location = new System.Drawing.Point(271, 496);
            this.btnPLCDynamicDataSave.Name = "btnPLCDynamicDataSave";
            this.btnPLCDynamicDataSave.Size = new System.Drawing.Size(128, 40);
            this.btnPLCDynamicDataSave.TabIndex = 10;
            this.btnPLCDynamicDataSave.Text = "Submit";
            this.btnPLCDynamicDataSave.UseVisualStyleBackColor = false;
            this.btnPLCDynamicDataSave.Click += new System.EventHandler(this.btnPLCDynamicDataSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "                             ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtDistanceTravelled);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtEncoderPPR);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtSpeedofConveyor);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtArmOpeningTime);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtLinearDistance);
            this.groupBox1.Controls.Add(this.txtEnterActivationDelay);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtEnterDeactivationDelay);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtPLCScanTime);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lbl);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(6, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1152, 407);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sorter";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(16, 293);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(633, 57);
            this.label13.TabIndex = 33;
            this.label13.Text = resources.GetString("label13.Text");
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label18.Location = new System.Drawing.Point(1103, 299);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 16);
            this.label18.TabIndex = 38;
            this.label18.Text = "(mm)";
            // 
            // txtDistanceTravelled
            // 
            this.txtDistanceTravelled.Location = new System.Drawing.Point(393, 21);
            this.txtDistanceTravelled.Multiline = true;
            this.txtDistanceTravelled.Name = "txtDistanceTravelled";
            this.txtDistanceTravelled.Size = new System.Drawing.Size(140, 29);
            this.txtDistanceTravelled.TabIndex = 2;
            this.txtDistanceTravelled.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDistanceTravelled_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label17.Location = new System.Drawing.Point(1106, 218);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 16);
            this.label17.TabIndex = 37;
            this.label17.Text = "(mm)";
            // 
            // txtEncoderPPR
            // 
            this.txtEncoderPPR.Location = new System.Drawing.Point(393, 80);
            this.txtEncoderPPR.Multiline = true;
            this.txtEncoderPPR.Name = "txtEncoderPPR";
            this.txtEncoderPPR.Size = new System.Drawing.Size(140, 29);
            this.txtEncoderPPR.TabIndex = 3;
            this.txtEncoderPPR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEncoderPPR_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label16.Location = new System.Drawing.Point(1106, 145);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 16);
            this.label16.TabIndex = 36;
            this.label16.Text = "(mm)";
            // 
            // txtSpeedofConveyor
            // 
            this.txtSpeedofConveyor.Location = new System.Drawing.Point(938, 21);
            this.txtSpeedofConveyor.Multiline = true;
            this.txtSpeedofConveyor.Name = "txtSpeedofConveyor";
            this.txtSpeedofConveyor.Size = new System.Drawing.Size(162, 29);
            this.txtSpeedofConveyor.TabIndex = 4;
            this.txtSpeedofConveyor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSpeedofConveyor_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label15.Location = new System.Drawing.Point(1103, 376);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 16);
            this.label15.TabIndex = 35;
            this.label15.Text = "(ms)";
            // 
            // txtArmOpeningTime
            // 
            this.txtArmOpeningTime.Location = new System.Drawing.Point(938, 80);
            this.txtArmOpeningTime.Multiline = true;
            this.txtArmOpeningTime.Name = "txtArmOpeningTime";
            this.txtArmOpeningTime.Size = new System.Drawing.Size(162, 29);
            this.txtArmOpeningTime.TabIndex = 5;
            this.txtArmOpeningTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtArmOpeningTime_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(16, 374);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(319, 19);
            this.label14.TabIndex = 34;
            this.label14.Text = "Cross check the PLC Scan Time && enter:";
            // 
            // txtLinearDistance
            // 
            this.txtLinearDistance.Location = new System.Drawing.Point(938, 138);
            this.txtLinearDistance.Multiline = true;
            this.txtLinearDistance.Name = "txtLinearDistance";
            this.txtLinearDistance.Size = new System.Drawing.Size(162, 29);
            this.txtLinearDistance.TabIndex = 6;
            this.txtLinearDistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLinearDistance_KeyPress);
            // 
            // txtEnterActivationDelay
            // 
            this.txtEnterActivationDelay.Location = new System.Drawing.Point(938, 213);
            this.txtEnterActivationDelay.Multiline = true;
            this.txtEnterActivationDelay.Name = "txtEnterActivationDelay";
            this.txtEnterActivationDelay.Size = new System.Drawing.Size(162, 29);
            this.txtEnterActivationDelay.TabIndex = 7;
            this.txtEnterActivationDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEnterActivationDelay_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(16, 199);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(637, 57);
            this.label12.TabIndex = 32;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // txtEnterDeactivationDelay
            // 
            this.txtEnterDeactivationDelay.Location = new System.Drawing.Point(938, 294);
            this.txtEnterDeactivationDelay.Multiline = true;
            this.txtEnterDeactivationDelay.Name = "txtEnterDeactivationDelay";
            this.txtEnterDeactivationDelay.Size = new System.Drawing.Size(162, 29);
            this.txtEnterDeactivationDelay.TabIndex = 8;
            this.txtEnterDeactivationDelay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEnterDeactivationDelay_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(16, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(457, 38);
            this.label11.TabIndex = 31;
            this.label11.Text = "Linear distance travelled by arm tip between open && closed\r\npositions in the dir" +
                "ections of conveyor belt:";
            // 
            // txtPLCScanTime
            // 
            this.txtPLCScanTime.Location = new System.Drawing.Point(938, 371);
            this.txtPLCScanTime.Multiline = true;
            this.txtPLCScanTime.Name = "txtPLCScanTime";
            this.txtPLCScanTime.Size = new System.Drawing.Size(162, 29);
            this.txtPLCScanTime.TabIndex = 9;
            this.txtPLCScanTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPLCScanTime_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Location = new System.Drawing.Point(1106, 86);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 16);
            this.label10.TabIndex = 30;
            this.label10.Text = "(ms)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(318, 38);
            this.label4.TabIndex = 13;
            this.label4.Text = "Distance travelled by belt in one rotation \r\nof the drive sprocket / pulley :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Location = new System.Drawing.Point(1106, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 16);
            this.label9.TabIndex = 29;
            this.label9.Text = "(mpm)";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.ForeColor = System.Drawing.Color.Black;
            this.lbl.Location = new System.Drawing.Point(16, 79);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(269, 19);
            this.lbl.TabIndex = 24;
            this.lbl.Text = "Pulses per revolution of Encoder :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.Location = new System.Drawing.Point(539, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 16);
            this.label8.TabIndex = 28;
            this.label8.Text = "(ppr)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(642, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 19);
            this.label5.TabIndex = 25;
            this.label5.Text = "Speed of conveyor :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label7.Location = new System.Drawing.Point(539, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 16);
            this.label7.TabIndex = 27;
            this.label7.Text = "(mm)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(642, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(245, 19);
            this.label6.TabIndex = 26;
            this.label6.Text = "Arm opening / Activation Time :";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.PLC_Dynamic;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblBagAssignment);
            this.panel1.Controls.Add(this.lblLinkDB);
            this.panel1.Controls.Add(this.lblPTLSys);
            this.panel1.Controls.Add(this.lblPLCData);
            this.panel1.Controls.Add(this.lblCustInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1163, 54);
            this.panel1.TabIndex = 2;
            // 
            // lblBagAssignment
            // 
            this.lblBagAssignment.AutoSize = true;
            this.lblBagAssignment.BackColor = System.Drawing.Color.Transparent;
            this.lblBagAssignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBagAssignment.Location = new System.Drawing.Point(921, 13);
            this.lblBagAssignment.Name = "lblBagAssignment";
            this.lblBagAssignment.Size = new System.Drawing.Size(169, 29);
            this.lblBagAssignment.TabIndex = 5;
            this.lblBagAssignment.Text = "                          ";
            this.lblBagAssignment.Click += new System.EventHandler(this.lblBagAssignment_Click);
            // 
            // lblLinkDB
            // 
            this.lblLinkDB.AutoSize = true;
            this.lblLinkDB.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkDB.Location = new System.Drawing.Point(763, 13);
            this.lblLinkDB.Name = "lblLinkDB";
            this.lblLinkDB.Size = new System.Drawing.Size(127, 29);
            this.lblLinkDB.TabIndex = 4;
            this.lblLinkDB.Text = "                   ";
            this.lblLinkDB.Click += new System.EventHandler(this.lblLinkDB_Click);
            // 
            // lblPTLSys
            // 
            this.lblPTLSys.AutoSize = true;
            this.lblPTLSys.BackColor = System.Drawing.Color.Transparent;
            this.lblPTLSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPTLSys.Location = new System.Drawing.Point(599, 13);
            this.lblPTLSys.Name = "lblPTLSys";
            this.lblPTLSys.Size = new System.Drawing.Size(127, 29);
            this.lblPTLSys.TabIndex = 3;
            this.lblPTLSys.Text = "                   ";
            this.lblPTLSys.Click += new System.EventHandler(this.lblPTLSys_Click);
            // 
            // lblPLCData
            // 
            this.lblPLCData.AutoSize = true;
            this.lblPLCData.BackColor = System.Drawing.Color.Transparent;
            this.lblPLCData.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLCData.Location = new System.Drawing.Point(409, 13);
            this.lblPLCData.Name = "lblPLCData";
            this.lblPLCData.Size = new System.Drawing.Size(157, 29);
            this.lblPLCData.TabIndex = 2;
            this.lblPLCData.Text = "                        ";
            this.lblPLCData.Click += new System.EventHandler(this.lblPLCData_Click);
            // 
            // lblCustInfo
            // 
            this.lblCustInfo.AutoSize = true;
            this.lblCustInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCustInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustInfo.Location = new System.Drawing.Point(234, 14);
            this.lblCustInfo.Name = "lblCustInfo";
            this.lblCustInfo.Size = new System.Drawing.Size(145, 29);
            this.lblCustInfo.TabIndex = 1;
            this.lblCustInfo.Text = "                      ";
            this.lblCustInfo.Click += new System.EventHandler(this.lblCustInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "                              ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmSorterAPIConfigurator2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 618);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterAPIConfigurator2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Sorter Configurator";
            this.Load += new System.EventHandler(this.frmSorterAPIConfigurator2_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Label lblCustInfo;
        private System.Windows.Forms.Label lblPLCData;
        private System.Windows.Forms.Label lblPTLSys;
        private System.Windows.Forms.Label lblLinkDB;
        private System.Windows.Forms.Label lblBagAssignment;
        private System.Windows.Forms.TextBox txtPLCScanTime;
        private System.Windows.Forms.TextBox txtEnterDeactivationDelay;
        private System.Windows.Forms.TextBox txtEnterActivationDelay;
        private System.Windows.Forms.TextBox txtLinearDistance;
        private System.Windows.Forms.TextBox txtArmOpeningTime;
        private System.Windows.Forms.TextBox txtSpeedofConveyor;
        private System.Windows.Forms.TextBox txtEncoderPPR;
        private System.Windows.Forms.TextBox txtDistanceTravelled;
        private System.Windows.Forms.Button btnReset2;
        private System.Windows.Forms.Button btnPLCDynamicDataSave;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnTestPLC;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}