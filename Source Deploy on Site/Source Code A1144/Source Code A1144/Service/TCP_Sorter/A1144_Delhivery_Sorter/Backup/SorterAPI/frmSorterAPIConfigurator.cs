﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using System.IO;
using System.Drawing.Drawing2D;
using System.Data.SqlClient;
using System.Data.OleDb;
//using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.IO.Ports;
using System.Globalization;

namespace SorterAPI
{
    public partial class frmSorterAPIConfigurator : Form
    {
        SerialPortManager _spManager;
        string[] WmyPort;
        string[] ScannerPort;
        DataSet dsPLCDiam = new DataSet();
        DataTable dtDws = new DataTable();
        DataTable dtSorterInfo = new DataTable();
        DataTable dtPTLsys = new DataTable();
        DataTable dtGnr = new DataTable();
        DataTable dtPLCData = new DataTable();
        DataSet dstResults = new DataSet();
        DataView myView;
        ErrorLog oErrorLog = new ErrorLog();
        public int IsDelete { get; set; }

        Label[] lblArray = new Label[32];
        static int Sorterx;
        static int Sortery;
        static int btnx;
        static int lblx;

        public frmSorterAPIConfigurator()
        {
            InitializeComponent();
           // tabCntrl.SelectedIndexChanged += new EventHandler(tabCntrl_SelectedIndexChanged);
           // FormClosing += new FormClosingEventHandler(frmRackPositions_Closing); 
            tabCntrl.DrawItem += new DrawItemEventHandler(this.tabCntrl_DrawItem);
            //this.label73.Text = "V\nE\nR\nT\nI\nC\nA\nL\n";
           // this.lblClickHere.Text = "C\nL\nI\nC\nK\n";
            //imageMap1.AddRectangle("1", 100, 10, 150, 80);
            //imageMap1.AddRectangle("2", 200, 10, 250, 80);
            //imageMap1.AddRectangle("3", 300, 10, 350, 80);
            //imageMap1.AddRectangle("4", 400, 10, 450, 80);
            //imageMap1.SendToBack();
          //  RepaintControls(); // for tab control gradient color          
        }
        TextBox[] textBoxes;

        int xValue;
        int yValue;

        #region Formfix Position
        protected override void WndProc(ref Message m)
        {
            const int WM_SYSCommonD = 0x0112;
            const int SC_MOVE = 0xF010;

            switch (m.Msg)
            {
                case WM_SYSCommonD:
                    int Commond = m.WParam.ToInt32() & 0xfff0;
                    if (Commond == SC_MOVE)
                        return;
                    break;
            }
            base.WndProc(ref m);
        }
        #endregion

        private void frmSorterAPIConfigurator_Load(object sender, EventArgs e)
        {
           string d = "        DateTime: "+DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
           lblDatetime.Text = d;

            this.tabCntrl_SelectedIndexChanged(this, null);
          //  groupBox1.BackColor = Color.FromArgb(0, 88, 44, 55);
          //  groupBox3.BackColor = Color.FromArgb(50, 88, 44, 55);

            if (IsDelete == 1)
            {
                //string path = @"G:\Kedar\Projects\SorterAPI\image";
                //FileInfo file = new FileInfo(path);
                //if (file.Exists)
                //{
                //file.Delete();
                //}
                ProceedtoSorter p = new ProceedtoSorter();
                p.Dispose();

                string[] filePaths = Directory.GetFiles(@"G:\Kedar\Projects\SorterAPI\image\temp");
                foreach (string filePath in filePaths)
                    if (filePath != "")
                    {
                        File.Delete(filePath);
                    }
            }
        }

        private void tabCntrl_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            if (e.Index == tabCntrl.SelectedIndex)
            {
                //e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(230, 86, 83)), e.Bounds);
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(28, 131, 136)), e.Bounds);
                e.Graphics.DrawString(tabCntrl.TabPages[e.Index].Text,
                    new Font(tabCntrl.Font, FontStyle.Bold),
                    Brushes.Wheat,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(4, 20, 21)), e.Bounds);
                e.Graphics.DrawString(tabCntrl.TabPages[e.Index].Text,
                    tabCntrl.Font,
                    //Brushes.Red;
                    new SolidBrush(Color.FromArgb(50, 150, 102)),
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }

        private void btnSorterwiseCassetteConfig_Click(object sender, EventArgs e)
        {
            frmConfigureSorterWiseCassettes CSWC = new frmConfigureSorterWiseCassettes();
            CSWC.ShowDialog();           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
        private void btnAddNewSorter_Click(object sender, EventArgs e)
        {
            int value = 2;
            textBoxes = new TextBox[value];
            for (int i = 1; i < value; i++)
            {
                textBoxes[i] = new TextBox();
                textBoxes[i].SetBounds(30, i * 30, 60, 20);
                this.Controls.Add(textBoxes[i]);
            }
        }

        private void tabCntrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tabCntrl.SelectedTab == tabCntrl.TabPages[0])
                {
                    // seriel port
                    _spManager = new SerialPortManager();
                    SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
                    serialSettingsBindingSource.DataSource = mySerialSettings;
                    portNameComboBox.DataSource = mySerialSettings.PortNameCollection;

                    WmyPort = SerialPort.GetPortNames();
                    drpWPortName.Items.AddRange(WmyPort);

                    ScannerPort = SerialPort.GetPortNames();
                    drpScanerPortName.Items.AddRange(ScannerPort);

                   // SerialSettings mySerialW = _spManager.CurrentSerialSettings;
                   //drpWPortName.DataSource = mySerialW.WPortNameCollection;
                  // baudRateComboBox.DataSource = mySerialSettings.BaudRateCollection;
                  //dataBitsComboBox.DataSource = mySerialSettings.DataBitsCollection;
                 // parityComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.Parity));
                 // stopBitsComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.StopBits));

                    //  _spManager.NewSerialDataRecieved += new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved);
                    this.FormClosing += new FormClosingEventHandler(frmSorterAPIConfigurator_FormClosing);
                    //
                    dsPLCDiam = clsAPI.getPLCDiemensionalData();
                    dtSorterInfo = dsPLCDiam.Tables[0];
                    dtDws = dsPLCDiam.Tables[1];

                    if (dtSorterInfo.Rows.Count > 0 && dtDws.Rows.Count > 0)
                    {
                        btnSavePLCDiemensionalData.Text = " Update ";
                        txtnumberofSorter.Text = dtSorterInfo.Rows[0][1].ToString();
                        txtDataLoadSensor.Text = dtSorterInfo.Rows[0][2].ToString();
                        if (Convert.ToInt32(dtSorterInfo.Rows[0][3]) == 1)
                        {
                            rdDWSYes.Checked = true;
                            rdDWSNo.Checked = false;
                        }
                        else
                        {
                            rdDWSNo.Checked = true;
                        }
                        if (Convert.ToInt32(dtSorterInfo.Rows[0][4]) == 1)
                        {
                            rdPTLYes.Checked = true;
                            rdPTLNo.Checked = false;
                        }
                        else
                        {
                            rdPTLNo.Checked = true;
                        }

                        if (Convert.ToInt32(dtSorterInfo.Rows[0][5]) == 1)
                        {
                            rdSortsYes.Checked = true;
                            rdSortsNo.Checked = false;
                        }
                        else
                        {
                            rdSortsNo.Checked = true;
                        }

                        portNameComboBox.Text = dtDws.Rows[0]["PortName"].ToString();
                        if (Convert.ToInt32(dtDws.Rows[0]["DataExportType"]) == 1)
                        {
                            rdXML.Checked = true;
                        }
                        else
                        {
                            rdCSV.Checked = true;
                        }
                        txtFolderLocation.Text = dtDws.Rows[0]["FolderLocation"].ToString();    
                        drpWPortName.Text = dtDws.Rows[0]["WPortname"].ToString();
                        txtPLCIP.Text = dtDws.Rows[0]["PLCIP"].ToString();
                        txtScannerIP.Text = dtDws.Rows[0]["ScannerIP"].ToString();
                        txtPLCPort.Text = dtDws.Rows[0]["PLCPort"].ToString();
                        //txtScanPort.Text = dtDws.Rows[0]["ScannerPort"].ToString();

                        drpScanerPortName.Text = dtDws.Rows[0]["ScannerPort"].ToString();
                        //string strBranches = "";
                        //strBranches = dt.Rows[0][3].ToString();
                        //string[] arr = strBranches.Split(',');
                        //txtb1.Text = arr[0];
                        //txtb2.Text = arr[1];                    
                    }
                }

                if (tabCntrl.SelectedTab == tabCntrl.TabPages[2])
                {
                    dtPLCData = clsAPI.getPLCData();
                    if (dtPLCData.Rows.Count > 0)
                    {
                        btnPLCDynamicDataSave.Text = "Update";

                        txtDistanceTravelled.Text = dtPLCData.Rows[0]["PullyDistance"].ToString();
                        txtEncoderPPR.Text = dtPLCData.Rows[0]["PPREncoder"].ToString();
                        txtSpeedofConveyor.Text = dtPLCData.Rows[0]["ConveyorSpeed"].ToString();
                        txtArmOpeningTime.Text = dtPLCData.Rows[0]["ArmActivationTime"].ToString();
                        txtLinearDistance.Text = dtPLCData.Rows[0]["LinearDistance"].ToString();
                        txtEnterActivationDelay.Text = dtPLCData.Rows[0]["ActConstantDistance"].ToString();
                        txtEnterDeactivationDelay.Text = dtPLCData.Rows[0]["DeactConstantDistance"].ToString();
                        txtPLCScanTime.Text = dtPLCData.Rows[0]["PLCScanTime"].ToString(); 
                    }
                }

                if (tabCntrl.SelectedTab == tabCntrl.TabPages[3])
                {
                    dtPTLsys = clsAPI.getPTLSystemData();
                    if (dtPTLsys.Rows.Count > 0)
                    {
                        btnPTLSystem.Text = "Update";
                        txtMinNoArticle.Text = dtPTLsys.Rows[0][1].ToString();
                        txtMaxNoArticle.Text = dtPTLsys.Rows[0][2].ToString();
                        txtMaxWeightArticle.Text = dtPTLsys.Rows[0][3].ToString();
                        txtBagClosurePrefix.Text = dtPTLsys.Rows[0][4].ToString();

                        if (Convert.ToInt32(dtPTLsys.Rows[0][5]) == 1)
                        {
                            PTLPanel.Enabled = true;
                            btnPTLSystem.Enabled = true;
                            btnPTLCancel.Enabled = true;
                        }
                        else
                        {
                            PTLPanel.Enabled = false;
                            btnPTLSystem.Enabled = false;
                            btnPTLCancel.Enabled = false;
                        }
                    }
                }

                if (tabCntrl.SelectedTab == tabCntrl.TabPages[4])
                {
                    drpDataSource.Items.Clear();
                    drpDataSource.Items.Add("SQLServer");
                    drpDataSource.Items.Add("MSAccess");
                    drpDataSource.Items.Add("MySQL");
                    drpDataSource.Items.Add("Oracle");
                }

                if (tabCntrl.SelectedTab == tabCntrl.TabPages[5])
                {
                    DataTable dtemp = new DataTable();
                    dtemp = dstResults.Tables["LocationAddr"];
                    if (dtemp != null)
                    {
                        dstResults.Tables["LocationAddr"].Clear();
                    }

                    ReadData("select Pincode,SorterNo from LocationAddr",
                        ref dstResults, "LocationAddr");

                   
                    //Creates a DataView from our table's default view
                    myView = ((DataTable)dstResults.Tables["LocationAddr"]).DefaultView;

                    //Assigns the DataView to the grid   
                  
                    dgvHubList.RowHeadersVisible = false;                    
                    dgvHubList.DataSource = myView;
                    dgvHubList.Columns[0].Width = 100;
                    dgvHubList.Columns[1].Width = 150;

                    //DataTable dtCircle = new DataTable();
                    //dtCircle = clsAPI.GetCircle();
                   
                    dsPLCDiam = clsAPI.getPLCDiemensionalData();
                    dtSorterInfo = dsPLCDiam.Tables[0];

                    int count = Convert.ToInt32(dtSorterInfo.Rows[0][1]);
                    drpBranch.Items.Clear();
                    for (int i = 1; i <= count; i++)
                    {
                        drpBranch.Items.Add(i);
                    }

                    drpBranchFilter.Items.Clear();
                    for (int i = 1; i <= count; i++)
                    {
                        drpBranchFilter.Items.Add(i);
                    }

                    //drpBinUp.Items.Clear();
                    //for (int i = 1; i <= 32; i++)
                    //{
                    //    drpBinUp.Items.Add(i);
                    //}
                }

                if (tabCntrl.SelectedTab == tabCntrl.TabPages[1])
                {
                    dtGnr = clsAPI.getGenralSettingsData();
                    if (dtGnr.Rows.Count > 0)
                    {
                        btnSaveGeneralSetting.Text = "Update";

                        txtComapanyName.Text = dtGnr.Rows[0][1].ToString();
                        txtAddr.Text = dtGnr.Rows[0][2].ToString();
                        txtContactPerson.Text = dtGnr.Rows[0][3].ToString();
                        txtContactEmail.Text = dtGnr.Rows[0][4].ToString();
                        txtContactNumber.Text = dtGnr.Rows[0][5].ToString();
                        if (dtGnr.Rows[0][6] != DBNull.Value)
                        {
                            byte[] img_arr1 = (byte[])dtGnr.Rows[0][6];
                            MemoryStream ms1 = new MemoryStream(img_arr1);
                            ms1.Seek(0, SeekOrigin.Begin);
                            pictureBox1.Image = Image.FromStream(ms1);
                        }
                        txtOurContactPerson.Text = dtGnr.Rows[0][7].ToString();
                        txtOurContactEmail.Text = dtGnr.Rows[0][8].ToString();
                        txtOurContactNumber.Text = dtGnr.Rows[0][9].ToString();
                    }
                }               
            }
            catch (Exception ex)
            {               
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void txtDistanceTravelled_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            //{
            //    e.Handled = true;
            //}

            //// only allow one decimal point
            //if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            //{
            //    e.Handled = true;
            //}
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtEncoderPPR_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtSpeedofConveyor_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtArmOpeningTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtLinearDistance_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtEnterActivationDelay_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtEnterDeactivationDelay_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtPLCScanTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btnPLCDynamicDataSave_Click(object sender, EventArgs e)
        {
            clsAPI obj = new clsAPI();
            if (txtDistanceTravelled.Text == "" || txtEncoderPPR.Text == "" || txtSpeedofConveyor.Text == ""
                || txtArmOpeningTime.Text == "" || txtLinearDistance.Text == "" || txtEnterActivationDelay.Text == ""
            || txtEnterDeactivationDelay.Text == "" || txtPLCScanTime.Text == "")
            {
                MessageBox.Show("All value are mandatory");
                //txtDistanceTravelled.Focus();
                // txtDistanceTravelled.BackColor = Color.Red;

            }
            else
            {
                obj.PullyDistance = Convert.ToInt32(txtDistanceTravelled.Text);
                obj.PPREncoder = Convert.ToInt32(txtEncoderPPR.Text);
                obj.ConveyorSpeed = Convert.ToInt32(txtSpeedofConveyor.Text);
                obj.ActivetionTime = Convert.ToInt32(txtArmOpeningTime.Text);
                obj.LinearDistance = Convert.ToInt32(txtLinearDistance.Text);
                obj.ActConstantDistance = Convert.ToInt32(txtEnterActivationDelay.Text);
                obj.DeactConstantDistance = Convert.ToInt32(txtEnterDeactivationDelay.Text);
                obj.PLCScanTime = Convert.ToInt32(txtPLCScanTime.Text);
                if (btnPLCDynamicDataSave.Text == "Update")
                {
                    int i = clsAPI.UpdatePLCDynamicData(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully updated");
                        //txtDistanceTravelled.Text = "";
                        //txtEncoderPPR.Text = "";
                        //txtSpeedofConveyor.Text = "";
                        //txtArmOpeningTime.Text = "";
                        //txtLinearDistance.Text = "";
                        //txtEnterActivationDelay.Text = "";
                        //txtEnterDeactivationDelay.Text = "";
                        //txtPLCScanTime.Text = "";
                    }
                }
                else
                {
                    int i = clsAPI.InsertPLCDynamicData(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully Saved");
                        //txtDistanceTravelled.Text = "";
                        //txtEncoderPPR.Text = "";
                        //txtSpeedofConveyor.Text = "";
                        //txtArmOpeningTime.Text = "";
                        //txtLinearDistance.Text = "";
                        //txtEnterActivationDelay.Text = "";
                        //txtEnterDeactivationDelay.Text = "";
                        //txtPLCScanTime.Text = "";
                    }
                }
            }
        }

        private void btnSavePLCDiemensionalData_Click(object sender, EventArgs e)
        {
            clsAPI objd = new clsAPI();
            if (txtDataLoadSensor.Text != "0" || txtnumberofSorter.Text != "0")
            {
               
                   // objd.StartPointSorter = Convert.ToInt32(txtStartPointSorter.Text);
                if (rdDWSYes.Checked == true)
                {
                    objd.IsDWS = 1;
                    
                    if(txtFolderLocation.Text !=""&&portNameComboBox.Text!=""&&baudRateComboBox.Text!=""&&txtPLCIP.Text!="" && txtPLCPort.Text!="")
                    {

                    objd.FolderLocation = txtFolderLocation.Text;
                    objd.portname = portNameComboBox.Text;
                    objd.baudRate = Convert.ToInt32(baudRateComboBox.Text);
                    objd.PLCIP = txtPLCIP.Text;
                    objd.PLCPort = Convert.ToInt32(txtPLCPort.Text);                    
                    objd.ScannerIP = txtScannerIP.Text;
                    //objd.ScannerPort = Convert.ToInt32(txtScanPort.Text);
                    //objd.dataBits = Convert.ToInt32(dataBitsComboBox.Text);
                    //objd.parity = parityComboBox.Text;
                    //objd.stopBits = stopBitsComboBox.Text;
                    }
                    else
                    {
                        MessageBox.Show("Enter DWS required data!!");
                        return;
                    }

                    if (rdXML.Checked == true)
                    {
                        objd.DataExportType = 1;
                    }
                    else
                    {
                        objd.DataExportType = 0;
                    }

                    if (portNameComboBox.Text == drpWPortName.Text || portNameComboBox.Text == drpScanerPortName.Text || drpScanerPortName.Text == drpWPortName.Text)
                    {
                        MessageBox.Show("VMS port name, weighing port name and Scanner port name should be different!!");
                        return;
                    }
                    else
                    {
                        objd.WPortName = drpWPortName.Text;

                        objd.ScannerPort = drpScanerPortName.Text;
                    }
                    
                }
                else
                {
                    objd.IsDWS = 0;
                }
                if (rdPTLYes.Checked == true)
                {
                    objd.IsPTL = 1;
                }
                else
                {
                    objd.IsPTL = 0;
                }
                if (rdSortsYes.Checked == true)
                {
                    objd.IsSorts = 1;
                }
                else
                {
                    objd.IsSorts = 0;
                }

                    objd.DataLoadSensor = Convert.ToInt32(txtDataLoadSensor.Text);
                    objd.NumberofSorter = Convert.ToInt32(txtnumberofSorter.Text);
                   
                    //objd.Branches =txtb1.Text + "," + txtb2.Text + "," + txtb3.Text + "," + txtb4.Text
                    //    + "," + txtb5.Text + "," + txtb6.Text + "," + txtb7.Text + "," + txtb8.Text
                    //    + "," + txtb9.Text + "," + txtb10.Text + "," + txtb11.Text + "," + txtb12.Text
                    //    + "," + txtb13.Text + ","+ txtb14.Text + "," + txtb15.Text + "," + txtb16.Text;

                    if (btnSavePLCDiemensionalData.Text == " Update ")
                    {
                        int i = clsAPI.UpdatePLCDiemensionalData(objd);
                        if (i > 0)
                        {
                            MessageBox.Show("Data successfully Update");
                          //  ClearTextBoxes(this.Controls);
                           // this.Load += new EventHandler(frmSorterAPIConfigurator_Load); 
                            
                        }
                    }
                    else
                    {
                        int i = clsAPI.InsertPLCDiemensionalData(objd);
                        if (i > 0)
                        {

                            MessageBox.Show("Data successfully Insert");
                          //  ClearTextBoxes(this.Controls);
                           // this.Load += new EventHandler(frmSorterAPIConfigurator_Load);
                        }
                    }
                }          
            else
            {
                MessageBox.Show("Enter sorter information");
            } 
        }

        private void ClearTextBoxes(Control.ControlCollection cc)
        {
            foreach (Control ctrl in cc)
            {
                TextBox tb = ctrl as TextBox;
                if (tb != null)
                    tb.Text = "0";
                else
                    ClearTextBoxes(ctrl.Controls);
            }
        }

        #region numeric keypress event
        private void txtnumberofSorter_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void txtStartPointSorter_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtDataLoadSensor_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb4_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb5_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb6_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb7_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb8_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb9_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb10_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb11_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb12_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb13_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb14_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb15_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtb16_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        #endregion
        
        //private int _ArrowDir;
        //static string Temppath = "";
        //static string imagepath = @"G:\Kedar\Projects\SorterAPI\image\conveyor.bmp";
        //static string path = @"G:\Kedar\Projects\SorterAPI\image";
        //static int posX, posY = 100;
        //Bitmap bmp;

        //public int RackNo1 { get; set; }

        //public int ArrowDir
        //{
        //    get
        //    {
        //        return _ArrowDir;
        //    }
        //    set
        //    {
        //        _ArrowDir = value;
        //    }
        //}
        //private int _PosDistance;
        //public int PosDistance
        //{
        //    get
        //    {
        //        return _PosDistance;
        //    }
        //    set
        //    {
        //        _PosDistance = value;
        //    }
        //}
        //System.Drawing.RectangleF rect;
        //public int ArrowPosition { get; set; }       

        private void btnProceedtoSorter_Click(object sender, EventArgs e)
        {
           // ProceedtoSorter frmPS = new ProceedtoSorter();
           //// frmPS.MdiParent = this;
           // frmPS.ShowDialog();

            frmSorterDirection sd = new frmSorterDirection();
            sd.ShowDialog();
        }      

        private void btnPTLSystem_Click(object sender, EventArgs e)
        {
            if (txtMinNoArticle.Text != "" && txtMaxNoArticle.Text != "" && txtMaxWeightArticle.Text != "" && txtBagClosurePrefix.Text != "")
            {
                clsAPI obj = new clsAPI();
                obj.MinNoArticle = Convert.ToInt32(txtMinNoArticle.Text);
                obj.MaxNoArticle = Convert.ToInt32(txtMaxNoArticle.Text);
                obj.MaxWeightArticle = Convert.ToInt32(txtMaxWeightArticle.Text);
                obj.BagClosurePrefix = txtBagClosurePrefix.Text;

                if (btnPTLSystem.Text == "Save")
                {
                    int i = clsAPI.InsertPTLSystem(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully Insert");
                    }
                }
                else
                {
                    int i = clsAPI.UpdatePTLSystem(obj);
                    if (i > 0)
                    {
                        MessageBox.Show("Data successfully Update");
                    }
                }
               


            }
            else
            {
                MessageBox.Show("All value are mandatory");
            }
        }

        private void txtMinNoArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxNoArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtMaxWeightArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        byte[] bimage;
        private void btnLogoUpload_Click(object sender, EventArgs e)
        {
            //Getting The Image From The System
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileInfo file = new System.IO.FileInfo(open.FileName);
                Bitmap img = new Bitmap(open.FileName);
                string imgname = open.FileName;
                
                if (img.Width <= 370 && img.Height <= 105)
                {
                    if (file.Length <= 50000)  //&& file.Length < MAX_SIZE 50kb
                    {

                    pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
                    pictureBox1.Image = img;


                    //  Bitmap bmp = new Bitmap(image);
                    FileStream fs = new FileStream(imgname, FileMode.Open, FileAccess.Read);
                    bimage = new byte[fs.Length];
                    fs.Read(bimage, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    }
                    else{
                        MessageBox.Show("File size must not exceed 50kb");
                    }
                }
                else
                {
                    MessageBox.Show("Height and Width must not exceed 105px*370px.");
                }


              //  ms1.Read(img_arr1, 0, img_arr1.Length);

                //Save bimage @image

            }           
        }

        private void btnSaveGeneralSetting_Click(object sender, EventArgs e)
        {
            if (txtComapanyName.Text != "" && txtAddr.Text != "" && txtContactPerson.Text != "" &&
                txtContactEmail.Text != "" && txtContactNumber.Text != "" && txtOurContactPerson.Text != "" &&
                txtOurContactEmail.Text != "" && txtOurContactNumber.Text != "")
            {
                Regex emailRegex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");

                if (!emailRegex.IsMatch(txtContactEmail.Text) && !emailRegex.IsMatch(txtOurContactEmail.Text))
                {
                    MessageBox.Show("E-Mail expected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    clsAPI obj = new clsAPI();
                    obj.CompanyName = txtComapanyName.Text;
                    obj.Address = txtAddr.Text;
                    obj.ContactPersonName = txtContactPerson.Text;
                    obj.ContactEmail = txtContactEmail.Text;
                    obj.ContactNumber = txtContactNumber.Text;
                    obj.CompanyLogo = bimage;
                    obj.OurContactPerson = txtOurContactPerson.Text;
                    obj.OurContactEmail = txtOurContactEmail.Text;
                    obj.OurContactNumber = txtOurContactNumber.Text;

                    if (btnSaveGeneralSetting.Text == "Save")
                    {
                        int i = clsAPI.InsertGeneralSetting(obj);
                        if (i > 0)
                        {
                            MessageBox.Show("Data successfully Insert");
                        }
                    }
                    else
                    {
                        int i = clsAPI.UpdateGeneralSettings(obj);
                        if (i > 0)
                        {
                            MessageBox.Show("Data successfully Update");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Red * value are mandatory");
            }
        }        

        private void btnAddNewLocation_Click(object sender, EventArgs e)
        {
            frmAddNewLocation fAddLoc = new frmAddNewLocation();
            fAddLoc.ShowDialog();
        } 

        private void drpDataSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDataSource.Text == "SQLServer")
            {
                pnlSql.Visible = true;
                pnlAccess.Visible = false;
                pnlMySQL.Visible = false;
                pnlOracle.Visible = false;
            }
            else if (drpDataSource.Text == "MSAccess")
            {
                pnlAccess.Visible = true;
                pnlSql.Visible = false;               
                pnlMySQL.Visible = false;
                pnlOracle.Visible = false;
            }
            else if (drpDataSource.Text == "MySQL")
            {
                pnlMySQL.Visible = true;
                pnlOracle.Visible = false;
                pnlAccess.Visible = false;
                pnlSql.Visible = false;
            }
            else if (drpDataSource.Text == "Oracle")
            {
                pnlOracle.Visible = true;
                pnlMySQL.Visible = false;                
                pnlAccess.Visible = false;
                pnlSql.Visible = false;
            }
        }
        string connetionString = null;
        
        private void btnTest_Click(object sender, EventArgs e)
        {
            
            
        }

        private void drpDBName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDataSource.Text == "SQLServer")
            {
                if (drpDBName.Text != "--Select--")
                {
                    string strcon = "Data Source=" + txtDBIpAddr.Text + ";User ID=" + txtDBUsername.Text + ";Password=" + txtDBPassword.Text + "; Integrated Security=Yes; Database = " + drpDBName.Text + ";";
                    using (SqlConnection con = new SqlConnection(strcon))
                    {
                        con.Open();
                        using (SqlCommand com = new SqlCommand("SELECT * FROM Sys.TABLES", con))
                        {
                            using (SqlDataReader reader = com.ExecuteReader())
                            {

                                drpTableName.Items.Clear();
                                while (reader.Read())
                                {
                                    drpTableName.Items.Add((string)reader["name"]);
                                }
                            }
                        }
                    }
                }
            }
            else if (drpDataSource.Text == "MSAccess")
            {
                //bind table from Access database
            }

            else if (drpDataSource.Text == "MySQL")
            {
                //bind table from MySql database
            }

            else if (drpDataSource.Text == "Oracle")
            {
                //bind table from oracle database
            }
        }

        private void drpTableName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDataSource.Text == "SQLServer")
            {
                if (drpDBName.Text != "--Select--")
                {
                    string strcon = "Data Source=" + txtDBIpAddr.Text + ";User ID=" + txtDBUsername.Text + ";Password=" + txtDBPassword.Text + "; Integrated Security=Yes; Database = " + drpDBName.Text + ";";
                    using (SqlConnection con = new SqlConnection(strcon))
                    {
                        con.Open();
                        using (SqlCommand com = new SqlCommand("select [name],column_id from sys.columns WHERE object_id = OBJECT_ID('"+drpTableName.Text+"')", con))
                        {
                            using (SqlDataReader reader = com.ExecuteReader())
                            {
                                
                                drpColumnName1.Items.Clear();
                                drpColumnName2.Items.Clear();
                                drpColumnName3.Items.Clear();
                                drpColumnName4.Items.Clear();
                                while (reader.Read())
                                {
                                    drpColumnName1.Items.Add((string)reader["name"]);
                                    drpColumnName2.Items.Add((string)reader["name"]);
                                    drpColumnName3.Items.Add((string)reader["name"]);
                                    drpColumnName4.Items.Add((string)reader["name"]);
                                   
                                }
                            }
                        }
                    }
                }
            }
        }

        private void drpOurDataSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpOurDataSource.Text == "SQLServer")
            {
                pnlOurSql.Visible = true;
                pnlOurAccess.Visible = false;
                pnlOurMySql.Visible = false;
                pnlOurOracle.Visible = false;
            }
            else if (drpOurDataSource.Text == "MSAccess")
            {
                pnlOurAccess.Visible = true;
                pnlOurSql.Visible = false;
                pnlOurMySql.Visible = false;
                pnlOurOracle.Visible = false;
            }
            else if (drpOurDataSource.Text == "MySql")
            {
                pnlOurMySql.Visible = true;
                pnlOurOracle.Visible = false;
                pnlOurAccess.Visible = false;
                pnlOurSql.Visible = false;
            }
            else if (drpOurDataSource.Text == "Oracle")
            {
                pnlOurOracle.Visible = true;
                pnlOurMySql.Visible = false;
                pnlOurAccess.Visible = false;
                pnlOurSql.Visible = false;
            }
        }

        ////private void btnFormula1_Click(object sender, EventArgs e)
        ////{          
        ////    if (drpOurColumnName1.Text != "")
        ////    {
        ////        frmFormula fl = new frmFormula();
        ////        string s = drpOurColumnName1.Text.Substring(1);
        ////        //string s = n.Substring(1);
        ////        fl.fvalue = s;
        ////        fl.Id = 1;
        ////        fl.ShowDialog();
        ////    }
        ////}

        ////private void btnFormula2_Click(object sender, EventArgs e)
        ////{
        ////    if (drpOurColumnName2.Text != "")
        ////    {
        ////        frmFormula fl = new frmFormula();
        ////        string s = drpOurColumnName2.Text.Substring(1);
        ////        //string s = n.Substring(1);
        ////        fl.fvalue = s;
        ////        fl.Id = 2;
        ////        fl.ShowDialog();
        ////    }
        ////}

        ////private void btnFormula3_Click(object sender, EventArgs e)
        ////{
        ////    if (drpOurColumnName3.Text != "")
        ////    {
        ////        frmFormula fl = new frmFormula();
        ////        string s = drpOurColumnName3.Text.Substring(1);
        ////        //string s = n.Substring(1);
        ////        fl.fvalue = s;
        ////        fl.Id = 3;
        ////        fl.ShowDialog();
        ////    }
        ////}

        ////private void btnFormula4_Click(object sender, EventArgs e)
        ////{
        ////    if (drpOurColumnName4.Text != "")
        ////    {
        ////        frmFormula fl = new frmFormula();
        ////        string s = drpOurColumnName4.Text.Substring(1);
        ////        //string s = n.Substring(1);
        ////        fl.fvalue = s;
        ////        fl.Id = 4;
        ////        fl.ShowDialog();
        ////    }
        ////}

        ////private void btnTestOurConn_Click(object sender, EventArgs e)
        ////{
        ////    if (drpOurDataSource.Text == "SQLServer")
        ////    {
        ////        SqlConnection cnn;
        ////        connetionString = "Data Source=" + txtOurDBIpAddr.Text + ";User ID=" + txtOurDBUsername.Text + ";Password=" + txtOurDBPassword.Text + "; Integrated Security=Yes";
        ////        cnn = new SqlConnection(connetionString);
        ////        try
        ////        {
        ////            cnn.Open();
        ////            MessageBox.Show("Connection was established successfully!!!");

        ////            using (SqlCommand com = new SqlCommand("select [name],database_id from sys.databases",cnn))
        ////            {
        ////                using (SqlDataReader reader = com.ExecuteReader())
        ////                {
        ////                    drpOurDBName.Items.Clear();
                           
        ////                    while (reader.Read())
        ////                    {
        ////                        drpOurDBName.Items.Add((string)reader["name"]);                               
        ////                    }
        ////                }
        ////            }
        ////            cnn.Close();

        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            MessageBox.Show("Can not open connection ! ");
        ////        }
        ////    }
        ////    else if (drpOurDataSource.Text == "MSAccess")
        ////    {
        ////        OleDbConnection cnn;
        ////        connetionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + txtOurDBSourcePathA.Text + ";Jet OLEDB:Database Password=" + txtOurDBPassworA.Text + ";";

        ////        cnn = new OleDbConnection(connetionString);
        ////        try
        ////        {
        ////            cnn.Open();
        ////            MessageBox.Show("Connection successfull ! ");
        ////            cnn.Close();
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            MessageBox.Show("Can not open connection ! ");
        ////        }
        ////    }
        ////    else if (drpOurDataSource.Text == "MySQL")
        ////    {
        ////        MySqlConnection cnn;

        ////        connetionString = "Server = " + txtOurServerMy.Text + "; Port = 1234;Uid = " + txtOurDBIdMy.Text + "; Pwd = " + txtOurDBPassMy.Text + ";";

        ////        cnn = new MySqlConnection(connetionString);
        ////        try
        ////        {
        ////            cnn.Open();
        ////            MessageBox.Show("Connection successfull ! ");
        ////            cnn.Close();
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            MessageBox.Show("Can not open connection ! ");
        ////        }
        ////    }
        ////    else if (drpOurDataSource.Text == "Oracle")
        ////    {
        ////        OleDbConnection cnn;

        ////        connetionString = "User Id = " + txtOurDBIdOracle.Text + "; Password = " + txtOurDBPassOracle.Text + ";Integrated Security = " + drpOurIntegratedSecurity.Text + ";";

        ////        cnn = new OleDbConnection(connetionString);
        ////        try
        ////        {
        ////            cnn.Open();
        ////            MessageBox.Show("Connection successfull ! ");
        ////            cnn.Close();
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            MessageBox.Show("Can not open connection ! ");
        ////        }
        ////    }
        ////}

        private void btnSaveDBInfo_Click(object sender, EventArgs e)
        {
            try
            {

                clsAPI obj = new clsAPI();

                obj.DataSource = drpDataSource.Text;
                if (drpDataSource.Text == "SQLServer")
                {
                    obj.DB1 = txtDBIpAddr.Text;
                    obj.DB2 = txtDBUsername.Text;
                    obj.DB3 = txtDBPassword.Text;
                }
                else if (drpDataSource.Text == "MSAccess")
                {
                    obj.DB1 = txtDBProviderA.Text;
                    obj.DB2 = txtDBSourcePathA.Text;
                    obj.DB3 = txtDBPassworA.Text;
                }
                else if (drpDataSource.Text == "MySQL")
                {
                    obj.DB1 = txtServerMy.Text;
                    obj.DB2 = txtDBPassMy.Text;
                    obj.DB3 = txtDBIdMy.Text;
                }
                else if (drpDataSource.Text == "Oracle")
                {
                    obj.DB1 = txtDBIdOracle.Text;
                    obj.DB2 = txtDBPassOracle.Text;
                    obj.DB3 = drpIntegratedSecurity.Text;
                }
                obj.DBName = drpDBName.Text;
                obj.TableName = drpTableName.Text;
                obj.Column1 = drpColumnName1.Text;
                obj.Column2 = drpColumnName2.Text;
                obj.Column3 = drpColumnName3.Text;
                obj.Column4 = drpColumnName4.Text;

                ///////////

                obj.OurDataSource = drpOurDataSource.Text;
                if (drpOurDataSource.Text == "SQLServer")
                {
                    obj.OurDB1 = txtOurDBIpAddr.Text;
                    obj.OurDB2 = txtOurDBUsername.Text;
                    obj.OurDB3 = txtOurDBPassword.Text;
                }
                else if (drpOurDataSource.Text == "MSAccess")
                {
                    obj.OurDB1 = txtOurDBProviderA.Text;
                    obj.OurDB2 = txtOurDBSourcePathA.Text;
                    obj.OurDB3 = txtOurDBPassworA.Text;
                }
                else if (drpOurDataSource.Text == "MySQL")
                {
                    obj.OurDB1 = txtOurServerMy.Text;
                    obj.OurDB2 = txtOurDBPassMy.Text;
                    obj.OurDB3 = txtOurDBIdMy.Text;
                }
                else if (drpOurDataSource.Text == "Oracle")
                {
                    obj.OurDB1 = txtOurDBIdOracle.Text;
                    obj.OurDB2 = txtOurDBPassOracle.Text;
                    obj.OurDB3 = drpOurIntegratedSecurity.Text;
                }
                ////obj.OurDBName = drpOurDBName.Text;
                ////obj.OurTableName = drpOurTableName.Text;
                ////obj.OurColumn1 = drpOurColumnName1.Text;
                ////obj.OurColumn2 = drpOurColumnName2.Text;
                ////obj.OurColumn3 = drpOurColumnName3.Text;
                ////obj.OurColumn4 = drpOurColumnName4.Text;

                obj.OurDBName = txtOurDBName.Text;
                obj.OurTableName = txtOurTableName.Text;
                obj.OurColumn1 = txtOurColumn1.Text;
                obj.OurColumn2 = txtOurColumn1.Text;
                obj.OurColumn3 = txtOurColumn1.Text;
                obj.OurColumn4 = txtOurColumn1.Text;

                obj.MinH = Convert.ToInt32(txtMinH.Text);
                obj.MaxH = Convert.ToInt32(txtMaxH.Text);
                obj.MinW = Convert.ToInt32(txtMinW.Text);
                obj.MaxW = Convert.ToInt32(txtMaxW.Text);
                obj.MinL = Convert.ToInt32(txtMinL.Text);
                obj.MaxL = Convert.ToInt32(txtMaxL.Text);
                obj.MinWe = Convert.ToInt32(txtMinWe.Text);
                obj.MaxWe = Convert.ToInt32(txtMaxWe.Text);

                //DataTable dtFormula = new DataTable();
                //dtFormula = clsAPI.getFormula();
                //for (int i = 0; i < dtFormula.Rows.Count; i++)
                //{
                //    if (dtFormula.Rows[i][1].ToString() != "" && i==0)
                //    {
                //        obj.Formula1 = dtFormula.Rows[i][1].ToString();
                //    }
                //    else if (dtFormula.Rows[i][1].ToString() != "" && i == 1)
                //    {
                //        obj.Formula2 = dtFormula.Rows[i][1].ToString();
                //    }
                //    else if (dtFormula.Rows[i][1].ToString() != "" && i == 2)
                //    {
                //        obj.Formula3 = dtFormula.Rows[i][1].ToString();
                //    }
                //    else if (dtFormula.Rows[i][1].ToString() != "" && i == 3)
                //    {
                //        obj.Formula4 = dtFormula.Rows[i][1].ToString();
                //    }
                //}

                int j = clsAPI.InsertDatabaseInfo(obj);
                if (j > 0)
                {
                    MessageBox.Show("Data successfully Insert");
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }

        }

        ////private void drpOurDBName_SelectedIndexChanged(object sender, EventArgs e)
        ////{
        ////    if (drpOurDataSource.Text == "SQLServer")
        ////    {
        ////        if (drpOurDBName.Text != "--Select--")
        ////        {
        ////            SqlConnection cnn;
        ////            connetionString = "Data Source=" + txtOurDBIpAddr.Text + ";User ID=" + txtOurDBUsername.Text + ";Password=" + txtOurDBPassword.Text + "; Integrated Security=Yes;Database = " + drpOurDBName.Text + ";";
        ////            cnn = new SqlConnection(connetionString);
        ////            try
        ////            {
        ////                cnn.Open();                        
        ////                using (SqlCommand com = new SqlCommand("SELECT * FROM Sys.TABLES", cnn))
        ////                {
        ////                    using (SqlDataReader reader = com.ExecuteReader())
        ////                    {
        ////                        drpOurTableName.Items.Clear();

        ////                        while (reader.Read())
        ////                        {
        ////                            drpOurTableName.Items.Add((string)reader["name"]);
        ////                        }
        ////                    }
        ////                }
        ////                cnn.Close();
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                MessageBox.Show("Can not open connection ! ");
        ////            }

        ////        }
        ////    }
        ////}
        ////private void drpOurTableName_SelectedIndexChanged(object sender, EventArgs e)
        ////{
        ////    if (drpOurDataSource.Text == "SQLServer")
        ////    {
        ////        if (drpOurDBName.Text != "--Select--")
        ////        {
        ////            string strcon = "Data Source=" + txtOurDBIpAddr.Text + ";User ID=" + txtOurDBUsername.Text + ";Password=" + txtOurDBPassword.Text + "; Integrated Security=Yes; Database = " + drpOurDBName.Text + ";";
        ////            using (SqlConnection con = new SqlConnection(strcon))
        ////            {
        ////                con.Open();
        ////                using (SqlCommand com = new SqlCommand("select [name],column_id from sys.columns WHERE object_id = OBJECT_ID('" + drpOurTableName.Text + "')", con))
        ////                {
        ////                    using (SqlDataReader reader = com.ExecuteReader())
        ////                    {
                               
        ////                        drpOurColumnName1.Items.Clear();
        ////                        drpOurColumnName2.Items.Clear();
        ////                        drpOurColumnName3.Items.Clear();
        ////                        drpOurColumnName4.Items.Clear();
        ////                        while (reader.Read())
        ////                        {
        ////                            drpOurColumnName1.Items.Add((string)reader["name"]);
        ////                            drpOurColumnName2.Items.Add((string)reader["name"]);
        ////                            drpOurColumnName3.Items.Add((string)reader["name"]);
        ////                            drpOurColumnName4.Items.Add((string)reader["name"]);
        ////                        }
        ////                    }
        ////                }
        ////            }
        ////        }
        ////    }
        ////}       
        
        private void btnSavePLCDiemensionalData_MouseHover(object sender, EventArgs e)
        {
            btnSavePLCDiemensionalData.BackColor = Color.FromArgb(62, 46, 52);
            btnSavePLCDiemensionalData.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnSavePLCDiemensionalData_MouseLeave(object sender, EventArgs e)
        {
            btnSavePLCDiemensionalData.BackColor = Color.FromArgb(255, 190, 0);
            btnSavePLCDiemensionalData.ForeColor = Color.FromArgb(62, 46, 52);
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            button2.BackColor = Color.FromArgb(62, 46, 52);
            button2.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = Color.FromArgb(255, 190, 0);
            button2.ForeColor = Color.FromArgb(62, 46, 52);
        }

        private void btnReset2_Click(object sender, EventArgs e)
        {
            txtDistanceTravelled.Text = "";
            txtEncoderPPR.Text = "";
            txtSpeedofConveyor.Text = "";
            txtArmOpeningTime.Text = "";
            txtLinearDistance.Text = "";
            txtEnterActivationDelay.Text = "";
            txtEnterDeactivationDelay.Text = "";
            txtPLCScanTime.Text = "";
        }

        private void lnkNewUser_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmUserCreation UserCreation = new frmUserCreation();
            UserCreation.ShowDialog();
        }

        private void btnConnTest_Click(object sender, EventArgs e)
        {

            if (drpDataSource.Text == "SQLServer")
            {
                SqlConnection cnn;
                connetionString = "Data Source=" + txtDBIpAddr.Text + ";User ID=" + txtDBUsername.Text + ";Password=" + txtDBPassword.Text + "; Integrated Security=Yes";
                cnn = new SqlConnection(connetionString);
                try
                {
                    cnn.Open();
                    MessageBox.Show("Connection was established successfully!!!");

                    DataTable dtDB = new DataTable();
                    dtDB = clsAPI.getSqlDatabase();
                    DataRow dr;
                    dr = dtDB.NewRow();
                    //dr.ItemArray = new object[] { 0, 0};
                    dr.ItemArray = new object[] { "--Select--", 0 };
                    dtDB.Rows.InsertAt(dr, 0);
                    drpDBName.DisplayMember = "name";
                    drpDBName.ValueMember = "database_id";
                    drpDBName.DataSource = dtDB;

                    cnn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can not open connection ! ");
                    oErrorLog.WriteErrorLog(ex.ToString());                     
                }
            }
            else if (drpDataSource.Text == "MSAccess")
            {
                OleDbConnection cnn;
                connetionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + txtDBSourcePathA.Text + ";Jet OLEDB:Database Password=" + txtDBPassworA.Text + ";";

                cnn = new OleDbConnection(connetionString);
                try
                {
                    cnn.Open();
                    MessageBox.Show("Connection successfull ! ");
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can not open connection ! ");
                    oErrorLog.WriteErrorLog(ex.ToString());   
                }
            }
            else if (drpDataSource.Text == "MySQL")
            {
               ///// MySqlConnection cnn;

               // connetionString = "Server = " + txtServerMy.Text + "; Port = 1234;Uid = " + txtDBIdMy.Text + "; Pwd = " + txtDBPassMy.Text + ";";

               //// cnn = new MySqlConnection(connetionString);
               // try
               // {
               //     cnn.Open();
               //     MessageBox.Show("Connection successfull ! ");
               //     cnn.Close();
               // }
               // catch (Exception ex)
               // {
               //     MessageBox.Show("Can not open connection ! ");
               //     oErrorLog.WriteErrorLog(ex.ToString());   
               // }
            }
            else if (drpDataSource.Text == "Oracle")
            {
                OleDbConnection cnn;

                connetionString = "User Id = " + txtDBIdOracle.Text + "; Password = " + txtDBPassOracle.Text + ";Integrated Security = " + drpIntegratedSecurity.Text + ";";

                cnn = new OleDbConnection(connetionString);
                try
                {
                    cnn.Open();
                    MessageBox.Show("Connection successfull ! ");
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can not open connection ! ");
                    oErrorLog.WriteErrorLog(ex.ToString());   
                }
            }
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            string outputInfo = "";
            string[] keyWords = txtSearch.Text.Split(' ');

            foreach (string word in keyWords)
            {
                if (outputInfo.Length == 0)
                {
                    //outputInfo = "(Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo = "Convert(Pincode, 'System.String') LIKE '" + word + "%'";
                    
                }
                else
                {
                    //outputInfo += " AND (Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo += " AND (Pincode LIKE '" + word + "%')";
                }
            }

            //Applies the filter to the DataView
            myView.RowFilter = outputInfo;
            
        }

        public void ReadData(string strSQL, ref DataSet dstMain, string strTableName)
        {
            try
            {
               // string connectionString = "Database=AdventureWorks;Data Source=localhost;Trusted_Connection=True";
              //  string connectionString = @"Data Source=ANC6\SQLEXPRESS;Initial Catalog=GPO;Integrated Security=Yes";
                string con = clsDataAccess.GetConnectionString();
                SqlConnection cnn = new SqlConnection(con);
                SqlCommand cmd = new SqlCommand(strSQL, cnn);
                cnn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dstMain, strTableName);
                da.Dispose();
                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                oErrorLog.WriteErrorLog(ex.ToString());   
            }
        }

        //private void rdNationalHub_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DataTable dtNHub = new DataTable();
        //        if (rdNationalHub.Checked == true)
        //        {
        //            dstResults.Tables["CityPincode"].Clear();

        //            ReadData("select Pincode,City from CityPincode",
        //                ref dstResults, "CityPincode");
                    
        //            //Creates a DataView from our table's default view
        //            myView = ((DataTable)dstResults.Tables["CityPincode"]).DefaultView;

        //            //dgvHubList.Columns.Count = 2;
                   

        //            dgvHubList.RowHeadersVisible = false;                    
        //            dgvHubList.DataSource = myView;
        //            dgvHubList.Columns[0].Width = 80;
        //            dgvHubList.Columns[1].Width = 120;

        //            //  dgvHubList.DataSource = dtNHub;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oErrorLog.WriteErrorLog(ex.ToString());
        //    }
        //}

        private void frmSorterAPIConfigurator_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void rdDWSYes_CheckedChanged(object sender, EventArgs e)
        {
            if (rdDWSYes.Checked == true)
            {
                grpVMSSystem.Visible = true;
            }
        }

        private void rdDWSNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rdDWSYes.Checked == false)
            {
                grpVMSSystem.Visible = false;
            }
        }       

        private void btnSaveLocation_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Length == 3 || txtSearch.Text.Length == 6)
            {
                DataTable dtch = new DataTable();
                int pin = Convert.ToInt32(txtSearch.Text);
                dtch = clsAPI.CheckPin(pin);
                if (dtch.Rows.Count > 0)
                {
                    string srtSoter = dtch.Rows[0]["SorterNo"].ToString();
                    DialogResult ch = MessageBox.Show("This pincode already existing in Bay no. " + srtSoter + ", do you want update it?? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (ch == DialogResult.Yes)
                    {
                        if (drpBranch.Text != "" && (drpBinUp.Text != "" || drpBinDown.Text != ""))
                        {
                            clsAPI obj = new clsAPI();
                            obj.Pincode = Convert.ToInt32(txtSearch.Text);
                            obj.SorterNo = Convert.ToInt32(drpBranch.Text);
                            if (drpBinUp.Text != "")
                            {
                                obj.Binno = Convert.ToInt32(drpBinUp.Text);
                            }
                            if (drpBinDown.Text != "")
                            {
                                obj.Binno = Convert.ToInt32(drpBinDown.Text);
                            }
                            //    obj.PlanDetailId = 0;// when data insert from Assignbag and LoactionSorter form that time Planid will be 0

                            int j = clsAPI.UpdateLocationAddr(obj);//InsertLocationSorter(obj);
                            if (j > 0)
                            {
                                MessageBox.Show("Record successfully Saved!!");
                                txtSearch.Text = "";
                                drpBranch.Text = "";
                                drpBinUp.Text = "";
                                drpBinDown.Text = "";
                                drpBinUp.Visible = false;
                                drpBinDown.Visible = false;
                                lblDown.Visible = false;
                                lblUp.Visible = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("All values are mandatory!!");
                        }
                    }
                }
                else
                {
                    if (drpBranch.Text != "" && (drpBinUp.Text != "" || drpBinDown.Text != ""))
                    {
                        clsAPI obj = new clsAPI();
                        obj.Pincode = Convert.ToInt32(txtSearch.Text);
                        obj.SorterNo = Convert.ToInt32(drpBranch.Text);
                        if (drpBinUp.Text != "")
                        {
                            obj.Binno = Convert.ToInt32(drpBinUp.Text);
                        }
                        if (drpBinDown.Text != "")
                        {
                            obj.Binno = Convert.ToInt32(drpBinDown.Text);
                        }
                        //  obj.PlanDetailId = 0;// when data insert from Assignbag and LoactionSorter form that time Planid will be 0

                        int j = clsAPI.SaveLocationAddr(obj);  //InsertLocationSorter(obj);//
                        if (j > 0)
                        {
                            MessageBox.Show("Record successfully Saved!!");
                            txtSearch.Text = "";
                            drpBranch.Text = "";
                            drpBinUp.Text = "";
                            drpBinDown.Text = "";
                            drpBinUp.Visible = false;
                            drpBinDown.Visible = false;
                            lblDown.Visible = false;
                            lblUp.Visible = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("All values are mandatory!!");
                    }
                }
            }
            else
            {
                MessageBox.Show("Only Three or Six digit pincode allowed!!");
            }
        }

        private void btnFldBrowser_Click(object sender, EventArgs e)
        {
            DialogResult result=this.folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
               txtFolderLocation.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtScnPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtPLCPort_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void txtPLCIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))  //only apllow 0-9 ,backspace and decimal
            {
                e.Handled = true;
                return;
            }
        }

        private void txtScannerIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8 && e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }
        }       

        public bool IsValidIP(string addr)
        {
            //create our match pattern
            string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.
([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
            //create our Regular Expression object
            Regex check = new Regex(pattern);
            //boolean variable to hold the status
            bool valid = false;
            //check to make sure an ip address was provided
            if (addr == "")
            {
                //no address provided so return false
                valid = false;
            }
            else
            {
                //address provided so use the IsMatch Method
                //of the Regular Expression object
                valid = check.IsMatch(addr, 0);
            }
            //return the results
            return valid;
        }

        private void btnTestDWS_Click(object sender, EventArgs e)
        {
           // string iptest = txtPLCIP.Text;
           //bool check = IsValidIP(iptest);
           //if (check == true)
           //{
           //    MessageBox.Show("Ip is Valid");
           //}
           //else
           //{
           //    MessageBox.Show("Ip is not Valid");
           //}

          // double value = 1234567890121;
         //  Console.WriteLine(value.ToString("###,#", CultureInfo.InvariantCulture));
        }

        private void drpBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtbin = new DataTable();
            int Branch;
            Branch = Convert.ToInt32(drpBranch.Text);
            dtbin = clsAPI.getBin(Branch);
            if (dtbin.Rows.Count > 0)
            {
                if (dtbin.Rows[0]["ArmDirection"].ToString() == "Up")
                {
                    int bin = Convert.ToInt32(dtbin.Rows[0]["NoofBags"]);
                    drpBinUp.Items.Clear();
                    for (int i = 1; i <= bin; i++)
                    {
                        drpBinUp.Items.Add(i);
                    }
                    lblUp.Visible = true;
                    drpBinUp.Visible = true;
                    lblDown.Visible = false;
                    drpBinDown.Visible = false;
                }
                else if (dtbin.Rows[0]["ArmDirection"].ToString() == "Down")
                {
                    int bin = Convert.ToInt32(dtbin.Rows[0]["NoofBags1"]);
                    drpBinDown.Items.Clear();
                    for (int i = 1; i <= bin; i++)
                    {
                        drpBinDown.Items.Add(i);
                    }
                    lblUp.Visible = false;
                    drpBinUp.Visible = false;
                    lblDown.Visible = true;
                    drpBinDown.Visible = true;
                }
                else
                {
                    int binup = Convert.ToInt32(dtbin.Rows[0]["NoofBagsUp"]);
                    int bindown = Convert.ToInt32(dtbin.Rows[0]["NoofBagsDown"]);

                    drpBinDown.Items.Clear();
                    for (int i = 1; i <= bindown; i++)
                    {
                        drpBinDown.Items.Add(i);
                    }
                    drpBinUp.Items.Clear();
                    for (int i = 1; i <= binup; i++)
                    {
                        drpBinUp.Items.Add(i);
                    }
                    lblUp.Visible = true;
                    drpBinUp.Visible = true;
                    lblDown.Visible = true;
                    drpBinDown.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Bin not assign to sorter " + Branch, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblUp.Visible = false;
                drpBinUp.Visible = false;
                lblDown.Visible = false;
                drpBinDown.Visible = false;
                drpBinUp.Items.Clear();
                drpBinDown.Items.Clear();
            }
        }

        private void btnLocationClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void drpBranchFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strBay = drpBranchFilter.Text;
            string outputInfo1 = string.Empty;
            // outputInfo1 = "SorterNo = '" + strBay + "')";
            outputInfo1 += "Convert(SorterNo, 'System.String') LIKE '" + strBay + "%'";

            //Applies the filter to the DataView
            myView.RowFilter = outputInfo1;
        }

        private void txtFilter_KeyUp(object sender, KeyEventArgs e)
        {
            string outputInfo = "";
            string[] keyWords = txtSearch.Text.Split(' ');

            foreach (string word in keyWords)
            {
                if (outputInfo.Length == 0)
                {
                    //outputInfo = "(Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo = "Convert(Pincode, 'System.String') LIKE '" + word + "%'";

                }
                else
                {
                    //outputInfo += " AND (Name LIKE '%" + word + "%' OR ProductModel LIKE '%" +
                    //    word + "%' OR Description LIKE '%" + word + "%')";
                    outputInfo += " AND (Pincode LIKE '" + word + "%')";
                }
            }

            //Applies the filter to the DataView
            myView.RowFilter = outputInfo;
        }

        private void txtFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btnPTLCancel_Click(object sender, EventArgs e)
        {

        }

       

       
        //private void rdStateHub_CheckedChanged(object sender, EventArgs e)
        //{
        //    DataTable StateHub = new DataTable();
        //    if (rdStateHub.Checked == true)
        //    {
        //        clsAPI obj = new clsAPI();
        //        obj.RegionId = 2;
        //        StateHub = clsAPI.GetDivision(obj);
        //        dgvHubList.DataSource = null;
        //        dgvHubList.AutoGenerateColumns = false;

        //        // 'Set Columns Count
        //        dgvHubList.ColumnCount = 2;

        //        // 'Add Columns

        //        dgvHubList.Columns[0].Name = "StateHubId";
        //        dgvHubList.Columns[0].HeaderText = "StateHub Id";
        //        dgvHubList.Columns[0].DataPropertyName = "StateHubId";

        //        dgvHubList.Columns[1].Name = "StateHub";
        //        dgvHubList.Columns[1].HeaderText = "StateHub";
        //        dgvHubList.Columns[1].DataPropertyName = "StateHub";

        //        dgvHubList.DataSource = StateHub;
        //    }
        //}      
   }
} 