﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using System.Globalization;
using System.Runtime.InteropServices;

namespace SorterAPI
{
    public partial class frmPlanDetails : Form
    {
        ErrorLog oErrorLog = new ErrorLog();
        Button[] buttonArray = new Button[32];
        DataTable dtgrid = new DataTable();
        DateTime CurrentDate = DateTime.Now.Date;
        DataTable dtpriority = new DataTable();
        int j;
        int Id;
        string filterField;
        string filterField_new;
        public frmPlanDetails()
        {
            InitializeComponent();
            tabCntrl.DrawItem += new DrawItemEventHandler(this.tabCntrl_DrawItem);
            buttongenerate();
            
        }
        public void buttongenerate()
        {
            int i;
            int x = 36;
            for (i = 1; i <= 8; i++)
            {
                if (i == 5)
                {
                    x = 25;
                }
                buttonArray[i] = new Button();
                buttonArray[i].Size = new Size(96, 34);
                buttonArray[i].Name = "btnS" + i;
                buttonArray[i].BackColor = Color.FromArgb(255, 190, 0);
                buttonArray[i].ForeColor = Color.FromArgb(62, 46, 52);
                buttonArray[i].Text = "#Sorter" + i;
                if (i <= 4)
                {                    
                    buttonArray[i].Location = new Point(x, 35);
                }
                else
                {
                    buttonArray[i].Location = new Point(x, 100);
                }
                buttonArray[i].Click += new EventHandler(ButtonClickOneEvent);
                buttonArray[i].Tag = i;
            
                grpGeneratebtn.Controls.Add(buttonArray[i]);
                
                x = x + 138;               
            }                       
        }

        private void frmPlanDetails_Load(object sender, EventArgs e)
        {
            FromdateUpdate.Text = CurrentDate.ToString();
            TodateUpdate.Text = CurrentDate.ToString();

            clsAPI obj = new clsAPI();
            obj.Fromdt = FromdateUpdate.Text.ToString();
            obj.Todt = FromdateUpdate.Text.ToString();
            
            dtpriority = clsAPI.getPriotrity(obj);
           // drpPriorityUpdate.Text = "";
            int count = 1;
            if (dtpriority.Rows.Count > 0)
            {
                count += Convert.ToInt32(dtpriority.Rows[0]["Count"]);
            }

            drpPriorityUpdate.Items.Clear();
            int i;
            for (i = 1; count >= i; i++)
            {
                drpPriorityUpdate.Items.Add(i);
            }

            Bindgrid();          
        }

        void ButtonClickOneEvent(object sender, EventArgs e)
        {
            try
            {
                Button button = sender as Button;
                if (button != null)
                {
                    int srtr = (int)button.Tag;
                    // MessageBox.Show(srtr.ToString());
                    //// now you know the button that was clicked
                    // switch ((int)button.Tag)
                    // {
                    //  case 1:
                    //this.Hide();
                    frmSorterPlan SP = new frmSorterPlan();
                    SP.mSorterNo = srtr;
                    SP.PlanDetail_Id = j;
                    SP.ShowDialog();
                    // break;
                    // ...
                    // }
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        #region tab design
        private void tabCntrl_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)   
        {
            if (e.Index == tabCntrl.SelectedIndex)
            {
                //e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(230, 86, 83)), e.Bounds);
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(28, 131, 136)), e.Bounds);
                e.Graphics.DrawString(tabCntrl.TabPages[e.Index].Text,
                    new Font(tabCntrl.Font, FontStyle.Bold),
                    Brushes.Wheat,
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
            else
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(4, 20, 21)), e.Bounds);
                e.Graphics.DrawString(tabCntrl.TabPages[e.Index].Text,
                    tabCntrl.Font,
                    //Brushes.Red;
                    new SolidBrush(Color.FromArgb(50, 150, 102)),
                    new PointF(e.Bounds.X + 3, e.Bounds.Y + 3));
            }
        }
        #endregion

        private void btnSavePlan_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtScope = new DataTable();

                clsAPI obj = new clsAPI();
                DateTime CurrentDate = DateTime.Now.Date;
                DateTime Fromdt = Convert.ToDateTime(dtFrom.Text.Trim()).Date;
                DateTime Todt = Convert.ToDateTime(dtTo.Text.Trim()).Date;

                if (txtPlanTitle.Text != "")
                {
                    if (txtPlanTitle.Text != "" && Fromdt >= CurrentDate && Todt >= Fromdt)
                    {
                        if (drpPlanPriority.Text != "")
                        {
                            obj.PlanTitle = txtPlanTitle.Text;
                            obj.Fromdt = dtFrom.Text;
                            obj.Todt = dtTo.Text;
                            obj.PlanPriority = Convert.ToInt32(drpPlanPriority.Text);
                            obj.UserId = SorterAPI.frmLogin.LoginInfo.UserID;
                            dtScope = clsAPI.InsertPlanDetails(obj);
                            if (dtScope.Rows.Count > 0)
                            {
                                j = Convert.ToInt32(dtScope.Rows[0]["SCOPE_IDENTITY"]);

                                if (j != 0)
                                {
                                    MessageBox.Show("Plan successfully saved, now configure sorter for this plan", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    grpGeneratebtn.Enabled = true;
                                    grpGeneratebtn.BackColor = Color.FromArgb(115, 205, 212);
                                    // MessageBox.Show("Plan successfully inserted!!");   115, 205, 212
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Select plan priority...", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Date... Please Give Correct Date....", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Enter plan title....", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void tabCntrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabCntrl.SelectedTab == tabCntrl.TabPages[0])
            {
                clsAPI obj = new clsAPI();    
                FromdateUpdate.Text = CurrentDate.ToString();
                TodateUpdate.Text = CurrentDate.ToString();

                obj.Fromdt = FromdateUpdate.Text;
                obj.Todt = FromdateUpdate.Text;

                dtpriority = clsAPI.getPriotrity(obj);
                drpPriorityUpdate.Text = "";
                int count = 1;
                if (dtpriority.Rows.Count > 0)
                {
                    count += Convert.ToInt32(dtpriority.Rows[0]["Count"]);
                }
                drpPriorityUpdate.Items.Clear();
                int i;
                for (i = 1; count >= i; i++)
                {
                    drpPriorityUpdate.Items.Add(i);
                }

                Bindgrid();

                txtPlanTitle.Text = "";
                grpGeneratebtn.Enabled = false;                
            }
            if (tabCntrl.SelectedTab == tabCntrl.TabPages[1])
            {
                clsAPI obj = new clsAPI();
                dtFrom.Text = CurrentDate.ToString();
                dtTo.Text = CurrentDate.ToString();

                obj.Fromdt = dtFrom.Text;
                obj.Todt = dtTo.Text;

                dtpriority = clsAPI.getPriotrity(obj);
                int count = 1;
                if (dtpriority.Rows.Count > 0)
                {
                    count += Convert.ToInt32(dtpriority.Rows[0]["Count"]);
                }
                drpPlanPriority.Items.Clear();
                int i;
                for (i = 1; count >= i; i++)
                {
                    drpPlanPriority.Items.Add(i);
                }
            }
        }

        public void Bindgrid()
        {
            try
            {  
                dtgrid = clsAPI.getPlanDetailsforGrid();

                dgvPlanDetails.DataSource = null;

                dgvPlanDetails.AutoGenerateColumns = false;
               // dgvPlanDetails.AllowUserToAddRows = false;
                dgvPlanDetails.ColumnCount = 10;
                dgvPlanDetails.RowHeadersVisible = false; // grid first column not visible
               
                dgvPlanDetails.Columns[0].Name = "Id1";
                dgvPlanDetails.Columns[0].HeaderText = "Sr. No.";
                dgvPlanDetails.Columns[0].DataPropertyName = "Id";

                dgvPlanDetails.Columns[1].Name = "PlanTitle";
                dgvPlanDetails.Columns[1].HeaderText = "Plan Title";
                dgvPlanDetails.Columns[1].DataPropertyName = "PlanTitle";

                dgvPlanDetails.Columns[2].Name = "FromDate";
                dgvPlanDetails.Columns[2].HeaderText = "From Date";
                dgvPlanDetails.Columns[2].DataPropertyName = "FromDate";

                dgvPlanDetails.Columns[3].Name = "ToDate";
                dgvPlanDetails.Columns[3].HeaderText = "To Date";
                dgvPlanDetails.Columns[3].DataPropertyName = "ToDate";

                dgvPlanDetails.Columns[4].Name = "PlanPriority";
                dgvPlanDetails.Columns[4].HeaderText = "Priority";
                dgvPlanDetails.Columns[4].DataPropertyName = "PlanPriority";

                dgvPlanDetails.Columns[5].Name = "CreatedDate";
                dgvPlanDetails.Columns[5].HeaderText = "Created Date";
                dgvPlanDetails.Columns[5].DataPropertyName = "CreatedDate";

                dgvPlanDetails.Columns[6].Name = "User_LoginName";
                dgvPlanDetails.Columns[6].HeaderText = "User Name";
                dgvPlanDetails.Columns[6].DataPropertyName = "User_LoginName";

                dgvPlanDetails.Columns[7].Name = "Edit";
                dgvPlanDetails.Columns[7].HeaderText = "Edit";
                dgvPlanDetails.Columns[7].DataPropertyName = "Edit";
                dgvPlanDetails.Columns[7].DefaultCellStyle.ForeColor = Color.Blue;

                dgvPlanDetails.Columns[8].Name = "Delete";
                dgvPlanDetails.Columns[8].HeaderText = "Delete";
                dgvPlanDetails.Columns[8].DataPropertyName = "Delete";
                dgvPlanDetails.Columns[8].DefaultCellStyle.ForeColor = Color.Blue;

                dgvPlanDetails.Columns[9].Name = "PId";
               //dgvPlanDetails.Columns[9].HeaderText = "Sr. No.";
                dgvPlanDetails.Columns[9].DataPropertyName = "PId";
                dgvPlanDetails.Columns[9].Visible = false;
                
                dgvPlanDetails.Columns[0].Width = 50;
                dgvPlanDetails.Columns[1].Width = 270;
                dgvPlanDetails.Columns[2].Width = 90;
                dgvPlanDetails.Columns[3].Width = 90;
                dgvPlanDetails.Columns[4].Width = 65;
                dgvPlanDetails.Columns[5].Width = 100;
                dgvPlanDetails.Columns[6].Width = 80;
                dgvPlanDetails.Columns[7].Width = 55;
                dgvPlanDetails.Columns[8].Width = 70;
               
                dgvPlanDetails.DataSource = dtgrid;

                for (int i = 0; i < dgvPlanDetails.Rows.Count - 1; i++)
                {
                    dgvPlanDetails.Rows[i].Cells[0].Value = (i + 1).ToString();                   
                }
                    
                filterField = "PlanTitle";
                filterField_new = "User_LoginName";
                //dgvPlanDetails.Sort(dgvPlanDetails.Columns["CreatedDate"], ListSortDirection.Descending); // for Aacending Descending
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void dgvPlanDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = dgvPlanDetails.Rows[e.RowIndex];
                    string id = row.Cells[9].Value.ToString();

                    DataRow[] foundRows;
                    foundRows = dtgrid.Select("PId ="+id);// Like '%" + id + "%'"); searching data from existing data table

                    txtPlanTitleUpdate.Text = foundRows[0][1].ToString();                    
                    FromdateUpdate.Text = foundRows[0][2].ToString();
                    TodateUpdate.Text = foundRows[0][3].ToString();
                    drpPriorityUpdate.Text = foundRows[0][4].ToString();
                    Id = Convert.ToInt32(id);

                    btnUpdate.Enabled = true;
                    btnReset.Enabled = true;                                      
                }
            }
            if(e.ColumnIndex == 8)
            {
                if (e.RowIndex >= 0)
                {
                  DialogResult ch=  MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                  if (ch == DialogResult.Yes)
                  {
                      DataGridViewRow row = dgvPlanDetails.Rows[e.RowIndex];
                      int id = Convert.ToInt32(row.Cells[9].Value);
                      int j = clsAPI.DeletePlanDetail(id);
                      if (j > 0)
                      {
                          MessageBox.Show("Plan successfully deleted!!");
                          Bindgrid();
                      }
                  }
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            dtgrid.DefaultView.RowFilter = string.Format("[{0}] LIKE '%{1}%'", filterField, textBox2.Text);
        }

        private void txtUserNameSearch_TextChanged(object sender, EventArgs e)
        {
            dtgrid.DefaultView.RowFilter = string.Format("[{0}] LIKE '%{1}%'", filterField_new, txtUserNameSearch.Text);
        } 

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DateTime CurrentDate = DateTime.Now.Date;
            DateTime Fromdt = Convert.ToDateTime(FromdateUpdate.Text.Trim()).Date;
            DateTime Todt = Convert.ToDateTime(TodateUpdate.Text.Trim()).Date;

            //DateTime Fromdt = DateTime.ParseExact(dtFrom.Text,"dd-MM-yyyy",new CultureInfo("en-US"),DateTimeStyles.None);
            //DateTime Todt = DateTime.ParseExact(dtTo.Text, "dd-MM-yyyy", new CultureInfo("en-US"),DateTimeStyles.None);

            if (txtPlanTitleUpdate.Text != "")
            {
                if (Fromdt >= CurrentDate && Todt >= Fromdt)
                {
                    clsAPI obj = new clsAPI();
                    obj.PlanTitle = txtPlanTitleUpdate.Text;
                    obj.Fromdt = FromdateUpdate.Text;
                    obj.Todt = TodateUpdate.Text;
                    obj.PlanPriority = Convert.ToInt32(drpPriorityUpdate.Text);
                    obj.Id = Id;
                    obj.UserId = SorterAPI.frmLogin.LoginInfo.UserID;
                    int j = clsAPI.UpdatePlanDetails(obj);
                    if (j > 0)
                    {
                        MessageBox.Show("Plan successfully updated!!");
                        btnUpdate.Enabled = false;
                        btnReset.Enabled = false;
                        txtPlanTitleUpdate.Text = "";
                        Bindgrid();
                    }

                }
                else
                {
                    MessageBox.Show("Invalid Date... Please Give Correct Date....");
                }
            }
            else
            {
                MessageBox.Show("Enter plan title..");
            }
            

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtPlanTitleUpdate.Text = "";
            drpPriorityUpdate.Text = "";
            FromdateUpdate.ResetText();
            TodateUpdate.ResetText();
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox1.Visible = true;
            }
        }      

        private void txtUserNameSearch_Leave(object sender, EventArgs e)
        {
            if (txtUserNameSearch.Text == "")
            {
                textBox3.Visible = true;
            }
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Visible = false;
            textBox2.Select();
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            textBox3.Visible = false;
            txtUserNameSearch.Select();
        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }        
            
    }
}
