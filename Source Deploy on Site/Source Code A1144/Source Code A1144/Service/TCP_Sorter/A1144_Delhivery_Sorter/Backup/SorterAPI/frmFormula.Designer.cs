﻿namespace SorterAPI
{
    partial class frmFormula
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEvalFormula = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCheck = new System.Windows.Forms.Button();
            this.txtCheckValue = new System.Windows.Forms.TextBox();
            this.btnMsg = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnGreaterEqual = new System.Windows.Forms.Button();
            this.btnLessEqual = new System.Windows.Forms.Button();
            this.btnGreater = new System.Windows.Forms.Button();
            this.btnNotEqual = new System.Windows.Forms.Button();
            this.btnCosingC = new System.Windows.Forms.Button();
            this.btnOpenC = new System.Windows.Forms.Button();
            this.btnClosingB = new System.Windows.Forms.Button();
            this.btnLess = new System.Windows.Forms.Button();
            this.btnOpenB = new System.Windows.Forms.Button();
            this.btnif = new System.Windows.Forms.Button();
            this.btnEqual = new System.Windows.Forms.Button();
            this.txtFormulaArea = new System.Windows.Forms.TextBox();
            this.btnUndo = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnFormulaSave = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnEvalFormula);
            this.panel1.Location = new System.Drawing.Point(4, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(370, 227);
            this.panel1.TabIndex = 47;
            // 
            // btnEvalFormula
            // 
            this.btnEvalFormula.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnEvalFormula.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEvalFormula.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEvalFormula.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnEvalFormula.Location = new System.Drawing.Point(208, 193);
            this.btnEvalFormula.Name = "btnEvalFormula";
            this.btnEvalFormula.Size = new System.Drawing.Size(88, 23);
            this.btnEvalFormula.TabIndex = 52;
            this.btnEvalFormula.Text = "Eval Formula";
            this.btnEvalFormula.UseVisualStyleBackColor = false;
            this.btnEvalFormula.Click += new System.EventHandler(this.btnEvalFormula_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCheck);
            this.panel2.Controls.Add(this.txtCheckValue);
            this.panel2.Location = new System.Drawing.Point(4, -16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(370, 227);
            this.panel2.TabIndex = 0;
            this.panel2.Visible = false;
            // 
            // btnCheck
            // 
            this.btnCheck.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnCheck.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnCheck.Location = new System.Drawing.Point(147, 81);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(55, 23);
            this.btnCheck.TabIndex = 49;
            this.btnCheck.Text = "Check";
            this.btnCheck.UseVisualStyleBackColor = false;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // txtCheckValue
            // 
            this.txtCheckValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCheckValue.Location = new System.Drawing.Point(125, 29);
            this.txtCheckValue.Name = "txtCheckValue";
            this.txtCheckValue.Size = new System.Drawing.Size(100, 20);
            this.txtCheckValue.TabIndex = 0;
            // 
            // btnMsg
            // 
            this.btnMsg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMsg.Location = new System.Drawing.Point(39, 178);
            this.btnMsg.Margin = new System.Windows.Forms.Padding(2);
            this.btnMsg.Name = "btnMsg";
            this.btnMsg.Size = new System.Drawing.Size(56, 24);
            this.btnMsg.TabIndex = 67;
            this.btnMsg.Text = "msg";
            this.btnMsg.UseVisualStyleBackColor = true;
            this.btnMsg.Click += new System.EventHandler(this.btnMsg_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnExit.Location = new System.Drawing.Point(303, 207);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(47, 23);
            this.btnExit.TabIndex = 66;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnGreaterEqual
            // 
            this.btnGreaterEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGreaterEqual.Location = new System.Drawing.Point(304, 146);
            this.btnGreaterEqual.Margin = new System.Windows.Forms.Padding(2);
            this.btnGreaterEqual.Name = "btnGreaterEqual";
            this.btnGreaterEqual.Size = new System.Drawing.Size(29, 24);
            this.btnGreaterEqual.TabIndex = 65;
            this.btnGreaterEqual.Text = ">=";
            this.btnGreaterEqual.UseVisualStyleBackColor = true;
            this.btnGreaterEqual.Click += new System.EventHandler(this.btnGreaterEqual_Click);
            // 
            // btnLessEqual
            // 
            this.btnLessEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLessEqual.Location = new System.Drawing.Point(271, 146);
            this.btnLessEqual.Margin = new System.Windows.Forms.Padding(2);
            this.btnLessEqual.Name = "btnLessEqual";
            this.btnLessEqual.Size = new System.Drawing.Size(29, 24);
            this.btnLessEqual.TabIndex = 64;
            this.btnLessEqual.Text = "<=";
            this.btnLessEqual.UseVisualStyleBackColor = true;
            this.btnLessEqual.Click += new System.EventHandler(this.btnLessEqual_Click);
            // 
            // btnGreater
            // 
            this.btnGreater.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGreater.Location = new System.Drawing.Point(246, 146);
            this.btnGreater.Name = "btnGreater";
            this.btnGreater.Size = new System.Drawing.Size(19, 24);
            this.btnGreater.TabIndex = 63;
            this.btnGreater.Text = ">";
            this.btnGreater.UseVisualStyleBackColor = true;
            this.btnGreater.Click += new System.EventHandler(this.btnGreater_Click);
            // 
            // btnNotEqual
            // 
            this.btnNotEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotEqual.Location = new System.Drawing.Point(190, 146);
            this.btnNotEqual.Margin = new System.Windows.Forms.Padding(2);
            this.btnNotEqual.Name = "btnNotEqual";
            this.btnNotEqual.Size = new System.Drawing.Size(26, 24);
            this.btnNotEqual.TabIndex = 62;
            this.btnNotEqual.Text = "!=";
            this.btnNotEqual.UseVisualStyleBackColor = true;
            this.btnNotEqual.Click += new System.EventHandler(this.btnNotEqual_Click);
            // 
            // btnCosingC
            // 
            this.btnCosingC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCosingC.Location = new System.Drawing.Point(141, 146);
            this.btnCosingC.Name = "btnCosingC";
            this.btnCosingC.Size = new System.Drawing.Size(19, 24);
            this.btnCosingC.TabIndex = 61;
            this.btnCosingC.Text = "}";
            this.btnCosingC.UseVisualStyleBackColor = true;
            this.btnCosingC.Click += new System.EventHandler(this.btnCosingC_Click);
            // 
            // btnOpenC
            // 
            this.btnOpenC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenC.Location = new System.Drawing.Point(116, 146);
            this.btnOpenC.Name = "btnOpenC";
            this.btnOpenC.Size = new System.Drawing.Size(19, 24);
            this.btnOpenC.TabIndex = 60;
            this.btnOpenC.Text = "{";
            this.btnOpenC.UseVisualStyleBackColor = true;
            this.btnOpenC.Click += new System.EventHandler(this.btnOpenC_Click);
            // 
            // btnClosingB
            // 
            this.btnClosingB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClosingB.Location = new System.Drawing.Point(91, 146);
            this.btnClosingB.Name = "btnClosingB";
            this.btnClosingB.Size = new System.Drawing.Size(19, 24);
            this.btnClosingB.TabIndex = 59;
            this.btnClosingB.Text = ")";
            this.btnClosingB.UseVisualStyleBackColor = true;
            this.btnClosingB.Click += new System.EventHandler(this.btnClosingB_Click);
            // 
            // btnLess
            // 
            this.btnLess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLess.Location = new System.Drawing.Point(221, 146);
            this.btnLess.Name = "btnLess";
            this.btnLess.Size = new System.Drawing.Size(19, 24);
            this.btnLess.TabIndex = 58;
            this.btnLess.Text = "<";
            this.btnLess.UseVisualStyleBackColor = true;
            this.btnLess.Click += new System.EventHandler(this.btnLess_Click);
            // 
            // btnOpenB
            // 
            this.btnOpenB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenB.Location = new System.Drawing.Point(66, 146);
            this.btnOpenB.Name = "btnOpenB";
            this.btnOpenB.Size = new System.Drawing.Size(19, 24);
            this.btnOpenB.TabIndex = 57;
            this.btnOpenB.Text = "(";
            this.btnOpenB.UseVisualStyleBackColor = true;
            this.btnOpenB.Click += new System.EventHandler(this.btnOpenB_Click);
            // 
            // btnif
            // 
            this.btnif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnif.Location = new System.Drawing.Point(39, 146);
            this.btnif.Margin = new System.Windows.Forms.Padding(2);
            this.btnif.Name = "btnif";
            this.btnif.Size = new System.Drawing.Size(22, 24);
            this.btnif.TabIndex = 56;
            this.btnif.Text = "if";
            this.btnif.UseVisualStyleBackColor = true;
            this.btnif.Click += new System.EventHandler(this.btnif_Click);
            // 
            // btnEqual
            // 
            this.btnEqual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEqual.Location = new System.Drawing.Point(166, 146);
            this.btnEqual.Name = "btnEqual";
            this.btnEqual.Size = new System.Drawing.Size(19, 24);
            this.btnEqual.TabIndex = 55;
            this.btnEqual.Text = "=";
            this.btnEqual.UseVisualStyleBackColor = true;
            this.btnEqual.Click += new System.EventHandler(this.btnEqual_Click);
            // 
            // txtFormulaArea
            // 
            this.txtFormulaArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFormulaArea.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(131)))), ((int)(((byte)(136)))));
            this.txtFormulaArea.Location = new System.Drawing.Point(12, 46);
            this.txtFormulaArea.MaxLength = 1000;
            this.txtFormulaArea.Multiline = true;
            this.txtFormulaArea.Name = "txtFormulaArea";
            this.txtFormulaArea.Size = new System.Drawing.Size(350, 94);
            this.txtFormulaArea.TabIndex = 54;
            // 
            // btnUndo
            // 
            this.btnUndo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnUndo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUndo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUndo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnUndo.Location = new System.Drawing.Point(73, 207);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(47, 22);
            this.btnUndo.TabIndex = 53;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = false;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(131)))), ((int)(((byte)(136)))));
            this.label5.Location = new System.Drawing.Point(9, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 15);
            this.label5.TabIndex = 51;
            this.label5.Text = "For ex. if(Weight<10){msg}";
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.Location = new System.Drawing.Point(106, 16);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(0, 16);
            this.lblHeading.TabIndex = 50;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnCancel.Location = new System.Drawing.Point(126, 207);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 49;
            this.btnCancel.Text = "BackSpace";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnFormulaSave
            // 
            this.btnFormulaSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(190)))), ((int)(((byte)(0)))));
            this.btnFormulaSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnFormulaSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFormulaSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(46)))), ((int)(((byte)(52)))));
            this.btnFormulaSave.Location = new System.Drawing.Point(24, 207);
            this.btnFormulaSave.Name = "btnFormulaSave";
            this.btnFormulaSave.Size = new System.Drawing.Size(44, 23);
            this.btnFormulaSave.TabIndex = 48;
            this.btnFormulaSave.Text = "Save";
            this.btnFormulaSave.UseVisualStyleBackColor = false;
            this.btnFormulaSave.Click += new System.EventHandler(this.btnFormulaSave_Click);
            // 
            // frmFormula
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(380, 246);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnMsg);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnGreaterEqual);
            this.Controls.Add(this.btnLessEqual);
            this.Controls.Add(this.btnGreater);
            this.Controls.Add(this.btnNotEqual);
            this.Controls.Add(this.btnCosingC);
            this.Controls.Add(this.btnOpenC);
            this.Controls.Add(this.btnClosingB);
            this.Controls.Add(this.btnLess);
            this.Controls.Add(this.btnOpenB);
            this.Controls.Add(this.btnif);
            this.Controls.Add(this.btnEqual);
            this.Controls.Add(this.txtFormulaArea);
            this.Controls.Add(this.btnUndo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblHeading);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnFormulaSave);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(131)))), ((int)(((byte)(136)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFormula";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formula";
            this.Load += new System.EventHandler(this.frmFormula_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnMsg;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnGreaterEqual;
        private System.Windows.Forms.Button btnLessEqual;
        private System.Windows.Forms.Button btnGreater;
        private System.Windows.Forms.Button btnNotEqual;
        private System.Windows.Forms.Button btnCosingC;
        private System.Windows.Forms.Button btnOpenC;
        private System.Windows.Forms.Button btnClosingB;
        private System.Windows.Forms.Button btnLess;
        private System.Windows.Forms.Button btnOpenB;
        private System.Windows.Forms.Button btnif;
        private System.Windows.Forms.Button btnEqual;
        private System.Windows.Forms.TextBox txtFormulaArea;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnEvalFormula;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnFormulaSave;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.TextBox txtCheckValue;

    }
}