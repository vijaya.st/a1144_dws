﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using SorterAPI.Common;
using System.Data.SqlClient;

namespace SorterAPI
{
    public partial class frmSorterAPIConfigurator5 : Form
    {
        ErrorLog oErrorLog = new ErrorLog();
        string connetionString = null;
        public frmSorterAPIConfigurator5()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                          Screen.PrimaryScreen.Bounds.Y + 100);

            //string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = System.Convert.ToInt32(screenWidth);
            //int H = System.Convert.ToInt32(screenHeight);

            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);

            //this.TopMost = true;
            //this.StartPosition = FormStartPosition.Manual;
           // this.MaximumSize = new Size(1200, 1000);
           // this.AutoScroll = true;
            
        }

        private void frmSorterAPIConfigurator5_Load(object sender, EventArgs e)
        {
        }
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblCustInfo_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator4 f4 = new frmSorterAPIConfigurator4();
            f4.Show();
            this.Close();
        }

        private void lblPLCData_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator2 f2 = new frmSorterAPIConfigurator2();
            f2.Show();
            this.Close();
        }

        private void lblPTLSys_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator3 f3 = new frmSorterAPIConfigurator3();
            f3.Show();
            this.Close();
        }

        private void lblLinkDB_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator5 f5 = new frmSorterAPIConfigurator5();
            f5.Show();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator1 f1 = new frmSorterAPIConfigurator1();
            f1.Show();
            this.Close();
        }

        private void lblBagAssignment_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator6 f6 = new frmSorterAPIConfigurator6();
            f6.Show();
            this.Close();
        }

        private void pnlClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void drpOurDataSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (drpOurDataSource.Text == "SQLServer")
            //{
            //    lblOurServerName.Text = "Enter Server Name :";
            //    lblOurServerId.Text = "Enter Server ID :";
            //    lblOurServerPass.Text = "Enter Server Password";              
            //}
            //else if (drpOurDataSource.Text == "MSAccess")
            //{
            //    lblOurServerName.Text = "Enter Database Provider :";
            //    lblOurServerId.Text = "Enter Data Source Path :";
            //    lblOurServerPass.Text = "Enter Database password :";                
            //}
            //else if (drpOurDataSource.Text == "MySql")
            //{
            //    lblOurServerName.Text = "Enter Server IP :";
            //    lblOurServerId.Text = "Enter Database Id :";
            //    lblOurServerPass.Text = "Enter Database Password :";                
            //}
            //else if (drpOurDataSource.Text == "Oracle")
            //{
            //    lblOurServerName.Text = "Enter Database Id :";
            //    lblOurServerId.Text = "Enter Database Password :";
            //    lblOurServerPass.Text = "Integrated Security :";
            //}
        }

        private void drpDataSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDataSource.Text == "SQLServer")
            {
                lblCustServerName.Text = "Enter Server Name :";
                lblCustServerId.Text = "Enter Server ID :";
                lblCustServerPass.Text = "Enter Server Password";
            }
            else if (drpDataSource.Text == "MSAccess")
            {
                lblCustServerName.Text = "Enter Database Provider :";
                lblCustServerId.Text = "Enter Data Source Path :";
                lblCustServerPass.Text = "Enter Database password :";

            }
            else if (drpDataSource.Text == "MySql")
            {
                lblCustServerName.Text = "Enter Server IP :";
                lblCustServerId.Text = "Enter Database Id :";
                lblCustServerPass.Text = "Enter Database Password :";

            }
            else if (drpDataSource.Text == "Oracle")
            {
                lblCustServerName.Text = "Enter Database Id :";
                lblCustServerId.Text = "Enter Database Password :";
                lblCustServerPass.Text = "Integrated Security :";
            }
        }       

        private void btnSaveDBInfo_Click(object sender, EventArgs e)
        {
            try
            {
                clsAPI obj = new clsAPI();
                obj.DataSource = drpDataSource.Text;
                obj.DB1 = txtDBIpAddr.Text;
                obj.DB2 = txtDBUsername.Text;
                obj.DB3 = txtDBPassword.Text;               

                obj.OurDataSource = drpOurDataSource.Text;
                obj.OurDB1 = txtOurServerName.Text;
                obj.OurDB2 = txtOurServerId.Text;
                obj.OurDB3 = txtOurServerPass.Text;               
               
                obj.OurDBName = drpOurDBName.Text; 
                obj.OurTableName = txtOurTableName.Text;
                obj.OurColumn1 = txtOurColumn1.Text;
                obj.OurColumn2 = txtOurColumn1.Text;
                obj.OurColumn3 = txtOurColumn1.Text;
                obj.OurColumn4 = txtOurColumn1.Text;

                obj.MinH = Convert.ToInt32(txtMinH.Text);
                obj.MaxH = Convert.ToInt32(txtMaxH.Text);
                obj.MinW = Convert.ToInt32(txtMinW.Text);
                obj.MaxW = Convert.ToInt32(txtMaxW.Text);
                obj.MinL = Convert.ToInt32(txtMinL.Text);
                obj.MaxL = Convert.ToInt32(txtMaxL.Text);
                obj.MinWe = Convert.ToInt32(txtMinWe.Text);
                obj.MaxWe = Convert.ToInt32(txtMaxWe.Text);

                //DataTable dtFormula = new DataTable();
                //dtFormula = clsAPI.getFormula();
                //for (int i = 0; i < dtFormula.Rows.Count; i++)
                //{
                //    if (dtFormula.Rows[i][1].ToString() != "" && i==0)
                //    {
                //        obj.Formula1 = dtFormula.Rows[i][1].ToString();
                //    }
                //    else if (dtFormula.Rows[i][1].ToString() != "" && i == 1)
                //    {
                //        obj.Formula2 = dtFormula.Rows[i][1].ToString();
                //    }
                //    else if (dtFormula.Rows[i][1].ToString() != "" && i == 2)
                //    {
                //        obj.Formula3 = dtFormula.Rows[i][1].ToString();
                //    }
                //    else if (dtFormula.Rows[i][1].ToString() != "" && i == 3)
                //    {
                //        obj.Formula4 = dtFormula.Rows[i][1].ToString();
                //    }
                //}

                int j = clsAPI.InsertDatabaseInfo(obj);
                if (j > 0)
                {
                    MessageBox.Show("Data successfully Insert");
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void btnTestOurServer_Click(object sender, EventArgs e)
        {           
                SqlConnection cnn;
                connetionString = "Data Source=" + txtOurServerName.Text + ";User ID=" + txtOurServerId.Text + ";Password=" + txtOurServerPass.Text + "; Integrated Security=Yes";
                cnn = new SqlConnection(connetionString);
                try
                {
                    cnn.Open();

                    MessageBox.Show("Connection was established successfully!!!");

                    //  SqlConnection cn = new SqlConnection(clsDataAccess.GetConnectionString());
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtDB = new DataTable();

                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select [name],database_id from sys.databases";
                    da.Fill(dtDB);

                    if (dtDB.Rows.Count > 0)
                    {
                        DataRow dr;
                        dr = dtDB.NewRow();
                        //dr.ItemArray = new object[] { 0, 0};
                        dr.ItemArray = new object[] { "--Select--", 0 };
                        dtDB.Rows.InsertAt(dr, 0);
                        drpOurDBName.DisplayMember = "name";
                        drpOurDBName.ValueMember = "database_id";
                        drpOurDBName.DataSource = dtDB;
                    }
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can not open connection ! ");
                    oErrorLog.WriteErrorLog(ex.ToString());
                }
        }

        private void btnConnTest_Click(object sender, EventArgs e)
        {
            if (drpDataSource.Text == "SQLServer")
            {
                SqlConnection cnn;
                connetionString = "Data Source=" + txtDBIpAddr.Text + ";User ID=" + txtDBUsername.Text + ";Password=" + txtDBPassword.Text + "; Integrated Security=Yes";
                cnn = new SqlConnection(connetionString);
                try
                {
                    cnn.Open();
                    MessageBox.Show("Connection was established successfully!!!");

                    //  SqlConnection cn = new SqlConnection(clsDataAccess.GetConnectionString());
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtDB = new DataTable();

                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select [name],database_id from sys.databases";
                    da.Fill(dtDB);

                    //    dtDB = clsAPI.getSqlDatabase();
                    if (dtDB.Rows.Count > 0)
                    {
                        DataRow dr;
                        dr = dtDB.NewRow();
                        //dr.ItemArray = new object[] { 0, 0};
                        dr.ItemArray = new object[] { "--Select--", 0 };
                        dtDB.Rows.InsertAt(dr, 0);
                        drpDBName.DisplayMember = "name";
                        drpDBName.ValueMember = "database_id";
                        drpDBName.DataSource = dtDB;
                    }

                    da.Dispose();
                    cmd.Dispose();
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can not open connection ! ");
                    oErrorLog.WriteErrorLog(ex.ToString());
                }
                finally
                {

                }
            }
            else if (drpDataSource.Text == "MSAccess")
            {
                OleDbConnection cnn;
                connetionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + txtDBIpAddr.Text + ";Jet OLEDB:Database Password=" + txtDBUsername.Text + ";";

                cnn = new OleDbConnection(connetionString);
                try
                {
                    cnn.Open();
                    MessageBox.Show("Connection successfull ! ");
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can not open connection ! ");
                    oErrorLog.WriteErrorLog(ex.ToString());
                }
            }
            else if (drpDataSource.Text == "MySQL")
            {
                ///// MySqlConnection cnn;

                // connetionString = "Server = " + txtServerMy.Text + "; Port = 1234;Uid = " + txtDBIdMy.Text + "; Pwd = " + txtDBPassMy.Text + ";";

                //// cnn = new MySqlConnection(connetionString);
                // try
                // {
                //     cnn.Open();
                //     MessageBox.Show("Connection successfull ! ");
                //     cnn.Close();
                // }
                // catch (Exception ex)
                // {
                //     MessageBox.Show("Can not open connection ! ");
                //     oErrorLog.WriteErrorLog(ex.ToString());   
                // }
            }
            else if (drpDataSource.Text == "Oracle")
            {
                OleDbConnection cnn;

                connetionString = "User Id = " + txtDBIpAddr.Text + "; Password = " + txtDBUsername.Text + ";Integrated Security = " + txtDBPassword.Text + ";";

                cnn = new OleDbConnection(connetionString);
                try
                {
                    cnn.Open();
                    MessageBox.Show("Connection successfull ! ");
                    cnn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can not open connection ! ");
                    oErrorLog.WriteErrorLog(ex.ToString());
                }
            }
        }
    }
}