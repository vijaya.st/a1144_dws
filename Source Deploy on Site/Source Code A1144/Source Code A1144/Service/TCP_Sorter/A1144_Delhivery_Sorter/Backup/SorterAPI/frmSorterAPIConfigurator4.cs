﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class frmSorterAPIConfigurator4 : Form
    {
        DataTable dtGnr = new DataTable();
        public frmSorterAPIConfigurator4()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                          Screen.PrimaryScreen.Bounds.Y + 100);

           // string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
           // string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
           // int W = System.Convert.ToInt32(screenWidth);
           // int H = System.Convert.ToInt32(screenHeight);

           // this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);

           //// this.TopMost = true;
           // this.StartPosition = FormStartPosition.Manual;
            //this.MaximumSize = new Size(1200, 800);
           // this.AutoScroll = true;
            
        }

        private void frmSorterAPIConfigurator4_Load(object sender, EventArgs e)
        {
           // string str_directory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
           // panel1.BackgroundImage = Image.FromFile(str_directory + "\\images\\customer_info_title.png");
           // panel2.BackgroundImage = Image.FromFile(str_directory + "\\images\\customer_info.png");
          //  pnlClose.BackgroundImage = Image.FromFile(str_directory + "\\images\\close1.png"); 

            getGeneralsrtting();
        }
        private void lblClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblCustInfo_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator4 f4 = new frmSorterAPIConfigurator4();
           // f4.BringToFront();
            f4.Show();
            this.Close();
        }

        private void lblPLCData_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator2 f2 = new frmSorterAPIConfigurator2();
           // f2.BringToFront();
            f2.Show();
            this.Close();
        }

        private void lblPTLSys_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator3 f3 = new frmSorterAPIConfigurator3();
           // f3.BringToFront();
            f3.Show();
            this.Close();
        }

        private void lblLinkDB_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator5 f5 = new frmSorterAPIConfigurator5();
           // f5.BringToFront();
            f5.Show();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator1 f1 = new frmSorterAPIConfigurator1();
            //f1.BringToFront();
            f1.Show();
            this.Close();
        }

        private void lblBagAssignment_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator6 f6 = new frmSorterAPIConfigurator6();
            //f6.BringToFront();
            f6.Show();
            this.Close();
        }

        private void pnlClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LnkManageUser_Click(object sender, EventArgs e)
        {
            frmUserCreation uc = new frmUserCreation();
            uc.BringToFront();
            uc.ShowDialog();
        }

        private void getGeneralsrtting()
        {
            dtGnr = clsAPI.getGenralSettingsData();
            if (dtGnr.Rows.Count > 0)
            {
                btnSaveGeneralSetting.Text = "Update";

                txtComapanyName.Text = dtGnr.Rows[0][1].ToString();
                txtAddr.Text = dtGnr.Rows[0][2].ToString();
                txtContactPerson.Text = dtGnr.Rows[0][3].ToString();
                txtContactEmail.Text = dtGnr.Rows[0][4].ToString();
                txtContactNumber.Text = dtGnr.Rows[0][5].ToString();
                if (dtGnr.Rows[0][6] != DBNull.Value)
                {
                    byte[] img_arr1 = (byte[])dtGnr.Rows[0][6];
                    MemoryStream ms1 = new MemoryStream(img_arr1);
                    ms1.Seek(0, SeekOrigin.Begin);
                    pictureBox2.Image = Image.FromStream(ms1);
                }
                txtOurContactPerson.Text = dtGnr.Rows[0][7].ToString();
                txtOurContactEmail.Text = dtGnr.Rows[0][8].ToString();
                txtOurContactNumber.Text = dtGnr.Rows[0][9].ToString();
            }
        }
            
        byte[] bimage;
        private void btnLogoUpload_Click(object sender, EventArgs e)
        {
            //Getting The Image From The System
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (open.ShowDialog() == DialogResult.OK)
            {
                System.IO.FileInfo file = new System.IO.FileInfo(open.FileName);
                Bitmap img = new Bitmap(open.FileName);
                string imgname = open.FileName;

                if (img.Width <= 370 && img.Height <= 105)
                {
                    if (file.Length <= 50000)  //&& file.Length < MAX_SIZE 50kb
                    {

                        pictureBox2.BackgroundImageLayout = ImageLayout.Stretch;
                        pictureBox2.Image = img;


                        //  Bitmap bmp = new Bitmap(image);
                        FileStream fs = new FileStream(imgname, FileMode.Open, FileAccess.Read);
                        bimage = new byte[fs.Length];
                        fs.Read(bimage, 0, Convert.ToInt32(fs.Length));
                        fs.Close();
                    }
                    else
                    {
                        MessageBox.Show("File size must not exceed 50kb");
                    }
                }
                else
                {
                    MessageBox.Show("Height and Width must not exceed 105px*370px.");
                }


                //  ms1.Read(img_arr1, 0, img_arr1.Length);

                //Save bimage @image

            }      
        }

        private void btnSaveGeneralSetting_Click(object sender, EventArgs e)
        {
            if (txtComapanyName.Text != "" && txtAddr.Text != "" && txtContactPerson.Text != "" &&
                txtContactEmail.Text != "" && txtContactNumber.Text != "" && txtOurContactPerson.Text != "" &&
                txtOurContactEmail.Text != "" && txtOurContactNumber.Text != "")
            {
                Regex emailRegex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");

                if (!emailRegex.IsMatch(txtContactEmail.Text) && !emailRegex.IsMatch(txtOurContactEmail.Text))
                {
                    MessageBox.Show("E-Mail expected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    clsAPI obj = new clsAPI();
                    obj.CompanyName = txtComapanyName.Text;
                    obj.Address = txtAddr.Text;
                    obj.ContactPersonName = txtContactPerson.Text;
                    obj.ContactEmail = txtContactEmail.Text;
                    obj.ContactNumber = txtContactNumber.Text;
                    obj.CompanyLogo = bimage;
                    obj.OurContactPerson = txtOurContactPerson.Text;
                    obj.OurContactEmail = txtOurContactEmail.Text;
                    obj.OurContactNumber = txtOurContactNumber.Text;

                    if (btnSaveGeneralSetting.Text == "Save")
                    {
                        int i = clsAPI.InsertGeneralSetting(obj);
                        if (i > 0)
                        {
                            MessageBox.Show("Data successfully Insert");
                        }
                    }
                    else
                    {
                        int i = clsAPI.UpdateGeneralSettings(obj);
                        if (i > 0)
                        {
                            MessageBox.Show("Data successfully Update");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Red * value are mandatory");
            }
        }       
    }
}