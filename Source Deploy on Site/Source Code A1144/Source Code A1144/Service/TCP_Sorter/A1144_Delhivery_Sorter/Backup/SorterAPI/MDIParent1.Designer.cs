﻿namespace SorterAPI
{
    partial class MDIParent1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlSearchBarcode = new System.Windows.Forms.Panel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.lblSearchBarcode = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBatchOperation = new System.Windows.Forms.Panel();
            this.picBatchOperation = new System.Windows.Forms.PictureBox();
            this.lblBarchOperation = new System.Windows.Forms.Label();
            this.pnlExit = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblExit = new System.Windows.Forms.Label();
            this.pnlUtility = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.lblUtility = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBoxClose = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlAdminMenu = new System.Windows.Forms.Panel();
            this.pnlOUMap = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblOUMapp = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlSearchBarcode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlBatchOperation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBatchOperation)).BeginInit();
            this.pnlExit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.pnlUtility.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.pnlOUMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.pnlOUMap);
            this.panel1.Controls.Add(this.pnlSearchBarcode);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnlBatchOperation);
            this.panel1.Controls.Add(this.pnlExit);
            this.panel1.Location = new System.Drawing.Point(133, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 328);
            this.panel1.TabIndex = 10;
            this.panel1.Visible = false;
            // 
            // pnlSearchBarcode
            // 
            this.pnlSearchBarcode.BackColor = System.Drawing.Color.Gray;
            this.pnlSearchBarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearchBarcode.Controls.Add(this.pictureBox9);
            this.pnlSearchBarcode.Controls.Add(this.lblSearchBarcode);
            this.pnlSearchBarcode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlSearchBarcode.ForeColor = System.Drawing.Color.Black;
            this.pnlSearchBarcode.Location = new System.Drawing.Point(2, 132);
            this.pnlSearchBarcode.Name = "pnlSearchBarcode";
            this.pnlSearchBarcode.Size = new System.Drawing.Size(219, 64);
            this.pnlSearchBarcode.TabIndex = 30;
            this.pnlSearchBarcode.Click += new System.EventHandler(this.pnlSearchBarcode_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.BackgroundImage = global::SorterAPI.Properties.Resources.SearchBarcode;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(4, 13);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(44, 34);
            this.pictureBox9.TabIndex = 0;
            this.pictureBox9.TabStop = false;
            // 
            // lblSearchBarcode
            // 
            this.lblSearchBarcode.AutoSize = true;
            this.lblSearchBarcode.BackColor = System.Drawing.Color.Transparent;
            this.lblSearchBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchBarcode.ForeColor = System.Drawing.Color.White;
            this.lblSearchBarcode.Location = new System.Drawing.Point(53, 21);
            this.lblSearchBarcode.Name = "lblSearchBarcode";
            this.lblSearchBarcode.Size = new System.Drawing.Size(144, 20);
            this.lblSearchBarcode.TabIndex = 1;
            this.lblSearchBarcode.Text = "Search Barcode";
            this.lblSearchBarcode.Click += new System.EventHandler(this.lblSearchBarcode_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(2, 67);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(219, 64);
            this.panel2.TabIndex = 2;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SorterAPI.Properties.Resources.Report11;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(4, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(44, 34);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(53, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Reports";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pnlBatchOperation
            // 
            this.pnlBatchOperation.BackColor = System.Drawing.Color.Gray;
            this.pnlBatchOperation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBatchOperation.Controls.Add(this.picBatchOperation);
            this.pnlBatchOperation.Controls.Add(this.lblBarchOperation);
            this.pnlBatchOperation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlBatchOperation.ForeColor = System.Drawing.Color.Black;
            this.pnlBatchOperation.Location = new System.Drawing.Point(2, 2);
            this.pnlBatchOperation.Name = "pnlBatchOperation";
            this.pnlBatchOperation.Size = new System.Drawing.Size(219, 64);
            this.pnlBatchOperation.TabIndex = 3;
            this.pnlBatchOperation.Click += new System.EventHandler(this.pnlBatchOperation_Click);
            // 
            // picBatchOperation
            // 
            this.picBatchOperation.BackColor = System.Drawing.Color.Transparent;
            this.picBatchOperation.BackgroundImage = global::SorterAPI.Properties.Resources.batchOperation;
            this.picBatchOperation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBatchOperation.Location = new System.Drawing.Point(6, 14);
            this.picBatchOperation.Name = "picBatchOperation";
            this.picBatchOperation.Size = new System.Drawing.Size(43, 38);
            this.picBatchOperation.TabIndex = 0;
            this.picBatchOperation.TabStop = false;
            // 
            // lblBarchOperation
            // 
            this.lblBarchOperation.AutoSize = true;
            this.lblBarchOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBarchOperation.ForeColor = System.Drawing.Color.White;
            this.lblBarchOperation.Location = new System.Drawing.Point(53, 25);
            this.lblBarchOperation.Name = "lblBarchOperation";
            this.lblBarchOperation.Size = new System.Drawing.Size(146, 20);
            this.lblBarchOperation.TabIndex = 1;
            this.lblBarchOperation.Text = "Batch Operation";
            this.lblBarchOperation.Click += new System.EventHandler(this.lblBarchOperation_Click);
            // 
            // pnlExit
            // 
            this.pnlExit.BackColor = System.Drawing.Color.Gray;
            this.pnlExit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlExit.Controls.Add(this.pictureBox6);
            this.pnlExit.Controls.Add(this.lblExit);
            this.pnlExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlExit.ForeColor = System.Drawing.Color.Black;
            this.pnlExit.Location = new System.Drawing.Point(2, 262);
            this.pnlExit.Name = "pnlExit";
            this.pnlExit.Size = new System.Drawing.Size(219, 64);
            this.pnlExit.TabIndex = 6;
            this.pnlExit.Click += new System.EventHandler(this.pnlExit_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = global::SorterAPI.Properties.Resources.CloseMenu;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(4, 13);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(44, 34);
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.BackColor = System.Drawing.Color.Transparent;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(53, 21);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(41, 20);
            this.lblExit.TabIndex = 1;
            this.lblExit.Text = "Exit";
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // pnlUtility
            // 
            this.pnlUtility.BackColor = System.Drawing.Color.Gray;
            this.pnlUtility.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlUtility.Controls.Add(this.pictureBox8);
            this.pnlUtility.Controls.Add(this.lblUtility);
            this.pnlUtility.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlUtility.ForeColor = System.Drawing.Color.Black;
            this.pnlUtility.Location = new System.Drawing.Point(451, 258);
            this.pnlUtility.Name = "pnlUtility";
            this.pnlUtility.Size = new System.Drawing.Size(218, 64);
            this.pnlUtility.TabIndex = 29;
            this.pnlUtility.Visible = false;
            this.pnlUtility.Click += new System.EventHandler(this.pnlUtility_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pictureBox8.BackgroundImage = global::SorterAPI.Properties.Resources.utilities;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(4, 13);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(44, 34);
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // lblUtility
            // 
            this.lblUtility.AutoSize = true;
            this.lblUtility.BackColor = System.Drawing.Color.Transparent;
            this.lblUtility.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUtility.ForeColor = System.Drawing.Color.White;
            this.lblUtility.Location = new System.Drawing.Point(53, 21);
            this.lblUtility.Name = "lblUtility";
            this.lblUtility.Size = new System.Drawing.Size(74, 20);
            this.lblUtility.TabIndex = 1;
            this.lblUtility.Text = "Utilities";
            this.lblUtility.Click += new System.EventHandler(this.lblUtility_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gray;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pictureBox4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(599, 166);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(219, 64);
            this.panel3.TabIndex = 29;
            this.panel3.Visible = false;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::SorterAPI.Properties.Resources.LocationSorter;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(3, 14);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(44, 36);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(53, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Assign Bay";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // pictureBoxClose
            // 
            this.pictureBoxClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.pictureBoxClose.BackgroundImage = global::SorterAPI.Properties.Resources.CloseMenu;
            this.pictureBoxClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxClose.Location = new System.Drawing.Point(31, 13);
            this.pictureBoxClose.Name = "pictureBoxClose";
            this.pictureBoxClose.Size = new System.Drawing.Size(61, 50);
            this.pictureBoxClose.TabIndex = 20;
            this.pictureBoxClose.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBoxClose, "DWS");
            this.pictureBoxClose.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 766);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1386, 22);
            this.statusStrip1.TabIndex = 16;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Margin = new System.Windows.Forms.Padding(0, 3, 650, 2);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(296, 17);
            this.toolStripStatusLabel1.Text = "Developed By Armstrong Machine and Builders Pvt Ltd";
            // 
            // lblTime
            // 
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(0, 17);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox5.Location = new System.Drawing.Point(938, 3);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(82, 85);
            this.pictureBox5.TabIndex = 18;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlMenu.BackgroundImage = global::SorterAPI.Properties.Resources.menu_icon;
            this.pnlMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMenu.Location = new System.Drawing.Point(133, 32);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(61, 52);
            this.pnlMenu.TabIndex = 26;
            this.pnlMenu.Click += new System.EventHandler(this.pnlMenu_Click);
            // 
            // pnlAdminMenu
            // 
            this.pnlAdminMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlAdminMenu.BackgroundImage = global::SorterAPI.Properties.Resources.admin_menu_icon;
            this.pnlAdminMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAdminMenu.Location = new System.Drawing.Point(214, 32);
            this.pnlAdminMenu.Name = "pnlAdminMenu";
            this.pnlAdminMenu.Size = new System.Drawing.Size(61, 52);
            this.pnlAdminMenu.TabIndex = 27;
            this.pnlAdminMenu.Click += new System.EventHandler(this.pnlAdminMenu_Click);
            // 
            // pnlOUMap
            // 
            this.pnlOUMap.BackColor = System.Drawing.Color.Gray;
            this.pnlOUMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOUMap.Controls.Add(this.pictureBox2);
            this.pnlOUMap.Controls.Add(this.lblOUMapp);
            this.pnlOUMap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlOUMap.ForeColor = System.Drawing.Color.Black;
            this.pnlOUMap.Location = new System.Drawing.Point(2, 197);
            this.pnlOUMap.Name = "pnlOUMap";
            this.pnlOUMap.Size = new System.Drawing.Size(218, 64);
            this.pnlOUMap.TabIndex = 31;
            this.pnlOUMap.Click += new System.EventHandler(this.pnlOUMap_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::SorterAPI.Properties.Resources.LocationSorter;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(4, 13);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 34);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // lblOUMapp
            // 
            this.lblOUMapp.AutoSize = true;
            this.lblOUMapp.BackColor = System.Drawing.Color.Transparent;
            this.lblOUMapp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOUMapp.ForeColor = System.Drawing.Color.White;
            this.lblOUMapp.Location = new System.Drawing.Point(53, 21);
            this.lblOUMapp.Name = "lblOUMapp";
            this.lblOUMapp.Size = new System.Drawing.Size(112, 20);
            this.lblOUMapp.TabIndex = 1;
            this.lblOUMapp.Text = "OU Mapping";
            this.lblOUMapp.Click += new System.EventHandler(this.lblOUMapp_Click);
            // 
            // MDIParent1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SorterAPI.Properties.Resources.mainbg_new_1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1386, 788);
            this.Controls.Add(this.pnlAdminMenu);
            this.Controls.Add(this.pnlUtility);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pictureBoxClose);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Name = "MDIParent1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sorter Application";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIParent1_Load);
            this.panel1.ResumeLayout(false);
            this.pnlSearchBarcode.ResumeLayout(false);
            this.pnlSearchBarcode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlBatchOperation.ResumeLayout(false);
            this.pnlBatchOperation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBatchOperation)).EndInit();
            this.pnlExit.ResumeLayout(false);
            this.pnlExit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.pnlUtility.ResumeLayout(false);
            this.pnlUtility.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.pnlOUMap.ResumeLayout(false);
            this.pnlOUMap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlBatchOperation;
        private System.Windows.Forms.PictureBox picBatchOperation;
        private System.Windows.Forms.Label lblBarchOperation;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripStatusLabel lblTime;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBoxClose;
        private System.Windows.Forms.Panel pnlExit;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlAdminMenu;
        private System.Windows.Forms.Panel pnlUtility;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label lblUtility;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlSearchBarcode;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label lblSearchBarcode;
        private System.Windows.Forms.Panel pnlOUMap;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblOUMapp;




    }
}



