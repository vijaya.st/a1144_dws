﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Reflection;
using SorterAPI.Common;
using DynamicButton;

namespace SorterAPI
{
    public partial class MDIParent1 : Form
    { 
      //  string conn;
     //   bool PropixExt;
       // string str_directory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
      
        public MDIParent1()
        {
            InitializeComponent();
         //   timer1.Start();
            this.Height = 1000;
          //  this.SuspendLayout();

            //string screenWidth = Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = Convert.ToInt32(screenWidth);
            //int H = Convert.ToInt32(screenHeight);

            //this.ClientSize = new System.Drawing.Size(W, H);

           // this.pnlMenu.Location = new Point(Screen.PrimaryScreen.Bounds.X + 110, //should be (0,0)
                      //    Screen.PrimaryScreen.Bounds.Y + 40);
           // this.lblNewMenu.Location = new Point(Screen.PrimaryScreen.Bounds.X + 110, //should be (0,0)
                        //  Screen.PrimaryScreen.Bounds.Y + 40);
            this.pnlMenu.BackColor = System.Drawing.Color.Transparent;
          //  pnlMenu.BackgroundImage = Image.FromFile(str_directory + "\\images\\menu_icon.png");
          //  pnlAdminMenu.BackgroundImage = Image.FromFile(str_directory + "\\images\\admin_menu_icon.png");
            
            foreach (Control ctl in this.Controls)
            {
                if (ctl is MdiClient)
                {
                    ctl.MouseDown += new MouseEventHandler(ctl_MouseDown);
                    break;
                }
            }             
        }

        void MDIParent1_MouseDown(object sender, MouseEventArgs e)
        {
            MessageBox.Show(e.Location.ToString());
        }
        
        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }             

        
        void toolStripClick(object sender, EventArgs e)
        {
            ToolStripItem myitem = (ToolStripItem)sender; 
        }

        private void toolStripButton1_MouseHover(object sender, EventArgs e)
        {
           panel1.Visible = true;
        }
       
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }       

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            frmSorterAPIConfigurator fr = new frmSorterAPIConfigurator();
            fr.Show();
        }
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (panel1.Visible == true)
            {
                panel1.Visible = false;
            }
            else
            {
                panel1.Visible = true;
            } 
        }        

        private void picAPI_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator1"];

            if (fc == null)
            {
                frmSorterAPIConfigurator1 SrtrAPI = new frmSorterAPIConfigurator1();
                SrtrAPI.MdiParent = this; 
                SrtrAPI.Show(); 
            }            
        }

        void ctl_MouseDown(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("You clicked on the MdiClient area!");
            if (panel1.Visible == true)
            {
                panel1.Visible = false;
            }
        }
      
        private void timer1_Tick(object sender, EventArgs e)
        {
         //   DateTime dt = DateTime.Now;
         //   lblTime.TextAlign= ContentAlignment.MiddleRight;
         //   this.lblTime.Text = "                                                        " + dt.ToString();
           // lbl.Text = dt.ToString(); 
        }

        private void MDIParent1_Load(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmBatchOperation batchOpr = new frmBatchOperation();
            batchOpr.MdiParent = this;
            batchOpr.BringToFront();
            batchOpr.Show();
        }

        private void pnlBatchOperation_Click(object sender, EventArgs e)
        {
            //foreach (Form aForm in Application.OpenForms)
            //{
            //    if (aForm.Name.Equals("SomeFormName"))
            //    {
            //        MessageBox.Show("SomeFormName Already Open, closing and repopening next");

            //        aForm.Close();                   
            //        break;
            //    }
            //}
             if (Application.OpenForms.OfType<frmBatchOperation>().Count() == 0)
             {
                 frmBatchOperation batchOpr = new frmBatchOperation();
                 panel1.Visible = false;
                // frmBatchOperation batchOpr = new frmBatchOperation();
                 batchOpr.MdiParent = this;
                 batchOpr.BringToFront();
                 batchOpr.Show();
             }
        }

        private void lblBarchOperation_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<frmBatchOperation>().Count() == 0)
            {
                frmBatchOperation batchOpr = new frmBatchOperation();
                panel1.Visible = false;
                // frmBatchOperation batchOpr = new frmBatchOperation();
                batchOpr.MdiParent = this;
                batchOpr.BringToFront();
                batchOpr.Show();
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            LocationSorter LS = new LocationSorter();
            LS.MdiParent = this;
            LS.BringToFront();
            LS.Show();
           // LS.ShowDialog();
        }

        private void panel4_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            LocationSorter LS = new LocationSorter();
            LS.MdiParent = this;
            LS.BringToFront();
            LS.Show();

        }     

        private void label1_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmReport1 r = new frmReport1();
            r.MdiParent = this;
            r.BringToFront();
            r.Show();
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmReport1 r = new frmReport1();
            r.MdiParent = this;
            r.BringToFront();
            r.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmPlanDetails pd = new frmPlanDetails();
            pd.ShowDialog();
        }

        private void pnlPlan_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
             frmPlanDetails pd = new frmPlanDetails();
             pd.ShowDialog();
        }

        private void lblPlan_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmPlanDetails pd = new frmPlanDetails();
            pd.ShowDialog();
        }

        private void pnlExit_Click(object sender, EventArgs e)
        {
          panel1.Visible = false;            
          //PropixExt = API.PropixAPI.ExitApplication();
          this.Close();
          this.Dispose();
          Application.Exit();

        }

        private void lblExit_Click(object sender, EventArgs e)
        {
          panel1.Visible = false;           
         // PropixExt =  API.PropixAPI.ExitApplication();
          this.Close();
          this.Dispose();
          Application.Exit();
        }

        private void pnlUtilities_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmUtility ut = new frmUtility();
            ut.MdiParent = this;
            ut.BringToFront();
            ut.Show();

           // ut.ShowDialog();
        }

        private void lblUtilities_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmUtility ut = new frmUtility();
            ut.MdiParent = this;
            ut.BringToFront();
            ut.Show();
        }

        private void pnlMenu_Click(object sender, EventArgs e)
        {
            if (panel1.Visible == true)
            {
                panel1.Visible = false;
            }
            else
            {
                panel1.Visible = true;
            } 
        }

        private void pnlAdminMenu_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator1"];

            if (fc == null)
            {
                frmSorterAPIConfigurator1 SrtrAPI = new frmSorterAPIConfigurator1();
                SrtrAPI.MdiParent = this;
                SrtrAPI.Show();
            }    
        }

        private void label2_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            BayAssign1 ba = new BayAssign1();
            ba.MdiParent = this;
            ba.BringToFront();
            ba.Show();
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            BayAssign1 ba = new BayAssign1();
            ba.MdiParent = this;
            ba.BringToFront();
            ba.Show();
        }
       
        private void pnlUtility_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            Utility u = new Utility();
            u.MdiParent = this;
            u.BringToFront();
            u.Show();
        }

        private void lblUtility_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            Utility u = new Utility();
            u.MdiParent = this;
            u.BringToFront();
            u.Show();
        }

        private void pnlSearchBarcode_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmBarcodeSearch s = new frmBarcodeSearch();
            s.MdiParent = this;
            s.BringToFront();
            s.Show();
        }

        private void lblSearchBarcode_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmBarcodeSearch s = new frmBarcodeSearch();
            s.MdiParent = this;
            s.BringToFront();
            s.Show();
        }

        private void pnlOUMap_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmPTL ptlfrm = new frmPTL();
            ptlfrm.MdiParent = this;
            ptlfrm.BringToFront();
            ptlfrm.Show();
        }

        private void lblOUMapp_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmPTL ptlfrm = new frmPTL();
            ptlfrm.MdiParent = this;
            ptlfrm.BringToFront();
            ptlfrm.Show();
        }
    }
} 