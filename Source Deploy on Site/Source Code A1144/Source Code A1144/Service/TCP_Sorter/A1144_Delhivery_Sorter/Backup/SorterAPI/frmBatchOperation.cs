﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI_CommonFunction;
using System.IO.Ports;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using SorterAPI.Common;
using System.Diagnostics;
using System.Threading;
using System.Configuration;
using S7;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using S7.Types;


namespace SorterAPI
{
    public partial class frmBatchOperation : Form
    {
        ErrorLog oErrorLog = new ErrorLog();
        SerialPort VMSPort = new SerialPort();
        SerialPort MTPort = new SerialPort();
        SerialPort BarcodePort = new SerialPort();

        DataSet ds = new DataSet();
        DataTable dtgrid = new DataTable();
        DataTable dtScan = new DataTable();
        DataTable dtNotScan = new DataTable();
        DataTable dtExportCSV = new DataTable();
        DataTable dtlist = new DataTable();
        DataTable dtNA = new DataTable();
        DataTable dtbar = new DataTable();

        int H = 0;
        int L = 0;
        int W = 0;

        string con = null;
        private delegate void SetTextDeleg(SerialPort VMSPort, string text);
        static int i = 0;
        DataTable dtBarcode = new DataTable();
        string barcode = null;
        string PLCIP = "";
        // PLC ////
        System.ComponentModel.BackgroundWorker worker = null;
        System.Threading.Timer timer = null;
        S7.PLC plc = null;
        private Socket mSocket;
        public ErrorCode lastErrorCode = 0;
        public string lastErrorString;

        //////////

        //static int status = 0;
        //static int fix = 1;
        //TimeSpan t;
        //int stoptime = 0;

        // SerialPortManager _spManager = new SerialPortManager();
        //  string conGPO = @"Data Source=WEG35229SR002;Initial Catalog=DWS;Integrated Security=No;UID=DWSOP; Pwd=Gpo!@#$%;";

        string conGPO = ConfigurationManager.ConnectionStrings["DWSConnectionString"].ConnectionString;

     //   string str_directory = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            
      //  string conGPO = @"Data Source=WEG35229SR002;Initial Catalog=DWS;Integrated Security=No;UID=DWSOP; Pwd=Gpo!@#$%;";

       // string conGPO = ConfigurationManager.ConnectionStrings["DWSConnectionString"].ConnectionString;

        public frmBatchOperation()
        {
            InitializeComponent();
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X + 200, //should be (0,0)
                         Screen.PrimaryScreen.Bounds.Y + 100);
            this.TopMost = true;
            this.StartPosition = FormStartPosition.Manual;
            

            //string screenWidth = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width.ToString();
            //string screenHeight = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height.ToString();
            //int W = System.Convert.ToInt32(screenWidth);
            //int H = System.Convert.ToInt32(screenHeight);

            //this.Location = new Point(Screen.PrimaryScreen.Bounds.X + W / 5, Screen.PrimaryScreen.Bounds.Y + H / 5);
                      
            lblTotal.BackColor = System.Drawing.ColorTranslator.FromHtml("#e8e8e8");
            lblScanFail.BackColor = System.Drawing.ColorTranslator.FromHtml("#e2b8a6");
            lblScanSuccess.BackColor = System.Drawing.ColorTranslator.FromHtml("#b0e2a6");
            lblonemin.BackColor = System.Drawing.ColorTranslator.FromHtml("#ebecc4");
            lblonehr.BackColor = System.Drawing.ColorTranslator.FromHtml("#a6d5e2");
            lbloneday.BackColor = System.Drawing.ColorTranslator.FromHtml("#c4c4ec");
        }

        private void frmBatchOperation_Load(object sender, EventArgs e)
        {
            btnCameraE.ForeColor = Color.Gray;
            btnCameraD.Enabled = true;
            btnCameraD.Font = new Font(btnCameraD.Font.Name, btnCameraD.Font.Size, FontStyle.Bold);


            //string str3 = "";
            //string[] splstr = str3.Split('#');
            //// int u = splstr.Length;
            //str3 = splstr[splstr.Length - 1].ToString();
            ////string bar = str3.Trim().Replace("#", "");
            //str3 = str3.Replace("/r/n", "");

            DataTable dtport = new DataTable();
            dtport = clsAPI.getPort();

            // BarcodePort.PortName = "COM6"; // Barcode(Propix)
            BarcodePort.PortName = dtport.Rows[0]["PortName"].ToString();
            BarcodePort.BaudRate = 57600;
            BarcodePort.Parity = Parity.None;
            BarcodePort.StopBits = StopBits.One;

           // MTPort.PortName = "COM5";
            MTPort.PortName = dtport.Rows[0]["WPortName"].ToString();
            MTPort.BaudRate = 9600;
            MTPort.Parity = Parity.None;
            MTPort.StopBits = StopBits.One;

            if (!MTPort.IsOpen)
            {

                MTPort.DataReceived += BarcodePort_DataReceived;
                //  MTPort.DataReceived += port_DataReceived;
                MTPort.ErrorReceived += new SerialErrorReceivedEventHandler(MTPort_ErrorReceived);
                MTPort.Open();
                MTPort.Write("SI");
            }

            if (!BarcodePort.IsOpen)
            {

                BarcodePort.DataReceived += BarcodePort_DataReceived;
                //  MTPort.DataReceived += port_DataReceived;
                BarcodePort.ErrorReceived += new SerialErrorReceivedEventHandler(MTPort_ErrorReceived);
                BarcodePort.Open();
            }

         //   dtPLCIP = clsAPI.getPLCIP();
            PLCIP = dtport.Rows[0]["PLCIP"].ToString();

            DataTable dtDWSPLCSetting = new DataTable();
            dtDWSPLCSetting = clsAPI.getDWSPLCSetting();

            if (dtDWSPLCSetting.Rows.Count > 0)
            {
                plc = new PLC(CPU_Type.S7300, PLCIP, 0, 2);
                ErrorCode errCode = plc.Open();

                if (errCode == ErrorCode.NoError)
                {
                    plc.Write("db203.dbd0", dtDWSPLCSetting.Rows[0]["DistTravebyConvencoderpulse"].ToString());
                    plc.Write("db203.dbd4", dtDWSPLCSetting.Rows[0]["PropixSpeed"].ToString());
                    plc.Write("db203.dbd12", dtDWSPLCSetting.Rows[0]["RequiredDistance"].ToString());
                    plc.Write("db203.dbw16", dtDWSPLCSetting.Rows[0]["IdDataLoadingZone"].ToString());
                    plc.Write("db203.dbw18", dtDWSPLCSetting.Rows[0]["DataTrakingZone"].ToString());

                    plc.Close();
                }
            }

           // tmrBit.Enabled = true;
        }

        //void VMSPort_PinChanged(object sender, SerialPinChangedEventArgs e)
        //{
        //    string info = string.Empty;
        //    switch (e.EventType)
        //    {
        //        case SerialPinChange.CDChanged:
        //            info = "Pin 1";
        //            break;
        //        case SerialPinChange.CtsChanged:
        //            info = "Pin 8";
        //            break;
        //        case SerialPinChange.DsrChanged:
        //            info = "Pin 6";
        //            break;
        //        case SerialPinChange.Ring:
        //            info = "Pin 9";
        //            break;
        //        default:
        //            break;
        //    }
        //}

        void MTPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            //oErrorLog.WriteErrorLog(ex.ToString());
            lblError.Text = "Connection Failed !!";
            tmrBit.Enabled = false;
        }      

        void BarcodePort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            //oErrorLog.WriteErrorLog(ex.ToString());
            lblError.Text = "Connection Failed !!";
            tmrBit.Enabled = false;
        }    

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] >= '0' && str[i] <= '9')
                    || (str[i] >= 'A' && str[i] <= 'z'
                        || (str[i] == '.' || str[i] == '_')))
                {
                    sb.Append(str[i]);
                }
            }
            return sb.ToString();
        }

        private void BarcodePort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            try
            {
                string Barcode1 = null;
             //   string Barcode2 = null;
              //  string Barcode3 = null;  

                string str3 = string.Empty;

                //str3 = BarcodePort.ReadExisting();   //Barcode Data
                str3 = BarcodePort.ReadLine();   //Barcode Data   #111,000,000,YA109442241IN

                string WeightData = string.Empty;
                WeightData = MTPort.ReadExisting();        //Weight Data  "      0.16 kg  Over\r\n" 
               // WeightData = "";
                string splstr = str3.Replace("#", "").Trim();

                string[] newBarcode = splstr.Split(',');
                //  oErrorLog.WriteErrorLog(str3.ToString());
                string splweight = string.Empty;

                if (WeightData != "")
                {
                    splweight = WeightData.Substring(5, 5);
                }
                else
                {
                    splweight = "0.000";
                }

               // int arraySize = newBarcode.Length;
               // if (arraySize == 4)
              //  {
                    H = Convert.ToInt32(Convert.ToDecimal(newBarcode[0])); //newstr.Substring(0, 3);//.Remove(0, 4);
                    W = Convert.ToInt32(Convert.ToDecimal(newBarcode[1])); //newstr.Substring(3, 3);//.Remove(3, 4);
                    L = Convert.ToInt32(Convert.ToDecimal(newBarcode[2])); // newstr.Substring(6, 3);//.Remove(7, 4);
                    Barcode1 = newBarcode[3].ToString();
          
                int V;


                ///////// S7 ///////////////
                ////#region S7
                //object L = new object();
                //object W = new object();
                //// object H = new object();
                ////   int V;

                //plc = new PLC(CPU_Type.S7300, PLCIP, 0, 2);  //"192.168.0.1"
                //ErrorCode errCode = plc.Open();
                //if (errCode == ErrorCode.NoError)
                //{
                //    //if (Barcode1 == "NA")
                //    //{ plc.Write("DB70.DBB10", 49); }

                //    L = plc.Read("DB200.DBD92");
                //    W = plc.Read("DB200.DBD96");

                //    //if (Barcode1 == "NA")
                //    //{ plc.Write("DB70.DBB10", 1); }

                //    //  H = newHeight.ToString();  /// plc.Read("DB200.DBD60");
                //    plc.Close();
                //}

                //////Debug.Print("Barcode " + newBarstr);
                ////#endregion
                ///////// S7 ///////////////


                if (Convert.ToInt32(L) == 0 && Convert.ToInt32(W) == 0 && Convert.ToInt32(H) == 0)
                {
                    L = 100;
                    W = 100;
                    H = 25;
                    V = 250;
                }
                else
                {
                    int le = 0;
                    int wi = 0;
                    if (L.ToString().Length > 3)
                    {
                        le = Convert.ToInt32(L.ToString().Substring(0, 3));
                        L = le;
                    }
                    if (W.ToString().Length > 3)
                    {
                        wi = Convert.ToInt32(W.ToString().Substring(0, 3));
                        W = wi;
                    }
                }
                
                if (H < 10)
                {
                    H = 10;
                }

                V = Convert.ToInt32(L) * Convert.ToInt32(H) * Convert.ToInt32(W) / 6000;
                //Thread.Sleep(50);
                DoUpdate(splweight, L, H, W, V, Barcode1);

                i = 0;
                tbData.Text = "";
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
                return;
            }
            finally
            {
               // if (BarcodePort.IsOpen)
                //{
                    BarcodePort.DiscardInBuffer();
                    BarcodePort.DiscardOutBuffer();
                //}
            }
        }

        public void DoUpdate(string s, object leng, object hei, object wid, object volu, string Bar1)//(object s, EventArgs e)
        {
        //    string bar = "N";
          //  dtBarcode = clsAPI.GetBarcode();    //  think this function call after insert all data //
        //    if (dtBarcode.Rows.Count > 0)
       //     {
         //       bar = dtBarcode.Rows[0]["Barcode"].ToString();
        //    }

            string strNew = string.Empty;
            if (s == "0.000")
            {
                strNew = s;
            }
            else
            {
                strNew = RemoveSpecialCharacters(s);
            }
            //Debug.Print("Barcode ## " + Pbar + " " + strNew);
            //string strFinal = strNew.Substring(3, 4);
            string strFinal = null;
            if (strNew != "") // (To be checked later)
            {
                if (strNew.Length <= 6)
                {
                    try
                    {
                        strFinal = strNew.Replace(".", "");
                        if (Convert.ToInt32(strFinal) < 010)
                        {
                            strFinal = "10"; //To be checked later
                        }

                        strFinal += "0"; //To be checked later 
                        //Thread.Sleep(100);
                        clsAPI obj = new clsAPI();
                        // obj.barcode = barcode;
                        obj.L = leng.ToString();
                        obj.H = hei.ToString();
                        obj.W = wid.ToString();
                        obj.V = volu.ToString();                    
                        obj.Weight = Convert.ToDouble(strFinal);
                        // obj.barcode = Pbar.ToString();
                        obj.barcode = Bar1.ToString();
                        //if (Bar2 == "" || Bar2 == null)
                        //{
                        //    obj.barcode2 = "1";
                        //}
                        //else
                        //{
                        //    obj.barcode2 = Bar2.ToString();
                        //}

                        //if (Bar3 == "" || Bar3 == null)
                        //{
                        //    obj.barcode3 = "1";
                        //}
                        //else
                        //{
                        //    obj.barcode3 = Bar3.ToString();
                        //}

                        dtbar = clsAPI.UpdateBarcodeNWeight(obj);

                        plc = new PLC(CPU_Type.S7300, PLCIP, 0, 2);  //"192.168.0.1"
                        ErrorCode errCode = plc.Open();
                        if (errCode == ErrorCode.NoError)
                        {
                            if (Bar1 == "NA")
                            { plc.Write("DB70.DBB10", 49); }

                          //  L = plc.Read("DB200.DBD92");
                                                      
                            plc.Close();
                        }


                      //  string Pbar = string.Empty;
                     //   string SorterNo, Weight;

                        //if (dtbar.Rows.Count > 0)
                        //{
                        //    if (dtbar.Rows[0]["BayNo"].ToString() == "")
                        //    {
                        //        // Pbar = Bar1;
                        //        SorterNo = "16";
                        //    }
                        //    else
                        //    {
                        //        // Pbar = dtbar.Rows[0]["Barcode"].ToString();
                        //        SorterNo = dtbar.Rows[0]["BayNo"].ToString();
                        //    }

                        //    Weight = dtbar.Rows[0]["weight"].ToString();

                        //    plc = new PLC(CPU_Type.S7300, PLCIP, 0, 2);  //"192.168.0.20"
                        //    ErrorCode errCode = plc.Open();

                        //    if (errCode == ErrorCode.NoError)
                        //    {
                        //        //plc.Write("DB601.DBW30", "A");
                        //        plc.Write("DB601.DBW30", SorterNo.ToString());
                        //        plc.Close();
                        //        ///Reset sorter
                        //        plc.Write("DB601.DBW30", "1");
                        //        plc.Close();
                        //        if (Convert.ToDouble(Weight) > 15)
                        //        {
                        //            plc.Write("DB2.DBB34", "A");
                        //            plc.Close();
                        //        }
                        //        else
                        //        {
                        //            plc.Write("DB2.DBB34", "1");
                        //            plc.Close();
                        //        }
                        //    }
                        //}
                        /////// S7 ///////////////
                    }

                    catch (Exception f)
                    {
                        oErrorLog.WriteErrorLog(f.ToString());
                    }
                    finally
                    {
                        // VMSPort.DiscardInBuffer();
                        //  VMSPort.DiscardOutBuffer();
                        BarcodePort.DiscardInBuffer();
                        BarcodePort.DiscardOutBuffer();
                
                        MTPort.DiscardInBuffer();
                        MTPort.DiscardOutBuffer();
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void BindGrid()
        {
            try
            {
                ds = clsAPI.getBatchOperation();

                dtgrid = ds.Tables[0];
                dtScan = ds.Tables[1];
                dtNotScan = ds.Tables[2];
                dtExportCSV = ds.Tables[3];
                dtlist = ds.Tables[4];
                dtNA = ds.Tables[5];

                if (dtgrid.Rows.Count > 0)
                {
                    dgvBatchOperation.RowHeadersVisible = false;

                    dgvBatchOperation.RowsDefaultCellStyle.BackColor = Color.White;
                   // dgvBatchOperation.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(115, 205, 212);
                    dgvBatchOperation.AlternatingRowsDefaultCellStyle.BackColor = Color.Gray;

                    Font f = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
                    dgvBatchOperation.DefaultCellStyle.Font = f;
                    //dgvReport.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                    dgvBatchOperation.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Bold);

                 //   dgvBatchOperation.AutoResizeColumns();
                  //  dgvBatchOperation.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                    dgvBatchOperation.DataSource = dtgrid;//xmlDataSet.Tables[0];
                    //id,BarCode,Length,Height,Width,Volume,Weight,RecordDateTime,Status
                    dgvBatchOperation.Columns[0].Width = 130;
                    dgvBatchOperation.Columns[1].Width = 100;
                    dgvBatchOperation.Columns[2].Width = 100;
                    dgvBatchOperation.Columns[3].Width = 100;
                    dgvBatchOperation.Columns[4].Width = 100;
                    dgvBatchOperation.Columns[5].Width = 150;
                    dgvBatchOperation.Columns[6].Width = 150;
                    dgvBatchOperation.Columns[7].Width = 170;
                    dgvBatchOperation.Columns[8].Width = 160;
                    dgvBatchOperation.Sort(dgvBatchOperation.Columns["Time Stamp"], ListSortDirection.Descending); // for Aacending Descending
                }
                string ScanSuccess = string.Empty;
                string ScanFail = string.Empty;

                if (dtScan.Rows.Count > 0)
                {
                    ScanSuccess = dtScan.Rows[0]["Scan"].ToString();
                    lblScanSuccess.Text = ScanSuccess;
                }
                if (dtNotScan.Rows.Count > 0)
                {
                    ScanFail = dtNotScan.Rows[0]["NotScan"].ToString();
                    lblScanFail.Text = ScanFail;
                }

                int Total = Convert.ToInt32(ScanSuccess) + Convert.ToInt32(ScanFail);
                lblTotal.Text = Total.ToString();

                if (dtlist.Rows.Count > 0)
                {
                    lblonemin.Text = dtlist.Rows[0]["M"].ToString();
                    lblonehr.Text = dtlist.Rows[0]["H"].ToString();
                    lbloneday.Text = dtlist.Rows[0]["D"].ToString();
                }
                if (dtNA.Rows.Count > 0)
                {
                    //lblNA.Text = dtNA.Rows[0]["NA"].ToString();
                    //lblN_A.Text = dtNA.Rows[0]["N_A"].ToString();
                }

            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            } 
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
           // String Todaysdate = DateTime.Now.ToString("dd-MMM-yyyy");
           // String Tdate = DateTime.Now.ToString("dd-MM-yyyy");
            string TodaydateTime = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
        }

        private void frmBatchOperation_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (BarcodePort.IsOpen)
            {
                BarcodePort.Close();
            } 
        } 

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string strFilePath = saveFileDialog1.FileName;
            CreateCSVFile(dtExportCSV, strFilePath + ".csv");
        }

        public void CreateCSVFile(DataTable dtDataTablesList, string strFilePath)
        {
            // Create the CSV file to which grid data will be exported.

            StreamWriter sw = new StreamWriter(strFilePath, false);

            //First we will write the headers.

            int iColCount = dtDataTablesList.Columns.Count;

            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dtDataTablesList.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);

            // Now write all the rows.

            foreach (DataRow dr in dtDataTablesList.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();

            MessageBox.Show("File Exported!!..");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {    
                //if (VMSPort.IsOpen)
                //{
                //    VMSPort.Close();
                //}
                if (MTPort.IsOpen)
                {
                    MTPort.Close();
                }

                if (BarcodePort.IsOpen)
                {
                    BarcodePort.Close();
                }
                this.Close();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void lblExportCSV_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void lblEMS_Click(object sender, EventArgs e)
        {

        }

        private void pnlClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
                //if (VMSPort.IsOpen)
                //{
                //    VMSPort.Close();
                //}
                if (MTPort.IsOpen)
                {
                    MTPort.Close();
                }
                if (BarcodePort.IsOpen)
                {
                    BarcodePort.Close();
                }
            }
            catch (Exception ex)
            {
                return;
            }            
        }

        private void btnCameraE_Click(object sender, EventArgs e)
        {
            Process pro = new Process();
            pro.StartInfo.FileName = @"D:\PARCEL_SORTING_13_12_2016\PACKi.exe";    // Path of Exe File.
            pro.StartInfo.Arguments = "True";   // False For Scanner
            pro.Start();
            pro.WaitForExit();
                btnCameraE.Enabled = false;
              //  btnCameraE.BackColor = Color.Gray;
                btnCameraE.ForeColor = Color.Gray;
                btnCameraD.ForeColor = Color.White;
                btnCameraD.Enabled = true;
                btnCameraD.Font = new Font(btnCameraD.Font.Name, btnCameraD.Font.Size, FontStyle.Bold);
                btnCameraE.Font = new Font(btnCameraE.Font.Name, btnCameraE.Font.Size, FontStyle.Regular); 
        }

        private void btnCameraD_Click(object sender, EventArgs e)
        {
            Process pro = new Process();
            pro.StartInfo.FileName = @"D:\PARCEL_SORTING_13_12_2016\PACKi.exe";    // Path of Exe File.
            pro.StartInfo.Arguments = "False";   // False For Scanner
            pro.Start();
            pro.WaitForExit();
            btnCameraD.Enabled = false;
         //   btnCameraD.BackColor = Color.Gray;
            btnCameraD.ForeColor = Color.Gray;
            btnCameraE.ForeColor = Color.White;
            btnCameraE.Enabled = true;
            btnCameraE.Font = new Font(btnCameraE.Font.Name, btnCameraD.Font.Size, FontStyle.Bold);
            btnCameraD.Font = new Font(btnCameraD.Font.Name, btnCameraD.Font.Size, FontStyle.Regular);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
           // plc.Write("DB70.DBB10", 0);
        }      
    }
}
