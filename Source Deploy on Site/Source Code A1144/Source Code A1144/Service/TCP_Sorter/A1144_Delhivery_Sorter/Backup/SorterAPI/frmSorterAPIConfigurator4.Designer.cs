﻿namespace SorterAPI
{
    partial class frmSorterAPIConfigurator4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnLogoUpload = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtContactNumber = new System.Windows.Forms.TextBox();
            this.txtContactEmail = new System.Windows.Forms.TextBox();
            this.txtContactPerson = new System.Windows.Forms.TextBox();
            this.txtAddr = new System.Windows.Forms.TextBox();
            this.txtComapanyName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.LnkManageUser = new System.Windows.Forms.LinkLabel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOurContactNumber = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtOurContactEmail = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOurContactPerson = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSaveGeneralSetting = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblBagAssignment = new System.Windows.Forms.Label();
            this.lblLinkDB = new System.Windows.Forms.Label();
            this.lblPTLSys = new System.Windows.Forms.Label();
            this.lblPLCData = new System.Windows.Forms.Label();
            this.lblCustInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "                             ";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pnlClose);
            this.panel2.Location = new System.Drawing.Point(1, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1199, 74);
            this.panel2.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label3.Location = new System.Drawing.Point(538, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(264, 29);
            this.label3.TabIndex = 3;
            this.label3.Text = "Customer Information";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::SorterAPI.Properties.Resources.customer_info_icon;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(423, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(82, 66);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pnlClose
            // 
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.Location = new System.Drawing.Point(1165, 5);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(31, 28);
            this.pnlClose.TabIndex = 0;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Location = new System.Drawing.Point(0, 130);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1200, 537);
            this.panel3.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.btnLogoUpload);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.txtContactNumber);
            this.panel4.Controls.Add(this.txtContactEmail);
            this.panel4.Controls.Add(this.txtContactPerson);
            this.panel4.Controls.Add(this.txtAddr);
            this.panel4.Controls.Add(this.txtComapanyName);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Location = new System.Drawing.Point(11, 10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(578, 514);
            this.panel4.TabIndex = 0;
            // 
            // btnLogoUpload
            // 
            this.btnLogoUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnLogoUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogoUpload.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogoUpload.ForeColor = System.Drawing.Color.White;
            this.btnLogoUpload.Location = new System.Drawing.Point(229, 447);
            this.btnLogoUpload.Name = "btnLogoUpload";
            this.btnLogoUpload.Size = new System.Drawing.Size(126, 29);
            this.btnLogoUpload.TabIndex = 18;
            this.btnLogoUpload.Text = "Upload";
            this.btnLogoUpload.UseVisualStyleBackColor = false;
            this.btnLogoUpload.Click += new System.EventHandler(this.btnLogoUpload_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(229, 363);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(309, 75);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label9.Location = new System.Drawing.Point(189, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(212, 22);
            this.label9.TabIndex = 16;
            this.label9.Text = "Customer Information";
            // 
            // txtContactNumber
            // 
            this.txtContactNumber.Location = new System.Drawing.Point(229, 311);
            this.txtContactNumber.MaxLength = 11;
            this.txtContactNumber.Multiline = true;
            this.txtContactNumber.Name = "txtContactNumber";
            this.txtContactNumber.Size = new System.Drawing.Size(309, 29);
            this.txtContactNumber.TabIndex = 15;
            // 
            // txtContactEmail
            // 
            this.txtContactEmail.Location = new System.Drawing.Point(229, 259);
            this.txtContactEmail.MaxLength = 50;
            this.txtContactEmail.Multiline = true;
            this.txtContactEmail.Name = "txtContactEmail";
            this.txtContactEmail.Size = new System.Drawing.Size(309, 29);
            this.txtContactEmail.TabIndex = 14;
            // 
            // txtContactPerson
            // 
            this.txtContactPerson.Location = new System.Drawing.Point(229, 207);
            this.txtContactPerson.MaxLength = 50;
            this.txtContactPerson.Multiline = true;
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.Size = new System.Drawing.Size(309, 29);
            this.txtContactPerson.TabIndex = 13;
            // 
            // txtAddr
            // 
            this.txtAddr.Location = new System.Drawing.Point(229, 118);
            this.txtAddr.Multiline = true;
            this.txtAddr.Name = "txtAddr";
            this.txtAddr.Size = new System.Drawing.Size(309, 57);
            this.txtAddr.TabIndex = 12;
            // 
            // txtComapanyName
            // 
            this.txtComapanyName.Location = new System.Drawing.Point(229, 66);
            this.txtComapanyName.Multiline = true;
            this.txtComapanyName.Name = "txtComapanyName";
            this.txtComapanyName.Size = new System.Drawing.Size(309, 29);
            this.txtComapanyName.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 363);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 19);
            this.label8.TabIndex = 10;
            this.label8.Text = "Company Logo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 258);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 19);
            this.label7.TabIndex = 9;
            this.label7.Text = "Contact Email : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 19);
            this.label6.TabIndex = 8;
            this.label6.Text = "Contact Person :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 310);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 19);
            this.label5.TabIndex = 7;
            this.label5.Text = "Contact Number :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 19);
            this.label4.TabIndex = 6;
            this.label4.Text = "Address:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(22, 65);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(141, 19);
            this.label26.TabIndex = 5;
            this.label26.Text = "Company Name :";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.LnkManageUser);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.txtOurContactNumber);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.txtOurContactEmail);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.txtOurContactPerson);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Location = new System.Drawing.Point(610, 10);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(578, 514);
            this.panel5.TabIndex = 1;
            // 
            // LnkManageUser
            // 
            this.LnkManageUser.AutoSize = true;
            this.LnkManageUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LnkManageUser.Location = new System.Drawing.Point(247, 242);
            this.LnkManageUser.Name = "LnkManageUser";
            this.LnkManageUser.Size = new System.Drawing.Size(109, 16);
            this.LnkManageUser.TabIndex = 19;
            this.LnkManageUser.TabStop = true;
            this.LnkManageUser.Text = "Manage Users";
            this.LnkManageUser.Click += new System.EventHandler(this.LnkManageUser_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label13.Location = new System.Drawing.Point(181, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(262, 22);
            this.label13.TabIndex = 18;
            this.label13.Text = "Our Contact Person Details";
            // 
            // txtOurContactNumber
            // 
            this.txtOurContactNumber.Location = new System.Drawing.Point(247, 185);
            this.txtOurContactNumber.MaxLength = 11;
            this.txtOurContactNumber.Multiline = true;
            this.txtOurContactNumber.Name = "txtOurContactNumber";
            this.txtOurContactNumber.Size = new System.Drawing.Size(309, 29);
            this.txtOurContactNumber.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(23, 184);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 19);
            this.label12.TabIndex = 16;
            this.label12.Text = "Contact Phone :";
            // 
            // txtOurContactEmail
            // 
            this.txtOurContactEmail.Location = new System.Drawing.Point(247, 126);
            this.txtOurContactEmail.MaxLength = 50;
            this.txtOurContactEmail.Multiline = true;
            this.txtOurContactEmail.Name = "txtOurContactEmail";
            this.txtOurContactEmail.Size = new System.Drawing.Size(309, 29);
            this.txtOurContactEmail.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 19);
            this.label11.TabIndex = 14;
            this.label11.Text = "Contact Email :";
            // 
            // txtOurContactPerson
            // 
            this.txtOurContactPerson.Location = new System.Drawing.Point(247, 73);
            this.txtOurContactPerson.Multiline = true;
            this.txtOurContactPerson.Name = "txtOurContactPerson";
            this.txtOurContactPerson.Size = new System.Drawing.Size(309, 29);
            this.txtOurContactPerson.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(23, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(218, 19);
            this.label10.TabIndex = 12;
            this.label10.Text = "Our Contact Person Name :";
            // 
            // btnSaveGeneralSetting
            // 
            this.btnSaveGeneralSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnSaveGeneralSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveGeneralSetting.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveGeneralSetting.ForeColor = System.Drawing.Color.White;
            this.btnSaveGeneralSetting.Location = new System.Drawing.Point(397, 683);
            this.btnSaveGeneralSetting.Name = "btnSaveGeneralSetting";
            this.btnSaveGeneralSetting.Size = new System.Drawing.Size(151, 44);
            this.btnSaveGeneralSetting.TabIndex = 10;
            this.btnSaveGeneralSetting.Text = "Update";
            this.btnSaveGeneralSetting.UseVisualStyleBackColor = false;
            this.btnSaveGeneralSetting.Click += new System.EventHandler(this.btnSaveGeneralSetting_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(649, 683);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 44);
            this.button1.TabIndex = 11;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.Customer_Info1;
            this.panel1.Controls.Add(this.lblBagAssignment);
            this.panel1.Controls.Add(this.lblLinkDB);
            this.panel1.Controls.Add(this.lblPTLSys);
            this.panel1.Controls.Add(this.lblPLCData);
            this.panel1.Controls.Add(this.lblCustInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1200, 54);
            this.panel1.TabIndex = 4;
            // 
            // lblBagAssignment
            // 
            this.lblBagAssignment.AutoSize = true;
            this.lblBagAssignment.BackColor = System.Drawing.Color.Transparent;
            this.lblBagAssignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBagAssignment.Location = new System.Drawing.Point(945, 13);
            this.lblBagAssignment.Name = "lblBagAssignment";
            this.lblBagAssignment.Size = new System.Drawing.Size(169, 29);
            this.lblBagAssignment.TabIndex = 5;
            this.lblBagAssignment.Text = "                          ";
            this.lblBagAssignment.Click += new System.EventHandler(this.lblBagAssignment_Click);
            // 
            // lblLinkDB
            // 
            this.lblLinkDB.AutoSize = true;
            this.lblLinkDB.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkDB.Location = new System.Drawing.Point(786, 13);
            this.lblLinkDB.Name = "lblLinkDB";
            this.lblLinkDB.Size = new System.Drawing.Size(127, 29);
            this.lblLinkDB.TabIndex = 4;
            this.lblLinkDB.Text = "                   ";
            this.lblLinkDB.Click += new System.EventHandler(this.lblLinkDB_Click);
            // 
            // lblPTLSys
            // 
            this.lblPTLSys.AutoSize = true;
            this.lblPTLSys.BackColor = System.Drawing.Color.Transparent;
            this.lblPTLSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPTLSys.Location = new System.Drawing.Point(611, 13);
            this.lblPTLSys.Name = "lblPTLSys";
            this.lblPTLSys.Size = new System.Drawing.Size(127, 29);
            this.lblPTLSys.TabIndex = 3;
            this.lblPTLSys.Text = "                   ";
            this.lblPTLSys.Click += new System.EventHandler(this.lblPTLSys_Click);
            // 
            // lblPLCData
            // 
            this.lblPLCData.AutoSize = true;
            this.lblPLCData.BackColor = System.Drawing.Color.Transparent;
            this.lblPLCData.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLCData.Location = new System.Drawing.Point(419, 13);
            this.lblPLCData.Name = "lblPLCData";
            this.lblPLCData.Size = new System.Drawing.Size(157, 29);
            this.lblPLCData.TabIndex = 2;
            this.lblPLCData.Text = "                        ";
            this.lblPLCData.Click += new System.EventHandler(this.lblPLCData_Click);
            // 
            // lblCustInfo
            // 
            this.lblCustInfo.AutoSize = true;
            this.lblCustInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCustInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustInfo.Location = new System.Drawing.Point(241, 14);
            this.lblCustInfo.Name = "lblCustInfo";
            this.lblCustInfo.Size = new System.Drawing.Size(145, 29);
            this.lblCustInfo.TabIndex = 1;
            this.lblCustInfo.Text = "                      ";
            this.lblCustInfo.Click += new System.EventHandler(this.lblCustInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "                             ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmSorterAPIConfigurator4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScrollMargin = new System.Drawing.Size(0, 100);
            this.ClientSize = new System.Drawing.Size(1202, 742);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSaveGeneralSetting);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterAPIConfigurator4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Sorter Configurator";
            this.Load += new System.EventHandler(this.frmSorterAPIConfigurator4_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblBagAssignment;
        private System.Windows.Forms.Label lblLinkDB;
        private System.Windows.Forms.Label lblPTLSys;
        private System.Windows.Forms.Label lblPLCData;
        private System.Windows.Forms.Label lblCustInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnSaveGeneralSetting;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtContactNumber;
        private System.Windows.Forms.TextBox txtContactEmail;
        private System.Windows.Forms.TextBox txtContactPerson;
        private System.Windows.Forms.TextBox txtAddr;
        private System.Windows.Forms.TextBox txtComapanyName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnLogoUpload;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtOurContactNumber;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtOurContactEmail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtOurContactPerson;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.LinkLabel LnkManageUser;
    }
}