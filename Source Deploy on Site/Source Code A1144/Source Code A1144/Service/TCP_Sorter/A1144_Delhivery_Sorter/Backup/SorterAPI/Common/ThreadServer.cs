﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
​

namespace SorterAPI.Common
{
    public delegate void ThreadHandler(object data, string ThreadName);
    class ThreadServer
    {
        public event ThreadHandler RaiseMe;

        object paramData = null;
        Thread t = null;
        public ThreadServer(object data, string name)
        {
            paramData = data;
            t = new Thread(new ParameterizedThreadStart(p => { if (RaiseMe != null) { RaiseMe(p, name); } }));
        }

        public void Start()
        {
            t.Start(paramData);
        }

        public void Stop()
        {
            t.Abort();
        }

        public void RecDataScanner()
        {
            SerialPort myScanner = new SerialPort();
            myScanner.PortName = "COM4";
            myScanner.BaudRate = 9600;
            myScanner.Open();
            myScanner.Close();
        }
        public void RecDataVMS()
        {
            SerialPort myVMS = new SerialPort();
            myVMS.PortName = "COM7";
            myVMS.BaudRate = 9600;
            myVMS.Open();
            myVMS.Close();
        }
        public void RecDataWeighing()
        {
            SerialPort myWeighing = new SerialPort();
            try
            {
                if ((myWeighing.IsOpen) == false)                                                                   //condition
                {
                    myWeighing.PortName = "COM3";
                    myWeighing.BaudRate = 9600;
                    myWeighing.Open();
                    myWeighing.Write("Hello World");

                    while (true)
                    {
                        if (myWeighing.BytesToRead > 0)
                        {
                            Byte b = (Byte)myWeighing.ReadByte();
                            Char c = (Char)myWeighing.ReadChar();
                            string Line = myWeighing.ReadLine();
                            string all = myWeighing.ReadExisting();

                            Debug.Print("Byte :" + b);
                            Debug.Print("\n");
                            Debug.Print("Char :" + c);
                            Debug.Print("\n");
                            Debug.Print("Line :" + Line);
                            Debug.Print("\n");
                            Debug.Print("All :" + all);
                            Debug.Print("\n");

                        }
                    }
                    myWeighing.Close();
                }
                
            }
            catch(Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }
    }
}
