﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using SorterAPI.Common;

namespace SorterAPI
{
    public partial class frmSelDirection : Form
    {
        ErrorLog oErrorLog = new ErrorLog();
        public int ArrowPosition1 { get; set; }
        public frmSelDirection()
        {
            InitializeComponent();
        }

        private void frmSelDirection_Load(object sender, EventArgs e)
        {
            //lblRotated3.Visible = false;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdup.Checked == true)
                {
                    if (txtDistance.Text.Length == 0)
                    {
                        MessageBox.Show("Please enter value in distance field.");
                        txtDistance.Focus();
                    }
                    else
                    {

                        // frmAddNewLocation f = new frmAddNewLocation();
                        // this.Close();
                        FormCollection fc = Application.OpenForms;
                        foreach (Form frm in fc)
                        {
                            if (frm.Text == "ProceedtoSorter")
                            {
                                frm.Hide();
                            }
                        }

                        //frmSorterAPIConfigurator f = new frmSorterAPIConfigurator();                 ;
                        //f.PosDistance = Convert.ToInt32(txtDistance.Text.Trim());
                        //f.ArrowDir = 1;
                        //f.ArrowPosition = ArrowPosition1;

                        ProceedtoSorter ps = new ProceedtoSorter();
                        ps.PosDistance = Convert.ToInt32(txtDistance.Text.Trim());
                        ps.ArrowDir = 1;
                        ps.ArrowPosition = ArrowPosition1;
                        ps.Show();

                        //Form frm = Application.OpenForms["frmSorterAPIConfigurator"];
                        //if (frm != null)
                        //{
                        //     frm.Hide();                      
                        //}

                        // f.Refresh();
                        this.Close();
                    }
                }
                else if (rddown.Checked == true)
                {
                    if (txtDistance.Text.Length == 0)
                    {
                        MessageBox.Show("Please enter value in distance field.");
                        txtDistance.Focus();
                    }
                    else
                    {

                        //  frmAddNewLocation f = new frmAddNewLocation();
                        // this.Close();
                        FormCollection fc = Application.OpenForms;
                        foreach (Form frm in fc)
                        {
                            if (frm.Text == "ProceedtoSorter")
                            {
                                frm.Hide();
                            }
                        }

                        ProceedtoSorter ps = new ProceedtoSorter();
                        ps.PosDistance = Convert.ToInt32(txtDistance.Text.Trim());
                        ps.ArrowDir = 2;
                        ps.ArrowPosition = ArrowPosition1;
                        ps.Show();

                        //frmSorterAPIConfigurator f = new frmSorterAPIConfigurator();
                        //f.PosDistance = Convert.ToInt32(txtDistance.Text.Trim());
                        //f.ArrowDir = 2;
                        //f.ArrowPosition = ArrowPosition1;                    
                        //f.Show();

                        this.Close();

                    }
                }
                else
                {
                    if (txtDistance.Text.Length == 0)
                    {
                        MessageBox.Show("Please enter value in distance field.");
                        txtDistance.Focus();
                    }
                    else
                    {
                        FormCollection fc = Application.OpenForms;
                        foreach (Form frm in fc)
                        {
                            if (frm.Text == "ProceedtoSorter")
                            {
                                frm.Hide();
                            }
                        }
                        //  if (frm != null)
                        //  {

                        //  }
                        //  frmAddNewLocation f = new frmAddNewLocation();


                        ProceedtoSorter ps = new ProceedtoSorter();
                        ps.PosDistance = Convert.ToInt32(txtDistance.Text.Trim());
                        ps.ArrowDir = 2;
                        ps.ArrowPosition = ArrowPosition1;
                        ps.Show();

                        //frmSorterAPIConfigurator f = new frmSorterAPIConfigurator();
                        //f.PosDistance = Convert.ToInt32(txtDistance.Text.Trim());
                        //f.ArrowDir = 3;
                        //f.ArrowPosition = ArrowPosition1;
                        //f.Show();

                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog(ex.ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtDistance_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btnOk_MouseHover(object sender, EventArgs e)
        {
            btnOk.BackColor = Color.FromArgb(62, 46, 52);
            btnOk.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnOk_MouseLeave(object sender, EventArgs e)
        {
            btnOk.BackColor = Color.FromArgb(255, 190, 0);
            btnOk.ForeColor = Color.FromArgb(62, 46, 52);
        }

        private void btnCancel_MouseHover(object sender, EventArgs e)
        {
            btnCancel.BackColor = Color.FromArgb(62, 46, 52);
            btnCancel.ForeColor = Color.FromArgb(255, 190, 0);
        }

        private void btnCancel_MouseLeave(object sender, EventArgs e)
        {
            btnCancel.BackColor = Color.FromArgb(255, 190, 0);
            btnCancel.ForeColor = Color.FromArgb(62, 46, 52);
        }        

    }
}
