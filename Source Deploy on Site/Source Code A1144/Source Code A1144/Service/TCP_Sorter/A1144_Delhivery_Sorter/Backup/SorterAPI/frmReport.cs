﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace SorterAPI
{
    public partial class frmReport : Form
    {
        DataSet ds = new DataSet();
        DataTable dtCount = new DataTable();
        DataTable dtGrid = new DataTable();

        public frmReport()
        {
            InitializeComponent();
            dtFrom.Format = DateTimePickerFormat.Custom;
            dtFrom.CustomFormat = "yyyy-MM-dd";

            dtTo.Format = DateTimePickerFormat.Custom;
            dtTo.CustomFormat = "yyyy-MM-dd";
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            Report(0);

            //drpSearchCri.DisplayMember = "Text";
            //drpSearchCri.ValueMember = "Value";
            //var items = new[] { 
            //    new { Text = "--Select--", Value = "0" }, 
            //    new { Text = "Daily", Value = "1" }, 
            //    new { Text = "Weekly", Value = "2" }, 
            //    new { Text = "Monthly", Value = "3" },
            //    new { Text = "Yearly", Value = "4" },
            //    new { Text = "Date Range", Value = "5" }
            //    };
            //drpSearchCri.DataSource = items;

            drpBaywise.DisplayMember = "Text";
            drpBaywise.ValueMember = "Value";
            var items1 = new[] { 
                new { Text = "--Select--", Value = "0" }, 
                new { Text = "1", Value = "1" }, 
                new { Text = "2", Value = "2" }, 
                new { Text = "3", Value = "3" },
                new { Text = "4", Value = "4" }, 
                new { Text = "5", Value = "5" }
                };
            drpBaywise.DataSource = items1;           
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Report(0);
        }

        public void Report(int btn)
        {   
            clsAPI obj = new clsAPI();

            if (btn == 0)
            {
                obj.Fromdt = dtFrom.Text;
                obj.Todt = dtTo.Text;
               
                if (chkBay.Checked == true && drpBaywise.Text != "--Select--")
                {
                    obj.SorterNo = Convert.ToInt32(drpBaywise.Text);
                }
                if (chkPin.Checked == true && txtPincode.Text != "")
                {
                    obj.Pincode = Convert.ToInt32(txtPincode.Text);
                }

                obj.btnValue = btn;
                ds = clsAPI.getDWSReport(obj);//getDWSCount_Data(obj);
            }
            else
            {
                obj.btnValue = btn;
                ds = clsAPI.getDWSReport(obj);
            }
            dtGrid = ds.Tables[0];
            dtCount = ds.Tables[1];

            dgvReport.RowsDefaultCellStyle.BackColor = Color.White;
           // dgvReport.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(115, 205, 212);
            dgvReport.AlternatingRowsDefaultCellStyle.BackColor = Color.Gray;

            //this.dgvReport.DefaultCellStyle.ForeColor = Color.Coral;
            //// Change back color of each row
            //this.dgvReport.RowsDefaultCellStyle.BackColor = Color.AliceBlue;
            //// Change GridLine Color
            //this.dgvReport.GridColor = Color.BlueViolet;
            //// Change Grid Border Style
            //this.dgvReport.BorderStyle = BorderStyle.Fixed3D;

            //dgvReport.DefaultCellStyle.SelectionBackColor = Color.Red;
            //dgvReport.DefaultCellStyle.SelectionForeColor = Color.Yellow;

            Font f = new Font("Microsoft Sans Serif",8,FontStyle.Bold);
            dgvReport.DefaultCellStyle.Font = f;
            //dgvReport.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            dgvReport.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif",9, FontStyle.Bold);

            dgvReport.AutoResizeColumns();
            dgvReport.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dgvReport.RowHeadersVisible = false;
            dgvReport.DataSource = dtGrid;

            lblTotal.Text = dtCount.Rows[0]["Total"].ToString();
            lblSuccesful.Text = dtCount.Rows[0]["Succesful"].ToString();
            lblNA.Text = dtCount.Rows[0]["Faild"].ToString();
        }

        private void chkBay_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBay.Checked == true)
            {
                drpBaywise.Enabled = true;

                if (chkPin.Checked == true)
                {
                    chkPin.Checked = false;
                    txtPincode.Enabled = false;
                    txtPincode.Text = "";
                }
            }
            else
            {
                drpBaywise.Enabled = false;
            }
        }

        private void chkPin_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPin.Checked == true)
            {
                txtPincode.Enabled = true;

                if (chkBay.Checked == true)
                {
                    chkBay.Checked = false;
                    drpBaywise.Enabled = false;
                    drpBaywise.Text = "--Select--";
                }
            }
            else
            {
                txtPincode.Enabled = false;
                txtPincode.Text = "";
            }            
        }

        private void chkCustomDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCustomDate.Checked == true)
            {
                dtFrom.Enabled = true;
                dtTo.Enabled = true;
            }
            else
            {
                dtFrom.Enabled = false;
                dtTo.Enabled = false;
            }
        }

        private void btnExporttoExcel_Click(object sender, EventArgs e)
        {
            //DateTime datetime = DateTime.Now;
            //string s = datetime.ToString("dd/MM/yyyy");
            //string strFilePath = @"D:\CSV\Repors_" + s + ".csv";
            //CreateCSVFile(dtGrid, strFilePath);
            saveFileDialog1.ShowDialog();
        }

        public void CreateCSVFile(DataTable dtDataTablesList, string strFilePath)
        {
            // Create the CSV file to which grid data will be exported.

            StreamWriter sw = new StreamWriter(strFilePath, false);

            //First we will write the headers.

            int iColCount = dtDataTablesList.Columns.Count;

            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dtDataTablesList.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);

            // Now write all the rows.

            foreach (DataRow dr in dtDataTablesList.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();

            MessageBox.Show("File Exported!!..");
        }

        private void btnDBBackup_Click(object sender, EventArgs e)
        {
           // DateTime datetime = DateTime.Now;
          //  string s = datetime.ToString("dd/MM/yyyy");
          //  BackupDatabase(@"D:\bak\GPO_" + s + ".bak");
        }

        //public static void BackupDatabase(string backUpFile)
        //{
        //    try
        //    {
        //        ServerConnection con = new ServerConnection(@"ANC6\SQLEXPRESS");
        //        Server server = new Server(con);
        //        Backup source = new Backup();
        //        source.Action = BackupActionType.Database;
        //        source.Database = "GPO";
        //        BackupDeviceItem destination = new BackupDeviceItem(backUpFile, DeviceType.File);
        //        source.Devices.Add(destination);
        //        source.SqlBackup(server);
        //        con.Disconnect();
        //        MessageBox.Show("The backup of database 'GPO' completed successfully.");
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string strFilePath = saveFileDialog1.FileName;
            CreateCSVFile(dtGrid, strFilePath+".csv");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMin_Click(object sender, EventArgs e)
        {
            Report(1);
        }

        private void btnHr_Click(object sender, EventArgs e)
        {
            Report(2);
        }

        private void btnDay_Click(object sender, EventArgs e)
        {
            Report(3);
        }

        private void dgvReport_CellClick(object sender, DataGridViewCellEventArgs e)
        {
          // string picpath = dgvReport.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();


            //string picpath = @"E:\testimg\utilities.png";
            //   //"emp_Lname" or the column index that you want to use               
            //  // picpath = picpath + sampleGrid["emp_Lname", grdTrans.CurrentCell.RowIndex].Value.toString() + ".jpg"
            //   picbox.ImageLocation = picpath;
            //   picbox.Visible = true;
        }

        private void pnlExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvReport_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //DataRowView drv;
            //if (e.RowIndex > 0)
            //{
            //    drv = dtGrid.DefaultView[e.RowIndex];
            //    Color c;
            //    if (drv["Barcode"].ToString() == "NA" || drv["Barcode"].ToString() == "N_A")
            //    {
            //        c = Color.Red;
            //    }
            //    else
            //    {
            //        c = Color.LightBlue;
            //    }
            //    e.CellStyle.BackColor = c;
            //}
        }
       
        private void btnSearchBarcode_Click(object sender, EventArgs e)
        {
            frmBarcodeSearch BS = new frmBarcodeSearch();
            BS.ShowDialog();
        }
    }
}
