﻿namespace SorterAPI
{
    partial class frmSorterAPIConfigurator5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button3 = new System.Windows.Forms.Button();
            this.btnSaveDBInfo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtServerType = new System.Windows.Forms.TextBox();
            this.txtOurServerName = new System.Windows.Forms.TextBox();
            this.txtOurServerId = new System.Windows.Forms.TextBox();
            this.drpOurDataSource = new System.Windows.Forms.ComboBox();
            this.txtOurServerPass = new System.Windows.Forms.TextBox();
            this.lblOurServerId = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblOurServerPass = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblOurServerName = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblCustServerId = new System.Windows.Forms.Label();
            this.lblCustServerPass = new System.Windows.Forms.Label();
            this.lblCustServerName = new System.Windows.Forms.Label();
            this.txtDBIpAddr = new System.Windows.Forms.TextBox();
            this.txtDBUsername = new System.Windows.Forms.TextBox();
            this.txtDBPassword = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.drpDataSource = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTestOurServer = new System.Windows.Forms.Button();
            this.btnConnTest = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOurColumn4 = new System.Windows.Forms.TextBox();
            this.txtOurColumn3 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtOurColumn2 = new System.Windows.Forms.TextBox();
            this.txtOurColumn1 = new System.Windows.Forms.TextBox();
            this.txtOurTableName = new System.Windows.Forms.TextBox();
            this.drpOurDBName = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtMaxWe = new System.Windows.Forms.TextBox();
            this.txtMinWe = new System.Windows.Forms.TextBox();
            this.txtMaxL = new System.Windows.Forms.TextBox();
            this.txtMinL = new System.Windows.Forms.TextBox();
            this.txtMaxW = new System.Windows.Forms.TextBox();
            this.txtMinW = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMaxH = new System.Windows.Forms.TextBox();
            this.txtMinH = new System.Windows.Forms.TextBox();
            this.drpColumnName4 = new System.Windows.Forms.ComboBox();
            this.drpColumnName3 = new System.Windows.Forms.ComboBox();
            this.drpColumnName2 = new System.Windows.Forms.ComboBox();
            this.drpColumnName1 = new System.Windows.Forms.ComboBox();
            this.drpTableName = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.drpDBName = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblBagAssignment = new System.Windows.Forms.Label();
            this.lblLinkDB = new System.Windows.Forms.Label();
            this.lblPTLSys = new System.Windows.Forms.Label();
            this.lblPLCData = new System.Windows.Forms.Label();
            this.lblCustInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(647, 690);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(153, 49);
            this.button3.TabIndex = 13;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // btnSaveDBInfo
            // 
            this.btnSaveDBInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnSaveDBInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveDBInfo.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveDBInfo.ForeColor = System.Drawing.Color.White;
            this.btnSaveDBInfo.Location = new System.Drawing.Point(395, 690);
            this.btnSaveDBInfo.Name = "btnSaveDBInfo";
            this.btnSaveDBInfo.Size = new System.Drawing.Size(153, 49);
            this.btnSaveDBInfo.TabIndex = 12;
            this.btnSaveDBInfo.Text = "Save";
            this.btnSaveDBInfo.UseVisualStyleBackColor = false;
            this.btnSaveDBInfo.Click += new System.EventHandler(this.btnSaveDBInfo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "                             ";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.txtServerType);
            this.panel3.Controls.Add(this.txtOurServerName);
            this.panel3.Controls.Add(this.txtOurServerId);
            this.panel3.Controls.Add(this.drpOurDataSource);
            this.panel3.Controls.Add(this.txtOurServerPass);
            this.panel3.Controls.Add(this.lblOurServerId);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.lblOurServerPass);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.lblOurServerName);
            this.panel3.Location = new System.Drawing.Point(9, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(571, 239);
            this.panel3.TabIndex = 2;
            // 
            // txtServerType
            // 
            this.txtServerType.BackColor = System.Drawing.Color.White;
            this.txtServerType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServerType.Enabled = false;
            this.txtServerType.Location = new System.Drawing.Point(275, 58);
            this.txtServerType.Multiline = true;
            this.txtServerType.Name = "txtServerType";
            this.txtServerType.Size = new System.Drawing.Size(216, 29);
            this.txtServerType.TabIndex = 22;
            this.txtServerType.Text = "SQLServer";
            // 
            // txtOurServerName
            // 
            this.txtOurServerName.BackColor = System.Drawing.Color.LightGray;
            this.txtOurServerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurServerName.Location = new System.Drawing.Point(275, 96);
            this.txtOurServerName.Multiline = true;
            this.txtOurServerName.Name = "txtOurServerName";
            this.txtOurServerName.Size = new System.Drawing.Size(216, 29);
            this.txtOurServerName.TabIndex = 11;
            // 
            // txtOurServerId
            // 
            this.txtOurServerId.BackColor = System.Drawing.Color.LightGray;
            this.txtOurServerId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurServerId.Location = new System.Drawing.Point(275, 145);
            this.txtOurServerId.Multiline = true;
            this.txtOurServerId.Name = "txtOurServerId";
            this.txtOurServerId.Size = new System.Drawing.Size(216, 29);
            this.txtOurServerId.TabIndex = 12;
            // 
            // drpOurDataSource
            // 
            this.drpOurDataSource.FormattingEnabled = true;
            this.drpOurDataSource.Items.AddRange(new object[] {
            "SQLServer",
            "MSAccess",
            "MySql",
            "Oracle"});
            this.drpOurDataSource.Location = new System.Drawing.Point(326, 31);
            this.drpOurDataSource.Name = "drpOurDataSource";
            this.drpOurDataSource.Size = new System.Drawing.Size(241, 21);
            this.drpOurDataSource.TabIndex = 21;
            this.drpOurDataSource.Visible = false;
            this.drpOurDataSource.SelectedIndexChanged += new System.EventHandler(this.drpOurDataSource_SelectedIndexChanged);
            // 
            // txtOurServerPass
            // 
            this.txtOurServerPass.BackColor = System.Drawing.Color.LightGray;
            this.txtOurServerPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOurServerPass.Location = new System.Drawing.Point(275, 187);
            this.txtOurServerPass.Multiline = true;
            this.txtOurServerPass.Name = "txtOurServerPass";
            this.txtOurServerPass.Size = new System.Drawing.Size(216, 29);
            this.txtOurServerPass.TabIndex = 13;
            // 
            // lblOurServerId
            // 
            this.lblOurServerId.AutoSize = true;
            this.lblOurServerId.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOurServerId.Location = new System.Drawing.Point(15, 146);
            this.lblOurServerId.Name = "lblOurServerId";
            this.lblOurServerId.Size = new System.Drawing.Size(166, 19);
            this.lblOurServerId.TabIndex = 20;
            this.lblOurServerId.Text = "Enter Server Name : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label9.Location = new System.Drawing.Point(155, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(182, 22);
            this.label9.TabIndex = 17;
            this.label9.Text = "Armstrong DB Info";
            // 
            // lblOurServerPass
            // 
            this.lblOurServerPass.AutoSize = true;
            this.lblOurServerPass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOurServerPass.Location = new System.Drawing.Point(15, 188);
            this.lblOurServerPass.Name = "lblOurServerPass";
            this.lblOurServerPass.Size = new System.Drawing.Size(191, 19);
            this.lblOurServerPass.TabIndex = 19;
            this.lblOurServerPass.Text = "Enter Server Password:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(15, 56);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(215, 19);
            this.label26.TabIndex = 6;
            this.label26.Text = "Select Our Database Type :";
            // 
            // lblOurServerName
            // 
            this.lblOurServerName.AutoSize = true;
            this.lblOurServerName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOurServerName.Location = new System.Drawing.Point(15, 97);
            this.lblOurServerName.Name = "lblOurServerName";
            this.lblOurServerName.Size = new System.Drawing.Size(114, 19);
            this.lblOurServerName.TabIndex = 18;
            this.lblOurServerName.Text = "Enter Server :";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.lblCustServerId);
            this.panel4.Controls.Add(this.lblCustServerPass);
            this.panel4.Controls.Add(this.lblCustServerName);
            this.panel4.Controls.Add(this.txtDBIpAddr);
            this.panel4.Controls.Add(this.txtDBUsername);
            this.panel4.Controls.Add(this.txtDBPassword);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.drpDataSource);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(605, 27);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(571, 239);
            this.panel4.TabIndex = 3;
            // 
            // lblCustServerId
            // 
            this.lblCustServerId.AutoSize = true;
            this.lblCustServerId.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustServerId.Location = new System.Drawing.Point(27, 145);
            this.lblCustServerId.Name = "lblCustServerId";
            this.lblCustServerId.Size = new System.Drawing.Size(166, 19);
            this.lblCustServerId.TabIndex = 32;
            this.lblCustServerId.Text = "Enter Server Name : ";
            // 
            // lblCustServerPass
            // 
            this.lblCustServerPass.AutoSize = true;
            this.lblCustServerPass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustServerPass.Location = new System.Drawing.Point(27, 187);
            this.lblCustServerPass.Name = "lblCustServerPass";
            this.lblCustServerPass.Size = new System.Drawing.Size(191, 19);
            this.lblCustServerPass.TabIndex = 31;
            this.lblCustServerPass.Text = "Enter Server Password:";
            // 
            // lblCustServerName
            // 
            this.lblCustServerName.AutoSize = true;
            this.lblCustServerName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustServerName.Location = new System.Drawing.Point(27, 96);
            this.lblCustServerName.Name = "lblCustServerName";
            this.lblCustServerName.Size = new System.Drawing.Size(114, 19);
            this.lblCustServerName.TabIndex = 30;
            this.lblCustServerName.Text = "Enter Server :";
            // 
            // txtDBIpAddr
            // 
            this.txtDBIpAddr.BackColor = System.Drawing.Color.LightGray;
            this.txtDBIpAddr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBIpAddr.Location = new System.Drawing.Point(290, 95);
            this.txtDBIpAddr.Multiline = true;
            this.txtDBIpAddr.Name = "txtDBIpAddr";
            this.txtDBIpAddr.Size = new System.Drawing.Size(216, 29);
            this.txtDBIpAddr.TabIndex = 27;
            // 
            // txtDBUsername
            // 
            this.txtDBUsername.BackColor = System.Drawing.Color.LightGray;
            this.txtDBUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBUsername.Location = new System.Drawing.Point(290, 144);
            this.txtDBUsername.Multiline = true;
            this.txtDBUsername.Name = "txtDBUsername";
            this.txtDBUsername.Size = new System.Drawing.Size(216, 29);
            this.txtDBUsername.TabIndex = 28;
            // 
            // txtDBPassword
            // 
            this.txtDBPassword.BackColor = System.Drawing.Color.LightGray;
            this.txtDBPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDBPassword.Location = new System.Drawing.Point(290, 186);
            this.txtDBPassword.Multiline = true;
            this.txtDBPassword.Name = "txtDBPassword";
            this.txtDBPassword.Size = new System.Drawing.Size(216, 29);
            this.txtDBPassword.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(27, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(183, 19);
            this.label11.TabIndex = 26;
            this.label11.Text = "Select Database Type :";
            // 
            // drpDataSource
            // 
            this.drpDataSource.FormattingEnabled = true;
            this.drpDataSource.Items.AddRange(new object[] {
            "SQLServer",
            "MSAccess",
            "MySql",
            "Oracle"});
            this.drpDataSource.Location = new System.Drawing.Point(290, 57);
            this.drpDataSource.Name = "drpDataSource";
            this.drpDataSource.Size = new System.Drawing.Size(239, 21);
            this.drpDataSource.TabIndex = 25;
            this.drpDataSource.SelectedIndexChanged += new System.EventHandler(this.drpDataSource_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label3.Location = new System.Drawing.Point(206, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 22);
            this.label3.TabIndex = 18;
            this.label3.Text = "Customer DB Info";
            // 
            // btnTestOurServer
            // 
            this.btnTestOurServer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.btnTestOurServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTestOurServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestOurServer.ForeColor = System.Drawing.Color.White;
            this.btnTestOurServer.Location = new System.Drawing.Point(12, 273);
            this.btnTestOurServer.Name = "btnTestOurServer";
            this.btnTestOurServer.Size = new System.Drawing.Size(571, 36);
            this.btnTestOurServer.TabIndex = 4;
            this.btnTestOurServer.Text = "Test";
            this.btnTestOurServer.UseVisualStyleBackColor = false;
            this.btnTestOurServer.Click += new System.EventHandler(this.btnTestOurServer_Click);
            // 
            // btnConnTest
            // 
            this.btnConnTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(71)))), ((int)(((byte)(71)))));
            this.btnConnTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConnTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConnTest.ForeColor = System.Drawing.Color.White;
            this.btnConnTest.Location = new System.Drawing.Point(605, 273);
            this.btnConnTest.Name = "btnConnTest";
            this.btnConnTest.Size = new System.Drawing.Size(571, 36);
            this.btnConnTest.TabIndex = 5;
            this.btnConnTest.Text = "Test";
            this.btnConnTest.UseVisualStyleBackColor = false;
            this.btnConnTest.Click += new System.EventHandler(this.btnConnTest_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(226)))), ((int)(((byte)(166)))));
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.txtOurColumn4);
            this.panel5.Controls.Add(this.txtOurColumn3);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.txtOurColumn2);
            this.panel5.Controls.Add(this.txtOurColumn1);
            this.panel5.Controls.Add(this.txtOurTableName);
            this.panel5.Controls.Add(this.drpOurDBName);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Location = new System.Drawing.Point(12, 315);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(571, 283);
            this.panel5.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(15, 240);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(182, 19);
            this.label13.TabIndex = 29;
            this.label13.Text = "Select Column Name : ";
            // 
            // txtOurColumn4
            // 
            this.txtOurColumn4.Location = new System.Drawing.Point(275, 239);
            this.txtOurColumn4.Multiline = true;
            this.txtOurColumn4.Name = "txtOurColumn4";
            this.txtOurColumn4.Size = new System.Drawing.Size(241, 20);
            this.txtOurColumn4.TabIndex = 28;
            // 
            // txtOurColumn3
            // 
            this.txtOurColumn3.Location = new System.Drawing.Point(275, 195);
            this.txtOurColumn3.Multiline = true;
            this.txtOurColumn3.Name = "txtOurColumn3";
            this.txtOurColumn3.Size = new System.Drawing.Size(241, 20);
            this.txtOurColumn3.TabIndex = 27;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(15, 196);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(182, 19);
            this.label23.TabIndex = 26;
            this.label23.Text = "Select Column Name : ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(15, 151);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(182, 19);
            this.label22.TabIndex = 25;
            this.label22.Text = "Select Column Name : ";
            // 
            // txtOurColumn2
            // 
            this.txtOurColumn2.Location = new System.Drawing.Point(275, 150);
            this.txtOurColumn2.Multiline = true;
            this.txtOurColumn2.Name = "txtOurColumn2";
            this.txtOurColumn2.Size = new System.Drawing.Size(241, 20);
            this.txtOurColumn2.TabIndex = 24;
            // 
            // txtOurColumn1
            // 
            this.txtOurColumn1.Location = new System.Drawing.Point(275, 106);
            this.txtOurColumn1.Multiline = true;
            this.txtOurColumn1.Name = "txtOurColumn1";
            this.txtOurColumn1.Size = new System.Drawing.Size(241, 20);
            this.txtOurColumn1.TabIndex = 23;
            // 
            // txtOurTableName
            // 
            this.txtOurTableName.Location = new System.Drawing.Point(275, 63);
            this.txtOurTableName.Multiline = true;
            this.txtOurTableName.Name = "txtOurTableName";
            this.txtOurTableName.Size = new System.Drawing.Size(241, 20);
            this.txtOurTableName.TabIndex = 22;
            // 
            // drpOurDBName
            // 
            this.drpOurDBName.FormattingEnabled = true;
            this.drpOurDBName.Location = new System.Drawing.Point(275, 20);
            this.drpOurDBName.Name = "drpOurDBName";
            this.drpOurDBName.Size = new System.Drawing.Size(241, 21);
            this.drpOurDBName.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 107);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(182, 19);
            this.label12.TabIndex = 20;
            this.label12.Text = "Select Column Name : ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(15, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(159, 19);
            this.label14.TabIndex = 18;
            this.label14.Text = "Select Table Name :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(190, 19);
            this.label16.TabIndex = 6;
            this.label16.Text = "Select Database Name :";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(213)))), ((int)(((byte)(226)))));
            this.panel6.Controls.Add(this.txtMaxWe);
            this.panel6.Controls.Add(this.txtMinWe);
            this.panel6.Controls.Add(this.txtMaxL);
            this.panel6.Controls.Add(this.txtMinL);
            this.panel6.Controls.Add(this.txtMaxW);
            this.panel6.Controls.Add(this.txtMinW);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.txtMaxH);
            this.panel6.Controls.Add(this.txtMinH);
            this.panel6.Controls.Add(this.drpColumnName4);
            this.panel6.Controls.Add(this.drpColumnName3);
            this.panel6.Controls.Add(this.drpColumnName2);
            this.panel6.Controls.Add(this.drpColumnName1);
            this.panel6.Controls.Add(this.drpTableName);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.drpDBName);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Location = new System.Drawing.Point(610, 315);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(571, 283);
            this.panel6.TabIndex = 7;
            // 
            // txtMaxWe
            // 
            this.txtMaxWe.Location = new System.Drawing.Point(472, 239);
            this.txtMaxWe.MaxLength = 4;
            this.txtMaxWe.Multiline = true;
            this.txtMaxWe.Name = "txtMaxWe";
            this.txtMaxWe.Size = new System.Drawing.Size(57, 20);
            this.txtMaxWe.TabIndex = 188;
            // 
            // txtMinWe
            // 
            this.txtMinWe.Location = new System.Drawing.Point(404, 239);
            this.txtMinWe.MaxLength = 4;
            this.txtMinWe.Multiline = true;
            this.txtMinWe.Name = "txtMinWe";
            this.txtMinWe.Size = new System.Drawing.Size(57, 20);
            this.txtMinWe.TabIndex = 187;
            // 
            // txtMaxL
            // 
            this.txtMaxL.Location = new System.Drawing.Point(472, 195);
            this.txtMaxL.MaxLength = 4;
            this.txtMaxL.Multiline = true;
            this.txtMaxL.Name = "txtMaxL";
            this.txtMaxL.Size = new System.Drawing.Size(57, 20);
            this.txtMaxL.TabIndex = 186;
            // 
            // txtMinL
            // 
            this.txtMinL.Location = new System.Drawing.Point(404, 195);
            this.txtMinL.MaxLength = 4;
            this.txtMinL.Multiline = true;
            this.txtMinL.Name = "txtMinL";
            this.txtMinL.Size = new System.Drawing.Size(57, 20);
            this.txtMinL.TabIndex = 185;
            // 
            // txtMaxW
            // 
            this.txtMaxW.Location = new System.Drawing.Point(472, 150);
            this.txtMaxW.MaxLength = 4;
            this.txtMaxW.Multiline = true;
            this.txtMaxW.Name = "txtMaxW";
            this.txtMaxW.Size = new System.Drawing.Size(57, 20);
            this.txtMaxW.TabIndex = 184;
            // 
            // txtMinW
            // 
            this.txtMinW.Location = new System.Drawing.Point(404, 150);
            this.txtMinW.MaxLength = 4;
            this.txtMinW.Multiline = true;
            this.txtMinW.Name = "txtMinW";
            this.txtMinW.Size = new System.Drawing.Size(57, 20);
            this.txtMinW.TabIndex = 183;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(485, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 16);
            this.label15.TabIndex = 182;
            this.label15.Text = "Max";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(417, 85);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 16);
            this.label20.TabIndex = 181;
            this.label20.Text = "Min";
            // 
            // txtMaxH
            // 
            this.txtMaxH.Location = new System.Drawing.Point(472, 104);
            this.txtMaxH.MaxLength = 4;
            this.txtMaxH.Multiline = true;
            this.txtMaxH.Name = "txtMaxH";
            this.txtMaxH.Size = new System.Drawing.Size(57, 20);
            this.txtMaxH.TabIndex = 180;
            // 
            // txtMinH
            // 
            this.txtMinH.Location = new System.Drawing.Point(404, 104);
            this.txtMinH.MaxLength = 4;
            this.txtMinH.Multiline = true;
            this.txtMinH.Name = "txtMinH";
            this.txtMinH.Size = new System.Drawing.Size(57, 20);
            this.txtMinH.TabIndex = 179;
            // 
            // drpColumnName4
            // 
            this.drpColumnName4.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpColumnName4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName4.FormattingEnabled = true;
            this.drpColumnName4.Location = new System.Drawing.Point(223, 239);
            this.drpColumnName4.Name = "drpColumnName4";
            this.drpColumnName4.Size = new System.Drawing.Size(174, 21);
            this.drpColumnName4.TabIndex = 178;
            // 
            // drpColumnName3
            // 
            this.drpColumnName3.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpColumnName3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName3.FormattingEnabled = true;
            this.drpColumnName3.Location = new System.Drawing.Point(223, 195);
            this.drpColumnName3.Name = "drpColumnName3";
            this.drpColumnName3.Size = new System.Drawing.Size(174, 21);
            this.drpColumnName3.TabIndex = 177;
            // 
            // drpColumnName2
            // 
            this.drpColumnName2.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName2.FormattingEnabled = true;
            this.drpColumnName2.Location = new System.Drawing.Point(223, 150);
            this.drpColumnName2.Name = "drpColumnName2";
            this.drpColumnName2.Size = new System.Drawing.Size(173, 21);
            this.drpColumnName2.TabIndex = 176;
            // 
            // drpColumnName1
            // 
            this.drpColumnName1.BackColor = System.Drawing.Color.LightGray;
            this.drpColumnName1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpColumnName1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpColumnName1.FormattingEnabled = true;
            this.drpColumnName1.Location = new System.Drawing.Point(223, 104);
            this.drpColumnName1.Name = "drpColumnName1";
            this.drpColumnName1.Size = new System.Drawing.Size(173, 21);
            this.drpColumnName1.TabIndex = 175;
            // 
            // drpTableName
            // 
            this.drpTableName.BackColor = System.Drawing.Color.LightGray;
            this.drpTableName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpTableName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drpTableName.FormattingEnabled = true;
            this.drpTableName.Location = new System.Drawing.Point(224, 63);
            this.drpTableName.Name = "drpTableName";
            this.drpTableName.Size = new System.Drawing.Size(173, 21);
            this.drpTableName.TabIndex = 174;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(461, 109);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(12, 16);
            this.label27.TabIndex = 189;
            this.label27.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(461, 244);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 16);
            this.label28.TabIndex = 192;
            this.label28.Text = ":";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(461, 199);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(12, 16);
            this.label29.TabIndex = 191;
            this.label29.Text = ":";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(461, 155);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(12, 16);
            this.label30.TabIndex = 190;
            this.label30.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(9, 240);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(182, 19);
            this.label17.TabIndex = 41;
            this.label17.Text = "Select Column Name : ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(9, 196);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(182, 19);
            this.label18.TabIndex = 38;
            this.label18.Text = "Select Column Name : ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(9, 151);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(182, 19);
            this.label19.TabIndex = 37;
            this.label19.Text = "Select Column Name : ";
            // 
            // drpDBName
            // 
            this.drpDBName.FormattingEnabled = true;
            this.drpDBName.Location = new System.Drawing.Point(224, 21);
            this.drpDBName.Name = "drpDBName";
            this.drpDBName.Size = new System.Drawing.Size(241, 21);
            this.drpDBName.TabIndex = 33;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(9, 107);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(182, 19);
            this.label21.TabIndex = 32;
            this.label21.Text = "Select Column Name : ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(9, 63);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(159, 19);
            this.label24.TabIndex = 31;
            this.label24.Text = "Select Table Name :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(9, 19);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(190, 19);
            this.label25.TabIndex = 30;
            this.label25.Text = "Select Database Name :";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.btnConnTest);
            this.panel2.Controls.Add(this.btnTestOurServer);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pnlClose);
            this.panel2.Location = new System.Drawing.Point(1, 59);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1194, 616);
            this.panel2.TabIndex = 3;
            // 
            // pnlClose
            // 
            this.pnlClose.BackColor = System.Drawing.Color.Transparent;
            this.pnlClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.pnlClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlClose.ForeColor = System.Drawing.Color.Transparent;
            this.pnlClose.Location = new System.Drawing.Point(1164, 1);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Size = new System.Drawing.Size(30, 25);
            this.pnlClose.TabIndex = 0;
            this.pnlClose.Click += new System.EventHandler(this.pnlClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.Link;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblBagAssignment);
            this.panel1.Controls.Add(this.lblLinkDB);
            this.panel1.Controls.Add(this.lblPTLSys);
            this.panel1.Controls.Add(this.lblPLCData);
            this.panel1.Controls.Add(this.lblCustInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1196, 54);
            this.panel1.TabIndex = 5;
            // 
            // lblBagAssignment
            // 
            this.lblBagAssignment.AutoSize = true;
            this.lblBagAssignment.BackColor = System.Drawing.Color.Transparent;
            this.lblBagAssignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBagAssignment.Location = new System.Drawing.Point(940, 13);
            this.lblBagAssignment.Name = "lblBagAssignment";
            this.lblBagAssignment.Size = new System.Drawing.Size(169, 29);
            this.lblBagAssignment.TabIndex = 5;
            this.lblBagAssignment.Text = "                          ";
            this.lblBagAssignment.Click += new System.EventHandler(this.lblBagAssignment_Click);
            // 
            // lblLinkDB
            // 
            this.lblLinkDB.AutoSize = true;
            this.lblLinkDB.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkDB.Location = new System.Drawing.Point(783, 13);
            this.lblLinkDB.Name = "lblLinkDB";
            this.lblLinkDB.Size = new System.Drawing.Size(127, 29);
            this.lblLinkDB.TabIndex = 4;
            this.lblLinkDB.Text = "                   ";
            this.lblLinkDB.Click += new System.EventHandler(this.lblLinkDB_Click);
            // 
            // lblPTLSys
            // 
            this.lblPTLSys.AutoSize = true;
            this.lblPTLSys.BackColor = System.Drawing.Color.Transparent;
            this.lblPTLSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPTLSys.Location = new System.Drawing.Point(608, 13);
            this.lblPTLSys.Name = "lblPTLSys";
            this.lblPTLSys.Size = new System.Drawing.Size(127, 29);
            this.lblPTLSys.TabIndex = 3;
            this.lblPTLSys.Text = "                   ";
            this.lblPTLSys.Click += new System.EventHandler(this.lblPTLSys_Click);
            // 
            // lblPLCData
            // 
            this.lblPLCData.AutoSize = true;
            this.lblPLCData.BackColor = System.Drawing.Color.Transparent;
            this.lblPLCData.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLCData.Location = new System.Drawing.Point(419, 13);
            this.lblPLCData.Name = "lblPLCData";
            this.lblPLCData.Size = new System.Drawing.Size(157, 29);
            this.lblPLCData.TabIndex = 2;
            this.lblPLCData.Text = "                        ";
            this.lblPLCData.Click += new System.EventHandler(this.lblPLCData_Click);
            // 
            // lblCustInfo
            // 
            this.lblCustInfo.AutoSize = true;
            this.lblCustInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCustInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustInfo.Location = new System.Drawing.Point(239, 14);
            this.lblCustInfo.Name = "lblCustInfo";
            this.lblCustInfo.Size = new System.Drawing.Size(145, 29);
            this.lblCustInfo.TabIndex = 1;
            this.lblCustInfo.Text = "                      ";
            this.lblCustInfo.Click += new System.EventHandler(this.lblCustInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "                             ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmSorterAPIConfigurator5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScrollMargin = new System.Drawing.Size(0, 100);
            this.ClientSize = new System.Drawing.Size(1197, 758);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnSaveDBInfo);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterAPIConfigurator5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Sorter Configurator";
            this.Load += new System.EventHandler(this.frmSorterAPIConfigurator5_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblBagAssignment;
        private System.Windows.Forms.Label lblLinkDB;
        private System.Windows.Forms.Label lblPTLSys;
        private System.Windows.Forms.Label lblPLCData;
        private System.Windows.Forms.Label lblCustInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnSaveDBInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtOurServerName;
        private System.Windows.Forms.TextBox txtOurServerId;
        private System.Windows.Forms.TextBox txtOurServerPass;
        private System.Windows.Forms.Label lblOurServerId;
        private System.Windows.Forms.Label lblOurServerPass;
        private System.Windows.Forms.Label lblOurServerName;
        private System.Windows.Forms.ComboBox drpOurDataSource;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTestOurServer;
        private System.Windows.Forms.Button btnConnTest;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtOurColumn4;
        private System.Windows.Forms.TextBox txtOurColumn3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtOurColumn2;
        private System.Windows.Forms.TextBox txtOurColumn1;
        private System.Windows.Forms.TextBox txtOurTableName;
        private System.Windows.Forms.ComboBox drpOurDBName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox txtMaxWe;
        private System.Windows.Forms.TextBox txtMinWe;
        private System.Windows.Forms.TextBox txtMaxL;
        private System.Windows.Forms.TextBox txtMinL;
        private System.Windows.Forms.TextBox txtMaxW;
        private System.Windows.Forms.TextBox txtMinW;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtMaxH;
        private System.Windows.Forms.TextBox txtMinH;
        private System.Windows.Forms.ComboBox drpColumnName4;
        private System.Windows.Forms.ComboBox drpColumnName3;
        private System.Windows.Forms.ComboBox drpColumnName2;
        private System.Windows.Forms.ComboBox drpColumnName1;
        private System.Windows.Forms.ComboBox drpTableName;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox drpDBName;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.TextBox txtDBIpAddr;
        private System.Windows.Forms.TextBox txtDBUsername;
        private System.Windows.Forms.TextBox txtDBPassword;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox drpDataSource;
        private System.Windows.Forms.Label lblCustServerId;
        private System.Windows.Forms.Label lblCustServerPass;
        private System.Windows.Forms.Label lblCustServerName;
        private System.Windows.Forms.TextBox txtServerType;
    }
}