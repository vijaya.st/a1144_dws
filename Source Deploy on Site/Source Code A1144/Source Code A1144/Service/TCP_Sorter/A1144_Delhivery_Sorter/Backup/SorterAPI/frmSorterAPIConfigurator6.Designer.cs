﻿namespace SorterAPI
{
    partial class frmSorterAPIConfigurator6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dgvHubList = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.drpBinDown = new System.Windows.Forms.ComboBox();
            this.drpBinUp = new System.Windows.Forms.ComboBox();
            this.lblDown = new System.Windows.Forms.Label();
            this.lblUp = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.drpBranch = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblSuccesful = new System.Windows.Forms.Label();
            this.btnSaveLocation = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblBagAssignment = new System.Windows.Forms.Label();
            this.lblLinkDB = new System.Windows.Forms.Label();
            this.lblPTLSys = new System.Windows.Forms.Label();
            this.lblPLCData = new System.Windows.Forms.Label();
            this.lblCustInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "                             ";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(3, 59);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1188, 63);
            this.panel2.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.label3.Location = new System.Drawing.Point(540, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 29);
            this.label3.TabIndex = 19;
            this.label3.Text = "Assign Bag Address";
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::SorterAPI.Properties.Resources.assign_bag_icon;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(432, 9);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(47, 45);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(143)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(3, 126);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1185, 526);
            this.panel4.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel5.Controls.Add(this.dgvHubList);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Location = new System.Drawing.Point(14, 13);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1160, 499);
            this.panel5.TabIndex = 0;
            // 
            // dgvHubList
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            this.dgvHubList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.DarkOrange;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHubList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvHubList.ColumnHeadersHeight = 45;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(157)))), ((int)(((byte)(157)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHubList.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvHubList.GridColor = System.Drawing.Color.White;
            this.dgvHubList.Location = new System.Drawing.Point(19, 15);
            this.dgvHubList.Name = "dgvHubList";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvHubList.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvHubList.RowHeadersWidth = 45;
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvHubList.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvHubList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHubList.Size = new System.Drawing.Size(539, 470);
            this.dgvHubList.TabIndex = 25;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnExit);
            this.panel6.Controls.Add(this.drpBinDown);
            this.panel6.Controls.Add(this.drpBinUp);
            this.panel6.Controls.Add(this.lblDown);
            this.panel6.Controls.Add(this.lblUp);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.drpBranch);
            this.panel6.Controls.Add(this.txtSearch);
            this.panel6.Controls.Add(this.lblSuccesful);
            this.panel6.Controls.Add(this.btnSaveLocation);
            this.panel6.Location = new System.Drawing.Point(576, 15);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(567, 470);
            this.panel6.TabIndex = 24;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(269, 336);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(121, 41);
            this.btnExit.TabIndex = 31;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            // 
            // drpBinDown
            // 
            this.drpBinDown.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBinDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBinDown.FormattingEnabled = true;
            this.drpBinDown.Location = new System.Drawing.Point(166, 222);
            this.drpBinDown.Name = "drpBinDown";
            this.drpBinDown.Size = new System.Drawing.Size(120, 33);
            this.drpBinDown.TabIndex = 30;
            this.drpBinDown.Visible = false;
            // 
            // drpBinUp
            // 
            this.drpBinUp.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBinUp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBinUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBinUp.FormattingEnabled = true;
            this.drpBinUp.Location = new System.Drawing.Point(166, 161);
            this.drpBinUp.Name = "drpBinUp";
            this.drpBinUp.Size = new System.Drawing.Size(120, 33);
            this.drpBinUp.TabIndex = 27;
            this.drpBinUp.Visible = false;
            // 
            // lblDown
            // 
            this.lblDown.AutoSize = true;
            this.lblDown.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblDown.Location = new System.Drawing.Point(16, 230);
            this.lblDown.Name = "lblDown";
            this.lblDown.Size = new System.Drawing.Size(138, 19);
            this.lblDown.TabIndex = 29;
            this.lblDown.Text = "Select Bin Down";
            this.lblDown.Visible = false;
            // 
            // lblUp
            // 
            this.lblUp.AutoSize = true;
            this.lblUp.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.lblUp.Location = new System.Drawing.Point(16, 169);
            this.lblUp.Name = "lblUp";
            this.lblUp.Size = new System.Drawing.Size(115, 19);
            this.lblUp.TabIndex = 28;
            this.lblUp.Text = "Select Bin Up";
            this.lblUp.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(16, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 19);
            this.label5.TabIndex = 26;
            this.label5.Text = "Select Bay";
            // 
            // drpBranch
            // 
            this.drpBranch.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.drpBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpBranch.FormattingEnabled = true;
            this.drpBranch.Location = new System.Drawing.Point(162, 101);
            this.drpBranch.Name = "drpBranch";
            this.drpBranch.Size = new System.Drawing.Size(270, 33);
            this.drpBranch.TabIndex = 25;
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(162, 43);
            this.txtSearch.MaxLength = 6;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(270, 31);
            this.txtSearch.TabIndex = 24;
            // 
            // lblSuccesful
            // 
            this.lblSuccesful.AutoSize = true;
            this.lblSuccesful.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuccesful.ForeColor = System.Drawing.Color.Black;
            this.lblSuccesful.Location = new System.Drawing.Point(16, 51);
            this.lblSuccesful.Name = "lblSuccesful";
            this.lblSuccesful.Size = new System.Drawing.Size(117, 19);
            this.lblSuccesful.TabIndex = 23;
            this.lblSuccesful.Text = "Enter Pincode";
            // 
            // btnSaveLocation
            // 
            this.btnSaveLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(163)))), ((int)(((byte)(138)))));
            this.btnSaveLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveLocation.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Bold);
            this.btnSaveLocation.ForeColor = System.Drawing.Color.White;
            this.btnSaveLocation.Location = new System.Drawing.Point(87, 336);
            this.btnSaveLocation.Name = "btnSaveLocation";
            this.btnSaveLocation.Size = new System.Drawing.Size(121, 41);
            this.btnSaveLocation.TabIndex = 21;
            this.btnSaveLocation.Text = "Submit";
            this.btnSaveLocation.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SorterAPI.Properties.Resources.Assign;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblBagAssignment);
            this.panel1.Controls.Add(this.lblLinkDB);
            this.panel1.Controls.Add(this.lblPTLSys);
            this.panel1.Controls.Add(this.lblPLCData);
            this.panel1.Controls.Add(this.lblCustInfo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1190, 54);
            this.panel1.TabIndex = 5;
            // 
            // lblBagAssignment
            // 
            this.lblBagAssignment.AutoSize = true;
            this.lblBagAssignment.BackColor = System.Drawing.Color.Transparent;
            this.lblBagAssignment.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBagAssignment.Location = new System.Drawing.Point(946, 13);
            this.lblBagAssignment.Name = "lblBagAssignment";
            this.lblBagAssignment.Size = new System.Drawing.Size(169, 29);
            this.lblBagAssignment.TabIndex = 5;
            this.lblBagAssignment.Text = "                          ";
            this.lblBagAssignment.Click += new System.EventHandler(this.lblBagAssignment_Click);
            // 
            // lblLinkDB
            // 
            this.lblLinkDB.AutoSize = true;
            this.lblLinkDB.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkDB.Location = new System.Drawing.Point(780, 13);
            this.lblLinkDB.Name = "lblLinkDB";
            this.lblLinkDB.Size = new System.Drawing.Size(127, 29);
            this.lblLinkDB.TabIndex = 4;
            this.lblLinkDB.Text = "                   ";
            this.lblLinkDB.Click += new System.EventHandler(this.lblLinkDB_Click);
            // 
            // lblPTLSys
            // 
            this.lblPTLSys.AutoSize = true;
            this.lblPTLSys.BackColor = System.Drawing.Color.Transparent;
            this.lblPTLSys.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPTLSys.Location = new System.Drawing.Point(612, 13);
            this.lblPTLSys.Name = "lblPTLSys";
            this.lblPTLSys.Size = new System.Drawing.Size(127, 29);
            this.lblPTLSys.TabIndex = 3;
            this.lblPTLSys.Text = "                   ";
            this.lblPTLSys.Click += new System.EventHandler(this.lblPTLSys_Click);
            // 
            // lblPLCData
            // 
            this.lblPLCData.AutoSize = true;
            this.lblPLCData.BackColor = System.Drawing.Color.Transparent;
            this.lblPLCData.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPLCData.Location = new System.Drawing.Point(419, 13);
            this.lblPLCData.Name = "lblPLCData";
            this.lblPLCData.Size = new System.Drawing.Size(157, 29);
            this.lblPLCData.TabIndex = 2;
            this.lblPLCData.Text = "                        ";
            this.lblPLCData.Click += new System.EventHandler(this.lblPLCData_Click);
            // 
            // lblCustInfo
            // 
            this.lblCustInfo.AutoSize = true;
            this.lblCustInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCustInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustInfo.Location = new System.Drawing.Point(239, 14);
            this.lblCustInfo.Name = "lblCustInfo";
            this.lblCustInfo.Size = new System.Drawing.Size(145, 29);
            this.lblCustInfo.TabIndex = 1;
            this.lblCustInfo.Text = "                      ";
            this.lblCustInfo.Click += new System.EventHandler(this.lblCustInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "                            ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BackgroundImage = global::SorterAPI.Properties.Resources.close1;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Transparent;
            this.btnClose.Location = new System.Drawing.Point(1155, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(31, 23);
            this.btnClose.TabIndex = 20;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmSorterAPIConfigurator6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1194, 665);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSorterAPIConfigurator6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Sorter Configurator";
            this.Load += new System.EventHandler(this.frmSorterAPIConfigurator6_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHubList)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblBagAssignment;
        private System.Windows.Forms.Label lblLinkDB;
        private System.Windows.Forms.Label lblPTLSys;
        private System.Windows.Forms.Label lblPLCData;
        private System.Windows.Forms.Label lblCustInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox drpBinDown;
        private System.Windows.Forms.ComboBox drpBinUp;
        private System.Windows.Forms.Label lblDown;
        private System.Windows.Forms.Label lblUp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox drpBranch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblSuccesful;
        private System.Windows.Forms.Button btnSaveLocation;
        private System.Windows.Forms.DataGridView dgvHubList;
        private System.Windows.Forms.Button btnClose;
    }
}