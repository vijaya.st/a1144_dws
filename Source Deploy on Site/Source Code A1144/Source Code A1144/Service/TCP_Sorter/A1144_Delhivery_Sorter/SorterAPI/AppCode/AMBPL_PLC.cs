﻿using System;
using System.Collections.Generic;
using System.Text;
using S7.Net;
using System.Data;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;

namespace AMBPL.Common
{
    public class AMBPL_PLC
    {
        public enum PLC_Type
        {
            S7200,
            S7300,
            S7400,
            S71200,
            S71500
        }

        Plc plc = null;
        static ErrorLog oErrorLog = new ErrorLog();

        public AMBPL_PLC(PLC_Type plcType, string IP, short rack, short slot)
        {
            CpuType cpu = CpuType.S7300;
            switch (plcType)
            {
                case PLC_Type.S7400:
                    cpu = CpuType.S7400;
                    break;
                case PLC_Type.S7200:
                    cpu = CpuType.S7200;
                    break;
                case PLC_Type.S71200:
                    cpu = CpuType.S71200;
                    break;
                case PLC_Type.S71500:
                    cpu = CpuType.S71500;
                    break;
                case PLC_Type.S7300:
                    cpu = CpuType.S7300;
                    break;


            }
            plc = new Plc(cpu, IP, rack, slot);
            
        }

        public ErrorCode Open()
        {
            if (plc.IsConnected)
                plc.Close();

            DateTime startTime = DateTime.Now;
            ErrorCode err = ErrorCode.NoError;
            while ((DateTime.Now - startTime).TotalSeconds < 30)
            {
                err = plc.Open();
                if (err == ErrorCode.NoError)//connected
                {
                    // oErrorLog.WriteErrorLog("PLC Connection Opened successfully on IP " + plc.IP);
                    break;
                }


            }
            if (err != ErrorCode.NoError)
            {
                oErrorLog.WriteErrorLog("Error To connect PLC : " + err.ToString());
            }
            return err;
        }
        public void Close()
        {
            try {
                plc.Close();
               // oErrorLog.WriteErrorLog("PLC Connection closed on IP " + plc.IP);
            } catch (Exception ex) {
                
            }
        }
        public object Read(string dbAddress)
        {
            try
            {
                return plc.Read(dbAddress);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Read DBAddress" + dbAddress + " : " + ex.ToString());
                return null;
            }
        }
        public object Read(int DB, int startByteAdd, VarType varType, int varCount)
        {
            try
            {
                return plc.Read(DataType.DataBlock, DB, startByteAdd, varType, varCount);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Read DBAddress DB " + DB.ToString() + " : " + ex.ToString());
                return null;
            }
        }

        public string[] Read(string[] arrDBAddress)
        {
            string[] returnArr = new string[arrDBAddress.Length];
            try
            {
                for (int cnt = 0; cnt < arrDBAddress.Length; cnt++)
                {
                    object val = plc.Read(arrDBAddress[cnt]);
                    if (val != null)
                        returnArr[cnt] = Convert.ToString(val);
                    else
                        returnArr[cnt] = null;
                }
             
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Read DBAddress Array" + arrDBAddress.ToString()  +" : " + ex.ToString());
             
            }
            return returnArr;
        }

        public string ReadByteArray(int DB, int startByteAdd, int arrayLength)
        {
            string returnVal = "";
            try
            {
                byte[] obj = plc.ReadBytes(DataType.DataBlock, DB, startByteAdd, arrayLength); //plc.Read(DataType.DataBlock, DB, startByteAdd,VarType.Byte, arrayLength);
                if (obj != null)
                {
                    returnVal = RemoveSpecialCharacters( System.Text.Encoding.UTF8.GetString(obj));
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Read DBAddress DB " + DB.ToString() + " : " + ex.ToString());

            }
            return returnVal;
        }
        public string ReadString(int DB, int startByteAdd, int length)
        {
            string returnVal = "";
            try
            {
                object obj = this.Read(DB, startByteAdd, S7.Net.VarType.String, length);
                if (obj != null)
                {
                    returnVal = (string)obj;
                    returnVal = RemoveSpecialCharacters(returnVal);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Read DBAddress DB " + DB.ToString() + " : " + ex.ToString());

            }
            return returnVal;
        }

        public void Write(string DBaddress, object value)
        {
            ErrorCode err = plc.Write(DBaddress, value);
            if (err != ErrorCode.NoError)
                oErrorLog.WriteErrorLog("Error in writing value to PLC on " + DBaddress + " : " + err.ToString());
        }

        public void Write(string[] DBaddressValues)
        {
            foreach (string addval in DBaddressValues)
            {
                string[] arrAddVal = addval.Split(':');
                ErrorCode err = plc.Write(arrAddVal[0], arrAddVal[1]);
                if (err != ErrorCode.NoError)
                    oErrorLog.WriteErrorLog("Error in writing array to PLC on " + arrAddVal[0] + " : " + err.ToString());
            }

        }

        public int ReadClass(object sourceClass, int db, int startByteAdr)
        {
            try
            {
                return plc.ReadClass(sourceClass, db, startByteAdr);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in ReadClass for DB" + db.ToString() + " : " + ex.ToString());
                return 0;
            }
        }

        public Dictionary<string, object> ReadMultipleVars(List<S7.Net.Types.DataItem> dbList)
        {
            try
            {
                plc.ReadMultipleVars(dbList);
                return getnerateValueDictionary(dbList);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in ReadMultipleVars : " + ex.ToString());
                return null;
            }
        }
        public void ReadMultipleVars(List<S7.Net.Types.DataItem> dbList, List<PLCAddress> lstDBAdd)
        {
            try
            {
                plc.ReadMultipleVars(dbList);
                setMultipleVals(dbList, lstDBAdd);
            }  
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in ReadMultipleVars & Setting PLCAddress List : " + ex.ToString());
               
            }
}
        private void setMultipleVals(List<S7.Net.Types.DataItem> dbList, List<PLCAddress> lstDBAdd)
        {
            try
            {
                
                foreach (S7.Net.Types.DataItem item in dbList)
                {
                    //string DBAddressAll = "";
                    //for (int cnt = 0; cnt < item.Count; cnt++)
                    //{
                        

                    //    string dbAddress = "DB" + item.DB.ToString() + ".";

                    //    if (item.VarType == VarType.Int)
                    //    {
                    //        dbAddress += "DBW" + (item.StartByteAdr + (cnt * 2)).ToString();
                    //    }
                    //    else if (item.VarType == VarType.DInt)
                    //    {
                    //        dbAddress += "DBD" + (item.StartByteAdr + (cnt * 4)).ToString();
                    //    }
                    //    else if (item.VarType == VarType.Byte)
                    //    {
                    //        dbAddress += "DBB" + (item.StartByteAdr + (cnt * 1)).ToString();
                    //    }
                    //    else if (item.VarType == VarType.Bit)
                    //    {
                    //        int reminder = 0;
                    //        int addInt = Math.DivRem(cnt, 8, out reminder);
                    //        dbAddress += "DBX" + (item.StartByteAdr + addInt).ToString() + "." + reminder.ToString();
                    //    }

                    //    if (cnt == 0)
                    //        DBAddressAll = dbAddress;
                    //    else
                    //        DBAddressAll += "," + dbAddress;



                    //}
                    if (item.Count > 1)
                    {
                        object[] arrvals = null;
                        if (item.Value != null)
                        {
                            if (item.VarType == VarType.Int)
                            {
                                Int16[] vals = (Int16[])item.Value;
                                arrvals = Array.ConvertAll(vals, element => (object)element);

                            }
                            else if (item.VarType == VarType.DInt)
                            {
                                Int32[] vals = (Int32[])item.Value;
                                arrvals = Array.ConvertAll(vals, element => (object)element);
                            }
                            else if (item.VarType == VarType.Bit)
                            {
                                System.Collections.BitArray bitArray = (System.Collections.BitArray)item.Value;

                                Boolean[] vals = new Boolean[bitArray.Length];
                                for (int bcnt = 0; bcnt < bitArray.Length; bcnt++)
                                {
                                    vals[bcnt] = Convert.ToBoolean(bitArray[bcnt]);
                                }
                                arrvals = Array.ConvertAll(vals, element => (object)element);
                            }
                        }
                        int cnt = 0;
                        string[] arrPlcAdd = (string[])item.PLCAddress;
                        //foreach (string dbAdd in DBAddressAll.Split(','))
                         foreach (string dbAdd in arrPlcAdd)
                            {
                                IEnumerable<PLCAddress> add1 = lstDBAdd.Where(x => x.DBAddress == dbAdd);
                            if (add1.Count<PLCAddress>() > 0)
                            {
                                PLCAddress plcAdd = add1.ElementAt<PLCAddress>(0);
                                try
                                {
                                    plcAdd.PLCValue = arrvals[cnt];
                                }
                                catch (Exception ex) { plcAdd.PLCValue = null; }

                            }
                            //add1.PLCValue = arrvals[cnt];
                            //_dicPLCDBValues.Add(dbAdd, arrvals[cnt]);
                            cnt++;
                        }
                    }
                    else
                    {
                        IEnumerable<PLCAddress> add1 = lstDBAdd.Where(x => x.DBAddress == Convert.ToString(item.PLCAddress));
                        if (add1.Count<PLCAddress>() > 0)
                        {
                            PLCAddress plcAdd = add1.ElementAt<PLCAddress>(0);
                            plcAdd.PLCValue = item.Value;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in setMultipleVals :" + ex.ToString());
            }
         }
        private Dictionary<string, object> getnerateValueDictionary(List<S7.Net.Types.DataItem> dbList)
        {
            Dictionary<string, object> _dicPLCDBValues = new Dictionary<string, object>();
            foreach (S7.Net.Types.DataItem item in dbList)
            {
                string DBAddressAll = "";
                for (int cnt = 0; cnt < item.Count; cnt++)
                {

                    string dbAddress = "DB" + item.DB.ToString() + ".";
                    if (item.VarType == VarType.Int)
                    {
                        dbAddress += "DBW" + (item.StartByteAdr + (cnt * 2)).ToString();
                    }
                    else if (item.VarType == VarType.DInt)
                    {
                        dbAddress += "DBD" + (item.StartByteAdr + (cnt * 4)).ToString();
                    }
                    else if (item.VarType == VarType.Byte)
                    {
                        //dbAddress += "DBB" + (item.StartByteAdr + (cnt * 1)).ToString();
                    }

                    if (cnt == 0)
                        DBAddressAll = dbAddress;
                    else
                        DBAddressAll += "," + dbAddress;


                    
                }
                if (item.Count > 1)
                {
                    object[] arrvals = null;
                    if (item.VarType == VarType.Int)
                    {
                        Int16[] vals = (Int16[])item.Value;
                        arrvals = Array.ConvertAll(vals, element => (object)element);

                    }
                    else if (item.VarType == VarType.DInt)
                    {
                        Int32[] vals = (Int32[])item.Value;
                        arrvals = Array.ConvertAll(vals, element => (object)element);
                    }

                    int cnt = 0;
                    foreach (string dbAdd in DBAddressAll.Split(','))
                    {
                        _dicPLCDBValues.Add(dbAdd, arrvals[cnt]);
                        cnt++;
                    }
                }
                else
                {
                    _dicPLCDBValues.Add(DBAddressAll, item.Value);
                }


            }
                return _dicPLCDBValues;

        }
        private void setDictionaryValues(List<S7.Net.Types.DataItem> DBList, Dictionary<string, object> _dicPLCDBValues)
        {
            for (int cnt = 0; cnt < DBList.Count; cnt++)
            {
                S7.Net.Types.DataItem item = (S7.Net.Types.DataItem)DBList[cnt];

                if (item.VarType == VarType.Int)
                {
                    if (item.Count > 1)
                    {
                        string valType = item.Value.ToString();


                        if (valType.IndexOf("Int16") != -1)
                        {
                            object[] arar = item.Value as object[];
                            Int16[] vals = (Int16[])item.Value;
                            string[] array2 = Array.ConvertAll(vals, element => element.ToString());
                            for (int valCnt = 0; valCnt < vals.Length; valCnt++)
                            {
                                try
                                {
                                   // _dicPLCDBValues.= vals[valCnt];
                                }
                                catch (Exception ex) { }
                            }
                        }
                        else if (valType.IndexOf("Int32") != -1)
                        {
                            int[] vals = (int[])item.Value;
                            for (int valCnt = 0; valCnt < vals.Length; valCnt++)
                            {
                                try
                                {
                                   // _dicPLCDBValues[DBList_Address[cnt][valCnt]] = vals[valCnt];
                                }
                                catch (Exception ex) { }
                            }



                        }
                        //int[] vals =(int[]) item.Value;
                    }
                    else
                    {
                      //  _dicPLCDBValues[DBList_Address[cnt][0]] = item.Value;
                    }

                  
                }
            }
        }

        private static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.?]+", "", RegexOptions.Compiled);
        }

    }
}
