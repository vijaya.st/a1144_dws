﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMBPL.Common
{
 public class ExternalDataSource
    {
        public string EquipmentType { get; set; }
        public string TypeOfData { get; set; }
        public string PLCLabel { get; set; }
       
        
        public string CommandToRead { get; set; }

    }
}
