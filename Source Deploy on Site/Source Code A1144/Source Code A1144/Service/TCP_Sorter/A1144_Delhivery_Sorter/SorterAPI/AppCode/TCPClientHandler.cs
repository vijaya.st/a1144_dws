﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

namespace AMBPL.Common
{

    public class TCPClientHandler
    {

        Thread _thread;
        public event EventHandler StringReceived;
        public event EventHandler ConnectionLost;
        public event EventHandler ConnectionSuccess;
        public event EventHandler ConnectionError;
        public System.Net.Sockets.TcpClient clientSocket = new System.Net.Sockets.TcpClient();
        NetworkStream serverStream;
        DateTime lastSuccessTime = DateTime.Now;
        Boolean isLive = false;
        string readCommand = string.Empty;
        private System.Timers.Timer timerConnection;
        public string ReceivingMessageEnd; //end string of receiving message so can understand end of receiving message
        public string SendingMessageEnd;//end string of sending message so can understand end of message from receiver

        ErrorLog elog = new ErrorLog();
     
        #region Properties
        public string IP { get; set; }
        public int Port { get; set; }
        public string DataReceived { get; set; }

        public string DeviceName { get; set; }
        public bool IsRemoveSpecialCharacters { get; set; }

        public bool IsInitiateDataReceiveEvent { get; set; }

        public bool IsConnected { get { return isLive; } }
        public string stringData = "";
        private string dataPacket = "";
        public string ReadCommand
        {
            get { return readCommand; }
            set { readCommand = value; }
        }

        public string EquipmentType { get; set; }

        public bool AutoConnnectOnDisconnect { get; set; }
        #endregion

        #region Initialisation
        public TCPClientHandler(string ip, int port)
        {
            this.IP = ip;
            this.Port = port;
            IsInitiateDataReceiveEvent = false;
            IsRemoveSpecialCharacters = false;
            AutoConnnectOnDisconnect = false;
            ReceivingMessageEnd = "";
            SendingMessageEnd = "";
            
        }

        public void Connect()
        {

            try
            {
                connect();
            }
            catch (SocketException e)
            {
                TCP_ConnectionEventArgs eventArgs = new TCP_ConnectionEventArgs();
                eventArgs.Error = e.Message;
                eventArgs.DeviceName = this.DeviceName;
                OnConnectionError(eventArgs);
            }
            finally
            {
                if (isLive || AutoConnnectOnDisconnect)
                {
                    if (IsInitiateDataReceiveEvent)
                    {
                        _thread = new Thread(doListen);
                        _thread.Start();
                    }
                    else
                    {
                        _thread = new Thread(doTestConnection);
                        _thread.Start();
                    }
                }


            }


        }

        private void connect()
        {
            try
            {
                clientSocket = new TcpClient();
                clientSocket.Connect(this.IP, this.Port);
                serverStream = clientSocket.GetStream();
                lastSuccessTime = DateTime.Now;
                isLive = true;
                TCP_ConnectionEventArgs eventArgs = new TCP_ConnectionEventArgs();
                eventArgs.Error ="";
                eventArgs.DeviceName = this.DeviceName;
                OnConnectionSuccess(eventArgs);
            }
            catch (SocketException e)
            {
                throw e;

            }


        }
        #endregion

        #region Events
        protected virtual void OnStringReceived(TCP_DataReceviedEventArgs e)
        {
            if (StringReceived != null)
                StringReceived(this, e);
        }

        protected virtual void OnConnectionLost(TCP_ConnectionEventArgs e)
        {
            if (ConnectionLost != null)
                ConnectionLost(this, e);
        }

        protected virtual void OnConnectionSuccess(TCP_ConnectionEventArgs e)
        {
            if (ConnectionSuccess != null)
                ConnectionSuccess(this, e);
        }
        protected virtual void OnConnectionError(TCP_ConnectionEventArgs e)
        {
            if (ConnectionError != null)
                ConnectionError(this, e);
        }
        #endregion

        #region ContinueDataReading
        private void doListen()
        {
            timerConnection = new System.Timers.Timer(10);
            timerConnection.Elapsed += ReadContinuous;

            timerConnection.Start();

        }

        private void ReadContinuous(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerConnection.Stop();
            try
            {
                if (isLive)
                {
                    bool blockingState = clientSocket.Client.Blocking;
                    stringData = "";
                    if (serverStream.DataAvailable)
                    {
                        byte[] inStream = new byte[clientSocket.Available];

                        serverStream.Read(inStream, 0, clientSocket.Available);
                        string strTemp = System.Text.Encoding.ASCII.GetString(inStream);
                        serverStream.Flush();

                        if (ReceivingMessageEnd != "")
                        {
                            int indEnd = strTemp.IndexOf(ReceivingMessageEnd);
                            if (indEnd != -1)
                            {
                                stringData = dataPacket + strTemp.Substring(0, strTemp.LastIndexOf(ReceivingMessageEnd) + ReceivingMessageEnd.Length);
                                dataPacket = strTemp.Substring(strTemp.LastIndexOf(ReceivingMessageEnd) + ReceivingMessageEnd.Length);
                            }
                            else
                                dataPacket += strTemp;
                        }
                        else
                            stringData = strTemp;

                        if (IsRemoveSpecialCharacters)
                            stringData = RemoveSpecialCharacters(stringData);

                        DataReceived = stringData;
                        lastSuccessTime = DateTime.Now;
                    }
                    else
                    {
                        if (AutoConnnectOnDisconnect)
                        {
                            CheckConnectionStatus();
                            stringData = "";
                        }
                    }
                    if (stringData.Trim() != "")
                    {
                        TCP_DataReceviedEventArgs eventArgs = new TCP_DataReceviedEventArgs();
                        eventArgs.DeviceName = this.DeviceName;
                        eventArgs.Data = stringData;
                        OnStringReceived( eventArgs);
                    }
                  
                    CheckConnectionStatus();
                    
                }
              
            }
            catch (Exception ex)
            {
                elog.WriteErrorLog("Error in ReadContinuous >> "+ ex.ToString());
            }
            if (isLive)
            {
                timerConnection.Start();
            }
            else if (AutoConnnectOnDisconnect)
            {
                try
                {
                    connect();

                }
                catch (Exception ex) { }
                finally { timerConnection.Start(); }
            }
        }

        private void CheckConnectionStatus()
        {
            DateTime currentTime = DateTime.Now;
            if ((currentTime - lastSuccessTime).TotalSeconds > 5)
            {
                
                bool part1 = clientSocket.Client.Poll(1000, SelectMode.SelectRead);
                bool part2 = (clientSocket.Client.Available == 0);
                if (part1 && part2)
                {//connection is closed
                    isLive = false;
                        Close();
                }
                else
                    lastSuccessTime = DateTime.Now;
            }
            
        }
     
        #endregion

        #region TriggerBaseDataReading
        /// <summary>
        /// Test connection with time delay of 1 second
        /// </summary>
        private void doTestConnection()
        {
            timerConnection = new System.Timers.Timer(1000);
            timerConnection.Elapsed += TestConnection; ;
            timerConnection.Start();
        }

        private void TestConnection(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerConnection.Stop();
            if (isLive)
            {
                CheckConnectionStatus();

            }
            else if (AutoConnnectOnDisconnect)
            {
                try
                {
                    connect();

                }
                catch (Exception ex) { }
                
            }
            timerConnection.Start();
        }

        public string ReadData()
        {
            if (!isLive)
                return "";

            string returndata = "";
            try
            {
                if (readCommand != "")
                {
                    string commandData = readCommand + System.Environment.NewLine;
                    Byte[] data = System.Text.Encoding.ASCII.GetBytes(commandData);
                    serverStream.Write(data, 0, data.Length);
                    Thread.Sleep(500);
                }

                if (serverStream.DataAvailable)
                {
                    byte[] inStream = new byte[clientSocket.Available];
                    serverStream.Read(inStream, 0, (int)clientSocket.Available);
                    returndata = System.Text.Encoding.ASCII.GetString(inStream);
                    //returndata = returndata.Replace("\n", "");
                    //returndata = returndata.Replace("\0", "");
                    if (IsRemoveSpecialCharacters)
                        returndata = RemoveSpecialCharacters(returndata);
                    serverStream.Flush();

                }

            }
            catch (Exception ex) { }
            return returndata;
        }


        #endregion

        #region Dispose
        ~TCPClientHandler()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                serverStream.Close();
                serverStream.Dispose();
                clientSocket.Close();
            }
            catch (Exception ex) { }
            if (_thread != null)
            {
                if (_thread.IsAlive)
                    _thread.Abort();


            }
        }

        public void WriteToServer(string Data)
        {
            if (isLive)
            {try
                {
                    NetworkStream networkStream = clientSocket.GetStream();
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(Data+SendingMessageEnd );
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    networkStream.Flush();
                }
                catch (Exception ex) { throw ex; }
            }
        }
        #endregion

        private static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.?]+", "", RegexOptions.Compiled);
        }

        public void Close()
        {
           
            serverStream.Close();
            clientSocket.Close();
            TCP_ConnectionEventArgs eventArgs = new TCP_ConnectionEventArgs();
            eventArgs.Error = "Disconnected";
            eventArgs.DeviceName = this.DeviceName;
            OnConnectionLost(eventArgs);
            isLive = false;
        }

    }

    public class TCP_ConnectionEventArgs : EventArgs
    {
        public string DeviceName { get; set; }
        public string Error { get; set; }
      
    }

    public class TCP_DataReceviedEventArgs : EventArgs
    {
        public string DeviceName { get; set; }
        public string Data { get; set; }

    }

}
