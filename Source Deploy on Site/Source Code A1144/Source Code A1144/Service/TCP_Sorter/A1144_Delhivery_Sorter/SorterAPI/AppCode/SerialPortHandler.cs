﻿using System;
using System.Text.RegularExpressions;
using System.IO.Ports;
using System.Threading;

namespace AMBPL.Common
{
    public class SerialPortHandler : IDisposable
    {

        string readCommand = string.Empty;
        Thread _thread;
        private System.Timers.Timer timerConnection;

        ErrorLog elog;

        public event EventHandler StringReceived;
        public event EventHandler ConnectionLost;
        public event EventHandler ConnectionSuccess;
        public event EventHandler ConnectionError;
        ErrorLog Log = new ErrorLog();
        #region properties
        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public string Parity { get; set; }

        public string StopBits { get; set; }

        public string DataReceived { get; set; }
        public bool InitiateDataReceiveEvent { get; set; }
        public bool RemoveSpecialCharacters { get; set; }
        public string DeviceName { get; set; }
        public bool AutoConnnectOnDisconnect { get; set; }
        public bool IsConnected
        {
            get {
                return (port == null ? false : port.IsOpen);
            }
        }
        public string ReadCommand
        {
            get { return readCommand; }
            set { readCommand = value; }
        }
        #endregion


        SerialPort port = null;
        

        public SerialPortHandler(string portName, int boudRate, string parity, string stopBits)
        {
            this.PortName = portName;
            this.BaudRate = boudRate;
            this.Parity = parity;
            this.StopBits = stopBits;
            InitiateDataReceiveEvent = false;
            RemoveSpecialCharacters = false;

            elog = new ErrorLog();
        }
     
        public void OpenPort()
        {
            if (port != null && port.IsOpen)
                port.Close();

            port = new SerialPort();
            port.PortName = PortName;
            port.BaudRate = BaudRate;
            //port.Parity = (Parity)Enum.Parse(typeof(Parity), Parity);
            port.Parity = SetParity();
            //port.StopBits = (StopBits)Enum.Parse(typeof(StopBits), StopBits);
            port.StopBits = SetStopBits();
            //            port.DataReceived += Port_DataReceived;
            port.DataBits = 8;
            port.ReadTimeout = 2000;
            port.WriteTimeout = 2000;

         

            port.PinChanged += Port_PinChanged;
            port.ErrorReceived += Port_ErrorReceived;
            if (InitiateDataReceiveEvent)
            {
                port.DataReceived += new SerialDataReceivedEventHandler(Port_DataReceived);
            }
            if (!port.IsOpen)
            {
                Open();

            }

            if (AutoConnnectOnDisconnect)
            {
                _thread = new Thread(doTestConnection);
                _thread.Start();
            }
        }
        private void Open()
        {
            try
            {
               
                port.Open();
                if (port.IsOpen)
                {
                    OnConnectionSuccess(new EventArgs());
                    Log.WriteErrorLog("Connection Success..");

                }
                else
                {
                    SerialPort_ConnectionErrorEventArgs eventArgs = new SerialPort_ConnectionErrorEventArgs();
             
                    eventArgs.Error = "Serial Port Connection Error";
                    OnConnectionError(eventArgs);
                    Log.WriteErrorLog("Connection Error..");
                }
            }
            catch (Exception ex)
            {
                SerialPort_ConnectionErrorEventArgs eventArgs = new SerialPort_ConnectionErrorEventArgs();
                eventArgs.Error = ex.Message;
                OnConnectionError(eventArgs);
                Log.WriteErrorLog("Connection Error..");
            }
        }

        private void doTestConnection()
        {
            timerConnection = new System.Timers.Timer(1000);
            timerConnection.Elapsed += TestConnection; ;
            timerConnection.Start();
        }

        private void TestConnection(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerConnection.Stop();
            if (!port.IsOpen)
            {
                Open();
            }

        }

        private void Port_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            // throw new NotImplementedException();
            SerialPort_ConnectionErrorEventArgs err = new SerialPort_ConnectionErrorEventArgs();
            err.Error = "Error in Serial Port Connection: Error Received";
            OnConnectionLost(err);
            try
            {
                port.Close();
            }
            catch (Exception ex) { }
        }

        private void Port_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            SerialPinChange SerialPinChange1 = 0;
           
            SerialPinChange1 = e.EventType;
            SerialPort_ConnectionErrorEventArgs err = new SerialPort_ConnectionErrorEventArgs();
            err.Error = "Error in Serial Port Connection: PinChanged";

            switch (SerialPinChange1)
            {
                case SerialPinChange.Break :
                   // elog.WriteErrorLog("Error in Serial Port Connection : Break is Set");
                    err.Error = "Error in Serial Port Connection: Break is Set";
                    //MessageBox.Show("Break is Set");
                    break;
                case SerialPinChange.CDChanged:
                    //elog.WriteErrorLog("Error in Serial Port Connection : CDChanged");
                    err.Error = "Error in Serial Port Connection: CDChanged";
                    //  MessageBox.Show("CD = " + signalState.ToString());
                    break;
                case SerialPinChange.CtsChanged:
                    //signalState = ComPort.CDHolding;
                    //lblCTSStatus.BackColor = Color.Red;
                    //elog.WriteErrorLog("Error in Serial Port Connection : CTS");
                    //MessageBox.Show("CTS = " + signalState.ToString());
                    err.Error = "Error in Serial Port Connection: CTS";
                    break;
                case SerialPinChange.DsrChanged:
                    //signalState = ComPort.DsrHolding;
                    //lblDSRStatus.BackColor = Color.Red;
                    //elog.WriteErrorLog("Error in Serial Port Connection : DsrChanged");
                    // MessageBox.Show("DSR = " + signalState.ToString());
                    err.Error = "Error in Serial Port Connection: DsrChanged";
                    break;
                case SerialPinChange.Ring:
                    //elog.WriteErrorLog("Error in Serial Port Connection : Ring");
                    //lblRIStatus.BackColor = Color.Red;
                    //MessageBox.Show("Ring Detected");
                    err.Error = "Error in Serial Port Connection: Ring";
                    break;
            }
            OnConnectionLost(err);
            try
            {
                port.Close();
            }
            catch (Exception ex) { }
        }

        private System.IO.Ports.Parity SetParity()
        {
           string  parity = Parity.ToLower().Trim();
            System.IO.Ports.Parity portParity = System.IO.Ports.Parity.None;
            switch (parity)
            {
                case "even":
                    portParity = System.IO.Ports.Parity.Even;
                    break;
                case "mark":
                    portParity = System.IO.Ports.Parity.Mark;
                    break;
                case "none":
                    portParity = System.IO.Ports.Parity.None;
                    break;
                case "odd":
                    portParity = System.IO.Ports.Parity.Odd;
                    break;
                case "space":
                    portParity = System.IO.Ports.Parity.Space;
                    break;
               
            }
            return portParity;
        }
        public string ReadData()
        {
            string stringData = "";

            try
            {
                if (readCommand != "")
                {
                    string commandData = readCommand + System.Environment.NewLine;

                    port.WriteLine(commandData);
                    elog.WriteErrorLog("Port Command to port ");
                    Thread.Sleep(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SleepTime"]));
                }
                if (port.BytesToRead > 0)
                {
                    stringData = port.ReadLine();
                   // elog.WriteErrorLog("Port ReadLine Succesful >> " + stringData);

                    if (stringData == "")
                    {
                        stringData = port.ReadExisting();
                     //   elog.WriteErrorLog("Port ReadExisting Succesful >> " + stringData);
                    }
                }

                port.DiscardInBuffer();
                port.DiscardOutBuffer();


                if (stringData == null)
                    stringData = "";
                

                if (RemoveSpecialCharacters)
                    stringData = _RemoveSpecialCharacters(stringData);

                DataReceived = stringData;

            }
            catch (Exception ex) { //elog.WriteErrorLog("Error in Serial Port Read Data : " + ex.ToString()); 
            }
            return stringData;
        }
        private System.IO.Ports.StopBits SetStopBits()
        {
            string stopbit = StopBits.ToLower().Trim();
            System.IO.Ports.StopBits portSB = System.IO.Ports.StopBits.One;
            switch (stopbit)
            {
                case "one":
                    portSB = System.IO.Ports.StopBits.One;
                    break;
                case "onepointfive":
                    portSB = System.IO.Ports.StopBits.OnePointFive;
                    break;
                case "none":
                    portSB = System.IO.Ports.StopBits.None;
                    break;
                case "two":
                    portSB = System.IO.Ports.StopBits.Two;
                    break;
               

            }
            return portSB;
        }
        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
          
            string stringData =  port.ReadLine();
            if (stringData == null)
                stringData = "";

            if (stringData.Trim() == "")
                stringData = port.ReadExisting();
            if(RemoveSpecialCharacters)
                stringData = _RemoveSpecialCharacters(stringData);
            DataReceived = stringData;

            if (stringData.Trim() != "")
            {
                SerialPort_DataReceviedEventArgs eventArgs = new SerialPort_DataReceviedEventArgs();
                eventArgs.DeviceName = this.DeviceName;
                eventArgs.Data = stringData;
                OnStringReceived(eventArgs);
            
                
            }
        }


        #region Events
        protected virtual void OnStringReceived(SerialPort_DataReceviedEventArgs e)
        {
            if (StringReceived != null)
                StringReceived(this, e);
        }


        protected virtual void OnConnectionLost(SerialPort_ConnectionErrorEventArgs e)
        {
            if (ConnectionLost != null)
                ConnectionLost(this, e);
        }

        protected virtual void OnConnectionSuccess(EventArgs e)
        {
            if (ConnectionSuccess != null)
                ConnectionSuccess(this, e);
        }
        protected virtual void OnConnectionError(SerialPort_ConnectionErrorEventArgs e)
        {
            if (ConnectionError != null)
                ConnectionError(this, e);
        }
        //protected virtual void OnStringReceived(SerialDataReceivedEventArgs e)
        //{
        //    if (StringReceived != null)
        //        StringReceived(this, e);
        //}
        #endregion

        private static string _RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.?]+", "", RegexOptions.Compiled);
        }

        #region dispose
        ~SerialPortHandler()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                port.DataReceived -= new SerialDataReceivedEventHandler(Port_DataReceived);
            }
            // Releasing serial port (and other unmanaged objects)
            if (port != null)
            {
                if (port.IsOpen)
                    port.Close();

                port.Dispose();
            }
        }
        #endregion
    }
    public class SerialPort_ConnectionErrorEventArgs : EventArgs
    {
        public string Error { get; set; }
    }
    public class SerialPort_DataReceviedEventArgs : EventArgs
    {
        public string DeviceName { get; set; }
        public string Data { get; set; }

    }
}
