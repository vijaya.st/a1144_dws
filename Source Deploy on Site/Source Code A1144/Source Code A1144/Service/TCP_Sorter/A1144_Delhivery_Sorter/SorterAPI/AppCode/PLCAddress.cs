﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMBPL.Common
{
  public  class PLCAddress
    {
        public string DBAddress;
        public string Label;
        public string Type;
        public string Mode;
        public short IsForPA;
        public short IsPACounter;
        public string ConveyorLabel;
        public string LineLabel;
        public string SignalValue;
        public object PLCValue;
        public bool IsInPLCDBTable;

        public PLCAddress() { IsInPLCDBTable = false; }
    }
}
