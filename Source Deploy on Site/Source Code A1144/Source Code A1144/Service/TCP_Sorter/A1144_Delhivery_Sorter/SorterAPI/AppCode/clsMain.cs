﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace AMBPL.Common
{
    public class clsMain
    {
        static ErrorLog oErrorLog = new ErrorLog();
        public static DataSet GetAWSSetting(int AWSLineNo)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("AWS_Setting_Get");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_LineNo", AWSLineNo);
                ds = ClsMySqlDataAcess.GetDataSet("AWS_Setting_Get", cmd);
            }
            catch (Exception ex) { throw ex; }

            return ds;
        }
        public static DataSet GetExternalDataSources(string ProjectCode, int AWSLineNo)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@p_ProjectCode", ProjectCode);
                cmd.Parameters.AddWithValue("@p_LineNo", AWSLineNo);
                ds = ClsMySqlDataAcess.GetDataSet("SC_ExternalDataSource_GetList_WinService", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in GetExternalDataSources :" + ex.ToString());
            }
            return ds;
        }
        public static void UpdateExtenalDataSource_Status(string deviceLabel, string ProjectCode, int status, int Line)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@p_Label", deviceLabel);
                cmd.Parameters.AddWithValue("@p_Status", status);
                cmd.Parameters.AddWithValue("@p_ProjectCode", ProjectCode);
                cmd.Parameters.AddWithValue("@p_Line", Line);
                ClsMySqlDataAcess.ExecuteNonQuery("Master_ExternalDataSource_UpdateStatus", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in UpdateExtenalDataSource_Status :" + ex.ToString());
            }
        }
        public static DataTable BarcodeInsert(string barcode, string w)
        {
            DataTable result = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("DWS_Data_Log_Insert");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Barcode", barcode);
                cmd.Parameters.AddWithValue("@Weight", w);
                result = clsDataAccess.GetDataTable("DWS_Data_Log_Insert", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }
        public static DataTable FaultyBarcodeInsert(string barcode)
        {
            DataTable result = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("Log_SystemAlerts_FaultyBarcode_Insert");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Barcode", barcode);
                result = clsDataAccess.GetDataTable("Log_SystemAlerts_FaultyBarcode_Insert", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }

        public static DataSet GetAllDbAddresses(int CellNo)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();

                cmd.Parameters.AddWithValue("@CellNo", CellNo);
                ds = clsDataAccess.GetDataSet("MasterDB_GetAllDB", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in WebScada_GetSetting" + ex.ToString());
            }
            return ds;
        }

        public static int UpdateWeight(string barcode, string weight)
        {
            int result = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("DWS_Data_Log_Update");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Barcode", barcode);
                cmd.Parameters.AddWithValue("@Weight", weight);

                result = clsDataAccess.ExecuteNonQuery("DWS_Data_Log_Update", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }
        public static DataSet UpdateLWHWithReson(int Length, int Width, int Height, decimal Weight, int RV, int LiquidVolume, int PackageType, string ReasonForFailed,int FailedCount)
        {
            DataSet result = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("DWS_Data_Log_LWH_Update_With_Reson");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@L", Length);
                cmd.Parameters.AddWithValue("@W", Width);
                cmd.Parameters.AddWithValue("@H", Height);
                cmd.Parameters.AddWithValue("@Weight", Weight);
                cmd.Parameters.AddWithValue("@RV", RV);
                cmd.Parameters.AddWithValue("@LiquidVolume", LiquidVolume);
                cmd.Parameters.AddWithValue("@PackageType", PackageType); 
                cmd.Parameters.AddWithValue("@ReasonForFailed", ReasonForFailed); 
                    cmd.Parameters.AddWithValue("@FailedCount", FailedCount);
                result = clsDataAccess.GetDataSet("DWS_Data_Log_LWH_Update_With_Reson", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }
        public static DataSet UpdateLWH(int Length, int Width, int Height, decimal Weight, int RV, int LiquidVolume, int PackageType)
        {
            DataSet result = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("DWS_Data_Log_LWH_Update");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@L", Length);
                cmd.Parameters.AddWithValue("@W", Width);
                cmd.Parameters.AddWithValue("@H", Height);
                cmd.Parameters.AddWithValue("@Weight", Weight);
                cmd.Parameters.AddWithValue("@RV", RV); 
                cmd.Parameters.AddWithValue("@LiquidVolume", LiquidVolume);
                cmd.Parameters.AddWithValue("@PackageType", PackageType);
                result = clsDataAccess.GetDataSet("DWS_Data_Log_LWH_Update", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }

        public static DataSet UpdateFlag()
        {
            DataSet result = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("Master_FaulDetails_Faults_Update");
                result = clsDataAccess.GetDataSet("Master_FaulDetails_Faults_Update", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }

        public static DataSet UpdateSendStaus(string barcode)
        {
            DataSet result = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_UdateDataNotSendStatus");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@barcode", barcode);
                result = clsDataAccess.GetDataSet("sp_UdateDataNotSendStatus", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }
        public static DataTable CheckDataStatus()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("DWS_Data_Send_CheckDataSend");
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@L", Length);
                //cmd.Parameters.AddWithValue("@W", Width);
                //cmd.Parameters.AddWithValue("@H", Height);
                return ClsMySqlDataAcess.GetDataTable("DWS_Data_Send_CheckDataSend", cmd);

            }

            catch (Exception ex) { throw ex; }

        }
        public static DataTable CheckDuplicateBarcode(string barcode)
        {

            try
            {
                SqlCommand cmd = new SqlCommand("DWS_Data_Send_CheckDuplicateBarcode");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Barcode", barcode);
                return ClsMySqlDataAcess.GetDataTable("DWS_Data_Send_CheckDuplicateBarcode", cmd);

            }

            catch (Exception ex) { throw ex; }

        }
        public static DataTable Fault_GetTagName()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                dt = clsDataAccess.GetDataTable("Master_FaultDetails_getTagName", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Master_FaultDetails_getTagName" + ex.ToString());
            }
            return dt;
        }
        
        public static DataSet GetAlertsDbAddresses()
        {
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();

                //cmd.Parameters.AddWithValue("@CellNo", CellNo);
                ds = clsDataAccess.GetDataSet("Master_FaultDetails_getDBAddress", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Master_FaultDetails_getDBAddress" + ex.ToString());
            }
            return ds;
        }

        public static int SystemIdleTime(int IdleStatus, string RobotIdleTag, string ErrorDescription)
        {
            int retVal = 0;
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add("@IdleStatus", SqlDbType.VarChar).Value = IdleStatus;
            cmd.Parameters.Add("@RobotIdleTag", SqlDbType.VarChar).Value = RobotIdleTag;
            //cmd.Parameters.Add("@CellNo", SqlDbType.VarChar).Value = CellNo;
            cmd.Parameters.Add("@ErrorDescription", SqlDbType.VarChar).Value = ErrorDescription;
            Object objResult = clsDataAccess.ExecuteScalar("Log_Reasonwise_Tdle_Time_Alerts_Insert", cmd);
            if (objResult != null)
                retVal = Convert.ToInt32(objResult);
            return retVal;
        }

        public static void UpdateFaultStatus(string xmlFault)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@xmlFault", SqlDbType.Xml).Value = xmlFault;
                clsDataAccess.ExecuteNonQuery("Master_FaultDetails_Update", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in UpdateFaultStatus :" + ex.ToString());
            }
        }
        
        public static void DeleteDWSRecord()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                clsDataAccess.ExecuteNonQuery("DWS_Data_Log_Delete_Record", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in DWS_Data_Log_Delete_Record :" + ex.ToString());
            }
        }
        
        public static void PLCLANStatus(string PLCLANCableDisconnected,int OperationType)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure; 
                cmd.Parameters.AddWithValue("@PLCLANCableDBAddress", PLCLANCableDisconnected);
                cmd.Parameters.AddWithValue("@OperationType", OperationType);
                clsDataAccess.ExecuteNonQuery("Master_FaultDetails_PLCLAN_Status_Update", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Master_FaultDetails_PLCLAN_Status_Update :" + ex.ToString());
            }
        }

        public static DataTable CheckZeroBarcode()
        {

            try
            {
                SqlCommand cmd = new SqlCommand("DWS_Data_Send_CheckZeroLWH");
                cmd.CommandType = CommandType.StoredProcedure;
                return ClsMySqlDataAcess.GetDataTable("DWS_Data_Send_CheckZeroLWH", cmd);

            }

            catch (Exception ex) { throw ex; }

        }
        public static int AWSDataInsert(string barcode, float weight, int line)
        {
            int result = 0;
            string InstallCode = "";

            if (line == 1)
            {
                InstallCode = "fk-mal-b-arm-01";
            }
            else if (line == 2)
            {
                InstallCode = "fk-mal-b-arm-02";
            }
            else if (line == 3)
            {
                InstallCode = "fk-mal-b-arm-03";
            }
            else if (line == 4)
            {
                InstallCode = "fk-mal-b-arm-04";
            }
            else if (line == 5)
            {
                InstallCode = "fk-mal-b-arm-05";
            }
            else if (line == 6)
            {
                InstallCode = "fk-mal-b-arm-06";
            }
            else if (line == 7)
            {
                InstallCode = "fk-mal-b-arm-07";
            }
            else if (line == 8)
            {
                InstallCode = "fk-mal-b-arm-08";
            }

            try
            {
                SqlCommand cmd = new SqlCommand("AWS_Data_Insert");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@p_LineNo", line);
                cmd.Parameters.AddWithValue("@p_BarcodeStr", barcode);
                cmd.Parameters.AddWithValue("@p_WeightKg", weight);
                cmd.Parameters.AddWithValue("@p_InstallCode", InstallCode);
                result = ClsMySqlDataAcess.ExecuteNonQuery("AWS_Data_Insert", cmd);
            }

            catch (Exception ex) { throw ex; }
            return result;
        }
        public static int UpdateStatusDeviceIP(string xml)
        {
            xml = xml.Replace("\r", "");
            xml = xml.Replace("\t", "");
            xml = xml.Replace("\n", "");

            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@xml", xml);
                //cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status;
                return clsDataAccess.ExecuteNonQuery("UpdateSystemMonitorIPStatus", cmd);
            }
            catch (Exception e)
            {
                return 0;
            }


        }
        public static void UpdateRejection_count(int count, int Line)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("p_Count", count);
                //cmd.Parameters.AddWithValue("@p_Status", status);
                //cmd.Parameters.AddWithValue("@p_ProjectCode", ProjectCode);
                cmd.Parameters.AddWithValue("p_Line", Line);
                ClsMySqlDataAcess.ExecuteNonQuery("Update_RejectionCount", cmd);
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in UpdateExtenalDataSource_Status :" + ex.ToString());
            }
        }
        public static DataSet GetforSAPAPI()
        {
            SqlCommand cmd = new SqlCommand();
            return ClsMySqlDataAcess.GetDataSet("sp_DatasendToSAP_Get", cmd);
        }
        public static DataSet GetforBarcode()
        {
            SqlCommand cmd = new SqlCommand();
            return ClsMySqlDataAcess.GetDataSet("sp_DatasendToSAP_Get_Barcode", cmd);
        }
        public static int UpdateIsSendData(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            return ClsMySqlDataAcess.ExecuteNonQuery("sp_IsSend_Update", cmd);
        }
        public static int UpdateIsSendDataByBarcode(string barcode, int status)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@barcode", barcode);
            cmd.Parameters.AddWithValue("@Status", status);
            return ClsMySqlDataAcess.ExecuteNonQuery("sp_DatasendToSAP_Success", cmd);
        }
        public static int GIUpdateIsSendDataByBarcode(string barcode, int status)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@barcode", barcode);
            cmd.Parameters.AddWithValue("@Status", status);
            return ClsMySqlDataAcess.ExecuteNonQuery("sp_GIDatasendToSAP_Success", cmd);
        }

    }
}
