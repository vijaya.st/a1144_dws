﻿using System;
using S7;
using AMBPL.Common;
using System.Data;
using System.IO;

namespace AMBPL.Common
{
  public static  class Common
    {
        public static string GetXML(object objClass)
        {

            StringWriter sw = new StringWriter();
            System.Xml.XmlTextWriter tw = null;
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(objClass.GetType());
                tw = new System.Xml.XmlTextWriter(sw);
                serializer.Serialize(tw, objClass);
            }
            catch (Exception ex)
            { }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }

        //public static AMBPL_PLC CreatePLC(DataRow drPLC)
        //{
        //    string plcLabel = drPLC["Label"].ToString();
        //    AMBPL_PLC plc = null;
        //    AMBPL_PLC.PLC_Type plctype = AMBPL_PLC.PLC_Type.S7300;
        //    switch (Convert.ToInt32(drPLC["PLCType"]))
        //    {
        //        case 1500:
        //            plctype = AMBPL_PLC.PLC_Type.S71500;
        //            break;
        //        case 1200:
        //            plctype = AMBPL_PLC.PLC_Type.S71200;
        //            break;
        //        case 200:
        //            plctype = AMBPL_PLC.PLC_Type.S7200;
        //            break;
        //        case 400:
        //            plctype = AMBPL_PLC.PLC_Type.S7400;
        //            break;
        //        default:
        //            plctype = AMBPL_PLC.PLC_Type.S7300;
        //            break;
        //    }
        //    plc = new AMBPL_PLC(plctype, drPLC["IP"].ToString(), Convert.ToInt16(drPLC["Rack"]), Convert.ToInt16(drPLC["Slot"]));

        //    return plc;
        //}
    }
}
