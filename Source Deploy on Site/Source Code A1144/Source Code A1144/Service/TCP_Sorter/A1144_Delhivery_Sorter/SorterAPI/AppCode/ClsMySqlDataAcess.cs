﻿using AMBPL.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace AMBPL.Common
{
    class ClsMySqlDataAcess
    {
        static string databaseOwner = "dbo";
        static ErrorLog oErrorLog = new ErrorLog();

        public SqlConnection connection;

        public static string GetConnectionString()
        {
            //  string con = @"Data Source= ANL6;Initial Catalog=A698;Integrated Security=No; UID=sa; Pwd=sql;";
            return System.Configuration.ConfigurationManager.AppSettings["MyeConstr"];
            // return System.Configuration.ConfigurationManager.AppSettings["eConstr"];
            //  return con;
        }

        //open Connection
        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (SqlException ex)
            {

                switch (ex.Number)
                {
                    case 0:
                        // MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        //  MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (SqlException ex)
            {
                //  MessageBox.Show(ex.Message);
                return false;
            }
        }


        public static DataTable GetDataTable(string ProcName, SqlCommand cmd)
        {
            SqlConnection cn = new SqlConnection(GetConnectionString());

            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        
        public static DataSet GetDataSet(string ProcName)
        {
            SqlConnection cn = new SqlConnection(GetConnectionString());
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
               SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cn.Close();
            }
            return ds;
        }

        public static DataSet GetDataSet(string ProcName, SqlCommand cmd)
        {
            SqlConnection cn = new SqlConnection(GetConnectionString());

            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cn.Close();
            }
            return ds;
        }

        public static int ExecuteNonQuery1(string myExecuteQuery, SqlCommand myCommand)
        {
            int ResultExecuteNonQuery = 0;
            SqlConnection cn = new SqlConnection(GetConnectionString());
            try
            {
               // MySqlCommand myCommand = new MySqlCommand(myExecuteQuery, cn);
                cn.Open();
                myCommand.Connection = cn;
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = myExecuteQuery;
                ResultExecuteNonQuery = myCommand.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                oErrorLog.WriteErrorLog("ExecuteNonQuery-  " + e.ToString());
            }
            return ResultExecuteNonQuery;
        }

        public static int ExecuteNonQuery(string ProcName, SqlCommand cmd)
        {
            int ResultExecuteNonQuery = 0;
            try
            {
                SqlConnection cn = new SqlConnection(GetConnectionString());
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                cn.Open();
                ResultExecuteNonQuery = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                oErrorLog.WriteErrorLog("ExecuteNonQuery-  " + e.ToString());
            }
            return ResultExecuteNonQuery;
        }

        public static int ExecuteNonQuery_Text(string query, SqlCommand cmd)
        {
            int ResultExecuteNonQuery = 0;
            try
            {
                SqlConnection cn = new SqlConnection(GetConnectionString());
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                cn.Open();
                ResultExecuteNonQuery = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
            return ResultExecuteNonQuery;
        }

    }
}
