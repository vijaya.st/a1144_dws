﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SorterAPI.Common;
//using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.Globalization;

namespace SorterAPI
{
    public partial class frmReport : Form
    {
        DataSet ds = new DataSet();
        DataTable dtCount = new DataTable();
        DataTable dtGrid = new DataTable();

        public frmReport()
        {
            InitializeComponent();
            dtFrom.Format = DateTimePickerFormat.Custom;
            dtFrom.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            comboBox1.SelectedIndex = 0;
            dtTo.Format = DateTimePickerFormat.Custom;
            dtTo.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            CultureInfo cul = CultureInfo.CurrentCulture;

            var firstDayWeek = cul.Calendar.GetWeekOfYear(DateTime.Now,CalendarWeekRule.FirstDay,DayOfWeek.Monday);

            int weekNum = cul.Calendar.GetWeekOfYear(
                DateTime.Now,
                CalendarWeekRule.FirstDay,
                DayOfWeek.Monday);
                for (int i = 1; i <weekNum; i++)
                {
                    comboBox2.Items.Add(" Week- " +(i));
                    
                }
            int a = DateTime.Now.DayOfYear / 7;
            comboBox2.SelectedIndex = a-1;
            //MessageBox.Show(weekNum.ToString());
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            Report(0);

        
            var items1 = new[] { 
                new { Text = "--Select--", Value = "0" }, 
                new { Text = "1", Value = "1" }, 
                new { Text = "2", Value = "2" }, 
                new { Text = "3", Value = "3" },
                new { Text = "4", Value = "4" }, 
                new { Text = "5", Value = "5" }
                };
                   
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex==1)
            {
                
                string newStr = comboBox2.SelectedItem.ToString().Remove(0, 6);
                getWeekwiseReport(Convert.ToInt32(newStr));
            }
            else
            {
                Report(0);
            }
        }

        public void Report(int btn)
        {   
            clsAPI obj = new clsAPI();

            if (btn == 0)
            {
                obj.Fromdt = dtFrom.Text;
                obj.Todt = dtTo.Text;
                obj.btnValue = btn;
                //MessageBox.Show(obj.Fromdt);
                if (Convert.ToDateTime(obj.Fromdt) > Convert.ToDateTime(obj.Todt))
                {
                   // MessageBox.Show("From Date should not be gretter than To Date. ");
                }
                else
                {
                    ds = clsAPI.getDWSReport(obj);//getDWSCount_Data(obj);
                    dtGrid = ds.Tables[0];
                    dtCount = ds.Tables[1];
                    lblTotal.Text = dtCount.Rows[0]["TotalCount"].ToString();
                    lblSuccesful.Text = dtCount.Rows[0]["Success"].ToString();
                    lblNA.Text = dtCount.Rows[0]["Faild"].ToString();
                }
            }
            else
            {
                obj.btnValue = btn;
                ds = clsAPI.getDWSReport(obj);
                dtGrid = ds.Tables[0];
                dtCount = ds.Tables[1];
                lblTotal.Text = dtCount.Rows[0]["TotalCount"].ToString();
                lblSuccesful.Text = dtCount.Rows[0]["Success"].ToString();
                lblNA.Text = dtCount.Rows[0]["Faild"].ToString();
            }
           

            dgvReport.RowsDefaultCellStyle.BackColor = Color.White;
            dgvReport.AlternatingRowsDefaultCellStyle.BackColor = Color.Gray;
            
            Font f = new Font("Microsoft Sans Serif",8,FontStyle.Bold);
            dgvReport.DefaultCellStyle.Font = f;
            dgvReport.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif",9, FontStyle.Bold);

            dgvReport.AutoResizeColumns();
            dgvReport.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dgvReport.RowHeadersVisible = false;
            dgvReport.DataSource = dtGrid;

            
        }
        public void getWeekwiseReport(int week)
        {
            ds = clsAPI.getWeekwiseDWSReport(week);
            dtGrid = ds.Tables[0];
            dtCount = ds.Tables[1];

            dgvReport.RowsDefaultCellStyle.BackColor = Color.White;
            dgvReport.AlternatingRowsDefaultCellStyle.BackColor = Color.Gray;

            Font f = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
            dgvReport.DefaultCellStyle.Font = f;
            dgvReport.ColumnHeadersDefaultCellStyle.Font = new Font("Microsoft Sans Serif", 9, FontStyle.Bold);

            dgvReport.AutoResizeColumns();
            dgvReport.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dgvReport.RowHeadersVisible = false;
            dgvReport.DataSource = dtGrid;

            lblTotal.Text = dtCount.Rows[0]["TotalCount"].ToString();
            lblSuccesful.Text = dtCount.Rows[0]["Success"].ToString();
            lblNA.Text = dtCount.Rows[0]["Faild"].ToString();
        }
       

        private void btnExporttoExcel_Click(object sender, EventArgs e)
        {
            
            saveFileDialog1.ShowDialog();
        }

        public void CreateCSVFile(DataTable dtDataTablesList, string strFilePath)
        {
            // Create the CSV file to which grid data will be exported.

            StreamWriter sw = new StreamWriter(strFilePath, false);

            //First we will write the headers.
            sw.Write(sw.NewLine);
            sw.Write("");
            sw.Write(sw.NewLine);
            sw.Write("Total Count : , " + lblTotal.Text +",");
            sw.Write("Success Count : , " + lblSuccesful.Text +",");
            sw.Write("Failed Count : , " + lblNA.Text + ",");
            sw.Write(sw.NewLine);
            sw.Write(sw.NewLine);
            sw.Write("");
            

            int iColCount = dtDataTablesList.Columns.Count;

            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dtDataTablesList.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);

            // Now write all the rows.

            foreach (DataRow dr in dtDataTablesList.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            

            
            sw.Close();

            MessageBox.Show("File Exported!!..");
        }

       
        
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string strFilePath = saveFileDialog1.FileName;
            CreateCSVFile(dtGrid, strFilePath+".csv");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        

        private void pnlExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex==1)
            {
                pnlWeek.Visible = true;
                pnlDate.Visible = false;
            }
            else
            {
                pnlWeek.Visible = false;
                pnlDate.Visible = true;
            }
        }

       
    }
}
