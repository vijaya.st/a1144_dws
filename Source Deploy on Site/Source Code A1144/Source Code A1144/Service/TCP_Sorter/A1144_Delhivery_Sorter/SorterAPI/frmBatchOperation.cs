﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO.Ports;
using SorterAPI.Common;
using System.Configuration;
using S7;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AMBPL.Common;
using S7.Net;
using System.Text;
using System.Net.NetworkInformation;

namespace SorterAPI
{
    public partial class frmBatchOperation : Form
    {
        DataSet ds = new DataSet();
        DataTable dtgrid = new DataTable();
        DataTable dtScan = new DataTable();
        DataTable dtNotScan = new DataTable();
        DataTable dtExportCSV = new DataTable();
        DataTable dtlist = new DataTable();
        DataTable dtNA = new DataTable();
        DataTable dtbar = new DataTable();
        DataTable dtBarcode = new DataTable();
        DataSet DBAddresss = new DataSet();
        DataSet dsPalletCountDB = new DataSet();


        DataTable ds2 = new DataTable();

        public ErrorCode lastErrorCode = 0;
        public string lastErrorString;
        public static string tcpWeightData, ParcelType = "";
        int port = 0;

        string TriggerFlag, Duplicatebarcodescan, Decision, ScanningFlag, DataRecieveFlag, CWBusyFlag, Invalidbarcodescan, Weighingbusy, Weighingerror = "";
        string Weightisbelowlimits, Weightisabovelimit, Dimensionabovelimit, CalibrationFailed, Checkforcalibrationbox, Dimensionbelowlimi = "";
        string Weighingonveyorstoppagewhenboxisonit, TwoBoxesonWeighingConveyor, BoxUnderWeightOverWeight, CalibrationSuccesful = "";
        string PLCLANCableDisconnected, HMILANCableDisconnected, CheckWeigherLANCableDisconnected, VMSLANCableDisconnected = "";
        string CameraLANCableDisconnected, PCHeartBeat;
        string faultreason = "";
        string Reason;
        string SystemIdleTag = string.Empty;
        string url = "";
        string url2 = "";
        int interval = 60;
        bool OkStatus = false;
        string conGPO = ConfigurationManager.ConnectionStrings["DWSConnectionString"].ConnectionString;
        int OperationType = 0;

        static AMBPL.Common.ErrorLog oErrorLog = new AMBPL.Common.ErrorLog();
        //public static AMBPL.Common.AMBPL_PLC plc1;
        Plc plc;
        public string PLCType = "", IP = "";
        public short rack, slot;
        List<string> Barcode1 = new List<string>();
        DataTable dtPLC = new DataTable();
        int intervalHeartBit = 1000;// 1min
        int intervalNetconnection = 1000;// 1min
        private Thread _threadLWH;
        static string Barcode;
        string SerialCOMPort, weighingCOMPort, AlertFAlertFlag;
        string PLCIP, HMI_LAN_IP, CheckWeigher_LAN_IP, VMS_LAN_IP, Camera_LAN_ping_IP;
        int CheckWeigher_LAN_Port, VMS_LAN_Port = 0;
        TCPClientHandler WeighingPort, WeightPort;
        SerialPort ScannerPort = new SerialPort();
        public static int Count = 0;
        private System.Timers.Timer _timerLWH;
        private System.Timers.Timer _timerInternectConnection;
        // MDIParent1 _parent = new MDIParent1();
        public static string brcd = "";
        public static int CellNo;


        /**************Dashboard Start*************/
        public frmBatchOperation()
        {
            InitializeComponent();
        }
        private void frmBatchOperation_Load(object sender, EventArgs e)
        {
            //SetConfiguration();

            CellNo = Convert.ToInt32(ConfigurationManager.AppSettings["CellNo"]);
            oErrorLog.WriteErrorLog("Service Started..." + DateTime.Now);
            SerialCOMPort = ConfigurationManager.AppSettings["ScannerCOMPort"];
            //////weighingCOMPort = System.Configuration.ConfigurationManager.AppSettings["WeighingCOMPort"];
            PLCIP = ConfigurationManager.AppSettings["PLCIP"];
            HMI_LAN_IP = ConfigurationManager.AppSettings["HMI_LAN_IP"];
            CheckWeigher_LAN_IP = ConfigurationManager.AppSettings["CheckWeigher_LAN_IP"];
            VMS_LAN_IP = ConfigurationManager.AppSettings["VMS_LAN_IP"];
            CheckWeigher_LAN_Port = Convert.ToInt32(ConfigurationManager.AppSettings["CheckWeigher_LAN_Port"]);
            VMS_LAN_Port = Convert.ToInt32(ConfigurationManager.AppSettings["VMS_LAN_Port"]);
            Camera_LAN_ping_IP = ConfigurationManager.AppSettings["Camera_LAN_ping_IP"];


            AlertFAlertFlag = ConfigurationManager.AppSettings["Alert"];
            plc = new Plc(CpuType.S71500, PLCIP, 0, 1);
            GetAllDBAddress();
            plc.Open();
            StartSerialPortScanner();
            StartEWeihghiPortScanner();
            StartWeightPortScanner();
            tmrBit.Start();
            //timer1.Start();

            oErrorLog.WriteErrorLog("PLC Connection Successfull..");
        }

        /**************Dashboard End*************/

        /**************Scanner Start*************/
        private void StartSerialPortScanner()
        {
            //oErrorLog.WriteErrorLog("In Scanner.. >> " + Barcode);
            ScannerPort = new SerialPort(SerialCOMPort, 9600, Parity.None, 8);
            ScannerPort.Open();
            oErrorLog.WriteErrorLog("Scanner Port Open Successfully..");
            if (!ScannerPort.IsOpen)
            {
                // plc.Write("DB1100.DBX527.1", true);  //pc connection lost scanner not ready
                oErrorLog.WriteErrorLog("Port not Open");
                ScannerPort.Open();
            }
            else
            {
                ScannerPort.DataReceived += ScannerPort_DataReceived;
            }
        }

        private void StartEWeihghiPortScanner()
        {
            try
            {
                //oErrorLog.WriteErrorLog("In Scanner.. >> " + Barcode);
                WeighingPort = new TCPClientHandler(VMS_LAN_IP, VMS_LAN_Port);
                oErrorLog.WriteErrorLog("Scanner Port Open Successfully..");
                WeighingPort.IsInitiateDataReceiveEvent = true;
                WeighingPort.AutoConnnectOnDisconnect = true;

                WeighingPort.StringReceived += WeighingPort_StringReceived1;

                WeighingPort.Connect();
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Exception in wheighing port connect.." + ex.ToString());
            }
        }

        private void WeighingPort_StringReceived1(object sender, EventArgs e)
        {
            try
            {
                if (!WeighingPort.IsConnected)
                {
                    // plc.Write("DB1100.DBX527.1", true);  //pc connection lost scanner not ready
                    oErrorLog.WriteErrorLog("WeighingPort Port not Open");
                    WeighingPort.Connect();
                }
                else
                {
                    TCP_DataReceviedEventArgs arg = (TCP_DataReceviedEventArgs)e;
                    string DimentionData = arg.Data;
                    //  oErrorLog.WriteErrorLog("DimentionData Recived from TCP.." + DimentionData);
                    int PassOrFailValue = 3;
                    int FailedCount = 1;
                    object isLBHAvailable = plc.Read(DataRecieveFlag); //Data Receive Flag
                                                                       //if (isLBHAvailable.ToString() == "1")
                    {
                        object L = 0;
                        object W = 0;
                        object H = 0;
                        object RealVolume = 0;
                        object LiquidVolume = 0;
                        object PackageType = 0;
                        oErrorLog.WriteErrorLog("Barcode " + brcd);
                        //  oErrorLog.WriteErrorLog("isLBHAvailable " + isLBHAvailable);
                        //P#DB1100.DBX6.0    260
                        //P#DB1100.DBX266.0    260
                        object DimentionString = DimentionData;//plc.Read(DataType.DataBlock, (Convert.ToInt32(1100)), Convert.ToInt32(6), S7.Net.VarType.String, 50);
                                                               // MessageBox.Show(tcpWeightData);
                        object WeightData = tcpWeightData;//plc.Read(DataType.DataBlock, (Convert.ToInt32(1100)), Convert.ToInt32(266), S7.Net.VarType.String, 50);
                        oErrorLog.WriteErrorLog("Weighing Data Received From PLC >> " + WeightData);
                        string Dimentionstr = _RemoveSpecialCharacters1(DimentionString.ToString());
                        oErrorLog.WriteErrorLog("Dimentionstr  Recived From PLC " + Dimentionstr);
                        string[] DimentionList = Dimentionstr.Split(':');
                        //oErrorLog.WriteErrorLog("DimentionString  " + DimentionList);
                        L = DimentionList[0];
                        W = DimentionList[1];
                        H = DimentionList[2];
                        RealVolume = DimentionList[3];
                        LiquidVolume = DimentionList[4];
                        var pckty = DimentionList[7];
                        PackageType = pckty.Substring(pckty.Length - 1);
                        if (Convert.ToInt32(PackageType) == 1)
                        {
                            PackageType = 0;
                        }
                        else
                        {
                            PackageType = 1;
                        }
                        //P#DB3.DBX1092.0   P#DB3.DBX1900.0

                        // object WeightData = plc.Read(DataType.DataBlock, (Convert.ToInt32(1100)), Convert.ToInt32(1296), S7.Net.VarType.String, 80);
                        // oErrorLog.WriteErrorLog("Weighing Data Received From PLC >> " + WeightData);
                        string A = WeightData.ToString();
                        string str = A.Substring(A.IndexOf(';') + 1);
                        string B = str.Substring(str.IndexOf(';') + 1);
                        string number = B.Split('|')[0].Trim();
                        string DW = number.Split('\0')[0].Trim() + '0';
                        //  oErrorLog.WriteErrorLog("final Weight String >> " + DW);

                        if (DW.Contains("NO"))
                        {
                            DW = "00";
                            oErrorLog.WriteErrorLog("Weighing Error in string.." + DW);
                        }
                        plc.Write(DataRecieveFlag, 0);  //Data Receive Flag
                                                        // oErrorLog.WriteErrorLog("Leghth,width,Height,Weight,RealVolume" + L.ToString() + ":" + W.ToString() + ":" + H.ToString() + ":" + DW + ":" + RealVolume);
                        if (AlertFAlertFlag == "ON")
                        {
                            if ((Convert.ToInt32(L) > 1200) || (Convert.ToInt32(W) > 1000) || (Convert.ToInt32(H) > 900))
                            {
                                plc.Write(Dimensionabovelimit, true); //above Dimention limit Recorded..
                                                                      // oErrorLog.WriteErrorLog("above Dimention limit Recorded..  " + Dimensionabovelimit.ToString());
                                ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Above Dimention limit Recorded..", FailedCount);
                                PassOrFailValue = 2;
                                plc.Write(Decision, PassOrFailValue);
                            }
                            if ((Convert.ToInt32(L) == 0) || (Convert.ToInt32(W) == 0) || (Convert.ToInt32(H) == 0))
                            {
                                plc.Write(Dimensionabovelimit, true); //above Dimention limit Recorded..
                                                                      // oErrorLog.WriteErrorLog("Dimensioner error/ Dimention Above limit recorded..  " + Dimensionabovelimit.ToString());
                                ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Dimensioner error/ Dimention Above limit recorded..", FailedCount);
                                PassOrFailValue = 2;
                                plc.Write(Decision, PassOrFailValue);
                            }
                            else if (Convert.ToInt32(DW) > 80000)
                            {
                                plc.Write(Weightisabovelimit, true); //above Weight limit Recorded..
                                                                     // oErrorLog.WriteErrorLog("above Weight limit Recorded..  " + Weightisabovelimit.ToString());
                                ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Above Weight limit Recorded..", FailedCount);
                                PassOrFailValue = 2;
                                plc.Write(Decision, PassOrFailValue);
                            }
                            else if ((Convert.ToInt32(L) < 150) || (Convert.ToInt32(W) < 50) || (Convert.ToInt32(H) < 20))
                            {
                                plc.Write(Dimensionbelowlimi, true); //Below Dimention limit Recorded.. 
                                                                     // oErrorLog.WriteErrorLog("Below Dimention limit Recorded..  " + Dimensionbelowlimi.ToString());
                                ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Below Dimention limit Recorded..", FailedCount);
                                PassOrFailValue = 2;
                                plc.Write(Decision, PassOrFailValue);
                            }
                            else if (Convert.ToInt32(DW) < 200)
                            {
                                plc.Write(Weightisbelowlimits, true); //Below Weight limit Recorded..
                                                                      // oErrorLog.WriteErrorLog("Below Weight limit Recorded..  " + Weightisbelowlimits.ToString());
                                ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Below Weight limit Recorded..", FailedCount);
                                PassOrFailValue = 2;
                                plc.Write(Decision, PassOrFailValue);
                            }
                            else
                            {
                                int achualWeight = Convert.ToInt32(DW);
                                if (achualWeight > (achualWeight + 50) || achualWeight < (achualWeight - 50))
                                {
                                    plc.Write(Weightisabovelimit, true);  //weight abow limit
                                    PassOrFailValue = 2;
                                    //MessageBox.Show("Weigth limit ecxeeded");
                                }
                                //  oErrorLog.WriteErrorLog("L : " + L + " W : " + W + " H :" + H);
                                DataSet ds = new DataSet();
                                DataTable dt = new DataTable();
                                DataTable dtGIStatus = new DataTable();
                                DataTable dtCalibration = new DataTable();

                                ds = clsMain.UpdateLWH(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType));
                                // oErrorLog.WriteErrorLog("lwh Updated successfully in database..");
                                WeightData = String.Empty;
                                //ds = clsMain.UpdateFlag();

                                plc.Write(Dimensionabovelimit, false);  //above dimention
                                plc.Write(Dimensionbelowlimi, false); //dimention below
                                plc.Write(Weightisbelowlimits, false); //Below Weight limit Recorded..
                                plc.Write(Weightisabovelimit, false);  //above weight limit Recorded..

                                dt = ds.Tables[0];
                                dtGIStatus = ds.Tables[2];
                                dtCalibration = ds.Tables[3];
                                if (dt.Rows[0]["Acount"].ToString() == "1")
                                {
                                    PassOrFailValue = 1;
                                    oErrorLog.WriteErrorLog("Duplicate Weight recorded (5 times Successively).. DB1100.DBW22 >> " + plc.Read("DB1100.DBW22"));
                                }

                                else if (dtCalibration.Rows[0]["CalibrationStatus"].ToString() == "1")
                                {
                                    // oErrorLog.WriteErrorLog("Calibration Successfull...");
                                    plc.Write(CalibrationFailed, false);  //calibration Failed
                                    plc.Write(CalibrationSuccesful, false);  //calibration successful
                                    PassOrFailValue = 1;

                                }
                                else if (dtCalibration.Rows[0]["CalibrationStatus"].ToString() == "0")
                                {
                                    plc.Write(CalibrationFailed, true); //Calibration Faild...
                                    PassOrFailValue = 2;
                                }
                                else
                                {
                                    PassOrFailValue = 1;
                                    //plc.Write("DB1100.DBX1556.4", false);
                                }

                                plc.Write(Decision, PassOrFailValue);
                                // oErrorLog.WriteErrorLog("Pass or Fail Value " + PassOrFailValue);
                            }
                        }
                        else
                        {
                            //plc.Write("DB1100.DBX1556.4", false);
                            ds = clsMain.UpdateLWH(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType));
                            //  oErrorLog.WriteErrorLog("lwh Updated successfully in database..");
                            plc.Write(Decision, PassOrFailValue);
                            //  oErrorLog.WriteErrorLog("Pass or Fail Value " + PassOrFailValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Exception in dimentions data update .." + ex.ToString());
            }
        }
        private void StartWeightPortScanner()
        {
            //oErrorLog.WriteErrorLog("In Scanner.. >> " + Barcode);
            WeightPort = new TCPClientHandler(CheckWeigher_LAN_IP, CheckWeigher_LAN_Port);

            oErrorLog.WriteErrorLog("Scanner Port Open Successfully..");
            WeightPort.IsInitiateDataReceiveEvent = true;
            WeightPort.AutoConnnectOnDisconnect = true;
            WeightPort.StringReceived += WeightPort_StringReceived;

            WeightPort.Connect();
        }

        private void WeightPort_StringReceived(object sender, EventArgs e)
        {

            if (!WeightPort.IsConnected || WeightPort == null)
            {
                // plc.Write("DB1100.DBX527.1", true);  //pc connection lost scanner not ready
                oErrorLog.WriteErrorLog("WeightPort Port not Open");
                WeightPort.Connect();
            }
            else
            {
                TCP_DataReceviedEventArgs arg = (TCP_DataReceviedEventArgs)e;
                tcpWeightData = arg.Data;
                oErrorLog.WriteErrorLog("Weight Recived from TCP.." + tcpWeightData);
            }

        }
        private void ScannerPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string b = ScannerPort.ReadExisting();
                Barcode = b.Replace("\n", "").Replace("\r", "");
                DataTable dt = new DataTable();
                brcd = Barcode;
                oErrorLog.WriteErrorLog(Barcode);
                Regex rg = new Regex(@"^S?[0 - 9]{ 11,15}$| ^[A - Z]{ 2}\d{ 9}[A-Z]{2}$|^1[0 - 9]{27}$|^\d{20}00\d{12}$");
                // Regex rg = new Regex(@"^ S?[0 - 9]{ 11, 13 }$|^[A - Z]{ 2}\d{ 9}[A-Z]{2}$|^\d{15}$");
                Regex regex = new Regex(@"^\d+$");

                if (Barcode != "NR" && Barcode != "" && (((Barcode.Length >= 11 && Barcode.Length <= 14) && regex.IsMatch(Barcode)) || rg.IsMatch(Barcode)))
                {
                    if (AlertFAlertFlag == "ON")
                    {
                        dt = clsMain.CheckDuplicateBarcode(Barcode);
                        if (dt.Rows[0]["DuplicateBarcode"].ToString() == "2")
                        {
                            // ds2 = clsMain.FaultyBarcodeInsert(Barcode);
                            oErrorLog.WriteErrorLog("Barcode Duplicate.. >> " + dt.Rows[0]["DuplicateBarcode"].ToString());
                            plc.Write(Duplicatebarcodescan, true);  //duplicate barcode scan
                            oErrorLog.WriteErrorLog("plc.Read == " + plc.Read(Duplicatebarcodescan));
                        }
                        else if (dt.Rows[0]["HCount"].ToString() == "1")
                        {
                            plc.Write(Checkforcalibrationbox, true);   //Checked with calibration box
                            oErrorLog.WriteErrorLog("100 parcels are Completeld..Please check with Matser Box..:" + Convert.ToBoolean(plc.Read(Checkforcalibrationbox)));
                        }
                        else if (dt.Rows[0]["FailedScanLimit"].ToString() == "1")
                        {
                            plc.Write("DB1100.DBX529.5", true);   //Failed scanned limit excedded
                            oErrorLog.WriteErrorLog("AWB scan Limit Exceded..:");
                        }

                        else
                        {
                            DataTable ds1 = new DataTable();
                            object A = plc.Read(CWBusyFlag);  //Weiging busy
                            if (A.ToString() == "1")
                            {
                                //Scanning Flag
                                ds1 = clsMain.BarcodeInsert(Barcode, "0");
                                if (ds1.Rows[0]["id"].ToString() == "2")
                                {
                                    plc.Write(ScanningFlag, 1);   //invalid barcode scan
                                }
                                else
                                {
                                    plc.Write(ScanningFlag, 0);   //Scanning Flag
                                }
                                // oErrorLog.WriteErrorLog("Barcode Received.. >> " + Barcode + " Length :--" + Barcode.Length);
                                plc.Write(Duplicatebarcodescan, false);
                                plc.Write(Checkforcalibrationbox, false); //Checked with calibration box
                                plc.Write(Invalidbarcodescan, false);
                                plc.Write("DB1100.DBX529.5", false);   //Failed scanned limit excedded
                            }
                        }
                    }
                    else
                    {
                        DataTable ds1 = new DataTable();
                        object A = plc.Read(CWBusyFlag);  //CWBusyFlag 
                        if (A.ToString() == "1")
                        {
                            // plc.Write(ScanningFlag, 1);  //Scanning Flag
                            ds1 = clsMain.BarcodeInsert(Barcode, "0");
                            if (ds1.Rows[0]["id"].ToString() == "1")
                            {
                                plc.Write(ScanningFlag, 0);
                            }
                            else
                            {
                                plc.Write(ScanningFlag, 1);
                            }
                            //  oErrorLog.WriteErrorLog("Barcode Received.. >> " + Barcode + " Length :--" + Barcode.Length);
                            plc.Write(Duplicatebarcodescan, false);
                            plc.Write(Checkforcalibrationbox, false);  //Checked with calibration box
                            plc.Write("DB1100.DBX529.5", false);   //Failed scanned limit excedded
                                                                   //plc.Write(Weighingbusy, false);  //Weighing busy
                        }
                        else
                        {
                            plc.Write(Weighingbusy, true);  //Weighing busy
                        }
                    }
                }
                else
                {
                    plc.Write(Invalidbarcodescan, true); //inavalid barcode scan
                    plc.Write(ScanningFlag, 2);   //  scanning flag
                                                  //  oErrorLog.WriteErrorLog("Invalid Barcode Scanned >> " + Barcode);
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in scanner barcode data receive :" + ex.Message);
            }
        }
        /**************Scanner End *************/


        //************************** Timer Start for LWH ******************//
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                //int PassOrFailValue = 3;
                //object isLBHAvailable = plc.Read(DataRecieveFlag); //Data Receive Flag
                //if (isLBHAvailable.ToString() == "1")
                //{
                //    plc.Write(TwoBoxesonWeighingConveyor, false); //Two Boxes on Weighing Conveyor
                //    plc.Write(BoxUnderWeightOverWeight, false);  //Box Under Weight / Over Weight
                //    plc.Write(Weighingonveyorstoppagewhenboxisonit, false);  //Weighing conveyor stoppage when box is on it

                //    object L = 0;
                //    object W = 0;
                //    object H = 0;
                //    object RealVolume = 0;
                //    object LiquidVolume = 0;
                //    object PackageType = 0;
                //    oErrorLog.WriteErrorLog("Barcode " + brcd);
                //    oErrorLog.WriteErrorLog("isLBHAvailable " + isLBHAvailable);
                //    //P#DB1100.DBX6.0    260
                //    //P#DB1100.DBX266.0    260
                //    object DimentionString = plc.Read(DataType.DataBlock, (Convert.ToInt32(1100)), Convert.ToInt32(6), S7.Net.VarType.String, 50);
                //    object WeightData = tcpWeightData;//plc.Read(DataType.DataBlock, (Convert.ToInt32(1100)), Convert.ToInt32(266), S7.Net.VarType.String, 50);
                //    oErrorLog.WriteErrorLog("Weighing Data Received From PLC >> " + WeightData);
                //    string Dimentionstr = _RemoveSpecialCharacters1(DimentionString.ToString());
                //    oErrorLog.WriteErrorLog("Dimentionstr  Recived From PLC " + Dimentionstr);
                //    string[] DimentionList = Dimentionstr.Split(':');
                //    //oErrorLog.WriteErrorLog("DimentionString  " + DimentionList);
                //    L = DimentionList[1];
                //    W = DimentionList[2];
                //    H = DimentionList[3];
                //    RealVolume = DimentionList[4];
                //    LiquidVolume = DimentionList[5];
                //    var pckty = DimentionList[6];
                //    PackageType = pckty.Substring(pckty.Length - 1);
                //    if (Convert.ToInt32(PackageType) == 1)
                //    {
                //        PackageType = 0;
                //    }
                //    else
                //    {
                //        PackageType = 1;
                //    }

                //    //P#DB3.DBX1092.0   P#DB3.DBX1900.0

                //    // object WeightData = plc.Read(DataType.DataBlock, (Convert.ToInt32(1100)), Convert.ToInt32(1296), S7.Net.VarType.String, 80);
                //    // oErrorLog.WriteErrorLog("Weighing Data Received From PLC >> " + WeightData);
                //    string A = WeightData.ToString();
                //    string str = A.Substring(A.IndexOf(';') + 1);
                //    string B = str.Substring(str.IndexOf(';') + 1);
                //    string number = B.Split('|')[0].Trim();
                //    string DW = number.Split('\0')[0].Trim() + '0';
                //    oErrorLog.WriteErrorLog("final Weight String >> " + DW);

                //    if (DW.Contains("3043") || DW.Contains("3044"))
                //    {
                //        plc.Write(TwoBoxesonWeighingConveyor, true); //Two Boxes on Weighing Conveyor
                //        oErrorLog.WriteErrorLog("Two Boxes on Weighing Conveyor" + TwoBoxesonWeighingConveyor.ToString());
                //    }
                //    if (DW.Contains("3001") || DW.Contains("3023"))
                //    {
                //        plc.Write(BoxUnderWeightOverWeight, true);  //Box Under Weight / Over Weight
                //        oErrorLog.WriteErrorLog("Box Under Weight / Over Weight" + BoxUnderWeightOverWeight.ToString());
                //    }
                //    if (DW.Contains("3046"))
                //    {
                //        plc.Write(Weighingonveyorstoppagewhenboxisonit, true);  //Weighing conveyor stoppage when box is on it
                //        oErrorLog.WriteErrorLog("Weighing conveyor stoppage when box is on it" + Weighingonveyorstoppagewhenboxisonit.ToString());
                //    }
                //    if (DW.Contains("NO"))
                //    {
                //        DW = "00";
                //        oErrorLog.WriteErrorLog("Weighing Error in string.." + DW);
                //    }
                //    plc.Write(DataRecieveFlag, 0);  //Data Receive Flag
                //    oErrorLog.WriteErrorLog("Leghth,width,Height,Weight,RealVolume" + L.ToString() + ":" + W.ToString() + ":" + H.ToString() + ":" + DW + ":" + RealVolume);
                //    if (AlertFAlertFlag == "ON")
                //    {

                //        if ((Convert.ToInt32(L) > 1200) || (Convert.ToInt32(W) > 1000) || (Convert.ToInt32(H) > 900))
                //        {
                //            plc.Write(Dimensionabovelimit, true); //above Dimention limit Recorded..
                //            oErrorLog.WriteErrorLog("above Dimention limit Recorded..  " + Dimensionabovelimit.ToString());
                //            ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Above Dimention limit Recorded..");
                //            PassOrFailValue = 2;
                //            plc.Write(Decision, PassOrFailValue);
                //        }
                //        else if (Convert.ToInt32(DW) > 50000)
                //        {
                //            plc.Write(Weightisabovelimit, true); //above Weight limit Recorded..
                //            oErrorLog.WriteErrorLog("above Weight limit Recorded..  " + Weightisabovelimit.ToString());
                //            ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Above Weight limit Recorded..");
                //            PassOrFailValue = 2;
                //            plc.Write(Decision, PassOrFailValue);
                //        }
                //        else if ((Convert.ToInt32(L) < 160) || (Convert.ToInt32(H) < 30) || (Convert.ToInt32(H) < 30))
                //        {
                //            plc.Write(Dimensionbelowlimi, true); //Below Dimention limit Recorded.. 
                //            oErrorLog.WriteErrorLog("Below Dimention limit Recorded..  " + Dimensionbelowlimi.ToString());
                //            ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Below Dimention limit Recorded..");
                //            PassOrFailValue = 2;
                //            plc.Write(Decision, PassOrFailValue);
                //        }
                //        else if (Convert.ToInt32(DW) < 200)
                //        {
                //            plc.Write(Weightisbelowlimits, true); //Below Weight limit Recorded..
                //            oErrorLog.WriteErrorLog("Below Weight limit Recorded..  " + Weightisbelowlimits.ToString());
                //            ds = clsMain.UpdateLWHWithReson(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType), "Below Weight limit Recorded..");
                //            PassOrFailValue = 2;
                //            plc.Write(Decision, PassOrFailValue);
                //        }
                //        else
                //        {
                //            int achualWeight = Convert.ToInt32(DW);
                //            if (achualWeight > (achualWeight + 50) || achualWeight < (achualWeight - 50))
                //            {
                //                plc.Write(Weightisabovelimit, true);  //weight abow limit
                //                PassOrFailValue = 2;
                //                //MessageBox.Show("Weigth limit ecxeeded");
                //            }
                //            oErrorLog.WriteErrorLog("L : " + L + " W : " + W + " H :" + H);
                //            DataSet ds = new DataSet();
                //            DataTable dt = new DataTable();
                //            DataTable dtGIStatus = new DataTable();
                //            DataTable dtCalibration = new DataTable();

                //            ds = clsMain.UpdateLWH(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType));
                //            oErrorLog.WriteErrorLog("lwh Updated successfully in database..");

                //            //ds = clsMain.UpdateFlag();

                //            plc.Write(Dimensionabovelimit, false);  //above dimention
                //            plc.Write(Dimensionbelowlimi, false); //dimention below
                //            plc.Write(Weightisbelowlimits, false); //Below Weight limit Recorded..
                //            plc.Write(Weightisabovelimit, false);  //above weight limit Recorded..

                //            dt = ds.Tables[0];
                //            dtGIStatus = ds.Tables[2];
                //            dtCalibration = ds.Tables[3];
                //            if (dt.Rows[0]["Acount"].ToString() == "1")
                //            {
                //                PassOrFailValue = 1;
                //                oErrorLog.WriteErrorLog("Duplicate Weight recorded (5 times Successively).. DB1100.DBW22 >> " + plc.Read("DB1100.DBW22"));
                //            }

                //            else if (dtCalibration.Rows[0]["CalibrationStatus"].ToString() == "1")
                //            {
                //                oErrorLog.WriteErrorLog("Calibration Successfull...");
                //                plc.Write(CalibrationFailed, false);  //calibration Failed
                //                plc.Write("DB1100.DBX527.2", false);  //calibration successful
                //                PassOrFailValue = 1;

                //            }
                //            else if (dtCalibration.Rows[0]["CalibrationStatus"].ToString() == "0")
                //            {
                //                plc.Write(CalibrationFailed, true); //Calibration Faild...
                //                PassOrFailValue = 2;
                //            }
                //            else
                //            {
                //                PassOrFailValue = 1;
                //                plc.Write("DB1100.DBX1556.4", false);
                //            }

                //            plc.Write(Decision, PassOrFailValue);
                //            oErrorLog.WriteErrorLog("Pass or Fail Value " + PassOrFailValue);
                //        }
                //    }
                //    else
                //    {
                //        plc.Write("DB1100.DBX1556.4", false);
                //        ds = clsMain.UpdateLWH(Convert.ToInt32(L), Convert.ToInt32(W), Convert.ToInt32(H), Convert.ToDecimal(DW), Convert.ToInt32(RealVolume), Convert.ToInt32(LiquidVolume), Convert.ToInt32(PackageType));
                //        oErrorLog.WriteErrorLog("lwh Updated successfully in database..");
                //        plc.Write(Decision, PassOrFailValue);
                //        oErrorLog.WriteErrorLog("Pass or Fail Value " + PassOrFailValue);
                //    }
                //}
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Exception in getLWH timer" + ex.ToString());
            }
            finally
            {
                // plc.Write(Decision, 1);
            }
        }
        private static string _RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^0-9_]", "", RegexOptions.Compiled);
        }
        private static string _RemoveSpecialCharacters1(string str)
        {
            return Regex.Replace(str, "[^0-9:]", "", RegexOptions.Compiled);
        }
        private void dgvBatchOperation_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        //************************** LWH Timer End ********************//

        private void tmrBit_Tick(object sender, EventArgs e)
        {
            try
            {
                //check ip is connected or not
                Ping ping = new Ping();
                PingReply PLC_LAN_pingReply = ping.Send(PLCIP);
                PingReply HMI_LANpingReply = ping.Send(HMI_LAN_IP);
                PingReply CheckWeigher_LAN_pingReply = ping.Send(CheckWeigher_LAN_IP);
                PingReply VMS_LAN_pingReply = ping.Send(VMS_LAN_IP);
                PingReply Camera_LAN_pingReply = ping.Send(Camera_LAN_ping_IP);

                if (PLC_LAN_pingReply.Status == IPStatus.Success)
                {
                    plc.Write(PLCLANCableDisconnected, false); //PLC LAN Cable Disconnected
                    OperationType = 0;
                    clsMain.PLCLANStatus(PLCLANCableDisconnected, OperationType);
                }
                else
                {
                    plc.Write(PLCLANCableDisconnected, true);  //PLC LAN Cable Disconnected
                    OperationType = 1;
                    clsMain.PLCLANStatus(PLCLANCableDisconnected, OperationType);
                }
                if (HMI_LANpingReply.Status == IPStatus.Success)
                {
                    plc.Write(HMILANCableDisconnected, false);  //HMI LAN Cable Disconnected
                }
                else
                {
                    plc.Write(HMILANCableDisconnected, true);  //HMI LAN Cable Disconnected
                }
                if (CheckWeigher_LAN_pingReply.Status == IPStatus.Success)
                {
                    object isWeighingLANDisconnected = plc.Read(CheckWeigherLANCableDisconnected);
                    // oErrorLog.WriteErrorLog("Weighing LAN Disconnected.." + isWeighingLANDisconnected);
                    if (isWeighingLANDisconnected.ToString() == "True")
                    {
                        oErrorLog.WriteErrorLog("Weighing LAN Disconnected.." + isWeighingLANDisconnected);
                        StartWeightPortScanner();
                        plc.Write(CheckWeigherLANCableDisconnected, false);
                    }
                    else
                    {
                        plc.Write(CheckWeigherLANCableDisconnected, false);  //Check Weigher LAN Cable Disconnected
                    }
                }
                else
                {

                    plc.Write(CheckWeigherLANCableDisconnected, true);  //Check Weigher LAN Cable Disconnected


                }
                if (VMS_LAN_pingReply.Status == IPStatus.Success)
                {
                    plc.Write(VMSLANCableDisconnected, false);  //VMS LAN Cable Disconnected
                }
                else
                {
                    plc.Write(VMSLANCableDisconnected, true);  //VMS LAN Cable Disconnected
                }
                if (Camera_LAN_pingReply.Status == IPStatus.Success)
                {
                    plc.Write(CameraLANCableDisconnected, false);  //Camera LAN Cable Disconnected
                }
                else
                {
                    plc.Write(CameraLANCableDisconnected, true);  //Camera LAN Cable Disconnected
                }

                plc.Write(PCHeartBeat, true); //to check PC Lan Connected or not

                DataTable dtDBAddresses = clsMain.Fault_GetTagName();
                string xmlFault = "";
                StringBuilder customerDoc = new StringBuilder();
                foreach (DataRow dr in dtDBAddresses.Rows)
                {
                    //PLCTagName id DBAddress for S7 PLC.
                    object TagValue;
                    string dBAdddress = dr["PLCTagName"].ToString();
                    TagValue = plc.Read(dBAdddress);
                    customerDoc.Append("<row><T>" + dBAdddress + "</T><V>" + TagValue + "</V></row>");
                    //oErrorLog.WriteErrorLog("Read From DB " + dBAdddress + ">> " + TagValue);
                }
                //To Read Reasonwise Idletime....
                dsPalletCountDB = clsMain.GetAlertsDbAddresses();
                foreach (DataRow dr in dsPalletCountDB.Tables[0].Rows)
                {
                    SystemIdleTag = dr["IdleTime_with_ReasonTag"].ToString();
                    //Cell_No = dr["CellNo"].ToString();
                    Reason = dr["ErrorDesription"].ToString();
                    object IdleStatus = plc.Read(SystemIdleTag);
                    clsMain.SystemIdleTime(Convert.ToInt32(IdleStatus), SystemIdleTag, Reason);
                    // oErrorLog.WriteErrorLog("Robot Idle Tag :->> " + RobotIdleTag + "  Idle Status:->> " + IdleStatus + "Cell No--> "+ Cell_No + "Reason --> "+Reason);

                }
                string xml = customerDoc.ToString();
                xmlFault = xml;
                clsMain.UpdateFaultStatus(xmlFault);
                //oErrorLog.WriteErrorLog("XML Sheet >> " + xmlFault);
                //********** check sensor malfunctioning ***********/

                //*************Delete Data of escape of last 60 days************************
                clsMain.DeleteDWSRecord();
                //*************Delete Data of escape of last 60 days************************
            }
            catch (Exception Ex)
            {
                oErrorLog.WriteErrorLog("Exception in alarm insert and LAN check thread " + Ex.ToString());
                // MessageBox.Show("Exception in internet connection thread " + Ex.ToString());
            }
        }

        //************************** Timer Start GI API Communication ******************//
        private void SetConfiguration()
        {
            try
            {
                port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Port"]);
                interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TimeInterval"]);
                url = System.Configuration.ConfigurationManager.AppSettings["URL"];     //For AWS 1
                url2 = System.Configuration.ConfigurationManager.AppSettings["URL2"];     //For AWS 1
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Confuguration App settings :" + ex.Message);
            }
        }
        //************************** Timer End GI API Communication ******************//

        //************************** Get All DB Address start ******************//
        private void GetAllDBAddress()
        {
            try
            {
                DBAddresss = clsMain.GetAllDbAddresses(CellNo);
                //To Read pallet Count....
                foreach (DataRow dr in DBAddresss.Tables[0].Rows)
                {
                    TriggerFlag = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[1].Rows)
                {
                    Decision = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[2].Rows)
                {
                    ScanningFlag = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[3].Rows)
                {
                    DataRecieveFlag = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[4].Rows)
                {
                    CWBusyFlag = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[5].Rows)
                {
                    Invalidbarcodescan = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[6].Rows)
                {
                    Duplicatebarcodescan = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[7].Rows)
                {
                    Weightisbelowlimits = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[8].Rows)
                {
                    Weightisabovelimit = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[9].Rows)
                {
                    Dimensionabovelimit = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[10].Rows)
                {
                    Dimensionbelowlimi = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[11].Rows)
                {
                    CalibrationFailed = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[12].Rows)
                {
                    Checkforcalibrationbox = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[13].Rows)
                {
                    Weighingbusy = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[14].Rows)
                {
                    Weighingerror = dr["DBAddress"].ToString();
                }
                foreach (DataRow dr in DBAddresss.Tables[15].Rows)
                {
                    TwoBoxesonWeighingConveyor = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[16].Rows)
                {
                    BoxUnderWeightOverWeight = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[17].Rows)
                {
                    Weighingonveyorstoppagewhenboxisonit = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[18].Rows)
                {
                    PLCLANCableDisconnected = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[19].Rows)
                {
                    HMILANCableDisconnected = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[20].Rows)
                {
                    CheckWeigherLANCableDisconnected = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[21].Rows)
                {
                    VMSLANCableDisconnected = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[22].Rows)
                {
                    CalibrationSuccesful = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[23].Rows)
                {
                    CameraLANCableDisconnected = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
                foreach (DataRow dr in DBAddresss.Tables[24].Rows)
                {
                    PCHeartBeat = dr["DBAddress"].ToString().Replace("\n", "").Replace("\r", "");
                }
            }
            catch (Exception ex)
            {
                oErrorLog.WriteErrorLog("Error in Get All DBAddress :" + ex.Message);
            }
        }

        //************************** Get All DB Address end ******************//
    }
}
