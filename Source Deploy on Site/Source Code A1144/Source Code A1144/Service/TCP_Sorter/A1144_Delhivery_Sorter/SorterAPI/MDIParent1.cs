﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Reflection;
using SorterAPI.Common;
//using DynamicButton;

namespace SorterAPI
{
    public partial class MDIParent1 : Form
    {
        public static Color btA1;
        public static Color btA2;
        public static Color btA3;
        public static Color btA4;
        public static Color btA5;
        public static Color btA6;
        public static Color btA7;
        public static Color btA8;
        public static Color btA9;
        public static Color btA10;



        public MDIParent1()
        {
            InitializeComponent();
            this.Height = 1000;
            this.pnlMenu.BackColor = System.Drawing.Color.Transparent;
            
            foreach (Control ctl in this.Controls)
            {
                if (ctl is MdiClient)
                {
                    ctl.MouseDown += new MouseEventHandler(ctl_MouseDown);
                    break;
                }
            }
        }

        void MDIParent1_MouseDown(object sender, MouseEventArgs e)
        {
            MessageBox.Show(e.Location.ToString());
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        void toolStripClick(object sender, EventArgs e)
        {
            ToolStripItem myitem = (ToolStripItem)sender;
        }

        private void toolStripButton1_MouseHover(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //frmSorterAPIConfigurator fr = new frmSorterAPIConfigurator();
            //fr.Show();
        }
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (panel1.Visible == true)
            {
                panel1.Visible = false;
            }
            else
            {
                panel1.Visible = true;
            }
        }

        private void picAPI_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator1"];

            if (fc == null)
            {
                //frmSorterAPIConfigurator1 SrtrAPI = new frmSorterAPIConfigurator1();
                //SrtrAPI.MdiParent = this;
                //SrtrAPI.Show();
            }
        }

        void ctl_MouseDown(object sender, MouseEventArgs e)
        {
            //MessageBox.Show("You clicked on the MdiClient area!");
            if (panel1.Visible == true)
            {
                panel1.Visible = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            btnEstopCPO1.BackColor = btA1;
            btnESTOPOP01.BackColor = btA2;
            btnEstopOP02.BackColor = btA3;
            btnA1Helathy.BackColor = btA4;
            btnA2healthy.BackColor = btA5;
            btnA4Healthy.BackColor = btA6;
            btnLineJam.BackColor = btA7;
            btnA2Jam.BackColor = btA8;
            btnA4Jam.BackColor = btA9;
            btn100Parcels.BackColor = btA10;
             
         
        }

        private void MDIParent1_Load(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmBatchOperation batchOpr = new frmBatchOperation();
          //  batchOpr.MdiParent = this;
            batchOpr.BringToFront();
            batchOpr.Show();
        }

        private void pnlBatchOperation_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.OfType<frmBatchOperation>().Count() == 0)
            {
                frmBatchOperation batchOpr = new frmBatchOperation();
                panel1.Visible = false;              
                frmBatchOperation.ParcelType = "S";
                batchOpr.MdiParent = this;
                batchOpr.BringToFront();
                batchOpr.Show();
            }
        }

        private void lblBarchOperation_Click(object sender, EventArgs e)
        {
         
            if (Application.OpenForms.OfType<frmBatchOperation>().Count() == 0)
            {
                frmBatchOperation batchOpr = new frmBatchOperation();
                panel1.Visible = false;
                // frmBatchOperation batchOpr = new frmBatchOperation();
                frmBatchOperation.ParcelType = "S";
                batchOpr.MdiParent = this;
                batchOpr.BringToFront();
                batchOpr.Show();
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            LocationSorter LS = new LocationSorter();
            LS.MdiParent = this;
            LS.BringToFront();
            LS.Show();
            // LS.ShowDialog();
        }

        private void panel4_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            LocationSorter LS = new LocationSorter();
            LS.MdiParent = this;
            LS.BringToFront();
            LS.Show();

        }

        private void label1_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmReport r = new frmReport();
            r.MdiParent = this;
            r.BringToFront();
            r.Show();
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmReport r = new frmReport();
            r.MdiParent = this;
            r.BringToFront();
            r.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
        }

        private void pnlPlan_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //frmPlanDetails pd = new frmPlanDetails();
            //pd.ShowDialog();
        }

        private void lblPlan_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //frmPlanDetails pd = new frmPlanDetails();
            //pd.ShowDialog();
        }

        private void pnlExit_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            //PropixExt = API.PropixAPI.ExitApplication();
            this.Close();
            this.Dispose();
            Application.Exit();

        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            // PropixExt =  API.PropixAPI.ExitApplication();
            this.Close();
            this.Dispose();
            Application.Exit();
        }

        private void pnlUtilities_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //frmUtility ut = new frmUtility();
            //ut.MdiParent = this;
            //ut.BringToFront();
            //ut.Show();

            // ut.ShowDialog();
        }

        private void lblUtilities_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //frmUtility ut = new frmUtility();
            //ut.MdiParent = this;
            //ut.BringToFront();
            //ut.Show();
        }

        private void pnlMenu_Click(object sender, EventArgs e)
        {
            if (panel1.Visible == true)
            {
                panel1.Visible = false;
            }
            else
            {
                panel1.Visible = true;
            }
        }

        private void pnlAdminMenu_Click(object sender, EventArgs e)
        {
            Form fc = Application.OpenForms["frmSorterAPIConfigurator1"];

            if (fc == null)
            {
                //frmSorterAPIConfigurator1 SrtrAPI = new frmSorterAPIConfigurator1();
                //SrtrAPI.MdiParent = this;
                //SrtrAPI.Show();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //BayAssign1 ba = new BayAssign1();
            //ba.MdiParent = this;
            //ba.BringToFront();
            //ba.Show();
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //BayAssign1 ba = new BayAssign1();
            //ba.MdiParent = this;
            //ba.BringToFront();
            //ba.Show();
        }

        

        private void pnlSearchBarcode_Click(object sender, EventArgs e)
        {
           
        }

        private void lblSearchBarcode_Click(object sender, EventArgs e)
        {
            
        }

        private void pnlOUMap_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //BayAssign1 ba = new BayAssign1();
            //ba.MdiParent = this;
            //ba.BringToFront();
            //ba.Show();
        }

        private void lblOUMapp_Click(object sender, EventArgs e)
        {
            //panel1.Visible = false;
            //BayAssign1 ba = new BayAssign1();
            //ba.MdiParent = this;
            //ba.BringToFront();
            //ba.Show();
        }

        private void lblBusinessParcel_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmBatchOperation bsp = new frmBatchOperation();
            frmBatchOperation.ParcelType = "P";            //Business and Express Parcel 
            bsp.MdiParent = this;
            bsp.BringToFront();
            bsp.Show();
        }

        private void pnlBusinessParcel_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            frmBatchOperation bsp = new frmBatchOperation();
            frmBatchOperation.ParcelType = "P";  //Business and Express Parcel 
            bsp.MdiParent = this;
            bsp.BringToFront();
            bsp.Show();
        }

        private void pnlMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlBatchOperation_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnA4Healthy_Click(object sender, EventArgs e)
        {

        }

        private void btnEstopCPO1_Click(object sender, EventArgs e)
        {

        }
    }
}