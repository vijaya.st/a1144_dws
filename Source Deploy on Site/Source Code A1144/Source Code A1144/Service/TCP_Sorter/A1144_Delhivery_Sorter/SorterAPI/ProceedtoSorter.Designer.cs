﻿namespace SorterAPI
{
    partial class ProceedtoSorter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRack1 = new System.Windows.Forms.Label();
            this.btnDiscard = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.imageMap1 = new SorterAPI.ImageMap();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRack1
            // 
            this.lblRack1.AutoSize = true;
            this.lblRack1.BackColor = System.Drawing.Color.White;
            this.lblRack1.Location = new System.Drawing.Point(109, 124);
            this.lblRack1.Name = "lblRack1";
            this.lblRack1.Size = new System.Drawing.Size(0, 13);
            this.lblRack1.TabIndex = 45;
            this.lblRack1.Visible = false;
            // 
            // btnDiscard
            // 
            this.btnDiscard.Location = new System.Drawing.Point(882, 9);
            this.btnDiscard.Name = "btnDiscard";
            this.btnDiscard.Size = new System.Drawing.Size(75, 23);
            this.btnDiscard.TabIndex = 46;
            this.btnDiscard.Text = "Discard";
            this.btnDiscard.UseVisualStyleBackColor = true;
            this.btnDiscard.Visible = false;
            this.btnDiscard.Click += new System.EventHandler(this.btnDiscard_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SorterAPI.Properties.Resources.LefttoRightnew;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(390, 234);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(369, 29);
            this.pictureBox1.TabIndex = 47;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(506, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Sorter sequence left to right";
            // 
            // imageMap1
            // 
            this.imageMap1.Image = null;
            this.imageMap1.ImeMode = System.Windows.Forms.ImeMode.Close;
            this.imageMap1.Location = new System.Drawing.Point(6, 85);
            this.imageMap1.Name = "imageMap1";
            this.imageMap1.Size = new System.Drawing.Size(1135, 119);
            this.imageMap1.TabIndex = 44;
            this.imageMap1.Paint += new System.Windows.Forms.PaintEventHandler(this.imageMap1_Paint);
            this.imageMap1.RegionClick += new SorterAPI.ImageMap.RegionClickDelegate(this.imageMap1_RegionClick);
            // 
            // ProceedtoSorter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 273);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnDiscard);
            this.Controls.Add(this.lblRack1);
            this.Controls.Add(this.imageMap1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProceedtoSorter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure Sorter";
            this.Load += new System.EventHandler(this.ProceedtoSorter_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ProceedtoSorter_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRack1;
        private ImageMap imageMap1;
        private System.Windows.Forms.Button btnDiscard;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
    }
}