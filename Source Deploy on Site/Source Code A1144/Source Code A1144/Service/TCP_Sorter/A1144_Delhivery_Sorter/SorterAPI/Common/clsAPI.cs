﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using SorterAPI_CommonFunction;
using System.Diagnostics;
using System.Data;
using AMBPL.Common;

namespace SorterAPI.Common
{
    public class clsAPI
    {
        public int SorterNo { get; set; }
        public int RackId { get; set; }
        public string Pincodestring { get; set; }
        public int PullyDistance { get; set; }
        public int PPREncoder { get; set; }
        public int ConveyorSpeed { get; set; }
        public int ActivetionTime { get; set; }
        public int LinearDistance { get; set; }
        public int ActConstantDistance { get; set; }
        public int DeactConstantDistance { get; set; }
        public int PLCScanTime { get; set; }

        public int StartPointSorter { get; set; }
        public int DataLoadSensor { get; set; }
        public string Branches { get; set; }
        public string imgPath { get; set; }
        public int NumberofSorter { get; set; }

        public int MinNoArticle { get; set; }
        public int MaxNoArticle { get; set; }
        public int MaxWeightArticle { get; set; }
        public string BagClosurePrefix { get; set; }

        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactNumber { get; set; }
        public byte[] CompanyLogo { get; set; }
        public string OurContactPerson { get; set; }
        public string OurContactEmail { get; set; }
        public string OurContactNumber { get; set; }
        public int CircleId { get; set; }
        public int RegionId { get; set; }

        public int DivisionId { get; set; }
        public string DestinationOffice { get; set; }
        public int Pincode { get; set; }
        public int PincodeFrom { get; set; }
        public int PincodeTo { get; set; }
        public string Parcel_Type { get; set; }
        public int CasseteAddr { get; set; }
        public int Binno { get; set; }

        public string DataSource { get; set; }
        public string DB1 { get; set; }
        public string DB2 { get; set; }
        public string DB3 { get; set; }
        public string DBName { get; set; }
        public string TableName { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }

        public string OurDataSource { get; set; }
        public string OurDB1 { get; set; }
        public string OurDB2 { get; set; }
        public string OurDB3 { get; set; }
        public string OurDBName { get; set; }
        public string OurTableName { get; set; }
        public string OurColumn1 { get; set; }
        public string OurColumn2 { get; set; }
        public string OurColumn3 { get; set; }
        public string OurColumn4 { get; set; }

        public string Formula { get; set; }
        public int MinH { get; set; }
        public int MaxH { get; set; }
        public int MinW { get; set; }
        public int MaxW { get; set; }
        public int MinL { get; set; }
        public int MaxL { get; set; }
        public int MinWe { get; set; }
        public int MaxWe { get; set; }

        public int Id { get; set; }

        public string UserName { get; set; }
        public string UserPass { get; set; }
        public int IsActive { get; set; }
        public int BatchEdit { get; set; }
        public int BatchView { get; set; }
        public int LocationEdit { get; set; }
        public int LocationView { get; set; }
        public int ReportView { get; set; }

        public int IsPTL { get; set; }
        public int IsDWS { get; set; }
        public int IsSorts { get; set; }

        public string portname { get; set; }
        public int baudRate { get; set; }
        public int dataBits { get; set; }
        public int DataExportType { get; set; }
        public string FolderLocation { get; set; }
        public string WPortName { get; set; }
        public string ScannerIP { get; set; }
        public string PLCIP { get; set; }

        public int Distance { get; set; }
        public int NoOfBagsUp { get; set; }
        public int NoOfBagsDown { get; set; }
        public string ArmDirection { get; set; }
        public string date { get; set; }
        public int IsOption { get; set; }
        public string textboxvalue { get; set; }
        public string customer { get; set; }

        public string PlanTitle { get; set; }
        public string Fromdt { get; set; }
        public string Todt { get; set; }
        public int PlanPriority { get; set; }
        public int PlanDetailId { get; set; }
        public int UserId { get; set; }
        public int PLCPort { get; set; }
        public string ScannerPort { get; set; }
        public string L { get; set; }
        public string H { get; set; }
        public string W { get; set; }
        public string V { get; set; }
        public string barcode { get; set; }
        public double Weight { get; set; }
        public int bit { get; set; }
        public int btnValue { get; set; }
        public string Status { get; set; }
        public string barcode1 { get; set; }
        public string barcode2 { get; set; }
        public string barcode3 { get; set; }
        public string SorterType { get; set; }

        public string DistTravebyConvencoderpulse { get; set; }
        public string PropixSpeed { get; set; }
        public string RequiredDistance { get; set; }
        public string IdDataLoadingZone { get; set; }
        public string DataTrakingZone { get; set; }

        public int IsMst { get; set; }
        public string BagType { get; set; }
        public string OuCode { get; set; }
        public string MstOUCode { get; set; }
        public int MstOUCodeId { get; set; }
        public int WbaudRate { get; set; }
        public string ParcelType { get; set; }



        public static int InsertPLCDynamicData(clsAPI obj)
        {
            SqlCommand cmdPLCData = new SqlCommand();
            try
            {
                cmdPLCData.Parameters.AddWithValue("@PullyDistance", obj.PullyDistance);
                cmdPLCData.Parameters.AddWithValue("@PPREncoder", obj.PPREncoder);
                cmdPLCData.Parameters.AddWithValue("@ConveyorSpeed", obj.ConveyorSpeed);
                cmdPLCData.Parameters.AddWithValue("@ActivetionTime", obj.ActivetionTime);
                cmdPLCData.Parameters.AddWithValue("@LinearDistance", obj.LinearDistance);
                cmdPLCData.Parameters.AddWithValue("@ActConstantDistance", obj.ActConstantDistance);
                cmdPLCData.Parameters.AddWithValue("@DeactConstantDistance", obj.DeactConstantDistance);
                cmdPLCData.Parameters.AddWithValue("@PLCScanTime", obj.PLCScanTime);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_PLCDynamicData_Insert", cmdPLCData);
        }

        public static int UpdatePLCDynamicData(clsAPI obj)
        {
            SqlCommand cmdPLCData = new SqlCommand();
            try
            {
                cmdPLCData.Parameters.AddWithValue("@PullyDistance", obj.PullyDistance);
                cmdPLCData.Parameters.AddWithValue("@PPREncoder", obj.PPREncoder);
                cmdPLCData.Parameters.AddWithValue("@ConveyorSpeed", obj.ConveyorSpeed);
                cmdPLCData.Parameters.AddWithValue("@ActivetionTime", obj.ActivetionTime);
                cmdPLCData.Parameters.AddWithValue("@LinearDistance", obj.LinearDistance);
                cmdPLCData.Parameters.AddWithValue("@ActConstantDistance", obj.ActConstantDistance);
                cmdPLCData.Parameters.AddWithValue("@DeactConstantDistance", obj.DeactConstantDistance);
                cmdPLCData.Parameters.AddWithValue("@PLCScanTime", obj.PLCScanTime);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_PLCDynamicData_Update", cmdPLCData);
        }

        public static DataTable getBininSorterData()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_BininSorterData_get", cmd);
        }
        public static int InsertPLCDiemensionalData(clsAPI objd)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@NumberofSorter", objd.NumberofSorter);
                cmd.Parameters.AddWithValue("@DataLoadSensor", objd.DataLoadSensor);
                cmd.Parameters.AddWithValue("@IsDWS", objd.IsDWS);
                cmd.Parameters.AddWithValue("@IsPTL", objd.IsPTL);
                cmd.Parameters.AddWithValue("@IsSorts", objd.IsSorts);

                cmd.Parameters.AddWithValue("@PortName", objd.portname);
                cmd.Parameters.AddWithValue("@BaudRate", objd.baudRate);
                cmd.Parameters.AddWithValue("@DataBits", objd.dataBits);

                cmd.Parameters.AddWithValue("@DataExportType", objd.DataExportType);
                cmd.Parameters.AddWithValue("@FolderLocation", objd.FolderLocation);
                cmd.Parameters.AddWithValue("@PLCIP", objd.PLCIP);
                cmd.Parameters.AddWithValue("@PLCPort", objd.PLCPort);
                cmd.Parameters.AddWithValue("@ScannerIP", objd.ScannerIP);
                cmd.Parameters.AddWithValue("@ScannerPort", objd.ScannerPort);

                cmd.Parameters.AddWithValue("@WPortName", objd.WPortName);

            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_PLCDiemensionalData_Insert", cmd);
        }
        public static int UpdatePLCDiemensionalData(clsAPI objd)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@NumberofSorter", objd.NumberofSorter);
                cmd.Parameters.AddWithValue("@DataLoadSensor", objd.DataLoadSensor);
                // cmd.Parameters.AddWithValue("@IsDWS", objd.IsDWS);
                cmd.Parameters.AddWithValue("@IsPTL", objd.IsPTL);
                cmd.Parameters.AddWithValue("@IsSorts", objd.IsSorts);

                cmd.Parameters.AddWithValue("@PortName", objd.portname);
                cmd.Parameters.AddWithValue("@BaudRate", objd.baudRate);
                // cmd.Parameters.AddWithValue("@DataBits", objd.dataBits);                
                cmd.Parameters.AddWithValue("@DataExportType", objd.DataExportType);
                cmd.Parameters.AddWithValue("@FolderLocation", objd.FolderLocation);

                cmd.Parameters.AddWithValue("@PLCIP", objd.PLCIP);
                cmd.Parameters.AddWithValue("@PLCPort", objd.PLCPort);
                cmd.Parameters.AddWithValue("@ScannerIP", objd.ScannerIP);
                cmd.Parameters.AddWithValue("@ScannerPort", objd.ScannerPort);
                cmd.Parameters.AddWithValue("@WPortName", objd.WPortName);
                cmd.Parameters.AddWithValue("@WbaudRate", objd.WbaudRate);

                cmd.Parameters.AddWithValue("@SorterType", objd.SorterType);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_PLCDiemensionalData_Update", cmd);
        }

        public static DataSet getPLCDiemensionalData()
        {
            SqlCommand cmd = new SqlCommand();
            return clsDataAccess.GetDataSet("sp_PLCDiemensionalData_get", cmd);
        }

        public static DataTable UpdateGetImgPath(clsAPI objimg)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@imgPath", objimg.imgPath);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.GetDataTable("sp_ImagePath_UpdateGet", cmd);
        }

        public static int InsertPTLSystem(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@MinNoArticle", obj.MinNoArticle);
                cmd.Parameters.AddWithValue("@MaxNoArticle", obj.MaxNoArticle);
                cmd.Parameters.AddWithValue("@MaxWeightArticle", obj.MaxWeightArticle);
                cmd.Parameters.AddWithValue("@BagClosurePrefix", obj.BagClosurePrefix);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_PTLSystem_Insert", cmd);
        }

        public static DataTable getPTLSystemData()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_PTLSystemData_get", cmd);
        }

        public static int UpdatePTLSystem(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@MinNoArticle", obj.MinNoArticle);
                cmd.Parameters.AddWithValue("@MaxNoArticle", obj.MaxNoArticle);
                cmd.Parameters.AddWithValue("@MaxWeightArticle", obj.MaxWeightArticle);
                cmd.Parameters.AddWithValue("@BagClosurePrefix", obj.BagClosurePrefix);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_PTLSystem_Update", cmd);
        }

        public static int InsertGeneralSetting(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@CompanyName", obj.CompanyName);
                cmd.Parameters.AddWithValue("@Address", obj.Address);
                cmd.Parameters.AddWithValue("@ContactPersonName", obj.ContactPersonName);
                cmd.Parameters.AddWithValue("@ContactEmail", obj.ContactEmail);
                cmd.Parameters.AddWithValue("@ContactNumber", obj.ContactNumber);
                cmd.Parameters.AddWithValue("@CompanyLogo", obj.CompanyLogo);
                cmd.Parameters.AddWithValue("@OurContactPerson", obj.OurContactPerson);
                cmd.Parameters.AddWithValue("@OurContactEmail", obj.OurContactEmail);
                cmd.Parameters.AddWithValue("@OurContactNumber", obj.OurContactNumber);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_GeneralSetting_Insert", cmd);
        }

        public static DataTable getGenralSettingsData()
        {
            SqlCommand cmd = new SqlCommand();
            ErrorLog oErrorLog = new ErrorLog();
            return clsDataAccess.GetDataTable("sp_GenralSettingsData_get", cmd);
            //try
            //{
            //}
            //catch (Exception ex)
            //{
            //    oErrorLog.WriteErrorLog(ex.ToString());
            //}
        }

        public static int UpdateGeneralSettings(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@CompanyName", obj.CompanyName);
                cmd.Parameters.AddWithValue("@Address", obj.Address);
                cmd.Parameters.AddWithValue("@ContactPersonName", obj.ContactPersonName);
                cmd.Parameters.AddWithValue("@ContactEmail", obj.ContactEmail);
                cmd.Parameters.AddWithValue("@ContactNumber", obj.ContactNumber);
                cmd.Parameters.AddWithValue("@CompanyLogo", obj.CompanyLogo);
                cmd.Parameters.AddWithValue("@OurContactPerson", obj.OurContactPerson);
                cmd.Parameters.AddWithValue("@OurContactEmail", obj.OurContactEmail);
                cmd.Parameters.AddWithValue("@OurContactNumber", obj.OurContactNumber);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_GeneralSetting_Update", cmd);
        }

        public static DataTable GetCircle()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_Circle_get", cmd);
        }

        public static DataTable GetRegion(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@CircleId", obj.CircleId);
            return CommonFunctions.GetDataTable("sp_Region_get", cmd);
        }

        public static DataTable GetDivision(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@RegionId", obj.RegionId);
            return CommonFunctions.GetDataTable("sp_Division_get", cmd);
        }

        public static int SaveNewLocation(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@DivisionId", obj.DivisionId);
                cmd.Parameters.AddWithValue("@DestinationOffice", obj.DestinationOffice);
                cmd.Parameters.AddWithValue("@Pincode", obj.Pincode);

            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_Location_Insert", cmd);
        }

        public static DataTable getPincode(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@DivisionId", obj.DivisionId);
            return CommonFunctions.GetDataTable("sp_LocationPincode_get", cmd);
        }

        public static int SaveLocationAddr(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@Pincode", obj.Pincode);
                cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);
                // cmd.Parameters.AddWithValue("@Binno", obj.Binno);
                // cmd.Parameters.AddWithValue("@CasseteAddr", obj.CasseteAddr);

            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_LocationBranchBin_Insert", cmd);
        }

        public static int UpdateLocationAddr(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@PincodeFrom", obj.PincodeFrom);
                cmd.Parameters.AddWithValue("@PincodeTo", obj.PincodeTo);
                cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);
                cmd.Parameters.AddWithValue("@ParcelType", obj.Parcel_Type);
                 

            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_LocationBranchBin_Update", cmd);
        }

        public static DataTable getSqlDatabase()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_DatabaseList_get", cmd);
        }

        public static int InsertDatabaseInfo(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@DataSource", obj.DataSource);
                cmd.Parameters.AddWithValue("@DB1", obj.DB1);
                cmd.Parameters.AddWithValue("@DB2", obj.DB2);
                cmd.Parameters.AddWithValue("@DB3", obj.DB3);
                cmd.Parameters.AddWithValue("@DBName", obj.DBName);
                cmd.Parameters.AddWithValue("@TableName", obj.TableName);
                cmd.Parameters.AddWithValue("@Column1", obj.Column1);
                cmd.Parameters.AddWithValue("@Column2", obj.Column2);
                cmd.Parameters.AddWithValue("@Column3", obj.Column3);
                cmd.Parameters.AddWithValue("@Column4", obj.Column4);

                cmd.Parameters.AddWithValue("@OurDataSource", obj.OurDataSource);
                cmd.Parameters.AddWithValue("@OurDB1", obj.OurDB1);
                cmd.Parameters.AddWithValue("@OurDB2", obj.OurDB2);
                cmd.Parameters.AddWithValue("@OurDB3", obj.OurDB3);
                cmd.Parameters.AddWithValue("@OurDBName", obj.OurDBName);
                cmd.Parameters.AddWithValue("@OurTableName", obj.OurTableName);
                cmd.Parameters.AddWithValue("@OurColumn1", obj.OurColumn1);
                cmd.Parameters.AddWithValue("@OurColumn2", obj.OurColumn2);
                cmd.Parameters.AddWithValue("@OurColumn3", obj.OurColumn3);
                cmd.Parameters.AddWithValue("@OurColumn4", obj.OurColumn4);

                cmd.Parameters.AddWithValue("@MinH", obj.MinH);
                cmd.Parameters.AddWithValue("@MaxH", obj.MaxH);
                cmd.Parameters.AddWithValue("@MinW", obj.MinW);
                cmd.Parameters.AddWithValue("@MaxW", obj.MaxW);

                cmd.Parameters.AddWithValue("@MinL", obj.MinL);
                cmd.Parameters.AddWithValue("@MaxL", obj.MaxL);
                cmd.Parameters.AddWithValue("@MinWe", obj.MinWe);
                cmd.Parameters.AddWithValue("@MaxWe", obj.MaxWe);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_ColumnMapping_Insert", cmd);
        }

        public static int InsertFormula(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@Id", obj.Id);
                cmd.Parameters.AddWithValue("@Formula", obj.Formula);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_Formula_tempSave", cmd);
        }

        public static DataTable getFormula()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_Formula_tempGet", cmd);
        }

        public static int InsertNewUser(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@UserName", obj.UserName);
                cmd.Parameters.AddWithValue("@UserPass", obj.UserPass);
                cmd.Parameters.AddWithValue("@IsActive", obj.IsActive);
                cmd.Parameters.AddWithValue("@BatchEdit", obj.BatchEdit);
                cmd.Parameters.AddWithValue("@BatchView", obj.BatchView);
                cmd.Parameters.AddWithValue("@LocationEdit", obj.LocationEdit);
                cmd.Parameters.AddWithValue("@LocationView", obj.LocationView);
                cmd.Parameters.AddWithValue("@ReportView", obj.ReportView);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_UserCreation_Insert", cmd);
        }
        public static DataTable getUserList(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@UserId", id);
            return CommonFunctions.GetDataTable("sp_NewUserList_Get", cmd);
        }

        public static int UpdateNewUser(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@UserId", obj.Id);
                cmd.Parameters.AddWithValue("@UserName", obj.UserName);
                cmd.Parameters.AddWithValue("@UserPass", obj.UserPass);
                cmd.Parameters.AddWithValue("@IsActive", obj.IsActive);
                cmd.Parameters.AddWithValue("@BatchEdit", obj.BatchEdit);
                cmd.Parameters.AddWithValue("@BatchView", obj.BatchView);
                cmd.Parameters.AddWithValue("@LocationEdit", obj.LocationEdit);
                cmd.Parameters.AddWithValue("@LocationView", obj.LocationView);
                cmd.Parameters.AddWithValue("@ReportView", obj.ReportView);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_UserCreation_Update", cmd);
        }

        public static DataTable getArmDirectionDataGrid(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@SorterId", id);
            return CommonFunctions.GetDataTable("sp_ArmDirectionDataGrid_Get", cmd);
        }

        public static int InsertArmDirection(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);
                cmd.Parameters.AddWithValue("@ArmDirection", obj.ArmDirection);
                cmd.Parameters.AddWithValue("@NoOfBagsUp", obj.NoOfBagsUp);
                cmd.Parameters.AddWithValue("@NoOfBagsDown", obj.NoOfBagsDown);
                cmd.Parameters.AddWithValue("@Distance", obj.Distance);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_ArmDirection_Insert", cmd);
        }

        public static int UpdateArmDirection(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                // cmd.Parameters.AddWithValue("@Id", obj.Id);
                cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);
                cmd.Parameters.AddWithValue("@ArmDirection", obj.ArmDirection);
                cmd.Parameters.AddWithValue("@NoOfBagsUp", obj.NoOfBagsUp);
                cmd.Parameters.AddWithValue("@NoOfBagsDown", obj.NoOfBagsDown);
                cmd.Parameters.AddWithValue("@Distance", obj.Distance);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return CommonFunctions.ExecuteNonQuery("sp_ArmDirection_Update", cmd);
        }

        public static int DeleteArmDirectionDataGrid(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@SorterNo", id);
            return CommonFunctions.ExecuteNonQuery("sp_ArmDirectionDataGrid_Delete", cmd);
        }

        public static DataTable getPLCData()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_PLCData_get", cmd);
        }

        public static DataTable getDWSCount_Data(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@IsOption", obj.IsOption);// for check 1 period or  2 option

            cmd.Parameters.AddWithValue("@IsCheck", obj.Id);
            cmd.Parameters.AddWithValue("@Date", obj.date); // for date wise search

            cmd.Parameters.AddWithValue("@textboxvalue", obj.textboxvalue); // searching barcode and location wise
            cmd.Parameters.AddWithValue("@Customer", obj.customer);   // selected customer wise search
            cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);   // selected sorter wise search

            return CommonFunctions.GetDataTable("sp_DWSCount_get", cmd);
        }

        public static DataTable InsertPlanDetails(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@PlanTitle", obj.PlanTitle);// 
            cmd.Parameters.AddWithValue("@Fromdt", obj.Fromdt);
            cmd.Parameters.AddWithValue("@Todt", obj.Todt);
            cmd.Parameters.AddWithValue("@PlanPriority", obj.PlanPriority);
            cmd.Parameters.AddWithValue("@UserId", obj.UserId);

            return CommonFunctions.GetDataTable("sp_PlanDetails_Insert", cmd);
        }

        public static int InsertLocationSorter(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Pincode", obj.Pincode);
            cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);
            cmd.Parameters.AddWithValue("@BagsUp", obj.NoOfBagsUp);
            cmd.Parameters.AddWithValue("@BagsDown", obj.NoOfBagsDown);
            cmd.Parameters.AddWithValue("@PlanDetailId", obj.PlanDetailId);

            return CommonFunctions.ExecuteNonQuery("sp_LocationBranchBin_Insert", cmd);
        }

        public static DataTable getPlanDetailsforGrid()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_PlanDetails_Getforgrid", cmd);
        }

        public static int UpdatePlanDetails(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@PlanTitle", obj.PlanTitle);
            cmd.Parameters.AddWithValue("@Fromdt", obj.Fromdt);
            cmd.Parameters.AddWithValue("@Todt", obj.Todt);
            cmd.Parameters.AddWithValue("@PlanPriority", obj.PlanPriority);
            cmd.Parameters.AddWithValue("@UserId", obj.UserId);

            return CommonFunctions.ExecuteNonQuery("sp_PlanDetails_Update", cmd);
        }

        public static int DeletePlanDetail(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Id", id);
            return CommonFunctions.ExecuteNonQuery("sp_PlanDetail_Delete", cmd);
        }

        public static DataTable getPriotrity(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Fromdt", obj.Fromdt);
            cmd.Parameters.AddWithValue("@Todt", obj.Todt);
            return CommonFunctions.GetDataTable("sp_Priority_get", cmd);
        }

        public static DataTable getBin(int branch)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@SorterNo", branch);
            return CommonFunctions.GetDataTable("sp_Bin_get", cmd);
        }

        public static DataTable CheckPin(int Pin)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Pin", Pin);
            return CommonFunctions.GetDataTable("sp_CheckPin_get", cmd);
        }

        public static DataSet getBatchOperation(string ParcelType)
        {
            SqlCommand cmd = new SqlCommand();
            
            return clsDataAccess.GetDataSet("sp_BatchOperation_get", cmd);
        }

        public static DataSet getPincode_sorter()
        {
            SqlCommand cmd = new SqlCommand();            
            return clsDataAccess.GetDataSet("sp_Pincode_sorter_get", cmd);
        }

        public static DataTable getPort()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_PortName_get", cmd);
        }

        public static int InserLHW(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@L", obj.L);
            cmd.Parameters.AddWithValue("@H", obj.H);
            cmd.Parameters.AddWithValue("@W", obj.W);

            return CommonFunctions.ExecuteNonQuery("sp_LHWV_Insert", cmd);
        }

        public static DataTable GetBarcode()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_Barcode_get", cmd);
        }

        public static DataTable UpdateBarcodeNWeight(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@barcode", obj.barcode);
            cmd.Parameters.AddWithValue("@L", obj.L);
            cmd.Parameters.AddWithValue("@H", obj.H);
            cmd.Parameters.AddWithValue("@W", obj.W);
            cmd.Parameters.AddWithValue("@V", obj.V);
            cmd.Parameters.AddWithValue("@Weight", obj.Weight);
            cmd.Parameters.AddWithValue("@Pincode", obj.Pincode);
            cmd.Parameters.AddWithValue("@ParcelType", obj.ParcelType);

            return CommonFunctions.GetDataTable("sp_BarcodeNWeight_Update", cmd);
        }

        public static DataTable GetVMSHeight()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_VMSHeight_Get", cmd);
        }

        public static int VMSError_Insert()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.ExecuteNonQuery("sp_VMSError_Insert", cmd);
        }

        public static int UpdateHurtBit()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.ExecuteNonQuery("sp_HurtBit_Update", cmd);
        }

        public static DataTable GetPLCHurtBit()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_PLCHurtBit_Get", cmd);
        }

        public static DataTable getEMSINData()
        {
            SqlCommand cmd = new SqlCommand();
            return CommonFunctions.GetDataTable("sp_EMSINData_Get", cmd);
        }

        public static DataSet getDWSReport(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@StartDate", obj.Fromdt);
            cmd.Parameters.AddWithValue("@EndDate", obj.Todt);
            return clsDataAccess.GetDataSet("sp_DWSReport_get", cmd);
        }
        public static DataSet getWeekwiseDWSReport(int WeekNo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@weekNo", WeekNo);
            //cmd.Parameters.AddWithValue("@EndDate", obj.Todt);
            return clsDataAccess.GetDataSet("sp_WeekwiseDWSReport_get", cmd);
        }

        public static DataTable GetLocatioBay()
        {
            SqlCommand cmd = new SqlCommand();

            return clsDataAccess.GetDataTable("sp_GetLocationBay_get", cmd);
        }

        public static int TakeBAKnDeleteLastMonthBeforeData()
        {
            SqlCommand cmd = new SqlCommand();
            return clsDataAccess.ExecuteNonQuery("sp_TakeBAKnDeleteLastMonthBeforeData", cmd);
        }

        public static DataTable GetSearchBarcodeData(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Barcode", obj.barcode);
            return clsDataAccess.GetDataTable("sp_SearchBarcodeData_Get", cmd);
        }


        public static int Barcode_Insert(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@Barcode", obj.barcode);
            cmd.Parameters.AddWithValue("@L", obj.L);
            cmd.Parameters.AddWithValue("@H", obj.H);
            cmd.Parameters.AddWithValue("@W", obj.W);
            cmd.Parameters.AddWithValue("@Bay", obj.Branches);
            cmd.Parameters.AddWithValue("@Bin", obj.Binno);
            cmd.Parameters.AddWithValue("@Pincode", obj.Pincode);
            cmd.Parameters.AddWithValue("@Status", obj.Status);

            return CommonFunctions.ExecuteNonQuery("sp_tblVMS_Insert", cmd);
        }

        public static DataTable getBarcodeBin(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Barcode", obj.barcode);
            return clsDataAccess.GetDataTable("sp_BarcodeBin_Get", cmd);
        }

        public static int UpdateDWSSettingData(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@DistTravebyConvencoderpulse", obj.DistTravebyConvencoderpulse);
            cmd.Parameters.AddWithValue("@PropixSpeed", obj.PropixSpeed);
            cmd.Parameters.AddWithValue("@RequiredDistance", obj.RequiredDistance);
            cmd.Parameters.AddWithValue("@IdDataLoadingZone", obj.IdDataLoadingZone);
            cmd.Parameters.AddWithValue("@DataTrakingZone", obj.DataTrakingZone);

            return CommonFunctions.ExecuteNonQuery("sp_DWSSettingData_Update", cmd);
        }
        public static DataTable getDWSPLCSetting()
        {
            SqlCommand cmd = new SqlCommand();
            return clsDataAccess.GetDataTable("sp_DWSPLCSetting_Get", cmd);
        }

        public static int InsertOuCode(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@BagType", obj.BagType);
            cmd.Parameters.AddWithValue("@OuCode", obj.OuCode);
            cmd.Parameters.AddWithValue("@IsMst", obj.IsMst);
            cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);
            cmd.Parameters.AddWithValue("@Binno", obj.Binno);
            cmd.Parameters.AddWithValue("@MstOUCode", obj.MstOUCode);
            cmd.Parameters.AddWithValue("@MstOUCodeId", obj.MstOUCodeId);


            return CommonFunctions.ExecuteNonQuery("sp_OuCode_Insert", cmd);
        }

        public static DataTable CheckOu(clsAPI obj)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@OuCode", obj.OuCode);
            cmd.Parameters.AddWithValue("@SorterNo", obj.SorterNo);
            cmd.Parameters.AddWithValue("@Binno", obj.Binno);

            return clsDataAccess.GetDataTable("sp_OuCode_Check", cmd);
        }

        public static DataTable DeleteOu(string OuCode)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@OuCode", OuCode);

            return clsDataAccess.GetDataTable("sp_OuCode_Delete", cmd);
        }
    }
}
