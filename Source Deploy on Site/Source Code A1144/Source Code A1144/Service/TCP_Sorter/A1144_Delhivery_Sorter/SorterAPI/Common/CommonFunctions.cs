﻿using System; 
using System.Collections.Generic; 
using System.ComponentModel; 
using System.Data; 
using System.Data.Common;
using System.Drawing; 
using System.Linq; 
using System.Text; 
using System.Windows.Forms; 
using System.Data.SqlClient;
using System.Configuration; 
using System.Diagnostics;
using SorterAPI.Common;
using AMBPL.Common;

namespace SorterAPI_CommonFunction
{
    public class CommonFunctions
    {
       // public static string xmlPath = @"G:\WindowsFormsApplication7\17-Dec-2015\gpo23-12-2015.xml";
        public static string xmlPath = @"G:\Kedar\Projects\17-Dec-2015\gpo30-12-2015.xml";

        public static string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }

        public static DataTable GetDataTable(string ProcName, SqlCommand cmd)
        {
          
                SqlConnection cn = new SqlConnection(clsDataAccess.GetConnectionString());
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = ProcName;
                    da.Fill(dt);
                }
                catch (Exception e)
                {
                    Debug.Write(e.Message);
                }
                finally
                {
                    da.Dispose();
                    cmd.Dispose();
                    cn.Close();
                }
                return dt;            
        }
        public static int ExecuteNonQuery(string ProcName, SqlCommand cmd)
        {
            int ResultExecuteNonQuery = 0;
            SqlConnection cn = new SqlConnection(clsDataAccess.GetConnectionString());
            try
            {
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                cn.Open();
                ResultExecuteNonQuery = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
            }
            return ResultExecuteNonQuery;
        }  
    }
}
