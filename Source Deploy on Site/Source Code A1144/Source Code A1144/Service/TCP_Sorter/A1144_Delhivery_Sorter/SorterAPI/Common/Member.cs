﻿using System; using System.Collections.Generic; using System.Linq; using System.Text; using System.Data.SqlClient; using System.Data;
using System.Diagnostics;
using SorterAPI_CommonFunction;
using SorterAPI.Common;
using AMBPL.Common;

namespace SorterAPI_Member
{
    public class Member
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        internal string FirstName { get; set; }
        internal string LastName { get; set; }
        internal DateTime DOJ { get; set; }
        internal string Department { get; set; }
        internal DateTime CreatedON { get; set; }
        public string Password { get; set; }
        public int IsActive { get; set; }
        public int status { get; set; }

        public static DataTable ValidateLogin(Member member)
        {
            SqlCommand cmdValLog = new SqlCommand();
            try
            {
                cmdValLog.Parameters.AddWithValue("@UserName", member.UserName);
                cmdValLog.Parameters.AddWithValue("@Password", member.Password);
            }
            catch(Exception e)
            {
                Debug.Write(e.Message);
                //throw new System.NotImplementedException();
            }
            return clsDataAccess.GetDataTable("sp_ValidateLogin", cmdValLog);
        }

        public static DataTable BindUserList()
        {
            try
            {
                SqlCommand cmdBindUserList = new SqlCommand();
                return clsDataAccess.GetDataTable("FetchAllUsers", cmdBindUserList);
            }
            catch
            {
                throw new System.NotImplementedException();
            }
        }

        public static int DeleteUser(Member member)
        {
            SqlCommand cmdDelUser = new SqlCommand();
            try
            {
                cmdDelUser.Parameters.AddWithValue("@Uid", member.UserId); 
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
                throw new System.NotImplementedException();
            }
            return clsDataAccess.ExecuteNonQuery("DeleteUser", cmdDelUser);
        }

        public static int AddEditUsers(Member member)
        { 
            SqlCommand cmdAddUser = new SqlCommand();
            try
            {
                cmdAddUser.Parameters.AddWithValue("@Username", member.UserName);
                cmdAddUser.Parameters.AddWithValue("@Password", member.Password);
                cmdAddUser.Parameters.AddWithValue("@FName", member.FirstName);
                cmdAddUser.Parameters.AddWithValue("@LName", member.LastName);
                cmdAddUser.Parameters.AddWithValue("@UserDOJ", member.DOJ);
                cmdAddUser.Parameters.AddWithValue("@Department", member.Department);
                cmdAddUser.Parameters.AddWithValue("@IsActive", member.IsActive);
                cmdAddUser.Parameters.AddWithValue("@CreatedOn", "");
                cmdAddUser.Parameters.AddWithValue("@Uid", 1);
                cmdAddUser.Parameters.AddWithValue("@status", 1); 
            }
            catch (Exception e)
            {
               Debug.Write(e.Message);
               throw new System.NotImplementedException();
            }
            return clsDataAccess.ExecuteNonQuery("AddEditUsers", cmdAddUser);
        }
    }
}