﻿namespace SorterAPI
{
    partial class MDIParent1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParent1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlBusinessParcel = new System.Windows.Forms.Panel();
            this.lblBusinessParcel = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pnlOUMap = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblOUMapp = new System.Windows.Forms.Label();
            this.pnlSearchBarcode = new System.Windows.Forms.Panel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.lblSearchBarcode = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlBatchOperation = new System.Windows.Forms.Panel();
            this.picBatchOperation = new System.Windows.Forms.PictureBox();
            this.lblBarchOperation = new System.Windows.Forms.Label();
            this.pnlExit = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lblExit = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBoxClose = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlAdminMenu = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.btn100Parcels = new System.Windows.Forms.Button();
            this.btnA4Jam = new System.Windows.Forms.Button();
            this.btnA2Jam = new System.Windows.Forms.Button();
            this.btnLineJam = new System.Windows.Forms.Button();
            this.btnEstopOP02 = new System.Windows.Forms.Button();
            this.btnESTOPOP01 = new System.Windows.Forms.Button();
            this.btnEstopCPO1 = new System.Windows.Forms.Button();
            this.btnA4Healthy = new System.Windows.Forms.Button();
            this.btnA2healthy = new System.Windows.Forms.Button();
            this.btnA1Helathy = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pnlBusinessParcel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.pnlOUMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnlSearchBarcode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlBatchOperation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBatchOperation)).BeginInit();
            this.pnlExit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).BeginInit();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.pnlBusinessParcel);
            this.panel1.Controls.Add(this.pnlOUMap);
            this.panel1.Controls.Add(this.pnlSearchBarcode);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnlBatchOperation);
            this.panel1.Controls.Add(this.pnlExit);
            this.panel1.Location = new System.Drawing.Point(133, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(221, 201);
            this.panel1.TabIndex = 10;
            this.panel1.Visible = false;
            // 
            // pnlBusinessParcel
            // 
            this.pnlBusinessParcel.BackColor = System.Drawing.Color.Gray;
            this.pnlBusinessParcel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBusinessParcel.Controls.Add(this.lblBusinessParcel);
            this.pnlBusinessParcel.Controls.Add(this.pictureBox8);
            this.pnlBusinessParcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlBusinessParcel.Enabled = false;
            this.pnlBusinessParcel.ForeColor = System.Drawing.Color.Black;
            this.pnlBusinessParcel.Location = new System.Drawing.Point(2, 202);
            this.pnlBusinessParcel.Name = "pnlBusinessParcel";
            this.pnlBusinessParcel.Size = new System.Drawing.Size(219, 64);
            this.pnlBusinessParcel.TabIndex = 29;
            this.pnlBusinessParcel.Visible = false;
            this.pnlBusinessParcel.Click += new System.EventHandler(this.pnlBusinessParcel_Click);
            // 
            // lblBusinessParcel
            // 
            this.lblBusinessParcel.AutoSize = true;
            this.lblBusinessParcel.BackColor = System.Drawing.Color.Transparent;
            this.lblBusinessParcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBusinessParcel.ForeColor = System.Drawing.Color.White;
            this.lblBusinessParcel.Location = new System.Drawing.Point(53, 21);
            this.lblBusinessParcel.Name = "lblBusinessParcel";
            this.lblBusinessParcel.Size = new System.Drawing.Size(98, 20);
            this.lblBusinessParcel.TabIndex = 1;
            this.lblBusinessParcel.Text = "Parcel Net";
            this.lblBusinessParcel.Click += new System.EventHandler(this.lblBusinessParcel_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pictureBox8.BackgroundImage = global::SorterAPI.Properties.Resources.utilities;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(4, 13);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(44, 34);
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // pnlOUMap
            // 
            this.pnlOUMap.BackColor = System.Drawing.Color.Gray;
            this.pnlOUMap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOUMap.Controls.Add(this.pictureBox2);
            this.pnlOUMap.Controls.Add(this.lblOUMapp);
            this.pnlOUMap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlOUMap.ForeColor = System.Drawing.Color.Black;
            this.pnlOUMap.Location = new System.Drawing.Point(2, 310);
            this.pnlOUMap.Name = "pnlOUMap";
            this.pnlOUMap.Size = new System.Drawing.Size(219, 64);
            this.pnlOUMap.TabIndex = 31;
            this.pnlOUMap.Visible = false;
            this.pnlOUMap.Click += new System.EventHandler(this.pnlOUMap_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::SorterAPI.Properties.Resources.LocationSorter;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(4, 13);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 34);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // lblOUMapp
            // 
            this.lblOUMapp.AutoSize = true;
            this.lblOUMapp.BackColor = System.Drawing.Color.Transparent;
            this.lblOUMapp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOUMapp.ForeColor = System.Drawing.Color.White;
            this.lblOUMapp.Location = new System.Drawing.Point(53, 21);
            this.lblOUMapp.Name = "lblOUMapp";
            this.lblOUMapp.Size = new System.Drawing.Size(152, 20);
            this.lblOUMapp.TabIndex = 1;
            this.lblOUMapp.Text = "Pincode Mapping";
            this.lblOUMapp.Click += new System.EventHandler(this.lblOUMapp_Click);
            // 
            // pnlSearchBarcode
            // 
            this.pnlSearchBarcode.BackColor = System.Drawing.Color.Gray;
            this.pnlSearchBarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSearchBarcode.Controls.Add(this.pictureBox9);
            this.pnlSearchBarcode.Controls.Add(this.lblSearchBarcode);
            this.pnlSearchBarcode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlSearchBarcode.ForeColor = System.Drawing.Color.Black;
            this.pnlSearchBarcode.Location = new System.Drawing.Point(2, 271);
            this.pnlSearchBarcode.Name = "pnlSearchBarcode";
            this.pnlSearchBarcode.Size = new System.Drawing.Size(219, 64);
            this.pnlSearchBarcode.TabIndex = 30;
            this.pnlSearchBarcode.Visible = false;
            this.pnlSearchBarcode.Click += new System.EventHandler(this.pnlSearchBarcode_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox9.BackgroundImage = global::SorterAPI.Properties.Resources.SearchBarcode;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(4, 13);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(44, 34);
            this.pictureBox9.TabIndex = 0;
            this.pictureBox9.TabStop = false;
            // 
            // lblSearchBarcode
            // 
            this.lblSearchBarcode.AutoSize = true;
            this.lblSearchBarcode.BackColor = System.Drawing.Color.Transparent;
            this.lblSearchBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchBarcode.ForeColor = System.Drawing.Color.White;
            this.lblSearchBarcode.Location = new System.Drawing.Point(53, 21);
            this.lblSearchBarcode.Name = "lblSearchBarcode";
            this.lblSearchBarcode.Size = new System.Drawing.Size(144, 20);
            this.lblSearchBarcode.TabIndex = 1;
            this.lblSearchBarcode.Text = "Search Barcode";
            this.lblSearchBarcode.Click += new System.EventHandler(this.lblSearchBarcode_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(3, 67);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(219, 64);
            this.panel2.TabIndex = 2;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SorterAPI.Properties.Resources.Report11;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(4, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(44, 34);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(53, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Reports";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pnlBatchOperation
            // 
            this.pnlBatchOperation.BackColor = System.Drawing.Color.Gray;
            this.pnlBatchOperation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBatchOperation.Controls.Add(this.picBatchOperation);
            this.pnlBatchOperation.Controls.Add(this.lblBarchOperation);
            this.pnlBatchOperation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlBatchOperation.ForeColor = System.Drawing.Color.Black;
            this.pnlBatchOperation.Location = new System.Drawing.Point(2, 2);
            this.pnlBatchOperation.Name = "pnlBatchOperation";
            this.pnlBatchOperation.Size = new System.Drawing.Size(219, 64);
            this.pnlBatchOperation.TabIndex = 3;
            this.pnlBatchOperation.Click += new System.EventHandler(this.pnlBatchOperation_Click);
            this.pnlBatchOperation.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlBatchOperation_Paint);
            // 
            // picBatchOperation
            // 
            this.picBatchOperation.BackColor = System.Drawing.Color.Transparent;
            this.picBatchOperation.BackgroundImage = global::SorterAPI.Properties.Resources.batchOperation;
            this.picBatchOperation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBatchOperation.Location = new System.Drawing.Point(6, 14);
            this.picBatchOperation.Name = "picBatchOperation";
            this.picBatchOperation.Size = new System.Drawing.Size(43, 38);
            this.picBatchOperation.TabIndex = 0;
            this.picBatchOperation.TabStop = false;
            // 
            // lblBarchOperation
            // 
            this.lblBarchOperation.AutoSize = true;
            this.lblBarchOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBarchOperation.ForeColor = System.Drawing.Color.White;
            this.lblBarchOperation.Location = new System.Drawing.Point(53, 25);
            this.lblBarchOperation.Name = "lblBarchOperation";
            this.lblBarchOperation.Size = new System.Drawing.Size(142, 20);
            this.lblBarchOperation.TabIndex = 1;
            this.lblBarchOperation.Text = "Batch operation";
            this.lblBarchOperation.Click += new System.EventHandler(this.lblBarchOperation_Click);
            // 
            // pnlExit
            // 
            this.pnlExit.BackColor = System.Drawing.Color.Gray;
            this.pnlExit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlExit.Controls.Add(this.pictureBox6);
            this.pnlExit.Controls.Add(this.lblExit);
            this.pnlExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlExit.ForeColor = System.Drawing.Color.Black;
            this.pnlExit.Location = new System.Drawing.Point(2, 132);
            this.pnlExit.Name = "pnlExit";
            this.pnlExit.Size = new System.Drawing.Size(219, 64);
            this.pnlExit.TabIndex = 6;
            this.pnlExit.Click += new System.EventHandler(this.pnlExit_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox6.BackgroundImage = global::SorterAPI.Properties.Resources.CloseMenu;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(4, 13);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(44, 34);
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.BackColor = System.Drawing.Color.Transparent;
            this.lblExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.White;
            this.lblExit.Location = new System.Drawing.Point(53, 21);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(41, 20);
            this.lblExit.TabIndex = 1;
            this.lblExit.Text = "Exit";
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gray;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.pictureBox4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(599, 166);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(219, 64);
            this.panel3.TabIndex = 29;
            this.panel3.Visible = false;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = global::SorterAPI.Properties.Resources.LocationSorter;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(3, 14);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(44, 36);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(53, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Assign Bay";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // pictureBoxClose
            // 
            this.pictureBoxClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.pictureBoxClose.BackgroundImage = global::SorterAPI.Properties.Resources.CloseMenu;
            this.pictureBoxClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxClose.Location = new System.Drawing.Point(31, 13);
            this.pictureBoxClose.Name = "pictureBoxClose";
            this.pictureBoxClose.Size = new System.Drawing.Size(61, 50);
            this.pictureBoxClose.TabIndex = 20;
            this.pictureBoxClose.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBoxClose, "DWS");
            this.pictureBoxClose.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 306);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(781, 22);
            this.statusStrip1.TabIndex = 16;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Margin = new System.Windows.Forms.Padding(0, 3, 650, 2);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(273, 17);
            this.toolStripStatusLabel1.Text = "Developed By Armstrong Machine Builders Pvt Ltd";
            // 
            // lblTime
            // 
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(0, 17);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(205)))), ((int)(((byte)(212)))));
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox5.Location = new System.Drawing.Point(737, 3);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(82, 85);
            this.pictureBox5.TabIndex = 18;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlMenu.BackgroundImage = global::SorterAPI.Properties.Resources.menu_icon;
            this.pnlMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMenu.Location = new System.Drawing.Point(133, 32);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(61, 52);
            this.pnlMenu.TabIndex = 26;
            this.pnlMenu.Visible = false;
            this.pnlMenu.Click += new System.EventHandler(this.pnlMenu_Click);
            this.pnlMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMenu_Paint);
            // 
            // pnlAdminMenu
            // 
            this.pnlAdminMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlAdminMenu.BackgroundImage = global::SorterAPI.Properties.Resources.admin_menu_icon;
            this.pnlAdminMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAdminMenu.Location = new System.Drawing.Point(214, 32);
            this.pnlAdminMenu.Name = "pnlAdminMenu";
            this.pnlAdminMenu.Size = new System.Drawing.Size(61, 52);
            this.pnlAdminMenu.TabIndex = 27;
            this.pnlAdminMenu.Visible = false;
            this.pnlAdminMenu.Click += new System.EventHandler(this.pnlAdminMenu_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.btn100Parcels);
            this.panel4.Controls.Add(this.btnA4Jam);
            this.panel4.Controls.Add(this.btnA2Jam);
            this.panel4.Controls.Add(this.btnLineJam);
            this.panel4.Controls.Add(this.btnEstopOP02);
            this.panel4.Controls.Add(this.btnESTOPOP01);
            this.panel4.Controls.Add(this.btnEstopCPO1);
            this.panel4.Controls.Add(this.btnA4Healthy);
            this.panel4.Controls.Add(this.btnA2healthy);
            this.panel4.Controls.Add(this.btnA1Helathy);
            this.panel4.Location = new System.Drawing.Point(1192, 163);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(236, 541);
            this.panel4.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 24);
            this.label3.TabIndex = 10;
            this.label3.Text = "Fault Notifications";
            this.label3.Visible = false;
            // 
            // btn100Parcels
            // 
            this.btn100Parcels.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btn100Parcels.FlatAppearance.BorderSize = 2;
            this.btn100Parcels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn100Parcels.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn100Parcels.Location = new System.Drawing.Point(11, 430);
            this.btn100Parcels.Margin = new System.Windows.Forms.Padding(2);
            this.btn100Parcels.Name = "btn100Parcels";
            this.btn100Parcels.Size = new System.Drawing.Size(218, 33);
            this.btn100Parcels.TabIndex = 9;
            this.btn100Parcels.Text = "100 Parcel Completed";
            this.btn100Parcels.UseVisualStyleBackColor = true;
            // 
            // btnA4Jam
            // 
            this.btnA4Jam.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnA4Jam.FlatAppearance.BorderSize = 2;
            this.btnA4Jam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnA4Jam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA4Jam.Location = new System.Drawing.Point(11, 392);
            this.btnA4Jam.Margin = new System.Windows.Forms.Padding(2);
            this.btnA4Jam.Name = "btnA4Jam";
            this.btnA4Jam.Size = new System.Drawing.Size(218, 33);
            this.btnA4Jam.TabIndex = 8;
            this.btnA4Jam.Text = "A4 Conveyor jam";
            this.btnA4Jam.UseVisualStyleBackColor = true;
            // 
            // btnA2Jam
            // 
            this.btnA2Jam.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnA2Jam.FlatAppearance.BorderSize = 2;
            this.btnA2Jam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnA2Jam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA2Jam.Location = new System.Drawing.Point(11, 353);
            this.btnA2Jam.Margin = new System.Windows.Forms.Padding(2);
            this.btnA2Jam.Name = "btnA2Jam";
            this.btnA2Jam.Size = new System.Drawing.Size(218, 33);
            this.btnA2Jam.TabIndex = 7;
            this.btnA2Jam.Text = "A2 Conveyor jam";
            this.btnA2Jam.UseVisualStyleBackColor = true;
            // 
            // btnLineJam
            // 
            this.btnLineJam.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnLineJam.FlatAppearance.BorderSize = 2;
            this.btnLineJam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLineJam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLineJam.Location = new System.Drawing.Point(11, 314);
            this.btnLineJam.Margin = new System.Windows.Forms.Padding(2);
            this.btnLineJam.Name = "btnLineJam";
            this.btnLineJam.Size = new System.Drawing.Size(218, 32);
            this.btnLineJam.TabIndex = 6;
            this.btnLineJam.Text = "Line full ";
            this.btnLineJam.UseVisualStyleBackColor = true;
            // 
            // btnEstopOP02
            // 
            this.btnEstopOP02.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnEstopOP02.FlatAppearance.BorderSize = 2;
            this.btnEstopOP02.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstopOP02.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstopOP02.Location = new System.Drawing.Point(10, 273);
            this.btnEstopOP02.Margin = new System.Windows.Forms.Padding(2);
            this.btnEstopOP02.Name = "btnEstopOP02";
            this.btnEstopOP02.Size = new System.Drawing.Size(218, 33);
            this.btnEstopOP02.TabIndex = 5;
            this.btnEstopOP02.Text = "OP02 Emergency stop";
            this.btnEstopOP02.UseVisualStyleBackColor = true;
            // 
            // btnESTOPOP01
            // 
            this.btnESTOPOP01.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnESTOPOP01.FlatAppearance.BorderSize = 2;
            this.btnESTOPOP01.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnESTOPOP01.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnESTOPOP01.Location = new System.Drawing.Point(10, 234);
            this.btnESTOPOP01.Margin = new System.Windows.Forms.Padding(2);
            this.btnESTOPOP01.Name = "btnESTOPOP01";
            this.btnESTOPOP01.Size = new System.Drawing.Size(218, 32);
            this.btnESTOPOP01.TabIndex = 4;
            this.btnESTOPOP01.Text = "OP01 Emergency stop";
            this.btnESTOPOP01.UseVisualStyleBackColor = true;
            // 
            // btnEstopCPO1
            // 
            this.btnEstopCPO1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnEstopCPO1.FlatAppearance.BorderSize = 2;
            this.btnEstopCPO1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstopCPO1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstopCPO1.Location = new System.Drawing.Point(11, 196);
            this.btnEstopCPO1.Margin = new System.Windows.Forms.Padding(2);
            this.btnEstopCPO1.Name = "btnEstopCPO1";
            this.btnEstopCPO1.Size = new System.Drawing.Size(218, 33);
            this.btnEstopCPO1.TabIndex = 3;
            this.btnEstopCPO1.Text = "CP01 Emergency stop";
            this.btnEstopCPO1.UseVisualStyleBackColor = true;
            this.btnEstopCPO1.Click += new System.EventHandler(this.btnEstopCPO1_Click);
            // 
            // btnA4Healthy
            // 
            this.btnA4Healthy.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnA4Healthy.FlatAppearance.BorderSize = 2;
            this.btnA4Healthy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnA4Healthy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA4Healthy.Location = new System.Drawing.Point(11, 157);
            this.btnA4Healthy.Margin = new System.Windows.Forms.Padding(2);
            this.btnA4Healthy.Name = "btnA4Healthy";
            this.btnA4Healthy.Size = new System.Drawing.Size(218, 32);
            this.btnA4Healthy.TabIndex = 2;
            this.btnA4Healthy.Text = "A4 Motor drive not healthy";
            this.btnA4Healthy.UseVisualStyleBackColor = true;
            this.btnA4Healthy.Click += new System.EventHandler(this.btnA4Healthy_Click);
            // 
            // btnA2healthy
            // 
            this.btnA2healthy.BackColor = System.Drawing.Color.Red;
            this.btnA2healthy.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnA2healthy.FlatAppearance.BorderSize = 2;
            this.btnA2healthy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnA2healthy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA2healthy.Location = new System.Drawing.Point(11, 119);
            this.btnA2healthy.Margin = new System.Windows.Forms.Padding(2);
            this.btnA2healthy.Name = "btnA2healthy";
            this.btnA2healthy.Size = new System.Drawing.Size(218, 33);
            this.btnA2healthy.TabIndex = 1;
            this.btnA2healthy.Text = "A2 Motor drive not healthy";
            this.btnA2healthy.UseVisualStyleBackColor = false;
            // 
            // btnA1Helathy
            // 
            this.btnA1Helathy.BackColor = System.Drawing.Color.LimeGreen;
            this.btnA1Helathy.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnA1Helathy.FlatAppearance.BorderSize = 2;
            this.btnA1Helathy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnA1Helathy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA1Helathy.Location = new System.Drawing.Point(11, 80);
            this.btnA1Helathy.Margin = new System.Windows.Forms.Padding(2);
            this.btnA1Helathy.Name = "btnA1Helathy";
            this.btnA1Helathy.Size = new System.Drawing.Size(218, 32);
            this.btnA1Helathy.TabIndex = 0;
            this.btnA1Helathy.Text = "A1 Motor drive not healthy";
            this.btnA1Helathy.UseVisualStyleBackColor = false;
            this.btnA1Helathy.Click += new System.EventHandler(this.button1_Click);
            // 
            // MDIParent1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(781, 328);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.pnlAdminMenu);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pictureBoxClose);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "MDIParent1";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DWS Application";
            this.Load += new System.EventHandler(this.MDIParent1_Load);
            this.panel1.ResumeLayout(false);
            this.pnlBusinessParcel.ResumeLayout(false);
            this.pnlBusinessParcel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.pnlOUMap.ResumeLayout(false);
            this.pnlOUMap.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnlSearchBarcode.ResumeLayout(false);
            this.pnlSearchBarcode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlBatchOperation.ResumeLayout(false);
            this.pnlBatchOperation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBatchOperation)).EndInit();
            this.pnlExit.ResumeLayout(false);
            this.pnlExit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClose)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlBatchOperation;
        private System.Windows.Forms.PictureBox picBatchOperation;
        private System.Windows.Forms.Label lblBarchOperation;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripStatusLabel lblTime;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBoxClose;
        private System.Windows.Forms.Panel pnlExit;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlAdminMenu;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlOUMap;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblOUMapp;
        private System.Windows.Forms.Panel pnlBusinessParcel;
        private System.Windows.Forms.Label lblBusinessParcel;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel pnlSearchBarcode;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label lblSearchBarcode;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button btn100Parcels;
        public System.Windows.Forms.Button btnA4Jam;
        public System.Windows.Forms.Button btnA2Jam;
        public System.Windows.Forms.Button btnLineJam;
        public System.Windows.Forms.Button btnEstopOP02;
        public System.Windows.Forms.Button btnESTOPOP01;
        public System.Windows.Forms.Button btnEstopCPO1;
        public System.Windows.Forms.Button btnA4Healthy;
        public System.Windows.Forms.Button btnA2healthy;
        public System.Windows.Forms.Button btnA1Helathy;
    }
}



