﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SorterAPI.Common;
//using MySql.Data.MySqlClient;
using AMBPL.Common;

namespace MainClass
{
   public class clsMain
   {       
        public int SorterNo { get; set; }       
        public string Barcode { get; set; }

        public static DataSet GetforSAPAPI()
        {
            SqlCommand cmd = new SqlCommand();
            return ClsMySqlDataAcess.GetDataSet("sp_DatasendToSAP_Get", cmd);
        }
        public static DataSet GetforBarcode()
        {
            SqlCommand cmd = new SqlCommand();
            return ClsMySqlDataAcess.GetDataSet("sp_DatasendToSAP_Get_Barcode", cmd);
        }

        public static int UpdateIsSendData(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            return ClsMySqlDataAcess.ExecuteNonQuery("sp_IsSend_Update", cmd);
        }
        public static int UpdateIsSendDataByBarcode(string barcode,int status,string Reason)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@barcode", barcode);
            cmd.Parameters.AddWithValue("@Status", status);
            cmd.Parameters.AddWithValue("@Reason", Reason);

            return ClsMySqlDataAcess.ExecuteNonQuery("sp_DatasendToSAP_Success", cmd);
        }
        public static int GIUpdateIsSendDataByBarcode(string barcode,int status)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@barcode", barcode);
            cmd.Parameters.AddWithValue("@Status", status);
            return ClsMySqlDataAcess.ExecuteNonQuery("sp_GIDatasendToSAP_Success", cmd);
        }

        public static int UpdateAPIResponce(string barcode, string Reason)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@Barcode", barcode);
            cmd.Parameters.AddWithValue("@Reason", Reason);
            return ClsMySqlDataAcess.ExecuteNonQuery("UpdateAPIResponce", cmd);
        }
    }
}
