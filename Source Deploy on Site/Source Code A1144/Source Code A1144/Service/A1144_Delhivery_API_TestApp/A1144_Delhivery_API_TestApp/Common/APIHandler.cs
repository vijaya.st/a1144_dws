using AMBPL.Common;
using MainClass;
//using MySql.Data.MySqlClient;
using SorterAPI.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace Armstrong.Common
{
    public static class APIHandler
    {
        /// <summary>
        /// Execute Post Request to Web API 
        /// </summary>
        /// <param name="url">URL of Web API Method</param>
        /// <param name="postData">Object of class to be pass to Web Method</param>
        /// <param name="onCompleteMethod">Name of method that will be called after complete execution(Error Or Success). 
        ///                                ResponseData will be passed as parameter. 
        ///                                Make sure it is Public. 
        /// </param>
        /// <returns></returns>
        /// string GIresponce = "";

      public static string GIresponce = "";
        public static string error1 = "";
        public static async void ExecutePostRequest(string url, object postData, string onCompleteMethod)
        {
            string ss1= GetCaller();
            var client = new HttpClient();
            

            Dictionary<string, string> values = GetClassDictionary(postData);
            var content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = null;
            try
            {
                response = await client.PostAsync(url, content);
                response.EnsureSuccessStatusCode();
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (response.IsSuccessStatusCode)
                {
                    var t2 = response.Content.ReadAsStringAsync();
                    t2.Wait();
                    var ss = t2.Result;

                    //this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { ss });
                }
                else
                {
                   // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error" });
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("404") != -1)
                {
                   // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error : Request Not Found" });
                }
                else
                {
                   // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error:" + ex.Message });
                }
            }

           // return response.StatusCode.ToString();
        }

        // --used
        public static async void ExecutePostRequest(string url, string jsonData,string Bar,string locbarcode,string APITokan)
         {
            string userpass = APITokan;//"a583497f03864d33d693fa3a081d948aa474e400"; //"MDM_OP:Order@123";
            int r = 0;
            var client = new HttpClient();
          
            var httpContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
            httpContent.Headers.Add("Application", "PROFILER");
            HttpResponseMessage response = null;

            try
            {
                
                var bytearr = Encoding.ASCII.GetBytes(userpass);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic ", userpass);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", userpass);
                response = client.PostAsync(url, httpContent).Result;

                //response = await client.PostAsync(url, httpContent);
                ErrorLog.WriteErrorLog("Data sent to Database..");
                string s = response.Content.ToString();
                response.EnsureSuccessStatusCode();

                HttpHeaders headers = response.Headers;
                HttpContent content = response.Content;
                string myContent = content.ReadAsStringAsync().Result;
                var rootElement = XElement.Parse(myContent);
                var sessionId = rootElement.Element("success").Value;
                
                var Msg = rootElement.Element("msg").Value;
                ErrorLog.WriteErrorLog("Update Responce!"+ sessionId);
                if (sessionId == "True")
                {
                    clsMain.UpdateIsSendDataByBarcode(locbarcode,1,"");
                    ErrorLog.WriteErrorLog("Updated record locally...!");
                }
                else
                {
                    clsMain.UpdateIsSendDataByBarcode(locbarcode, 2,Msg);
                    ErrorLog.WriteErrorLog("API Responce :"+ myContent);
                }
               
               
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("404") != -1)
                {
                    ErrorLog.WriteErrorLog("Error 404 >> "+ ex.ToString());
                    // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error : Request Not Found" });
                }
                else
                {
                    ErrorLog.WriteErrorLog("Error >> " + ex.ToString());
                    // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error:" + ex.Message });
                }
                clsMain.UpdateIsSendDataByBarcode(locbarcode, 2, "");
                clsMain.GIUpdateIsSendDataByBarcode(locbarcode, 2);
            }

            // return response.StatusCode.ToString();
        }

        //--

        public static async void ExecutePostRequestCAL(string url, string jsonData, string Bar, string locbarcode, string APITokan)
        {
            string userpass = APITokan;//"a583497f03864d33d693fa3a081d948aa474e400"; //"MDM_OP:Order@123";
            int r = 0;
            var client = new HttpClient();



            var httpContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
            httpContent.Headers.Add("Application", "PROFILER");
            HttpResponseMessage response = null;



            try
            {



                var bytearr = Encoding.ASCII.GetBytes(userpass);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic ", userpass);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userpass);
                response = client.PostAsync(url, httpContent).Result;



                //response = await client.PostAsync(url, httpContent);
                ErrorLog.WriteErrorLog("Data sent to Database..");
                string s = response.Content.ToString();
                response.EnsureSuccessStatusCode();



                HttpHeaders headers = response.Headers;
                HttpContent content = response.Content;
                string myContent = content.ReadAsStringAsync().Result;

                //var rootElement = XElement.Parse(myContent);
                //var sessionId = rootElement.Element("success").Value;



                var Msg = ""; //rootElement.Element("msg").Value;
                              // ErrorLog.WriteErrorLog("Update Responce!" + sessionId);
                if (Convert.ToInt32(response.StatusCode) == 200)
                {
                    
                    clsMain.UpdateIsSendDataByBarcode(locbarcode, 1, "");
                    ErrorLog.WriteErrorLog("Updated record locally...!");
                }
                else
                {
                    clsMain.UpdateIsSendDataByBarcode(locbarcode, 2, Msg);
                    ErrorLog.WriteErrorLog("API Responce :" + myContent);
                }




            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("404") != -1)
                {
                    ErrorLog.WriteErrorLog("Error 404 >> " + ex.ToString());
                    // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error : Request Not Found" });
                }
                else
                {
                    ErrorLog.WriteErrorLog("Error >> " + ex.ToString());
                    // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error:" + ex.Message });
                }
                clsMain.UpdateIsSendDataByBarcode(locbarcode, 2, "");
                clsMain.GIUpdateIsSendDataByBarcode(locbarcode, 2);
            }



            // return response.StatusCode.ToString();
        }

        public static async void ExecutePostRequestCali(string url, string jsonData, string Bar, string locbarcode, string APITokan)
        {
            string userpass = APITokan;//"a583497f03864d33d693fa3a081d948aa474e400"; //"MDM_OP:Order@123";
            int r = 0;
            var client = new HttpClient();

            var httpContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
            //httpContent.Headers.Add("Application", "PROFILER");
            HttpResponseMessage response = null;

            try
            {

                var bytearr = Encoding.ASCII.GetBytes(userpass);
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic ", userpass);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userpass);
                response = client.PostAsync(url, httpContent).Result;

                //response = await client.PostAsync(url, httpContent);
                ErrorLog.WriteErrorLog("Data sent to Database..");
                string s = response.Content.ToString();
                response.EnsureSuccessStatusCode();

                HttpHeaders headers = response.Headers;
                HttpContent content = response.Content;
                string myContent = content.ReadAsStringAsync().Result;
                var rootElement = XElement.Parse(myContent);
                var sessionId = rootElement.Element("success").Value;

                var Msg = rootElement.Element("msg").Value;
                ErrorLog.WriteErrorLog("Update Responce!" + sessionId);
                if (sessionId == "True")
                {
                    clsMain.UpdateIsSendDataByBarcode(locbarcode, 1, "");
                    ErrorLog.WriteErrorLog("Updated record locally...!");
                }
                else
                {
                    clsMain.UpdateIsSendDataByBarcode(locbarcode, 2, Msg);
                    ErrorLog.WriteErrorLog("API Responce :" + myContent);
                }


            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("404") != -1)
                {
                    ErrorLog.WriteErrorLog("Error 404 >> " + ex.ToString());
                    // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error : Request Not Found" });
                }
                else
                {
                    ErrorLog.WriteErrorLog("Error >> " + ex.ToString());
                    // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error:" + ex.Message });
                }
                clsMain.UpdateIsSendDataByBarcode(locbarcode, 2, "");
                clsMain.GIUpdateIsSendDataByBarcode(locbarcode, 2);
            }

            // return response.StatusCode.ToString();
        }



        public static async  void ExecutePostRequestGI(string url, string jsonData, string Bar, string locbarcode,string APITokan)
        {
            string userpass = APITokan;//"a583497f03864d33d693fa3a081d948aa474e400";
            int r = 0;
            var client = new HttpClient();
            var httpContent = new StringContent(jsonData, Encoding.ASCII, "application/json");
            httpContent.Headers.Add("Application", "PROFILER");
            HttpResponseMessage response = null;
            try
            {
                var bytearr = Encoding.ASCII.GetBytes(userpass);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", userpass);
                response = client.PostAsync(url, httpContent).Result;
                

                //response = await client.PostAsync(url, httpContent);
                ErrorLog.WriteErrorLog("GI  Data sent to Database..");
                response.EnsureSuccessStatusCode();
                HttpHeaders headers = response.Headers;
                HttpContent content = response.Content;
                string myContent = content.ReadAsStringAsync().Result;
                var rootElement = XElement.Parse(myContent);
                GIresponce = rootElement.Element("status").Value;
                 error1= rootElement.Element("error").Value;
                if (GIresponce == "1")
                {
                    
                    clsMain.GIUpdateIsSendDataByBarcode(locbarcode,1);
                    //clsMain.GIUpdateIsSendDataByBarcode(locbarcode, 1);

                }
                else
                {
                    GIresponce = myContent;
                    clsMain.UpdateIsSendDataByBarcode(locbarcode,2, error1);
                    clsMain.GIUpdateIsSendDataByBarcode(locbarcode,2);
                    //clsMain.UpdateAPIResponce(locbarcode, error1);
                    ErrorLog.WriteErrorLog("Error!"+ GIresponce);


                }

                ErrorLog.WriteErrorLog("GI Flag Updated locally...!");

            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("404") != -1)
                {
                    ErrorLog.WriteErrorLog("Error 404 >> " + ex.ToString());
                    
                }
                else
                {
                    ErrorLog.WriteErrorLog("Error >> " + ex.ToString());
                    
                }
                clsMain.UpdateIsSendDataByBarcode(locbarcode, 2,"--");
                clsMain.GIUpdateIsSendDataByBarcode(locbarcode, 2);


            }


        }


        public static async void  ExecuteGetRequest(string url, string onCompleteMethod)
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = null;
            try
            {
                response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (response.IsSuccessStatusCode)
                {
                    var t2 = response.Content.ReadAsStringAsync();
                    t2.Wait();
                    var ss = t2.Result;

                  //  this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { ss });
                }
                else
                {
                   // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error" });
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("404") != -1)
                {
                    //this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error : Request Not Found" });
                }
                else
                {
                    //this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error:" + ex.Message });
                }
            }

           // return response.StatusCode.ToString();



            //HttpResponseMessage response = await client.GetAsync(url);

            //string str = "";



            //if (response.IsSuccessStatusCode)
            //{
            //    var kk = await response.Content.ReadAsStringAsync();
            //    str = kk.ToString();
            //}


            //return str;
        }

        public static async void  ExecuteGetRequest(string url, Dictionary<string, string> querystringKeyVal, string onCompleteMethod)
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string queryString = string.Empty;
            int qCnt = 1;
            foreach (KeyValuePair<string, string> qrystring in querystringKeyVal)
            {
                queryString += qrystring.Key + "=" + qrystring.Value;
                if (qCnt > 1)
                    queryString += "&";
                qCnt++;
            }
            url += "?" + queryString;

            HttpResponseMessage response = null;
            try
            {
                response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (response.IsSuccessStatusCode)
                {
                    var t2 = response.Content.ReadAsStringAsync();
                    t2.Wait();
                    var ss = t2.Result;

                    //this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { ss });
                }
                else
                {
                  //  this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error" });
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("404") != -1)
                {
                   // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error : Request Not Found" });
                }
                else
                {
                   // this.GetType().GetMethod(onCompleteMethod).Invoke(this, new[] { "Error:" + ex.Message });
                }
            }

            //return response.StatusCode.ToString();



            //HttpResponseMessage response = await client.GetAsync(url);

            //string str = "";



            //if (response.IsSuccessStatusCode)
            //{
            //    var kk = await response.Content.ReadAsStringAsync();
            //    str = kk.ToString();
            //}


            //return str;
        }

        private static Dictionary<string, string> GetClassDictionary(Object objClass)
        {

            PropertyInfo[] infos = objClass.GetType().GetProperties();

            Dictionary<string, string> dix = new Dictionary<string, string>();

            foreach (PropertyInfo info in infos)
            {
                // if (info.GetValue(objClass, null) != null)
                string val = Convert.ToString(info.GetValue(objClass, null));
                dix.Add(info.Name, val);
            }
            return dix;
        }

        private static string GetCaller()
        {
            //var ss= MethodBase.GetCurrentMethod().DeclaringType;
            // var sf = new StackFrame();
            // var meth = sf.GetMethod();
            // var cls = meth.DeclaringType;

            // //var trace = new StackTrace(1);
            // //var frame = trace.GetFrame(0);
            // //var caller = frame.GetMethod();
            // //var callingClass = caller.DeclaringType.Name;
            // //var callingMethod = caller.Name;
            // //return String.Format("Called by {0}.{1}", callingClass, callingMethod);
            // var mth = new StackTrace().GetFrame(1).GetMethod();
            // var clsa = mth.ReflectedType.Name;

            StackTrace stackTrace = new StackTrace();           // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)
            foreach(StackFrame frm in stackFrames)
            {
                var caller = frm.GetMethod();
                var callingClass = caller.DeclaringType.Name;

            }

            return "";
        }
    }
}