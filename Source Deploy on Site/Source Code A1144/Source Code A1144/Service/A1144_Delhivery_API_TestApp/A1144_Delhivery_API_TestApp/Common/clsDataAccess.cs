﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Armstrong.Common;

namespace SorterAPI.Common
{
   public class clsDataAccess
    {
        static string databaseOwner = "dbo";
        public static string GetConnectionString()
        {
          //  string con = @"Data Source= ANL6;Initial Catalog=A698;Integrated Security=No; UID=sa; Pwd=sql;";
          return  System.Configuration.ConfigurationManager.AppSettings["eConstr"];
          //  return con;
        }
        
        public static DataTable GetDataTable(string ProcName, SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection cn = new SqlConnection(GetConnectionString());
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteErrorLog(ex.Message);
            }
            return dt;
        }

        public static int ExecuteNonQuery(string ProcName, SqlCommand cmd)
        {
            int ResultExecuteNonQuery = 0;
            try
            {
                SqlConnection cn = new SqlConnection(GetConnectionString());
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = ProcName;
                cn.Open();
                ResultExecuteNonQuery = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                ErrorLog.WriteErrorLog("ExecuteNonQuery-  " + e.Message);
            }
            return ResultExecuteNonQuery;
        }

        public static DataSet GetDataSet(string procedureName, SqlCommand Commond)
        {
            SqlConnection connection = new SqlConnection(GetConnectionString());
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();

            try
            {
                Commond.CommandText = databaseOwner + "." + procedureName;
                Commond.Connection = connection;

                //Mark As Stored Procedure
                Commond.CommandType = CommandType.StoredProcedure;

                da.SelectCommand = Commond;
                da.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
            }
            return ds;
        }

        public static int ExecuteNonQuery_Text(string query, SqlCommand cmd)
        {
            int ResultExecuteNonQuery = 0;
            try
            {
                SqlConnection cn = new SqlConnection(GetConnectionString());
                cmd.Connection = cn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                cn.Open();
                ResultExecuteNonQuery = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
            return ResultExecuteNonQuery;
        }
    }
}
