﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using Amazon.S3.Transfer;
using System.IO;
using Armstrong.Common;
using System.Configuration;

namespace A1143_Delhivery_API_TestApp.Common
{
   
    public static class AmazonS3Uploader
    {
         private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USWest2;
       
       public static async Task WritingAnObjectAsync(IAmazonS3 client,string bucketName,string filePath,string Keyname,string barcode)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    DateTime datetime = DateTime.Now;
                    var putRequest2 = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        Key = Keyname + barcode + "-"+datetime.ToString("ddMMyyyy-Hmm") + ".jpg",//profiler/armstrong/test.jpg",
                        FilePath = filePath,
                        ContentType = "text/plain"
                    };

                    putRequest2.Metadata.Add("x-amz-meta-title", "Image");
                    PutObjectResponse response2 = await client.PutObjectAsync(putRequest2);
                    //Write Code to update image status in database.
                    ErrorLog.WriteErrorLog("Image Uploaded Successfully..");
                    File.Delete(filePath);
                    ErrorLog.WriteErrorLog("Image deleted from Local Source drive..");
                }
                else
                {
                    ErrorLog.WriteErrorLog(filePath+" Image is not exist in camera folder.");
                }
            }
            catch (AmazonS3Exception e)
            {
                ErrorLog.WriteErrorLog("Error to upload image"+e.ToString());
            }
            catch (Exception e)
            {
                ErrorLog.WriteErrorLog("Error to upload image" + e.ToString());
            }
        }
    }
}

