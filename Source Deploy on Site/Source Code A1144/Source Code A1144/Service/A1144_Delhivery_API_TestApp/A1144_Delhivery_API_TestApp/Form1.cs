﻿using Armstrong.Common;
using MainClass;
using System;
using System.Data;
using System.Windows.Forms;
using S7.Net;
using System.Configuration;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using System.IO;
using Amazon.S3.IO;
using A1143_Delhivery_API_TestApp;
using A1143_Delhivery_API_TestApp.Common;

namespace A1143_Delhivery_API_TestApp
{
    public partial class Form1 : Form
    {
        System.Timers.Timer timeDelay;
        int port = 0;
        string projectCode = "";
        string WeightURL = "", WeightAPIToken = "";
        string GIURL = "", GIAPIToken = "";
        string CalibrationURL = "", CalibrationToken = "";
        string APIKey = "";
        string SecretKey = "";
       
        int interval = 60;
        private System.Timers.Timer _timerLWH;
        string PLCIP, Internet_Connection_Lost_Alarm, Data_Sent_Alarm, Decision_Flag;
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.APSoutheast1;
        private static IAmazonS3 s3Client;
        private  string bucketName ="";
        private  string keyName    ="";
        private  string filePath   ="";
        public string BarcodeString = "";


        Plc plc;
        public Form1()
        {
            InitializeComponent();
            SetConfiguration();
            bucketName  = ConfigurationManager.AppSettings["BucketName"].ToString();
            keyName = ConfigurationManager.AppSettings["KeyName"].ToString();
            filePath = ConfigurationManager.AppSettings["LocalFilePath"].ToString();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timeDelay = new System.Timers.Timer(interval * 100);  // interval 
            timeDelay.Elapsed += TimeDelay_Elapsed;
            timeDelay.Start();
            ErrorLog.WriteErrorLog("Application Start");

            PLCIP = ConfigurationManager.AppSettings["PLCIP"];
            Internet_Connection_Lost_Alarm = ConfigurationManager.AppSettings["Internet_Connection_Lost_Alarm"];
            Data_Sent_Alarm = ConfigurationManager.AppSettings["Data_Sent_Alarm"];
            Decision_Flag = ConfigurationManager.AppSettings["Decision_Flag"];
            APIKey = ConfigurationManager.AppSettings["APIKey"];
            SecretKey = ConfigurationManager.AppSettings["SecretKey"];
            plc = new Plc(CpuType.S71500, PLCIP, 0, 1);
            //plc.Open();
           sendImage("1009410051424");
        }
        public async void sendImage(string barcode)
        {

            try
            {
                if (barcode != "") { 
                s3Client = new AmazonS3Client("AKIASYPQWAPQZZOZQ6JP", "vP2PebF468M/rNAHea0voQlg7uTcUjcnygORaRan", RegionEndpoint.APSoutheast1);

                AmazonS3Uploader.WritingAnObjectAsync(s3Client, bucketName,@filePath+barcode+".jpg",keyName, barcode).Wait();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteErrorLog("Error" + ex.ToString());
            }
        }


        private void SetConfiguration()
        {
            try
            {
                port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimeInterval"]);
                WeightURL = ConfigurationManager.AppSettings["WeightURL"];     //For AWS 1
                GIURL = ConfigurationManager.AppSettings["GIURL"];     //For AWS 1
                WeightAPIToken = ConfigurationManager.AppSettings["WeightAPIToken"];     //For AWS 1
                GIAPIToken = ConfigurationManager.AppSettings["GIAPIToken"];     //For AWS 1
                CalibrationURL = ConfigurationManager.AppSettings["CalibrationURL"];     //For AWS 1
                CalibrationToken = ConfigurationManager.AppSettings["CalibrationToken"];     //For AWS 1
            }
            catch (Exception ex)
            {
                ErrorLog.WriteErrorLog("Error in Confuguration App settings :" + ex.Message);
            }
        }
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new System.Net.WebClient())
                using (client.OpenRead("http://google.com/generate_204"))
                    //using (client.OpenRead("https://staging-express.delhivery.com"))
                    return true;
            }
            catch
            {
                return false;
            }
        }

        private void TimeDelay_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            DataSet dt = new DataSet();
            DataSet dt1 = new DataSet();
            int PassOrFailValue = 0;
            timeDelay.Stop();
            try
            {
                if (!CheckForInternetConnection())
                {
                    //  ds2 = clsMain.FaultyBarcodeInsert(Barcode);
                    //tmrBit.Stop();
                    plc.Write(Internet_Connection_Lost_Alarm, true);  //internet connection lost
                    ErrorLog.WriteErrorLog("Internet Connection Lost.. ");
                    //tmrBit.Start();
                }
                else
                {
                    plc.Write(Internet_Connection_Lost_Alarm, false); //internet connection lost
                }

                dt1 = clsMain.GetforBarcode();
                dt = clsMain.GetforSAPAPI();
                if (dt.Tables[1].Rows.Count > 0 && dt.Tables[2].Rows.Count == 0)
                {
                    for (int i = 0; i < dt.Tables[1].Rows.Count; i++)
                    {
                         BarcodeString = (dt.Tables[1].Rows[i]["Barcode"]).ToString();
                        string jsonData2 = "";
                        if (BarcodeString == "NoRead")
                        {
                            string b = "";
                            string jsonData = "";
                            jsonData += "{\"wbn\":\"" + dt.Tables[1].Rows[i]["Barcode"].ToString() + "\",\"l\":\"" + dt.Tables[1].Rows[i]["Length"].ToString();
                            jsonData += "\",\"b\":\"" + dt.Tables[1].Rows[i]["Width"].ToString() + "\",\"h\":\"" + dt.Tables[1].Rows[i]["Height"].ToString() + "\",\"Wt\":\"" + dt.Tables[1].Rows[i]["Weight"].ToString() + "";
                            jsonData += "\",\"v\":\"1.2\",\"rv\":\"555555.2\"\"";
                            b += "'NoRead',";
                            jsonData = jsonData.Remove(jsonData.Length - 1);
                            b = b.Remove(b.Length - 1);
                            jsonData += "}";
                            ErrorLog.WriteErrorLog("json-  " + jsonData);
                            APIHandler.ExecutePostRequest(WeightURL, jsonData, b);
                        }
                        else
                        {
                            string b = "";
                            string jsonData = "";
                            string locbarcode = dt.Tables[1].Rows[i]["Barcode"].ToString();
                            jsonData += "{\"wbn\":\"" + dt.Tables[1].Rows[i]["Barcode"].ToString() + "\",\"l\":\"" + dt.Tables[1].Rows[i]["Length"].ToString();
                            jsonData += "\",\"b\":\"" + dt.Tables[1].Rows[i]["Width"].ToString() + "\",\"h\":\"" + dt.Tables[1].Rows[i]["Height"].ToString() + "\",\"wt\":\"" + dt.Tables[1].Rows[i]["Weight"].ToString() + "";
                            jsonData += "\",\"v\":\"" + dt.Tables[1].Rows[i]["Volume"].ToString() + "\",\"rv\":\"" + dt.Tables[1].Rows[i]["RlVolume"].ToString() + "\"\"";
                            b += "'" + dt.Tables[1].Rows[i]["Barcode"] + "',";
                            jsonData = jsonData.Remove(jsonData.Length - 1);
                            b = b.Remove(b.Length - 1);
                            jsonData += "}";
                            jsonData2 += "{\"center\":\"Surat_Kacholi_GW (Gujarat)\",\"ref_ids\":[" + "\"" + dt.Tables[1].Rows[i]["Barcode"].ToString() + "\"],\"large\": true}";
                            ErrorLog.WriteErrorLog("GI- JSON  " + jsonData2);
                            ErrorLog.WriteErrorLog("GI-API Execution...");
                            APIHandler.ExecutePostRequestGI(GIURL, jsonData2, b, dt.Tables[1].Rows[i]["Barcode"].ToString(), GIAPIToken);
                            ErrorLog.WriteErrorLog("GI-API Execution Successfull..." + GIURL);
                            ErrorLog.WriteErrorLog("json-  " + jsonData);
                            ErrorLog.WriteErrorLog("Weight-API Execution...");
                            if (APIHandler.GIresponce == "1")
                            {
                                plc.Write(Data_Sent_Alarm, false);  //data send
                                APIHandler.ExecutePostRequest(WeightURL, jsonData, b, dt.Tables[1].Rows[i]["Barcode"].ToString(), WeightAPIToken);
                                ErrorLog.WriteErrorLog("Weight-API Execution Successfull...");
                                if (BarcodeString!="") { 
                                sendImage(BarcodeString);
                                }
                            }
                            else
                            {

                                plc.Write(Data_Sent_Alarm, true);  //data not send to wms
                                clsMain.UpdateIsSendDataByBarcode(locbarcode, 2, APIHandler.error1);
                                clsMain.GIUpdateIsSendDataByBarcode(locbarcode, 2);

                                ErrorLog.WriteErrorLog("GI Faild..");
                                PassOrFailValue = 2;
                                plc.Write(Decision_Flag, PassOrFailValue);
                            }
                        }
                    }
                }
                if (dt.Tables[2].Rows.Count > 0)
                {
                    ErrorLog.WriteErrorLog("Calibration Data enter ");

                    for (int i = 0; i < dt.Tables[2].Rows.Count; i++)
                    {
                         BarcodeString = (dt.Tables[2].Rows[i]["Barcode"]).ToString();
                        ErrorLog.WriteErrorLog("Calibration Data Barcode " + BarcodeString);
                        string b = "";

                        string jsonData = "";
                        string locbarcode = dt.Tables[2].Rows[i]["Barcode"].ToString();
                        jsonData += "{\"version\":\"v1\"";
                        jsonData += ",\"data\": {\"wbn\":\"" + dt.Tables[2].Rows[i]["Barcode"].ToString() + "\",\"length\":" + dt.Tables[2].Rows[i]["Length"].ToString();
                        jsonData += ",\"breadth\":" + dt.Tables[2].Rows[i]["width"].ToString() + ",\"time\":\"" + dt.Tables[2].Rows[i]["time"].ToString() + "\",\"source\":\"" + dt.Tables[2].Rows[i]["source"].ToString() + "\",\"machineusername\":\"" + dt.Tables[2].Rows[i]["machineusername"].ToString() + "\",\"scanlocation\":\"" + dt.Tables[2].Rows[i]["scanlocation"].ToString() + "";
                        jsonData += "\",\"height\":" + dt.Tables[2].Rows[i]["height"].ToString() + ",\"weight\":" + dt.Tables[2].Rows[i]["Weight"].ToString() + "";
                        jsonData += ",\"rv\":" + dt.Tables[2].Rows[i]["Volume"].ToString() + ",\"boxvolume\":" + dt.Tables[2].Rows[i]["RlVolume"].ToString() + "";
                        jsonData += ",\"defined_l\":" + dt.Tables[2].Rows[i]["defined_l"].ToString() + ",\"defined_b\":" + dt.Tables[2].Rows[i]["defined_b"].ToString() + "";
                        jsonData += ",\"defined_h\":" + dt.Tables[2].Rows[i]["defined_h"].ToString() + ",\"defined_wt\":" + dt.Tables[2].Rows[i]["defined_wt"].ToString() + "";
                        jsonData += "},\"schema_name\":\"profiler-weight ";
                        b += "'" + dt.Tables[2].Rows[i]["Barcode"] + "',";
                        jsonData = jsonData.Remove(jsonData.Length - 1);
                        b = b.Remove(b.Length - 1);
                        jsonData += "\"}";
                        ErrorLog.WriteErrorLog("Calibration Data " + jsonData);
                        APIHandler.ExecutePostRequestCAL(CalibrationURL, jsonData, b, dt.Tables[2].Rows[i]["Barcode"].ToString(), CalibrationToken);
                        ErrorLog.WriteErrorLog("Calibration Data Send Successfully.. ");
                        plc.Write(Data_Sent_Alarm, false);   //data send
                        if (BarcodeString != "")
                        {
                            sendImage(BarcodeString);
                        }
                    }
                }

              //  sendImage(BarcodeString);
            }
            catch (Exception ex)
            {
                //plc.Write(Data_Sent_Alarm, true);  //Data not send0
                //clsMain.UpdateIsSendDataByBarcode(dt1.Tables[0].Rows[0]["Barcode"].ToString(), 2, ex.ToString());
                //clsMain.GIUpdateIsSendDataByBarcode(dt1.Tables[0].Rows[0]["Barcode"].ToString(), 2);
                //ErrorLog.WriteErrorLog("Exception >>  " + ex.ToString());
                //PassOrFailValue = 2;
                //plc.Write(Decision_Flag, PassOrFailValue);
            }
            finally
            {
                timeDelay.Start();
            }
        }

    }
}