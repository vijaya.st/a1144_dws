﻿var myApp = angular.module("myApp", ['ui.bootstrap']);
myApp.controller("myController", function ($scope, $http) {
    $scope.NoDataDiv = true;
    $('.table_data').hide();
    $scope.Type = "";
    var dateObj = new Date('<%=todayDate%>');
    $scope.Type = dateObj;
    $scope.maxSize = 5;     // Limit number for pagination display number.  
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
    $scope.pageSizeSelected = 10; // Maximum number of items per page.  

    var Line1 = [], Line2 = [], Line3 = [], CSeries = [];

    function time_convert(num) {
        var hours = Math.floor(num / 3600);
        var minutes = num % 60;
        return parseFloat(hours + "." + minutes);
    }

    $scope.Search = function () {
        //alert();
        //$scope.selCell = selCell.value;
        if ($scope.seldatepicker == undefined || $scope.seldatepicker == "") {
            toastr.options.positionClass = 'toast-top-center';
            toastr.error('Select Period First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
            return;
        }
        if ($scope.seldatepicker == "date") {
            $scope.Type = "Day";
            $scope.txtStartDate = document.getElementById("txtDate").value;
            $scope.txtWeek = 0;
            $scope.txtMonth = 0;
            $scope.txtEndDate = "";
            if ($scope.txtStartDate == "" || $scope.txtStartDate == undefined) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('Please Select Date ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }
        }
        if ($scope.seldatepicker == "week") {
            $scope.Type = "Week";
            var w = document.getElementById("txtWeek").value;
            $scope.txtWeek = w.substring(6);
            $scope.txtDate = 0;
            $scope.txtMonth = 0;
            $scope.txtStartDate = "";
            $scope.txtEndDate = "";
            if ($scope.txtWeek == "" || $scope.txtWeek == undefined) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('Please Select Week ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }
        }
        if ($scope.seldatepicker == "month") {
            $scope.Type = "Month";
            var m = document.getElementById("txtMonth").value;
            $scope.txtMonth = m.substring(5);
            $scope.txtDate = 0;
            $scope.txtWeek = 0;
            $scope.txtStartDate = "";
            $scope.txtEndDate = "";
            if ($scope.txtMonth == "" || $scope.txtMonth == undefined) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('Please Select Month ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }
        }
        if ($scope.seldatepicker == "dateRange") {
            $scope.Type = "dateRange";
            $scope.txtStartDate = document.getElementById("txtStartDate").value;
            $scope.txtEndDate = document.getElementById("txtEndDate").value;

            $scope.txtDate = 0;
            $scope.txtWeek = 0;
            $scope.txtMonth = 0;
            if ($scope.txtStartDate > $scope.txtEndDate) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('start Date is must be lesser then end date..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }
            else if ($scope.txtStartDate == undefined || $scope.txtStartDate == "" || $scope.txtEndDate == undefined || $scope.txtEndDate == "") {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('Please Select StartDate and EndDate ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }

        }

        $scope.txtStartDate = $scope.txtStartDate.substring(0, 10) + " " + $scope.txtStartDate.substring(11, 16);
        $scope.txtEndDate = $scope.txtEndDate.substring(0, 10) + " " + $scope.txtEndDate.substring(11, 16);

        var httpreq = {
            method: "POST",
            url: "DataReport.aspx/DWSDataSearch",
            data: { Type: $scope.Type, Week: $scope.txtWeek, Month: $scope.txtMonth, StartDate: $scope.txtStartDate, EndDate: $scope.txtEndDate, pageIndex: $scope.pageIndex, pageSize: $scope.pageSizeSelected },
        }
        $http(httpreq).success(function (response) {

            if (response.d != "") {
                json = JSON.parse(response.d);

                var filtered = {};
                $('.table_data').show();
                $scope.NoDataDiv = false;
                $scope.listData = angular.fromJson(json);
                $scope.list = $scope.listData.Table;
                $scope.list1 = $scope.listData.Table1;
                //console.dir($scope.list);
                //console.dir($scope.list1);
                $scope.totalCount = $scope.listData.Table1[0].totalCount;

            }
            else {
                $('.table_data').hide();
                $scope.NoDataDiv = true;
            }
        })
    };

    //Loading employees list on first time  
    //$scope.getEmployeeList();

    //This method is calling from pagination number  
    $scope.pageChanged = function () {
        $scope.Search();
    };

    //This method is calling from dropDown  
    $scope.changePageSize = function () {
        $scope.pageIndex = 1;
        $scope.Search();
    };

    $scope.SortList = function (orderBy) {
        if ($scope.orderList == orderBy) {
            if ($scope.orderDirection)
                $scope.orderDirection = false;
            else
                $scope.orderDirection = true;
        }
        else {
            $scope.orderList = orderBy;
        }
    }


    $scope.Export = function () {
        $scope.selCell = selCell.value;
        if ($scope.seldatepicker == undefined || $scope.seldatepicker == "") {
            toastr.options.positionClass = 'toast-top-center';
            toastr.error('Select Period First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
            return;
        }
        if ($scope.seldatepicker == "date") {
            $scope.Type = "Day";
            $scope.txtStartDate = document.getElementById("txtDate").value;
            $scope.txtWeek = 0;
            $scope.txtMonth = 0;
            $scope.txtEndDate = "";
            if ($scope.txtStartDate == "" || $scope.txtStartDate == undefined) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('Please Select Date ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }
        }
        if ($scope.seldatepicker == "week") {
            $scope.Type = "Week";
            var w = document.getElementById("txtWeek").value;
            $scope.txtWeek = w.substring(6);
            $scope.txtDate = 0;
            $scope.txtMonth = 0;
            $scope.txtStartDate = "";
            $scope.txtEndDate = "";
            if ($scope.txtWeek == "" || $scope.txtWeek == undefined) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('Please Select Week ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }
        }
        if ($scope.seldatepicker == "month") {
            $scope.Type = "Month";
            var m = document.getElementById("txtMonth").value;
            $scope.txtMonth = m.substring(5);
            $scope.txtDate = 0;
            $scope.txtWeek = 0;
            $scope.txtStartDate = "";
            $scope.txtEndDate = "";
            if ($scope.txtMonth == "" || $scope.txtMonth == undefined) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('Please Select Month ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }
        }
        if ($scope.seldatepicker == "dateRange") {
            $scope.Type = "dateRange";
            $scope.txtStartDate = document.getElementById("txtStartDate").value;
            $scope.txtEndDate = document.getElementById("txtEndDate").value;

            $scope.txtDate = 0;
            $scope.txtWeek = 0;
            $scope.txtMonth = 0;
            if ($scope.txtStartDate > $scope.txtEndDate) {
                toastr.options.positionClass = 'toast-top-center';
                toastr.error('start Date is must be lesser then end date..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                return;
            }

        }
        $scope.txtStartDate = $scope.txtStartDate.substring(0, 10) + " " + $scope.txtStartDate.substring(11, 16);
        $scope.txtEndDate = $scope.txtEndDate.substring(0, 10) + " " + $scope.txtEndDate.substring(11, 16);
        var httpreq = {
            method: "POST",
            url: "DataReport.aspx/Export",
            data: { Type: $scope.Type, Week: $scope.txtWeek, Month: $scope.txtMonth, StartDate: $scope.txtStartDate, EndDate: $scope.txtEndDate },
        }
        $http(httpreq).success(function (response) {
            $scope.ExportData = angular.fromJson(response.d);
            $scope.ArrayExport = $scope.ExportData.Table;
            //console.dir($scope.ArrayExport);

            if ($scope.ArrayExport == "") {
                toastr.options.positionClass = 'toast-top-center';
                toastr.info('No Data Found ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
            }
            else {
                JSONToCSVConvertor($scope.ArrayExport, "", true);
            }

            function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
                JSONData = angular.copy(JSONData);

                var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
                var CSV = '';
                CSV += ReportTitle + '\r\n\n';
                if (ShowLabel) {
                    var row = "";
                    for (var index in arrData[0]) {
                        row += index + ',';
                    }
                    row = row.slice(0, -1);
                    CSV += row + '\r\n';
                }

                for (var i = 0; i < arrData.length; i++) {
                    var row = "";
                    for (var index in arrData[i]) {
                        var txt = arrData[i][index];
                        if (index == "start" || index == "end") {
                            var d = new Date(txt);
                            txt = convertDate(d);
                        }
                        row += '"' + txt + '",';
                    }
                    row.slice(0, row.length - 1);
                    CSV += row + '\r\n';
                }

                if (CSV == '') {
                    return;
                }

                var today = new Date().toJSON().slice(0, 10).replace(/-/g, '/');;
                var fileName = "Report_ " + today;
                fileName += ReportTitle.replace(/ /g, "_");
                var blob = new Blob([CSV], { type: 'text/csv' });

                if (window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(blob, fileName);
                }
                else {
                    var elem = window.document.createElement('a');
                    elem.href = window.URL.createObjectURL(blob);
                    elem.download = fileName + ".csv";
                    document.body.appendChild(elem);
                    elem.click();
                    document.body.removeChild(elem);
                }
                toastr.options.positionClass = 'toast-top-center';
                toastr.success('Data Exported..!!', 'Great', { timeOut: 2000, closeButton: true, preventDuplicates: true });
            }

        })
    };

});


$(document).ready(function () {
    $('#date').hide();
    $('#month').hide();
    $('#week').hide();
    $('#dateRange').hide();
    $('#SKU').hide();
    $('#seldatepicker').change(function () {
        if ($('select[name=DatePicker] option:selected').val() == 'date') {
            $('#date').show();
            $('#week').hide();
            $('#month').hide();
            $('#dateRange').hide();
            document.getElementById("txtWeek").value = "";
            document.getElementById("txtMonth").value = "";
            document.getElementById("txtStartDate").value = "";
            document.getElementById("txtEndDate").value = "";
        } else if ($('select[name=DatePicker] option:selected').val() == 'week') {
            $('#week').show();
            $('#date').hide();
            $('#month').hide();
            $('#dateRange').hide();
            document.getElementById("txtDate").value = "";
            document.getElementById("txtMonth").value = "";
            document.getElementById("txtStartDate").value = "";
            document.getElementById("txtEndDate").value = "";
        } else if ($('select[name=DatePicker] option:selected').val() == 'month') {
            $('#month').show();
            $('#date').hide();
            $('#week').hide();
            $('#dateRange').hide();
            document.getElementById("txtDate").value = "";
            document.getElementById("txtWeek").value = "";
            document.getElementById("txtStartDate").value = "";
            document.getElementById("txtEndDate").value = "";
        }
        else if ($('select[name=DatePicker] option:selected').val() == 'dateRange') {
            $('#dateRange').show();
            $('#date').hide();
            $('#week').hide();
            $('#month').hide();
            document.getElementById("txtDate").value = "";
            document.getElementById("txtWeek").value = "";
            document.getElementById("txtMonth").value = "";
        }
    });
});

