﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="BoxCalibrationSetting.aspx.cs" Inherits="A1143_Delhivery.Pages.Setting.BoxCalibrationSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title>Delhivery | Calibration setting</title>
    <link href="../../global/webapp/BoxCalibrationSetting.css" rel="stylesheet" />
    <script type="text/javascript">
        var app = angular.module('myApp', []);
        app.controller("myController", function ($scope, $http, $interval) {
            // alert("hi");
            $scope.inlistData = <%=listData %>; //;// Bind Your List Data here
            $scope.BoxSetting1 = <%=BoxSetting %>;
            $scope.listData = angular.fromJson($scope.inlistData);
            $scope.BoxSetting = angular.fromJson($scope.BoxSetting1);
            $scope.txtSearch = '';

            $scope.StartTimer = function () {
                $scope.Timer = $interval(function () {
                    $scope.DoTimerAction();
                }, 1000);
            };
            $scope.StopTimer = function () {
                $scope.Message = "Timer stopped.";
                if (angular.isDefined($scope.Timer)) {
                    $interval.cancel($scope.Timer);
                }
            };
            $scope.DoTimerAction = function () {
                //alert();
                $scope.RefreshList();
               
            };

            //************************** function To Refresh List
            $scope.RefreshList = function () {
               // alert();
                var httpreq = {
                    method: "POST",
                    url: "BoxCalibrationSetting.aspx/GetCalibrationCount",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'dataType': 'json'
                    },
                    data: {},
                }
                $http(httpreq).success(function (response) {
                    $scope.listDataCount = angular.fromJson(response.d);
                    $scope.MasterCount = $scope.listDataCount.Table[0].MasterCount;
                  //  alert($scope.MasterCount);
                })
            };



            //************************** function To Refresh List
            $scope.GetCalibrationList = function () {

                var httpreq = {
                    method: "POST",
                    url: "BoxCalibrationSetting.aspx/GetCalibrationList",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'dataType': 'json'
                    },
                    data: {},
                }
                $http(httpreq).success(function (response) {
                    $scope.listData = angular.fromJson(response.d);
                    $scope.GetBoxCount();
                    $scope.txtbarcode = "";
                    document.getElementById("txtbarcode").readOnly = false;
                })
            };

            angular.element(document).ready(function () {

                $scope.GetBoxCount();

            });

            $scope.GetBoxCount = function () {
                //alert();
                var httpreq = {
                    method: "POST",
                    url: "BoxCalibrationSetting.aspx/BoxCount",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'dataType': 'json'
                    },
                    data: {},
                }
                $http(httpreq).success(function (response) {
                    $scope.BoxSetting = angular.fromJson(response.d);
                    $scope.fieldname = $scope.BoxSetting[0].FieldName;
                    $scope.Value = $scope.BoxSetting[0].Value;
                    document.getElementById("txtbarcode").readOnly = false;
                })
            };


            //******************** End Reset List******************
            //************************** function To Refresh List
            $scope.Reset = function () {
                $scope.txtbarcode = "";
                $scope.txtweight = "";
                $scope.txtlength = "";
                $scope.txtwidth = "";
                $scope.txtheight = "";
                $scope.txtvolume = "";
                document.getElementById("txtbarcode").readOnly = false;
            }
            //******************** End Reset List******************
            //************************** function To Save Data***************
            // Same function will be used for Insert and Update
            // for Insert Id will be 0. $scope.hiddenId =0  
            //for update Id will be set to $scope.hiddenId
            $scope.serachList = function (Barcode) {
                for (var cnt = 0; cnt < $scope.listData.length; cnt++) {
                    if ($scope.listData[cnt].Barcode == Barcode) {
                        return $scope.listData[cnt];
                    }
                }
            }
            $scope.Save = function () {

                if ($scope.txtbarcode == undefined || $scope.txtbarcode == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Barcode First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtweight == undefined || $scope.txtweight == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Weight First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtlength == undefined || $scope.txtlength == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Length First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtwidth == undefined || $scope.txtwidth == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Width First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtheight == undefined || $scope.txtheight == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Height First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }

                var obj = { Barcode: $scope.txtbarcode, Weight: $scope.txtweight, Length: $scope.txtlength, Width: $scope.txtwidth, Height: $scope.txtheight };
                //alert($scope.txtbarcode);
                var JSONData = JSON.stringify(obj);
                //alert(JSONData);
                var httpreq = {
                    method: "POST",
                    url: "BoxCalibrationSetting.aspx/InsertCalibrationBox",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'dataType': 'json'
                    },
                    data: { JSONData },
                }
                $http(httpreq).success(function (response) {
                    $scope.GetCalibrationList();
                    $scope.Reset();
                    $scope.hiddenId = response.d;  // save function should send Id of records, which will be store back to hidden field
                    // add your additiona code to show and hide div over here
                    document.getElementById("txtbarcode").readOnly = false;

                })

            }

            $scope.SaveTimeSetting = function () {
                if ($scope.txttime == undefined || $scope.txttime == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Time First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                var obj = { Time: $scope.txttime };
                var JSONData = JSON.stringify(obj);
                //alert(JSONData);
                var httpreq = {
                    method: "POST",
                    url: "BoxCalibrationSetting.aspx/UpdateTime",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'dataType': 'json'
                    },
                    data: { JSONData },
                }
                $http(httpreq).success(function (response) {
                    $scope.GetCalibrationList();
                    $scope.Reset();
                    $scope.GetBoxCount();
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.success('Updated successfully ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                    // swal("Data Inserted successfully..");
                    $scope.hiddenId = response.d;  // save function should send Id of records, which will be store back to hidden field
                    // add your additiona code to show and hide div over here
                    document.getElementById("txtbarcode").readOnly = false;

                })

            }


            $scope.SaveCountSetting = function () {
                if ($scope.txtboxcount == undefined || $scope.txtboxcount == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Box Count First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                var obj = { MBoxCount: $scope.txtboxcount };
                var JSONData = JSON.stringify(obj);
                //alert(JSONData);
                var httpreq = {
                    method: "POST",
                    url: "BoxCalibrationSetting.aspx/UpdateBoxCount",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'dataType': 'json'
                    },
                    data: { JSONData },
                }
                $http(httpreq).success(function (response) {
                    $scope.GetCalibrationList();
                    $scope.Reset();
                    $scope.GetBoxCount();
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.success('Updated successfully ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                    // swal("Data Inserted successfully..");
                    $scope.hiddenId = response.d;  // save function should send Id of records, which will be store back to hidden field
                    // add your additiona code to show and hide div over here
                    document.getElementById("txtbarcode").readOnly = false;

                })

            }

            //************************** End To Save Data


            $scope.Update = function () {

                if ($scope.txtbarcode == undefined || $scope.txtbarcode == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Barcode First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtweight == undefined || $scope.txtweight == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Weight First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtlength == undefined || $scope.txtlength == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Length First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtwidth == undefined || $scope.txtwidth == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Width First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtheight == undefined || $scope.txtheight == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Height First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                else if ($scope.txtvolume == undefined || $scope.txtvolume == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.error('Enter Volume First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                }
                //else if ($index >=5) {
                //    toastr.options.positionClass = 'toast-top-center';
                //    toastr.error('Already inserted 5 Calibration Boxes ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                //    return;
                //}

                var obj = { Barcode: $scope.txtbarcode, Weight: $scope.txtweight, Length: $scope.txtlength, Width: $scope.txtwidth, Height: $scope.txtheight, Volume: $scope.txtvolume };
                //alert($scope.txtbarcode);
                var JSONData = JSON.stringify(obj);
                //alert(JSONData);
                var httpreq = {
                    method: "POST",
                    url: "BoxCalibrationSetting.aspx/UpdateCalibrationBox",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'dataType': 'json'
                    },
                    data: { JSONData },
                }
                $http(httpreq).success(function (response) {
                    $("#btnUpdate").hide();
                    $("#btnNew").hide();
                    $("#btnSave").show();
                    $scope.GetCalibrationList();
                    $scope.Reset();
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.success('Data Updated successfully ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                    return;
                    // swal("Data Inserted successfully..");
                    $scope.hiddenId = response.d;  // save function should send Id of records, which will be store back to hidden field
                    // add your additiona code to show and hide div over here
                    document.getElementById("txtbarcode").readOnly = false;

                })

            }


            //************************** Delete Data**********************************
            $scope.DeleteCalibrationBox = function (Barcode) {
                //alert(Barcode);

                var searchItem = $scope.serachList(Barcode);
                $scope.hiddenId = Barcode;

                swal({
                    title: "Are you Sure!",
                    text: "Do you want to delete user? ",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK!",
                    closeOnConfirm: true
                }, function (isConfirm) {
                    if (!isConfirm) return;

                    var httpreq = {
                        method: "POST",
                        url: "BoxCalibrationSetting.aspx/DeleteCalibratioBox",
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'dataType': 'json'
                        },
                        data: { Barcode: $scope.hiddenId },
                    }
                    $http(httpreq).success(function (response) {
                        //alert("Data deleted successfully..");
                        $scope.GetCalibrationList();
                        toastr.options.positionClass = 'toast-top-center';
                        toastr.success('Delated successfully ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                        return;
                        $scope.hiddenId = response.d;  // save function should send Id of records, which will be store back to hidden field
                        // add your additiona code to show and hide div over here
                        document.getElementById("txtbarcode").readOnly = false;


                    })

                })


            };

            //************************** End To Delete Data******************************//

            $scope.EditCalibrationBox = function (Barcode, Weight, Length, Height, Width, Volume) {
                document.getElementById("txtbarcode").readOnly = true;
                $("#btnSave").hide();
                $("#btnUpdate").show();
                $("#btnCancel").show();
                $("#btnRefresh").hide();

                //alert(Barcode);
                var searchItem = $scope.serachList(Barcode);

                $scope.txtbarcode = Barcode;
                //$scope.txtemployee_name1 = searchItem.employee_name;
                $("#txtbarcode").attr("class", "form-control ");
                $scope.txtweight = Weight;
                $("#txtweight").attr("class", "form-control");

                $scope.txtlength = Length;
                $("#txtlength").attr("class", "form-control ");

                $scope.txtwidth = Width;
                $("#txtwidth").attr("class", "form-control ");

                $scope.txtheight = Height;
                $("#txtheight").attr("class", "form-control ");

                $scope.txtvolume = Volume;
                $("#txtvolume").attr("class", "form-control ");

            };

            $scope.New = function () {
                $("#btnSave").show();
                $("#btnUpdate").hide();
                $("#btnNew").hide();
                $scope.txtbarcode = "";
                $scope.txtweight = "";
                $scope.txtlength = "";
                $scope.txtwidth = "";
                $scope.txtheight = "";
                $scope.txtvolume = "";
                document.getElementById("txtbarcode").readOnly = false;
            };
            $scope.Refresh = function () {
                $("#btnSave").show();
                $("#btnUpdate").hide();
                $("#btnNew").hide();
                $scope.txtbarcode = "";
                $scope.txtweight = "";
                $scope.txtlength = "";
                $scope.txtwidth = "";
                $scope.txtheight = "";
                $scope.txtvolume = "";
                document.getElementById("txtbarcode").readOnly = false;
            };
            $scope.Cancel = function () {
                $("#btnSave").show();
                $("#btnUpdate").hide();
                $("#btnNew").hide();
                $("#btnRefresh").show();
                $("#btnCancel").hide();
                $scope.txtbarcode = "";
                $scope.txtweight = "";
                $scope.txtlength = "";
                $scope.txtwidth = "";
                $scope.txtheight = "";
                $scope.txtvolume = "";
                document.getElementById("txtbarcode").readOnly = false;
            };

            $scope.display = function () {
                $("#txttime").show();
                $("#btnSave1").show();
                $("#countSave").hide();
                $("#txtboxcount").hide();
            }

            $scope.displayCount = function () {
                $("#txtboxcount").show();
                $("#btnSave1").hide();
                $("#txttime").hide();
                $("#countSave").show();
            }


            $scope.serachList = function (id) {
                for (var cnt = 0; cnt < $scope.listData.length; cnt++) {
                    if ($scope.listData[cnt].id == id) {
                        return $scope.listData[cnt];
                    }
                }
            }

        });
    </script>
    <style>
        #detail #form1 input {
            width: 50%;
            background-color: #fff !important;
            padding: 5px 10px;
        }

        #form1 table tr {
            margin-bottom: 10px !important;
        }

        .top_row .title {
            background-color: #6e7b87;
            color: white;
            height: 35px;
            padding-left: 20px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Calibration" ng-app="myApp" ng-controller="myController">
        <div class="calibration">

            <div class="row" id="detail">
                <div class="col-md-5">
                    <div class="row">
                        <div class="top_row">
                            <div class="col-md-12 title">

                                <h5 class="main_title">Calibration Box</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 12px;">
                        <div class="col-md-12">
                            <form id="form1" class="form-inline">
                                <table style="width: 100%;">
                                    <tr style="padding-bottom: 10px;">
                                        <td colspan="2">
                                            <div class="form-group form-material floating" style="display: flex;">
                                                <label class="pull-right" for="fname">AWB</label>
                                                <input type="text" ng-model="txtbarcode" class="form-control" required name="txtbarcode" id="txtbarcode" required placeholder="Enter AWB number"><br>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group form-material floating">
                                                <label for="txtlength">Length</label>
                                                <input type="text" ng-model="txtlength" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required name="txtlength" id="txtlength" required placeholder="Enter Length in mm"><br>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="form-group form-material floating">
                                                <label for="txtwidth">width</label><br>
                                                <input type="text" ng-model="txtwidth" class="form-control " onkeypress='return event.charCode >= 48 && event.charCode <= 57' required name="txtwidth" id="txtwidth" required placeholder="Enter width in mm"><br>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group form-material floating">
                                                <label for="txtheight">Height</label>
                                                <input type="text" ng-model="txtheight" class="form-control " onkeypress='return event.charCode >= 48 && event.charCode <= 57' required name="txtheight" id="txtheight" required placeholder="Enter Height in mm"><br>
                                            </div>

                                        </td>
                                        <td>

                                            <div class="form-group form-material floating">
                                                <label for="txtweight">Weight</label>
                                                <input type="text" ng-model="txtweight" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required name="txtweight" id="txtweight" required placeholder="Enter Weight gram"><br>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </form>
                        </div>
                    </div>

                    <div class="row no-gutters">
                        <div class="center" style="display: flex; margin-left: auto; margin-right: auto; margin-top: 10px;">

                            <button type="button" id="btnSave" class="btn btn-primary" value="Save" ng-click="Save()" style="margin-right: 10px;">
                                <img style="margin-bottom: 3px;" src="../../Images/save.png" />
                                Save</button>
                            <button style="display: none; margin-right: 10px;" type="button" id="btnUpdate" class="btn btn-primary" value="Update" ng-click="Update()">
                                <img src="../../Images/save_as.png" style="margin-bottom: 3px;" />
                                Update</button>
                            <button type="button" id="btnRefresh" class="btn btn-success" value="Refresh" ng-click="Refresh()">
                                <img src="../../Images/refresh.png" style="margin-bottom: 3px;" />
                                Reset</button>
                            <button style="display: none;" type="button" id="btnCancel" class="btn btn-success" value="Cancel" ng-click="Cancel()">
                                <img src="../../Images/close.png" style="margin-bottom: 3px;" />
                                Cancel</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="row no-gutters">
                        <div class="top_row">
                            <div class="col-md-12 title">

                                <h5 class="main_title">List of 5 Calibration Box</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters" style="padding-top: 10px; padding-right: 10px;">
                        <div class="table_data table-responsive">
                            <table class="table" style="width: 100%;">
                                <thead>
                                    <tr>

                                        <th>Sr.No</th>
                                        <th>AWB</th>
                                        <th>Length(mm)</th>
                                        <th>Width(mm)</th>
                                        <th>Height(mm)</th>
                                        <th>Weight(g)</th>
                                        <th>Volume(cm3)</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in listData | filter:txtSearch">
                                        <td style="text-align: center;">{{$index+1}}</td>
                                        <td>{{item.Barcode}}</td>

                                        <td>{{item.Length}}</td>
                                        <td>{{item.Width}}</td>
                                        <td>{{item.Height}}</td>

                                        <td>{{item.Weight}}</td>
                                        <td>{{item.Volume}}</td>
                                        <td style="text-align: center;">
                                            <button id="btnEdit" data-toggle="tooltip" data-original-title="Edit" id="btnEdit" ng-click="EditCalibrationBox(item.Barcode,item.Weight,item.Length,item.Height,item.Width,item.Volume)" style="border: none;" title="Edit Barcode">
                                                <img src="../../global/images/edit_1.png" style="width: 16px; height: auto;" />
                                            </button>

                                            <button id="btnDelete" ng-click="DeleteCalibrationBox(item.Barcode)" style="border: none;" title="Delete Barcode">
                                                <img src="../../global/images/delete_1.png" style="width: 16px; height: auto;" />
                                            </button>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="9" class="center" style="text-align: center;">
                                            <div ng-switch="(listData | filter:txtSearch).length">
                                                <span class="ng-empty text-danger" ng-switch-when="5">* Only 5 calibration boxes are allowed..</span>
                                            </div>

                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="9" class="text-center" style="border-top: none;">
                                            <div ng-switch="(listData | filter:txtSearch).length">
                                                <span class="ng-empty text-danger" ng-switch-when="0">No Record(s) Found</span>
                                            </div>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <hr />
        </div>

        <div class="row ">

            <div class="col-md-5" id="cali">
                <div class="row no-gutters">
                    <div class="top_row">
                        <div class="col-md-12 title">

                            <h5 class="main_title">Calibration Box Setting</h5>
                        </div>
                    </div>

                </div>
                <div class="row no-gutters" style="padding: 10px; margin-left: auto; width: 90%;">
                    <div class="col-md-6">
                        <div class="form-group form-material floating fr-cust_radio">
                            <label class="radio_button" ng-click="display()">
                                Time
                                      <input type="radio" checked="checked" name="radio">
                                <span class="checkmark" id="checkmark"></span>
                            </label>
                            <input type="text" ng-model="txttime" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required name="txttime" id="txttime" required style="margin-left: auto; background-color: #fff; width: 100%; display: none;" placeholder="Enter time in min i.e 1=60min" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-material floating fr-cust_radio">
                            <label class="radio_button" ng-click="displayCount()">
                                Box Count
                                      <input type="radio" checked="checked" name="radio">
                                <span class="checkmark" id="checkmark"></span>
                            </label>
                            <input type="type" ng-model="txtboxcount" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required name="txtboxcount" id="txtboxcount" required style="margin-left: auto; background-color: #fff; width: 100%; display: none;" placeholder="Enter Box Count" />
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="center" id="Calbutton" style="display: block; margin-left: auto; margin-right: auto; margin-top: 10px; margin-bottom: 20px;">

                        <button type="button" id="btnSave1" class="btn btn-primary" value="Save" ng-click="SaveTimeSetting()" style="display: none;">
                            <img style="margin-bottom: 3px;" src="../../Images/save.png" />
                            Save</button>
                        <button type="button" id="countSave" class="btn btn-primary" value="Save" ng-click="SaveCountSetting()" style="display: none;">
                            <img style="margin-bottom: 3px;" src="../../Images/save.png" />
                            Save</button>

                    </div>
                </div>
            </div>


            <div class="col-md-7"  ng-init="StartTimer()">
                <div class="row no-gutters">
                    <div class="top_row">
                        <div class="col-md-12 title">

                            <h5 class="main_title">Current Box Setting</h5>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters" style="padding-top: 10px; padding-right: 10px; margin-bottom: 20px;">
                    <div class="table_data">
                        <table class="table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>{{fieldname}}</th>
                                    <th>Current count</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{Value}}</td>
                                    <td>{{MasterCount}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center" style="border-top: none;">
                                        <div ng-switch="(item | filter:txtSearch).length">
                                            <span class="ng-empty text-danger" ng-switch-when="0">No Record(s) Found</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
