﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="A1143_Delhivery.Pages.Reports.dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <head runat="server">
        <title>Delhivery | Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="/../../JS/dashboard/dashboard.js"></script>
        <style>
            
            .divRed1{
                color:red !important;
            }
            
            .divgreen{
                color:green !important;
            }
            .modal-content .modal-body .table_wrap tbody tr td{
                background-color:red;
                color: white;
            }

           
            #Modal_Notify_announciation .modal-body {
                height: 400px;
                overflow-y: auto;
                padding: 5px 15px;
            }
            .modal-backdrop{z-index:1074;}
            #Modal_Notify_announciation.modal{z-index:9999;}
        </style>
    </head>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="dashboard" class="mdash" ng-app="myApp" ng-controller="myController" ng-init="StartTimer()">

        <div class="container-fluid">
            <div id="icons" class="col-md-12 mrtop10 mrbot10">
                <div class="row">

                    <div class="col-md-3 col-xs-12 mrbot10  pl-0 pr-0">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon">
                                    <img class="img-fluid" src="../../global/Images/barcode.svg" alt="...">
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">AWB</h4>
                                        <h4 class="card-text text-center">{{Barcode}}</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12 mrbot10">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon1">
                                    <img class="img-fluid" src="../../global/images/total_count.svg" alt="...">
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">Today's Count</h4>
                                        <h4 class="card-text text-center">{{totalCount}}</h4>

                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>

                    <div class="col-md-3 col-xs-12 mrbot10">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon2">
                                    <img class="img-fluid" src="../../global/images/Volume_31.png" />
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">Volume(cm3)</h4>
                                        <h4 class="card-text text-center">{{Volume}}</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12 mrbot10  pl-0 pr-0">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon3">
                                    <img class="img-fluid" src="../../global/images/Volume_31.png" />
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">Real Volume(cm3)</h4>
                                        <h4 class="card-text text-center">{{RealVolume}}</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-md-3 col-xs-12 mrbot10 pl-0 pr-0">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon4">
                                    <img src="../../global/images/Length_31.png" />
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">Length(cm)</h4>
                                        <h4 class="card-text text-center">{{Length}}</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12 mrbot10 ">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon5">
                                    <img src="../../global/images/Width_11.png" />
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">Width(cm)</h4>
                                        <h4 class="card-text text-center">{{Width}}</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-12 mrbot10">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon6">
                                    <img src="../../global/images/Height_11.png" />
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">Height(cm)</h4>
                                        <h4 class="card-text text-center">{{Height}}</h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-3 col-xs-12 mrbot10  pl-0 pr-0">
                        <div class="card" style="">
                            <div class="row">
                                <div class="col-md-3 icon7">
                                    <img class="img-fluid" src="../../Images/weight.png" alt="...">
                                </div>
                                <div class="col-md-9">
                                    <div class="card-body">
                                        <h4 class="card-title text-center">Weight(g)</h4>
                                        <h4 class="card-text text-center">{{Weight}}</h4>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
            <div class="col-md-12 mrbot10">
                <div class="main_table">


                    <div class="row no-gutters">
                        <div class="col-md-8">
                            <div style="display: flex; justify-content: flex-end;">
                                <h6 style="margin-right:8%;" id="tabletitle">Processed Packages (Last 10 Records)</h6>

                                <input id="searchTextBox" ng-model="txtSearch1" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required class="inline clearable inlinesearch" placeholder="Search Barcode..">
                                <button id="searchbutton" class="inlinesearch btn btn-info" ng-click="SearchData()" style="display: none;">

                                    <img src="../../Images/search_white.png" style="margin-bottom: 2px;" />
                                </button>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="display: flex; justify-content: flex-end; margin-bottom: 10px;">

                                <button type="button" id="serachbtn" class="btn btn-primary search_btn inline" ng-click="Search()" title="Search Barcode." style="margin-right: 10px;">
                                    Search
                <img src="../../images/search_white.png" style="margin-bottom: 1px;" /></button>
                                <button type="button" id="serachclose" class="btn btn-default search_btn inline" ng-click="close()" style="display: none; margin-right: 10px;">

                                    <img src="../../global/images/x-mark.png" style="margin-bottom: 1px;" /></button>

                                <button type="button" id="exportbtn" class="btn btn-primary inline" ng-click="Export()" title="Export Today's Data">

                                    <img style="margin-left: 1px; margin-bottom: 2px;" src="../../global/images/export.svg"></button>
                            </div>
                        </div>
                    </div>


                    <div class="table_data" style="margin-bottom: 0px;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Sr.No</th>
                                    <th scope="col">AWB</th>

                                    <th scope="col">Length(cm)</th>
                                    <th scope="col">Width(cm)</th>
                                    <th scope="col">Height(cm)</th>
                                    <th scope="col">Weight(g)</th>
                                    <th scope="col">Volume(cm3)</th>
                                    <th scope="col" class="w130">Real Volume(cm3)</th>
                                    <th scope="col" class="w100">Package Type</th>
                                    <th scope="col" class="w90">Data Sent</th>
                                    <th scope="col">Failed Reason </th>
                                    <th scope="col" class="w150">Timestamp</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in listData">
                                    <td>{{$index+1}}</td>
                                    <td title="{{item.Barcode}}"><span class="tabulator-cell" style="width: 130px;">{{item.Barcode}}</td>

                                    <td>{{item.Length}}</td>
                                    <td>{{item.Width}}</td>
                                    <td>{{item.Height}}</td>
                                    <td>{{item.Weight}}</td>
                                    <td>{{item.Volume}}</td>
                                    <td>{{item.RealVolume}}</td>
                                    <td>{{item.PackageType}}</td>
                                    <td ng-class="{'divRed1': item.IsSend == 'Failed','divgreen': item.IsSend == 'Success'}">{{item.IsSend}}</td>
                                    <td title="{{item.ReasonForFailed}}"><span class="tabulator-cell" style="width: 100px;">{{item.ReasonForFailed}}</span></td>
                                    <td>{{item.Timestamp}}</td>
                                </tr>
                                <tr>
                                    <td colspan="12" class="text-center" style="font-size: 15px; background-color: white; border-bottom: 1px solid white; border-right: 1px solid white; border-left: 1px solid white;">
                                        <div ng-switch="(listData | filter:txtSearch).length">
                                            <span class="ng-empty text-danger" ng-switch-when="0">No Record(s) Found</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="table_data" id="searchtable" style="margin-bottom: 0px; display: none;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Sr.No</th>
                                    <th scope="col">AWB</th>

                                    <th scope="col">Length(cm)</th>
                                    <th scope="col">Width(cm)</th>
                                    <th scope="col">Height(cm)</th>
                                    <th scope="col">Weight(g)</th>
                                    <th scope="col">Volume(mm3)</th>
                                    <th scope="col">Real Volume(mm3)</th>
                                    <th scope="col">Package Type</th>
                                    <th scope="col">Data Send</th>
                                    <th scope="col">Reason for Failed </th>
                                    <th scope="col">Datetime</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in listData1">
                                    <td>{{$index+1}}</td>
                                    <td>{{item.Barcode}}</td>

                                    <td>{{item.Length}}</td>
                                    <td>{{item.Width}}</td>
                                    <td>{{item.Height}}</td>
                                    <td>{{item.Weight}}</td>
                                    <td>{{item.Volume}}</td>
                                    <td>{{item.RealVolume}}</td>
                                    <td>{{item.PackageType}}</td>
                                    <td ng-class="{'divRed1': item.IsSend == 'Failed','divgreen': item.IsSend == 'Success'}">{{item.IsSend}}</td>
                                    <td title="{{item.ReasonForFailed}}"><span class="tabulator-cell" style="width: 130px;">{{item.ReasonForFailed}}</span></td>
                                    <td>{{item.Datetime}}</td>
                                </tr>
                                <tr>
                                    <td colspan="12" class="text-center" style="font-size: 15px; background-color: white; border-bottom: 1px solid white; border-right: 1px solid white; border-left: 1px solid white;">
                                        <div ng-switch="(listData1 | filter:txtSearch1).length">
                                            <span class="ng-empty text-danger" ng-switch-when="0">No Record(s) Found</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <%-- Model--%>

            <div class="modal fade" id="Modal_Notify_announciation" role="dialog" tabindex="-1" style="padding-right: 17px; display: none;">
                <div class="modal-dialog" style="max-width: 750px; min-height: 100px;margin-top:80px;">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Alarm Annunciation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                        <div class="modal-body" style="min-height: 100px;">
                            <div class="">

                                <table class="table_wrap table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="w30">Sr.No</th>
                                            <th class="w100">Time</th>
                                            <th class="w100">Date</th>
                                            <th class="w450">Error Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item in AnnunciationData" ng-click="open()">
                                            <td>{{item.RowNum}}</td>
                                            <td>{{item.Time}}</td>
                                            <td>{{item.Date}}</td>
                                            <td>{{item.ErrorDescription}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>



            <%-- Model End--%>



                    <div id="IDannounciation" style="display: none;" class="footer" ng-click="open()">
                        <div class="fr_scroll">

                            <table class="table_wrap table" ng-click="open()">
                                <tbody>
                                    <tr ng-repeat="item in AnnunciationData" ng-click="open()">
                                        <td style="text-align: left; font-weight: 600; color: red; border-top: none; padding-top: 0px; padding-bottom: 0px;cursor:pointer;"  ng-click="open()">* Alert {{$index+1}} : - {{item.ErrorDescription}}</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
