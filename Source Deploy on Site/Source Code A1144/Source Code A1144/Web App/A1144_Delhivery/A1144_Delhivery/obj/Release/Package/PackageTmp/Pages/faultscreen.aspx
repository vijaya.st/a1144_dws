﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="faultscreen.aspx.cs" Inherits="A1143_Delhivery.Pages.faultscreen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title>Delhivery | System Alarm's</title>
    <link href="../global/webapp/FaultScreen.css" rel="stylesheet" />
    <script src="../JS/webapp/FaultScreen.js"></script>
    <style>
        .div-left {
            float: left;
            padding-left: 10px;
        }

        .divRed h6 {
            color: #fff !important;
        }

        .fault_box h6 {
            font-size: 14px;
        }

        .div-right {
            float: right;
            padding-right: 10px;
        }

        .table_wrap thead th {
            background-color: #003fb3;
            color: #fff;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="faultpage" ng-app="myApp" ng-controller="myController" ng-init="StartTimer()">
        <div class="top_row">
            <div class="col-md-12" style="background-color: #6e7b87; color: white; height: 37px; padding-left: 20px;">

                <h4 class="main_title">System Alerts</h4>
            </div>
        </div>

        <div class="inner_content">
            <div class="container-fluid">
                <div class="row">
                    <div class="left_side">
                        <div class="row no-gutters">

                            <div class="form-group col-md-3">
                                <div class="row">
                                    <label class="col-md-4" for="exampleFormControlSelect1">Select</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="seldatepicker" id="seldatepicker" name="DatePicker">
                                            <option value="" selected="">Select of</option>
                                            <option value="date">Date</option>
                                            <option value="week">Week</option>
                                            <option value="month">Month</option>
                                            <option value="dateRange">DateTime range</option>
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="form-group col-md-3" id="date">
                                <div class="row">
                                    <label class="col-md-4" for="exampleFormControlInput1">Date</label>
                                    <div class="col-md-8">
                                        <input type="Date" class="form-control" id="txtDate" placeholder="Select Date" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="week">
                                <div class="row no-gutters">
                                    <label class="col-md-4" for="exampleFormControlInput1">Week</label>
                                    <div class="col-md-8">
                                        <input type="week" class="form-control" id="txtWeek" placeholder="Select Week" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="month">
                                <div class="row">
                                    <label class="col-md-4" for="exampleFormControlInput1">Month</label>
                                    <div class="col-md-8">
                                        <input type="month" class="form-control" id="txtMonth" placeholder="Select Month" value="" />
                                    </div>
                                </div>
                            </div>
                            <div id="dateRange" class="col-md-6">
                                <div class="row no-gutters">
                                    <div class="form-group col-md-6 pr-0 pl-0">
                                        <div class="row">
                                            <label class="col-md-4 pl-0 pr-0" for="exampleFormControlInput1">From Date</label>
                                            <div class="col-md-8 pl-0 pr-0">
                                                <input type="datetime-local" class="form-control" id="txtStartDate" placeholder="Start Date" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6 pl-0 pr-0">
                                        <div class="row">
                                            <label class="col-md-4 pr-0" for="exampleFormControlInput1">To Date</label>
                                            <div class="col-md-8 pl-0 pr-0">
                                                <input type="datetime-local" class="form-control" id="txtEndDate" placeholder="End Date" value="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="RobotCell">
                                <div class="row ">
                                    <label class="col-md-4" for="exampleFormControlSelect1">DWS No.</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="selCell" id="selCell">
                                            <option value="" selected="">Select DWS</option>
                                            <option value="1">MAA_Poonamallee_HB_A1143_1</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 cust_btn_wrap">
                                <div class="row">
                                    <button type="button" class="btn btn-primary" ng-click="Search()" title="Search Data">

                                        <img src="/Images/search.svg"></button>
                                    <button type="button" id="exportbtn" class="btn btn-primary inline" ng-click="Export()" title="Export Data To Excel">

                                        <img style="margin-left: 1px; margin-bottom: 2px;" src="../../global/images/export.svg"></button>
                                    <button type="button" id="alertHide" class="btn btn-primary" ng-click="Hide()" title="hide alerts (switch into Table Type Data)">
                                        Hide Alert</button>
                                    <button type="button" id="alertTableHide" class="btn btn-primary" ng-click="HideTable()" style="display: none;">
                                        Hide Table</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row fault_box" style="margin-bottom: 20px;">
                    <div class="col-md-2 " ng-repeat="data in listData">
                        <div class="card mb-3" style="border: none;" title="{{data.FaultName}}" id="faults_ID" ng-click="open(data.ID)">

                            <div class="col-md-12" id="one" style="box-shadow: 0 0 4px #000; border-radius: 18px 18px; padding: 0.1rem; overflow: hidden; line-height: 55px; background-color: #fff;" ng-class="{'divRed':data.Status==1}">
                                <div class="card-body" style="padding: 0.3rem; height: 60px;">
                                    <h6 class="card-title text-center">{{data.FaultName}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="table_data table-responsive" style="margin-bottom: 0px; display: none; background-color: #fff;">
                    <table class="table" style="margin-bottom: 30px;">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th style="cursor: pointer" ng-click="SortList('Barcode')">AlertName</th>
                                <th>StartTime</th>
                                <th>EndTime</th>
                                <th>TotalTime</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in list | filter:txtSearch | orderBy:orderList:orderDirection">
                                <td>{{$index+1}}</td>
                                <td>{{item.FaultName}}</td>
                                <td>{{item.StartTime}}</td>
                                <td>{{item.EndTime}}</td>
                                <td>{{item.TotalTime}} mm:ss</td>
                            </tr>
                            <tr>
                                <td colspan="11" class="text-center">
                                    <div ng-switch="(list | filter:txtSearch).length">
                                        <span class="ng-empty text-danger" ng-switch-when="0">No Record(s) Found</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot id="tfoot">
                            <tr>
                                <td align="center" colspan="11">
                                    <div class="div-left">
                                        <span class="form-group pull-left page-size form-inline">
                                            <select id="ddlPageSize" class="form-control"
                                                ng-model="pageSizeSelected"
                                                ng-change="changePageSize()">
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="1000">1000</option>
                                            </select>
                                        </span>
                                    </div>
                                    <div class="pull-right pagination div-right" style="display: flex;">
                                        <a class="btn btn-primary" style="color: #161616;">Total Time {{FinalTotalTime}} hh:mm:ss</a>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>



            </div>
        </div>



        <%-- Model--%>

        <div class="modal fade" id="Modal_Notify_announciation" role="dialog" tabindex="-1" style="padding-right: 17px; display: none;">
            <div class="modal-dialog" style="max-width: 750px; min-height: 100px; margin-top: 80px;">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Rectification action</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" style="min-height: 100px;">
                        <div class="">

                            <table class="table_wrap table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="w30" style="width: 50px;">Sr. No</th>
                                        <th class="w30">Fault Name</th>
                                        <th class="w30">Solution</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="data in Rectification_Action"  ng-class="{'divRed':data.Status==1}">
                                        <td style="text-align: center;">{{$index+1}}</td>
                                        <td>{{data.FaultName}}</td>
                                        <td>{{data.Rectification_Action}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>



        <%-- Model End--%>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
