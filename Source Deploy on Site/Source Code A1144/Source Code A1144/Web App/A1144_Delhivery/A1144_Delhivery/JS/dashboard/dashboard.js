﻿var app = angular.module("myApp", []);
app.controller("myController", function ($scope, $http, $interval) {
    $scope.Barcode = 0;
    $scope.Weight = 0;
    $scope.Length = 0;
    $scope.Width = 0;
    $scope.Height = 0;
    $scope.Volume = 0;
    $scope.RealVolume = 0;
    $scope.totalCount = 0;

    $scope.StartTimer = function () {
        $scope.Timer = $interval(function () {
            $scope.DoTimerAction();
        }, 1000);
    };
    $scope.StopTimer = function () {
        $scope.Message = "Timer stopped.";
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
    };
    $scope.DoTimerAction = function () {
        //alert();
        $scope.DisplayData();
        $scope.RefreshList();
        $scope.GetHeartBit();
    };

    $scope.DisplayData = function () {
        //$scope.Date = new Date();
        //$scope.Barcode = 0;
        var httpreq = {
            method: "POST",
            url: "dashboard.aspx/ShowData",
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'dataType': 'json',

            },
            async: false,

            data: { },
        }
        $http(httpreq).success(function (response) {
            $scope.countData = angular.fromJson(response.d);
            //console.dir($scope.countData);
            $scope.Barcode = $scope.countData.Table[0].Barcode;
            $scope.Weight = $scope.countData.Table[0].Weight;
            $scope.Length = $scope.countData.Table[0].Length;
            $scope.Width = $scope.countData.Table[0].Width;
            $scope.Height = $scope.countData.Table[0].Height;
            $scope.Volume = $scope.countData.Table[0].Volume;
            $scope.RealVolume = $scope.countData.Table[0].RealVolume;
            $scope.totalCount = $scope.countData.Table1[0].totalCount;
            
        });
    }

    //************************** function To Refresh List
    $scope.RefreshList = function () {
        //alert();
        var httpreq = {
            method: "POST",
            url: "dashboard.aspx/GetList",
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'dataType': 'json'
            },
            data: {},
        }
        $http(httpreq).success(function (response) {
            $scope.listData = angular.fromJson(response.d);
            //alert($scope.listData);
            //$scope.txtqualification = "";
            //$scope.txtemployee_name = "";
        })
    };


   //******************** End Reset List******************

    //******************** Get HeartBeat Start******************
    $scope.open = function () {
        if ($("#Modal_Notify_announciation").css("display") == "none") {
            $("#Modal_Notify_announciation").modal('show');

            //   $('#IDannounciation').show();
        }
    }

    $scope.GetHeartBit = function () {

        //alert("in");
        $.ajax({
            type: "POST",
            //  url: "Base.Master/GetData",
            url: "dashboard.aspx/GetData",
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                //alert(r.d);
                var jsonData = JSON.parse(r.d);
                var DeviceDataGet = jsonData.Table1;
                var AnnunciationData = jsonData.Table;
                $scope.AnnunciationData = jsonData.Table;
                var DeviceDataList = jsonData.Table2;

                // alert($scope.HeartBit_PLC.length);

                if (DeviceDataGet.length > 0)
                    $scope.HeartBitStatus = 0;
                else
                    $scope.HeartBitStatus = 1;

                if (!showAnnunciationFault)
                    return;

                if ($("#Modal_Notify").css("display") != "none")
                    $('#Modal_Notify').modal('hide');

                if (AnnunciationData.length > 0) {

                    $('#IDannounciation').show();

                }
                else {


                    $('#IDannounciation').hide();

                }



            },
            error: function (r) {
                if (r.responseText.indexOf("SessionError" >= 0)) {
                    document.location = "/SessionError.aspx";
                }
                //alert("errr>>" + r.responseText);
            },
            failure: function (r) {
                //alert("failure>>" + r.responseText);
            }
        });
    }
    //******************** Get HeartBeat End******************
    $scope.SearchData = function () {
        //alert();
        if ($scope.txtSearch1 == undefined || $scope.txtSearch1 == "") {
            toastr.options.positionClass = 'toast-top-center';
            toastr.error('Enter Barcode First ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
            return;
        }
        var httpreq = {
            method: "POST",
            url: "dashboard.aspx/SearchBarcodeData",
            data: { Barcode: $scope.txtSearch1},
        }
        $http(httpreq).success(function (response) {

            //alert(response.d);
            
            if (response.d != "") {
                json = JSON.parse(response.d);
                
                var filtered = {};
                $scope.listData1 = angular.fromJson(json);
                if ($scope.listData1 == "") {
                    toastr.options.positionClass = 'toast-top-center';
                    toastr.info('No Data Found ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
                }
                else {
                    $(".table_data").hide();
                    $("#searchtable").show();
                }
                
            }
        })
    };
    $scope.Search = function () {
        $("#searchTextBox").show();
        $("#searchbutton").show();
        $("#serachclose").show();
        $("#serachbtn").hide(); 
        $("#tabletitle").hide();
    };
    $scope.close = function () {
        $("#searchTextBox").hide();
        $("#searchbutton").hide();
        $("#serachclose").hide();
        $("#serachbtn").show();
        $(".table_data").show();
        $("#searchtable").hide();
        $("#tabletitle").show();
        $scope.txtSearch1 = "";
    };


    //******************** Export Start******************
    $scope.Export = function () {
        //alert();
        
        var httpreq = {
            method: "POST",
            url: "dashboard.aspx/Export",
            data: {  },
        }
        $http(httpreq).success(function (response) {
            $scope.ExportData = angular.fromJson(response.d);
            $scope.ArrayExport = $scope.ExportData.Table;
           // console.dir($scope.ArrayExport);

            if ($scope.ArrayExport == "") {
                toastr.options.positionClass = 'toast-top-center';
                toastr.info('No Data Found ..!!', 'Wait', { timeOut: 2000, closeButton: true, preventDuplicates: true });
            }
            else {
                JSONToCSVConvertor($scope.ArrayExport, "", true);
            }

            function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
                JSONData = angular.copy(JSONData);

                var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
                var CSV = '';
                CSV += ReportTitle + '\r\n\n';
                if (ShowLabel) {
                    var row = "";
                    for (var index in arrData[0]) {
                        row += index + ',';
                    }
                    row = row.slice(0, -1);
                    CSV += row + '\r\n';
                }

                for (var i = 0; i < arrData.length; i++) {
                    var row = "";
                    for (var index in arrData[i]) {
                        var txt = arrData[i][index];
                        if (index == "start" || index == "end") {
                            var d = new Date(txt);
                            txt = convertDate(d);
                        }
                        row += '"' + txt + '",';
                    }
                    row.slice(0, row.length - 1);
                    CSV += row + '\r\n';
                }

                if (CSV == '') {
                    return;
                }

                var today = new Date().toJSON().slice(0, 10).replace(/-/g, '/');;
                var fileName = "Report_ " + today;
                fileName += ReportTitle.replace(/ /g, "_");
                var blob = new Blob([CSV], { type: 'text/csv' });

                if (window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(blob, fileName);
                }
                else {
                    var elem = window.document.createElement('a');
                    elem.href = window.URL.createObjectURL(blob);
                    elem.download = fileName + ".csv";
                    document.body.appendChild(elem);
                    elem.click();
                    document.body.removeChild(elem);
                }
                toastr.options.positionClass = 'toast-top-center';
                toastr.success('Data Exported..!!', 'Great', { timeOut: 2000, closeButton: true, preventDuplicates: true });
            }

        })
    };

    //******************** Export End******************
})

function tog(v) { return v ? "addClass" : "removeClass"; }
$(document).on("input", ".clearable", function () {
    $(this)[tog(this.value)]("x");
}).on("mousemove", ".x", function (e) {
    $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]("onX");
}).on("touchstart click", ".onX", function (ev) {
    ev.preventDefault();
    $(this).removeClass("x onX").val("").change();
});