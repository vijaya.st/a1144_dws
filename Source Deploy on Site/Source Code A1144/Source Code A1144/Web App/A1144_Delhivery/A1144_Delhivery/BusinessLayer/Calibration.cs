﻿using A1144_Delhivery.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace A1144_Delhivery.BusinessLayer
{
    public class Calibration
    {

        public static DataTable GetCalibrationList()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@ProjectCode", System.Web.HttpContext.Current.Session["ProjectCode"].ToString());
            cmd.CommandText = "Master_CalibrationBoxSetting_GetList";
            try
            {
                dt = CommonDataLayer.GetDataTable("Master_CalibrationBoxSetting_GetList", cmd);
            }
            catch (Exception ex) { throw ex; }
            return dt;
        }

    }
}